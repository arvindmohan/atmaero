function xprime=S1(t,x)
% System 1, effect in \Omega_1
A1=[-2 -2;4 1];
xprime=A1*[x(1);x(2)];
end