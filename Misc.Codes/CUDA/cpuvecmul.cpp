// CUDA progam - Arvind Mohan - June 6,2013
// Summing 2 Arrays

#include<stdio.h>
#include <fstream>

#define N (1000000000)

//const int threadsPerBlock = 256;

// Declare add function for Device

int* vecmul(int *a,int *b)
{
  int *c = new int [N];
  for (int i=0;i<N;i++)
  {
   c[i] = a[i] * b[i];
  }
  return(c);
}  


int main(void)
{
// Allocate Memory  on Host
int  *a_h = new int [N];
int  *b_h = new int [N];
int  *c_h = new int [N];

//Initialize Host Array

for (int i=0;i<N;i++)
{
    a_h[i] = i;
    b_h[i] = (i+1);
}  
  
c_h = vecmul(a_h,b_h);

std::ofstream myfile;
myfile.open("cpuoutput.dat");
/*
for (int k=0;k<N;k++)
{
  myfile <<  c_h[k]  << "\t";
}
myfile.close();
*/
// Free Memory from Host

return 0;
}












