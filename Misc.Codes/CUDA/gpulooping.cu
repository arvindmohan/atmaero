// Multiply vectors on a GPU
#include "nvidiafile.h"

#define N   2000

__global__ void mul( int *x, int *y, int *z ) {
    int tid = blockIdx.x;  
    if (tid < N)
        z[tid] = x[tid]*y[tid];
}

int main( void ) 

{
    int x[N], y[N], z[N];
    int *dev_x, *dev_y, *dev_z;

    // GPU mem. alloc.
    HANDLE_ERROR( cudaMalloc( (void**)&dev_x, N * sizeof(int) ) );
    HANDLE_ERROR( cudaMalloc( (void**)&dev_y, N * sizeof(int) ) );
    HANDLE_ERROR( cudaMalloc( (void**)&dev_z, N * sizeof(int) ) );

    
    for (int i=0; i<N; i++) 
    {
        x[i] = -i;
        y[i] = i * i;
    }

    // shift data to GPU
    HANDLE_ERROR( cudaMemcpy( dev_x, x, N * sizeof(int),
                              cudaMemcpyHostToDevice ) );
    HANDLE_ERROR( cudaMemcpy( dev_y, y, N * sizeof(int),
                              cudaMemcpyHostToDevice ) );

    mul<<<N,1>>>( dev_x, dev_y, dev_z );

    // get data back to CPU
    HANDLE_ERROR( cudaMemcpy( z, dev_z, N * sizeof(int),
                              cudaMemcpyDeviceToHost ) );

 
    for (int i=0; i<N; i++) {
        printf( "%d + %d = %d\n", x[i], y[i], z[i] );
    }

    // clear mem.
    HANDLE_ERROR( cudaFree( dev_x ) );
    HANDLE_ERROR( cudaFree( dev_y ) );
    HANDLE_ERROR( cudaFree( dev_z ) );

    return 0;
}
