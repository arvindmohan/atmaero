// Summing 2 Arrays

#include<stdio.h>
#include <fstream>

#define N (1000000000)

//const int threadsPerBlock = 256;

// Declare add function for Device

__global__ void vecmul(int *a,int *b,int *c)
{
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    if (tid >= N) {return;}
     
    c[tid] = a[tid] * b[tid];
}  


int main(void)
{
// Allocate Memory  on Host
int  *a_h = new int[N];
int  *b_h = new int[N];
int  *c_h = new int[N];

// Allocate Memory on GPU
  
int *a_d;
int *b_d;
int *c_d;  

cudaMalloc((void**)&a_d,N*sizeof(int));
cudaMalloc((void**)&b_d,N*sizeof(int));
cudaMalloc((void**)&c_d,N*sizeof(int));

//Initialize Host Array

for (int i=0;i<N;i++)
{
    a_h[i] = i;
    b_h[i] = (i+1);
}  
  
// Copy Data from Host to Device

cudaMemcpy(a_d,a_h,N*sizeof(int),cudaMemcpyHostToDevice);
cudaMemcpy(b_d,b_h,N*sizeof(int),cudaMemcpyHostToDevice);

// Run Kernel
int blocks = (N - 1)/256 + 1;
vecmul<<<blocks,256>>>(a_d,b_d,c_d);

// Copy Data from Device to Host

cudaMemcpy(c_h,c_d,N*sizeof(int),cudaMemcpyDeviceToHost);

// Free Device Memory

cudaFree(a_d);
cudaFree(b_d);
cudaFree(c_d);

std::ofstream myfile;
myfile.open("gpuoutput.dat");

for (int k=0;k<N;k++)
{
  myfile <<  c_h[k]  << "\t";
}
myfile.close();


// Free Memory from Host

free(a_h);
free(b_h);
free(c_h);

return 0;
}






// Print results
/*
printf("a is");
for (int j=0;j<N;j++)
{
  printf("%d",a_h[j]);
}
printf("\n");
printf("b is");
for (int j=0;j<N;j++)
{
  printf("%d",b_h[j]);
}
printf("\n");
printf("c is");
for (int j=0;j<N;j++)
{
  printf("%d",c_h[j]);
}
printf("\n");
*/
/*
std::ofstream myfile;
myfile.open("output.dat");

for (int k=0;k<N;k++)
{
  myfile <<  c_h[k]  << "\t";
}
myfile.close();
*/





