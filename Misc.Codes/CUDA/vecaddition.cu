// CUDA progam - Arvind Mohan - June 6,2013
// Summing 2 Arrays

#include<stdio.h>

#define N (128)

const int threadsPerBlock = 256;

// Declare add function for Device

__global__ void vecadd(int *a,int *b,int *c,int *lol)
{
    __shared__ float cache[threadsPerBlock];
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    int cacheIndex = threadIdx.x;
    //*lol = 11; 
    if (tid >= N) {return;}
     
    c[tid] = a[tid] + b[tid];

cache[cacheIndex] = tid;
*lol = cache[cacheIndex];   
__syncthreads();    
 }  


int main(void)
{
// Allocate Memory  on Host
int  *a_h = new int[N];
int  *b_h = new int[N];
int  *c_h = new int[N];
int  value = 0; 

// Allocate Memory on GPU
  
int *a_d;
int *b_d;
int *c_d;  
int *val;

cudaMalloc((void**)&a_d,N*sizeof(int));
cudaMalloc((void**)&b_d,N*sizeof(int));
cudaMalloc((void**)&c_d,N*sizeof(int));
cudaMalloc((void**)&val,1*sizeof(int));

//Initialize Host Array

for (int i=0;i<N;i++)
{
    a_h[i] = i;
    b_h[i] = (i+5);
}  
  
// Copy Data from Host to Device

cudaMemcpy(a_d,a_h,N*sizeof(int),cudaMemcpyHostToDevice);
cudaMemcpy(b_d,b_h,N*sizeof(int),cudaMemcpyHostToDevice);

// Run Kernel
int blocks = int(N - 0.5)/256 + 1;
vecadd<<<blocks,256>>>(a_d,b_d,c_d,val);

// Copy Data from Device to Host

cudaMemcpy(c_h,c_d,N*sizeof(int),cudaMemcpyDeviceToHost);
cudaMemcpy(&value,val,1*sizeof(int),cudaMemcpyDeviceToHost);

// Free Device Memory

cudaFree(a_d);
cudaFree(b_d);
cudaFree(c_d);
cudaFree(val);

// Print results

printf("a is");
for (int j=0;j<N;j++)
{
  printf("%d",a_h[j]);
}

printf("b is");
for (int j=0;j<N;j++)
{
  printf("%d",b_h[j]);
}

printf("c is");
for (int j=0;j<N;j++)
{
  printf("%d",c_h[j]);
}

printf("value is %d",value,"\n");


// Free Memory from Host

free(a_h);
free(b_h);
free(c_h);

return 0;
}












