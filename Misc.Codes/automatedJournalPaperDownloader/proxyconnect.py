# Connecting through Proxies

import urllib2

opener = urllib2.build_opener(
                urllib2.HTTPHandler(),
                urllib2.HTTPSHandler(),
                urllib2.ProxyHandler({'http': 'http://anonymouse.org'}))
urllib2.install_opener(opener)
content = urllib2.urlopen('http://www.news.google.com').read()

with open('webresults.html','w') as f:
   f.write(content)




