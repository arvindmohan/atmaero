""" Get data from wavelets.ens.fr for wavelet analysis stuff """
from bs4 import BeautifulSoup
import httplib2
import urllib2
import urllib
import sys
import os

# Optional - Do the entire operation with Proxies

proxy = urllib2.Proxyhandler({'http': 'anonymouse.org'})
opener = urllib2.build_opener(proxy)
urllib2.install_opener(opener)
request = urllib2.Request("http://wavelets.ens.fr/PUBLICATIONS/ARTICLES/PDF")
response = urllib2.urlopen(request)
page = BeautifulSoup(response)

total = 0
for link in page.find_all('a'):
     if 'pdf' in link.get('href'):
        total = total  + 1   
print 'no. of PDF files is: ',total

count = 0
folder = 'PDF_downl' + str(total)
os.mkdir(folder)
os.chdir(folder)
for link in page.find_all('a'):
    if 'pdf' in link.get('href'):
      flink = 'http://wavelets.ens.fr/PUBLICATIONS/ARTICLES/PDF/' + str(link.get('href'))
      fname = 'wavelets_ens_fr' +  str(link.get('href'))
      print 'Downloading file...',fname 
      urllib.urlretrieve(flink,fname) 
      try:
             os.path.isfile(fname)
             #with open(name): pass
      except IOError:           
             print 'File',fname,'download error'
      count = count + 1
      print 'Files remaining:',(total-count)      
