%Eigensystem Realization Algorithm - Traditional Version
% Ref: Reduced-order models for control of fluids using the eigensystem realization algorithm (2009)
%Theor. Comput. Fluid Dyn. DOI 10.1007/s00162-010-0184-8

close all 
clear all
clc
disp('Eigensystem Realization Algorithm of Flow Data')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
disp('High Fidelity Computational Multiphysics Lab - The Ohio State University')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

%% USER INPUT
cd('./data_ERAtest/');
pwd
numofsnaps = 40;
Mc = floor(numofsnaps/2);
Mo = Mc; %numofsnaps - Mc;
delta = 125; 
timestep = 10^-4; % From CFD solver
init_file  = 118000;%  <== CHANGE if needed.
final_file = init_file + ((numofsnaps-1)*delta);

projection_modes = 10;
% Header File info for Plot3d output
xhead = 160;
yhead = 148;
zhead = 1;
nvar = 1;

% Order of Reduction

r = (projection_modes*Mo)/4;
%num of outputs
q = numofsnaps;
%num of inputs
p = 1;

%% POD of Markov Parameters 
disp('Reading In Snapshot Data...');
clear temp

m=1;
for i=init_file:delta:final_file
    u(:,m)=importdata(['u' num2str(i) '.mat']);
    m=m+1;
end
disp('Operation Successful.');

[gridsize,Nsnap] = size(u);

Umean(:,1)=(mean(u'))';

%*********  Let's build the Snapshot POD kernel C  ************
C = zeros(Nsnap,Nsnap);

disp('Calculating Covariance matrix');
C = u'*u; 
C = C/Nsnap ; 

disp('Computing Eigendecomposition of Covariance Matrix...')
[EigenVect,EigenValues] = eig(C);  
disp('Operation Successful.');

clear C
lambda = flipud(diag(EigenValues));  
clear EigenValues
aPOD = fliplr(EigenVect); 
clear EigenVect
disp('====================================================')
%******* aPOD is the POD temporal coefficient
[times,modes]=size(aPOD); 

for j = 1:modes,
    aPOD(:,j) = aPOD(:,j)*sqrt(lambda(j)*Nsnap);
end
save aPOD_testing aPOD;

phiU = zeros(gridsize,modes);

phiU = u*aPOD;

disp('Calculating Modes');
for j=1:modes,  
    phiU(:,j)=phiU(:,j)/(lambda(j)*Nsnap); 
end
disp('Operation Successful.');
disp('====================================================')

save phiU phiU -v7.3

total = sum(lambda);
individual = lambda*100/total;
Energy = zeros(modes,1);
En = 0;

disp('Calculating Energy')
for j = 1:modes,    
    Energy(j) = En + individual(j);
    En = Energy(j);
end
disp('Operation Successful.');
disp('====================================================')

%% Output Projection for Modified Markov Parameters
disp('Output Projection for Modified Markov Parameters')

theta = phiU(:,1:projection_modes);

data = theta'*u;

disp('Operation Successful.');
disp('====================================================')


%% ERA algorithm 

[datarows,datacols] = size(data);
% numofsnaps = 10;
% data = 5.*rand(numofsnaps,numofsnaps);
% disp('Operation Successful.');

% Designate Control and Output Snapshots

data_c = data(:,1:Mc);
data_o = data(:,Mc:end-1);

% Shifted Snapshots
datashift_c = data(:,2:(Mc + 1));
datashift_o = data(:,(Mc + 1):end);

disp('Build Hankel Matrix H...');
% Build Hankel Matrix H

H = hankel(data_c,data_o);

% Thin SVD Operation
disp('Computing SVD')
[U1,E1,V1star] = svd(H,0);
save H H -v7.3
%clear H
disp('Operation Successful.');
disp('Rank of H is');
rank(H)

% Reduced Order Matrices

Ur = U1(:,1:r);
Vr = V1star(:,1:r);
Er = E1(1:r,1:r); 

% Build Hankel Matrix H1

H1 = hankel(datashift_c,datashift_o);

% Build reduced Order System Matrices

Ar = (Er.^-0.5)*(Ur')*(H')*(Vr)*(Er.^-0.5);

Br = (Er.^0.5)*(Vr(:,1:p)');

Cr = (Ur(1:q,:))*(Er.^0.5);






