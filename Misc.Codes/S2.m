function xprime=S2(t,x)
% System 2, effect in \Omega_2
A2=[-2 2 ; -4 1];
xprime=A2*[x(1);x(2)];
end