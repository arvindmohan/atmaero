clear all;close all;clc;

load sln.mat
A1 = sln.A1;
A2 = sln.A2;
A = sln.A;
P = sln.P;

[x,y]=meshgrid(-30:0.5:20);

for i=1:length(x)
    for j=1:length(y)
        V1(i,j) = [x(i,j) y(i,j)]*(A1'*P+P*A1)*[x(i,j); y(i,j)];
        V2(i,j) = [x(i,j) y(i,j)]*(A2'*P+P*A2)*[x(i,j); y(i,j)];        
    end
end

surf(x,y,V1,'FaceColor','red','EdgeColor','none')
hold on 
surf(x,y,V2,'FaceColor','blue','EdgeColor','none')

figure
V = V1-V2;
V(V>0)=1;
V(V<0)=0;
contourf(x,y,V1-V2,[0,0])