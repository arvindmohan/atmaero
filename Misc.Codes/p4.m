clear all; close all; clc;

A1 = [-4 -5; 7 7];
A2 = [3 5; -7 -13];

A = 0.54*A1+0.46*A2

eig(A)

n=2;

echo on
cvx_begin sdp
variable P(n,n) symmetric
P>=eye(n)
A'*P+P*A+0.1*P<0
cvx_end
echo off

sln.A1 = A1;
sln.A2 = A2;
sln.A = A;
sln.P = P;

save sln.mat sln

