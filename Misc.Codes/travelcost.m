function [best_path,path_cost,totalcost] = travelcost(n,c,Vd,V2,V1,V0)
%Calculates minimum travel cost between 4 cities using Dynamic Programming

largenum = 1e5;

Vd = largenum*(0.5 + rand(n-1,1)); % Direct travel cost

Vd(length(Vd),1) = Vd(length(Vd),1)/largenum; % Making destination city non-infinite val. for computation.

% Dynamic Programming starts

[val_direct,id_direct] = min(Vd); % min. direct travel cost

[val2min,id2] = min(V2); % Min cost at 2nd horizon and city index
 
v2cost = min(val_direct,val2min); % Compare direct and 2nd horizon min cost

if v2cost == val_direct
    %disp('Minimum cost in direct travel from City:')
    best_path = id_direct;
    %disp(best_path)
    %disp('Cost is')
    path_cost = Vd(id_direct);
    %disp(path_cost)
    totalcost = path_cost;
    return %break
end


best_path(:,n-1) = id2; % Best route in 2nd horizon

% 1st Horizon selecting indices

if id2 == 1
    V1 = largenum*V1;
    V1(1:id2+1,1) = V1(1:id2+1,1)/largenum;
elseif id2 == 2
    V1 = largenum*V1;
    V1(id2+1:id2+2,1) = V1(id2+1:id2+2,1)/largenum;
elseif id2 == 3
    V1 = largenum*V1;
    V1(id2+2:id2+3,1) = V1(id2+2:id2+3,1)/largenum;
end

[val1min,id1] = min(V1); % Min cost at 1st horizon and city index

v1cost = min(val_direct,(val2min + val1min)); % Compare direct and 1st horizon min cost

if v1cost == val_direct
    %disp('Minimum cost in direct travel from City:')
    best_path = id_direct;
    %disp(best_path)
    %disp('Cost is')
    path_cost = Vd(id_direct);
    %disp(path_cost)
    totalcost = path_cost;
    return %break
end

% Find City ID for 1st horizon

if id2 == 1 && id1 == 1
    cityid1 = 2;
elseif id2 == 1 && id1 == 2
    cityid1 = 3;
elseif id2 == 2 && id1 == 3
    cityid1 = 1;
elseif id2 == 2 && id1 == 4
    cityid1 = 3;
elseif id2 == 3 && id1 == 5
    cityid1 = 1;
elseif id2 == 3 && id1 == 6
    cityid1 = 2;
end


best_path(:,n-2) = cityid1; % Best route in 1st horizon

% Zeroth Horizon selecting indices

if id2 == 1 && id1 == 1
    id0 = 4;
    cityid0 = 3;
elseif id2 == 1 && id1 == 2
    id0 = 6;
    cityid0 = 2;
elseif id2 == 2 && id1 == 3
    id0 = 2;
    cityid0 = 3;
elseif id2 == 2 && id1 == 4
    id0 = 5;
    cityid0 = 1;
elseif id2 == 3 && id1 == 5
    id0 = 1;
    cityid0 = 2;
elseif id2 == 3 && id1 == 6
    id0 = 3;
    cityid0 = 1;
end

v0cost = min(val_direct,(val2min + V0(id0))); % Compare direct and zeroth horizon min cost

if v0cost == val_direct
    %disp('Minimum cost in direct travel from City:')
    best_path = id_direct;
    %disp(best_path)
    %disp('Cost is')
    path_cost = Vd(id_direct);
    %disp(path_cost)
    totalcost = path_cost;
    return %break
end


best_path(:,n-3) = cityid0;    
    
% disp('The best path among cities (1,2,3) to 4 is:')
 best_path;
% disp('The path cost among cities (1,2,3) to 4 is:')
 path_cost = [V0(id0),V1(id1),V2(id2)];
% disp('The total cost is:')
 totalcost = V0(id0) + V1(id1) + V2(id2);

end

