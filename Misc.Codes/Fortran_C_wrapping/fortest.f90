! Fortran test code to interface with C++ code
! return struct from C++

 program fortest 
 implicit none
 integer a(4)
 integer b(4)
 integer c(4)
 common/abc/a,b,c 
 save /abc/
! integer, dimension(:),allocatable :: a 
! integer, dimension(:),allocatable :: b
! integer, dimension(:),allocatable :: c
 integer y,z
 y = 2
 z = 2
! integer size = y*z
! allocate(a(size))
! allocate(b(size))
! allocate(c(size))
! integer a(4)
! integer b(4)
! integer c(4)

  write(*,*) a
 call myfunc(y,z)

  print*,'after function is called:'
  print*, 'a is'
  write(*,*) a
  print*, 'b is'
  write(*,*) b
  print*, 'c is'
  write(*,*) c
 !format ('a =',a,'b=',b,'c=',c)

 stop

 end


