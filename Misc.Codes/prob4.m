% Prob 3 Hybrid Systems HW 3 
% Arvind T. Mohan
% Stabilization via LMIs

A1 = [-0.5 0.3 0.4;1 0 0;0 1 0];
A2 = [-0.7 0.1 -0.2;1 0 0;0 1 0];
A3 = [0.6 -0.7 0.2;1 0 0;0 1 0];
B = [1;0;0];
m = 3;
n = 1;

cvx_begin sdp
variable X(m,m) symmetric
variable Y(n,m)
X >= eye(3)
X*A1'+Y'*B'+A1*X+B*Y+X <= 0
X*A2'+Y'*B'+A2*X+B*Y+X <= 0
X*A3'+Y'*B'+A3*X+B*Y+X <= 0
cvx_end
P = inv(X);
K = Y/X;