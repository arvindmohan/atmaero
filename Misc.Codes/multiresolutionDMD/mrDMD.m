%% MULTI-RESOLUTION DMD
% Based on Nathan Kutz et al., June 2015 ArXiv.

% Classical DMD - Parallelized
close all 
clear
clc
disp('Dynamic Mode Decomposition of Flow Data')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
disp('High Fidelity Computational Multiphysics Lab - The Ohio State University')
disp('Arvind Mohan - PhD Student, Ohio State')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

%% USER INPUT
cd('./snapshots/');
pwd;
numofsnaps = 200;
delta = 125;
timestep = 1e-4; % From CFD solver - non-DIMENSIONAL TIME FOR EVERY 'DT'
init_file  = 2647000;%  <== CHANGE if needed.
final_file = (init_file-delta) + ((numofsnaps)*delta);
reconmodes = numofsnaps-1;
meanfname = 'Umean';

% Header File info for Plot3d output
xhead = 160;
yhead = 148;
zhead = 1;
nvar = 1;

%% DMD algorithm
dim = [xhead yhead zhead];
gridSize = prod(dim);
Nsnap = numofsnaps;

disp('Building Snapshot Mean...');

tic
% Compute Mean
temp = zeros(gridSize,1);
parfor i=1:Nsnap
   filenum = (init_file-delta) + i*delta;
   %disp(['Reading file rhou' num2str(filenum) '.mat']);
   temp = temp + importdata(['u' num2str(filenum) '.mat']);
end

Umean = temp/Nsnap;

clear temp
% Step 1: Building Matrix V1^n-1

V1 = zeros(gridSize,Nsnap-1);
parfor i=1:Nsnap-1
    filenum = (init_file-delta)+ i*delta;
    V1(:,i) = importdata(['u' num2str(filenum) '.mat']);
end

% Computation Starts

% Step 3: Singular Value Decomposition of the V1 matrix (Economy)
disp('SVD in process...')
[U,E,W] = svd(V1,0);
disp('Operation Successful.');


clear V1
% Step 2: Building V2 Matrix

V2 = zeros(gridSize,Nsnap-1);
parfor i=1:Nsnap-1
    filenum = (init_file-delta) + i*delta;
    V2(:,i) = importdata(['u' num2str(filenum+delta) '.mat']);
end

% Step 4: Computing S matrix

S = U'*V2*W/E;

clear V2 W E
% Step 5: Eigendecomposition of Matrix S
disp('Computing Eigendecomposition of S Matrix...')
[eigenvecs,eigenvals] = eig(S);
disp('Operation Successful.');
clear S

ritz = diag(eigenvals); % saving the Ritz values
clear eigenvals


%% Post Processing of DMD data

% Computing Modes

disp('Computing DMD Modes...')
modes = U*eigenvecs;
clear U eigenvecs
[~,nummodes] = size(modes);
disp('Operation Successful.'); 

% Computing Floquet values
dt = timestep*delta; % Actual time interval

floqvals = log(ritz)/dt;

% Sorting Ritz values in descending order
[~,index] = sort(imag(floqvals),'ascend');
floqvals = floqvals(index);
modes = modes(:,index);
ritz = ritz(index);

% cd('../')
% !mkdir L1J1
% cd('./L1J1')

%% Reconstruction

%Computing Scaling Coeffs for modes using 1st snapshot
disp('Computing Scaling Coeffs');
firstfile = ['u' num2str(init_file) '.mat'];
u1 = importdata(firstfile);
d = modes\u1;
clear u1

% Computing Scaled modes

modes_scaled = zeros(gridSize,nummodes);

parfor i=1:nummodes
   modes_scaled(:,i) =  modes(:,i)*d(i);
end

% Compute Scaled Mode Norms

mode_norms = zeros(nummodes,1);
parfor i=1:nummodes
   mode_norms(i) = norm(modes_scaled(:,i));
end

mode_ampl_unordered = mode_norms/(max(mode_norms));

% Build Vandermonde Matrix using Ritz values

T = fliplr(vander(ritz));

% Computing Reconstructed Velocity
disp('Computing Reconstructed Data . . .');

reconFull = modes_scaled(:,1:reconmodes)*T(1:reconmodes,:);
disp('Operation Successful.');

cd('../')
%save('mrDMD','-v7.3');

%% Slow Mode Removal and Snapshot generation for next level
slowModeInit = 99;
slowModeEnd = 101;
slowModeRecon = modes_scaled(:,slowModeInit:slowModeEnd)*T(slowModeInit:slowModeEnd,:);
snapshots_L1 = reconFull - slowModeRecon;
removedModes_L1 = modes(:,slowModeInit:slowModeEnd);
removedRitz_L1 = ritz(slowModeInit:slowModeEnd);
removedAmps_L1 = mode_ampl_unordered(slowModeInit:slowModeEnd);
ritz_L1 = ritz;

save('L1removed','removed*','ritz_L1','slowModeInit','slowModeEnd','-v7.3');

!mkdir snapshotsL1
cd('./snapshotsL1')
id = 0;
for i=init_file:delta:(final_file-delta)
     id = id + 1;
     temp = snapshots_L1(:,id);
     save(['u' num2str(i) '.mat'],'temp','-v7.3');
     disp(i)
end














