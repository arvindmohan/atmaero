%% mrDMD PostProcess and Plotting
%clear;clc
% Header File info for Plot3d output
xhead = 160;
yhead = 148;
zhead = 1;
nvar = 1;

% Writing DMD modes to ASCII PLOT3D files
disp('Writing DMD modes to Plot3D files');
header = [xhead yhead zhead nvar];
nummodes = 4;
for i=1:nummodes
fid = fopen(['mrDMDmodeL3J2_' num2str(i) '.dat'], 'w');
fprintf(fid, '%i \t',header);
fprintf(fid,'\n');
fprintf(fid, '%f \t',removedModes_L3J2(:,i));
fclose(fid);
end
disp('====================================================')

freq = log(removedRitz_L3J2)/(2*pi);