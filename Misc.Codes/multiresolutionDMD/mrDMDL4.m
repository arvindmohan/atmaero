%% MULTI-RESOLUTION DMD
% Based on Nathan Kutz et al., June 2015 ArXiv.
% LEVEL 2 DECOMPOSITION

% Classical DMD - Parallelized
close all 
clear
clc
disp('Dynamic Mode Decomposition of Flow Data')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
disp('High Fidelity Computational Multiphysics Lab - The Ohio State University')
disp('Arvind Mohan - PhD Student, Ohio State')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

% USER INPUT
cd('./snapshotsL3J4/');
pwd;
delta = 125;
timestep = 1e-4; % From CFD solver - non-DIMENSIONAL TIME FOR EVERY 'DT'
init_file  = 2668625;%  <== CHANGE if needed.
final_file = 2671375;
numofsnaps = 1 + (final_file - init_file)/delta; 
numofsnaps = 23;
reconmodes = numofsnaps-1;
meanfname = 'Umean';

% Header File info for Plot3d output
xhead = 160;
yhead = 148;
zhead = 1;
nvar = 1;

% DMD algorithm
dim = [xhead yhead zhead];
gridSize = prod(dim);
Nsnap = numofsnaps;

disp('Building Snapshot Mean...');

tic
% Compute Mean
temp = zeros(gridSize,1);
parfor i=1:Nsnap
   filenum = (init_file-delta) + i*delta;
   %disp(['Reading file rhou' num2str(filenum) '.mat']);
   temp = temp + importdata(['u' num2str(filenum) '.mat']);
end

Umean = temp/Nsnap;

clear temp
% Step 1: Building Matrix V1^n-1

V1 = zeros(gridSize,Nsnap-1);
parfor i=1:Nsnap-1
    filenum = (init_file-delta)+ i*delta;
    V1(:,i) = importdata(['u' num2str(filenum) '.mat']);
end

% Computation Starts

% Step 3: Singular Value Decomposition of the V1 matrix (Economy)
disp('SVD in process...')
[U,E,W] = svd(V1,0);
disp('Operation Successful.');


clear V1
% Step 2: Building V2 Matrix

V2 = zeros(gridSize,Nsnap-1);
parfor i=1:Nsnap-1
    filenum = (init_file-delta) + i*delta;
    V2(:,i) = importdata(['u' num2str(filenum+delta) '.mat']);
end

% Step 4: Computing S matrix

S = U'*V2*W/E;

clear V2 W E
% Step 5: Eigendecomposition of Matrix S
disp('Computing Eigendecomposition of S Matrix...')
[eigenvecs,eigenvals] = eig(S);
disp('Operation Successful.');
clear S

ritz = diag(eigenvals); % saving the Ritz values
clear eigenvals


% Post Processing of DMD data

% Computing Modes

disp('Computing DMD Modes...')
modes = U*eigenvecs;
clear U eigenvecs
[~,nummodes] = size(modes);
disp('Operation Successful.'); 

% Computing Floquet values
dt = timestep*delta; % Actual time interval

floqvals = log(ritz)/dt;

% Sorting Ritz values in descending order
[~,index] = sort(imag(floqvals),'ascend');
floqvals = floqvals(index);
modes = modes(:,index);
ritz = ritz(index);

% cd('../')
% !mkdir L1J1
% cd('./L1J1')

% Reconstruction

%Computing Scaling Coeffs for modes using 1st snapshot
disp('Computing Scaling Coeffs');
firstfile = ['u' num2str(init_file) '.mat'];
u1 = importdata(firstfile);
d = modes\u1;
clear u1

% Computing Scaled modes

modes_scaled = zeros(gridSize,nummodes);

parfor i=1:nummodes
   modes_scaled(:,i) =  modes(:,i)*d(i);
end

% Compute Scaled Mode Norms

mode_norms = zeros(nummodes,1);
parfor i=1:nummodes
   mode_norms(i) = norm(modes_scaled(:,i));
end

mode_ampl_unordered = mode_norms/(max(mode_norms));

% Build Vandermonde Matrix using Ritz values

T = fliplr(vander(ritz));

% Computing Reconstructed Velocity
disp('Computing Reconstructed Data . . .');

reconFull = modes_scaled(:,1:reconmodes)*T(1:reconmodes,:);
disp('Operation Successful.');

cd('../')
%save('mrDMD','-v7.3');
disp('DONE!!!')
%% Build Time coefficients 'a' for reqd. modes

disp('Building Time coefficients for every mode ');
initmode = 12 ; endmode = 15;
a = zeros(nummodes,nummodes);
for j = initmode:endmode
    for i=1:nummodes
       a(i,j) = d(j)*(ritz(j))^(i);
    end
end

% Plotting Time Trace
endtime = numofsnaps*dt;
time = linspace(0,endtime,nummodes);

trace = figure(1);
%set(gcf, 'Position', [pos(1) pos(2) 8*100, 5*100]); %<- Set size
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties

plot(time,real(a(:,12)),'+', ...
   time,real(a(:,13)),'r',...
   time,real(a(:,14)),'k',...
   time,real(a(:,15)),'c','LineWidth',2)
   %time,real(a(:,53)),'g',...
   %time,real(a(:,54)),'-','LineWidth',2)
xlabel('Time(s)','Fontsize',14,'FontWeight','bold')
ylabel('Real(a(t))','Fontsize',14,'FontWeight','bold')
%title('Time trace of DMD Amplitude Real(Cn)','Fontsize',14,'FontWeight','bold')
grid on
hleg1 = legend('Mode 1','Mode 2','Mode 3','Mode 4','Mode 5','Mode 6');
set(hleg1,'Location','NorthEast');
xlim([0 26])


%% Slow Mode Removal and Snapshot generation for next level
clear removed*
slowModeInit = 10;
slowModeEnd = 13;
slowModeRecon = modes_scaled(:,slowModeInit:slowModeEnd)*T(slowModeInit:slowModeEnd,:);
snapshots_L4J8 = reconFull - slowModeRecon;
removedModes_L4J8 = modes(:,slowModeInit:slowModeEnd);
removedRitz_L4J8 = ritz(slowModeInit:slowModeEnd);
removedAmps_L4J8 = mode_ampl_unordered(slowModeInit:slowModeEnd);
ritz_L4J8 = ritz;
save('L4J8removed','removed*','ritz_L4J8','slowModeInit','slowModeEnd','-v7.3');

% save snapshots in new dir
!mkdir snapshotsL4J8
cd('./snapshotsL4J8')
id = 0;
for i=init_file:delta:(final_file-delta)
     id = id + 1;
     temp = snapshots_L4J8(:,id);
     save(['u' num2str(i) '.mat'],'temp','-v7.3');
     disp(i)
end
cd('../')













