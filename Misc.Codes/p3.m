clear all;close all;clc;

%% vector field

A1 = [-2 -2;4 1];
A2 = [-2 2 ; -4 1];

[x,y]=meshgrid(-10:0.1:10);
f1=zeros(size(x));
f2=zeros(size(y));

for i=1:length(x)
    for j=1:length(y)
        if x(i,j)>=0
            v1 = A1*[x(i,j);y(i,j)];
            f1(i,j) = v1(1);
            f2(i,j) = v1(2);
        else
            v2 = A2*[x(i,j);y(i,j)];
            f1(i,j) = v2(1);
            f2(i,j) = v2(2);
        end
    end
end
figure(1)
streamslice(x,y,f1,f2);


%% SDP for common LF

n=2;



cvx_begin sdp
variable P(n,n) symmetric
variable alp(1)
variable beta1(1)
variable beta2(1)
minimize(alp+beta1+beta2)
alp>0.1
beta1>0.1
beta2>0.1
P>alp*eye(n)
A1'*P+P*A1+beta1*eye(n)<0
A2'*P+P*A2+beta2*eye(n)<0
cvx_end

%% SDP for piecewise

E1 = [1 0];
E2 = [-1 0];
C1 = [E1;eye(n)];
C2 = [E2;eye(n)];

echo on
cvx_begin sdp
variable T(3,3) symmetric
variable W1(1) nonnegative
variable U1(1) nonnegative
variable W2(1) nonnegative
variable U2(1) nonnegative

T>=0.1*eye(3)
C1'*T*C1 - E1'*W1*E1 > 0.1*eye(n)
C2'*T*C2 - E2'*W2*E2 > 0.1*eye(n)
A1'*C1'*T*C1+C1'*T*C1*A1+E1'*U1*E1+0.1*eye(n)< 0
A2'*C2'*T*C2+C2'*T*C2*A2+E2'*U2*E2+0.1*eye(n)< 0

cvx_end
echo off

P1=C1'*T*C1
P2=C2'*T*C2



%% Draw LF

for i=1:length(x)
    for j=1:length(y)
        if x(i,j)>=0
            V(i,j) = [x(i,j) y(i,j)]*P1*[x(i,j); y(i,j)];
            dV(i,j) = [x(i,j) y(i,j)]*(A1'*P1+P1*A1)*[x(i,j); y(i,j)];
        else
            V(i,j) = [x(i,j) y(i,j)]*P2*[x(i,j); y(i,j)];
            dV(i,j) = [x(i,j) y(i,j)]*(A2'*P2+P2*A2)*[x(i,j); y(i,j)];
        end
    end
end

V = V;
dV = dV;
figure(2)
mesh(x,y,V,'LineWidth',5)
% ,'FaceColor','red','EdgeColor','none')
figure(3)
contour(x,y,V,10,'ShowText','on','LineWidth',1)
% ,'FaceColor','blue','EdgeColor','none')


%% Trajectories

global D
D=0.3;

n=10;

xin{1}=[-2,-8];
xin{2}=[-2,-4];
xin{3}=[-1,-8];
xin{4}=[-1,-4];
xin{5}=[0,-8];
xin{6}=[0,-4];
xin{7}=[0,0];
xin{8}=[2,-8];
xin{9}=[2,-4];
xin{10}=[1,-8];
xin{11}=[1,-4];

for i=1:11
    x00 = xin{i};
    x0=x00;
    % Reset time;
    offset=0;
    
    filename=strcat('name',num2str(7));
    
    options=odeset('RelTol',1e-6);
    if x00(1)>=0
        for j=1:n
            k=2*j-1;
            
            options=odeset('Events',@lagevent1);
            [t{k},xt{k},te{k},xe{k},~]=ode45(@S1, [offset,100+offset], x0, options);
            x0=xt{k}(end,:);
            offset=t{k}(end);
            
            options=odeset('Events',@lagevent2);
            [t{k+1},xt{k+1},te{k+1},xe{k+1},~]=ode45(@S2, [offset,100+offset], x0, options);
            x0=xt{k+1}(end,:);
            offset=t{k+1}(end);
            
        end
    else
        
        for j=1:n
            k=2*j-1;
            
            options=odeset('Events',@lagevent2);
            [t{k},xt{k},te{k},xe{k},~]=ode45(@S2, [offset,100+offset], x0, options);
            x0=xt{k}(end,:);
            offset=t{k}(end);
            
            options=odeset('Events',@lagevent1);
            [t{k+1},xt{k+1},te{k+1},xe{k+1},~]=ode45(@S1, [offset,100+offset], x0, options);
            x0=xt{k+1}(end,:);
            offset=t{k+1}(end);
            
            
        end
    end
    
    sl{i}.tt=[];
    sl{i}.xx=[];
    for j=1:2*n
        sl{i}.tt=[sl{i}.tt;t{j}];
        sl{i}.xx=[sl{i}.xx;xt{j}];
    end
end
for i=1:11
    figure(i+5)
    streamslice(x,y,f1,f2);
    hold on
    h(i)=plot(sl{i}.xx(:,1),sl{i}.xx(:,2),'r');
    hold on
    % grid on
    h(i+1)=line([-D,-D],[-10,10],'Color','k');
    h(i+2)=line([D,D],[-10,10],'Color','k');
    
    plot(sl{i}.xx(1,1),sl{i}.xx(1,2),'d','MarkerEdgeColor','r','MarkerFaceColor','r','MarkerSize',10);
    set(h,'LineWidth',2);
    % axis auto;
    axis fill;
    hold off
    xlabel('{x_1}')
    ylabel('{x_2}')
    
    Font_figures='Times New Roman';
    Font_size=18;
    
    set(gca,'FontName',Font_figures,'FontSize',Font_size);
end














