import numpy as np
import modred as MR
import os

os.chdir("./data_ERAtest")
data = np.loadtxt('GLsnap1.dat')
print 'data is',data
data_size = len(data)
no_snaps = 40

markov = np.zeros((data_size,no_snaps))
#print np.shape(markov)
markov[:,0] = data
#print 'markov is'
#print markov[:,1]

for i in xrange(1,no_snaps+1):
	filename = 'GLsnap' + str(i) + '.dat'
	print i
	markov[:,i-1] = np.loadtxt(filename)	
	
#print 'markov is'
#print markov[:,19]

# Obtain array "Markovs" with dims [time, output, input]
myERA = MR.ERA()
A, B, C = myERA.compute_model(markov, 10)
sing_vals = myERA.sing_vals
os.chdir("../MODREDresults")
myERA.put_model('A.dat', 'B.dat', 'C.dat')
myERA.put_sing_vals('Singvals.dat')

print 'Size of A matrix:',A.shape
print 'Size of B matrix:',B.shape
print 'Size of C matrix:',C.shape
print 'Mc and Mo are',mc,mo
