% Lanczos algorithm call
% Arvind Mohan

clc;clear all;close all

% Load the Appropriate Test sparse matrix
load nasa1824

A = Problem.A;
b = Problem.b;

[m,~] = size(b);
k = m;
vinit = randn(m,1);

% CG Algo invocation
[V,T,w] = lanczos(A,k,vinit);

% Computing Eigenvals

lam = eig(T);
lam_act = eig(A);

disp('Min,Max Eigval from Lanczos:')
disp(min(lam))
disp(max(lam))
disp('Min,Max Eigval from MATLAB function:')
disp(min(lam_act))
disp(max(lam_act))

disp('Done!!!')