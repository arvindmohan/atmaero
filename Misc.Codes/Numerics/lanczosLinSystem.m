function [T,v,w] = lanczos(A,b,xinit,m)
%Lanczos Algorithm
    %beta = zeros(m,1);
    w = zeros(m,1);
    v = zeros(m,m);
    T = zeros(m,m);
   
   % Initialization with xinit guess
   r = b - A*xinit;
   beta = norm(r);
   v(:,1) = r/beta;
   
   % Lanczos Algo run starts
   w = A*v(:,1);
   alpha = w'*v(:,1);
   w = w - alpha*v(:,1); 
   beta = norm(w); 
   v(:,2) = w/beta; 
   T(1,1) = alpha;
       
    for j=2:m
       w = A*v(:,j) - beta*v(:,j-1);
       alpha = w'*v(:,j);
       w = w - alpha*v(:,j); 
       beta = norm(w); 
       v(:,j+1) = w/beta; 
       T(j,j-1) = beta;
       T(j-1,j) = beta;
       T(j,j) = alpha;
    end

end

