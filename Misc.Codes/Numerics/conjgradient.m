function [AL,BE,x,res] = conjgrad(A,b,x,tol)
% Conjugate Gradient Algorithm
    r=b-A*x;
    p=r;
    conv = 1e6;
  
    for i=1:conv
        rprod=r'*r;
        alpha=rprod/(p'*(A*p));
        x=x+alpha*p;
        r=r-alpha*(A*p);
        rprod_new=r'*r;
        if sqrt(rprod_new)<tol
              break;
        end
        %disp(sqrt(rprod_new))
        beta = rprod_new/rprod;
        p=r+beta*p;
        rprod=rprod_new;
        res(i) = sqrt(rprod);
        AL(i) = alpha;
        BE(i) = beta;
    end
end