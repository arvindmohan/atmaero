#ifndef PRINTING_H_INCLUDED
#define PRINTING_H_INCLUDED
#include <iostream>

void print_array2d (std::vector< std::vector<double> >& x, const int& m, const int& n)
{
    //std::cout << "The 2d array is:" << "\n";
    for (int i=0;i<m;i++)
    {
        for (int j=0;j<n;j++)
        {
            std::cout << x[i][j] << "\t";  //printf("%f", phi_init[i][j]);
        }
        std::cout << "\n";
    }
}

void print_array1d (std::vector<double>& x)
{
    const int asize = x.size();
  //std::cout << "The 1d array is:" << "\n";
    for (int i=0;i<asize;i++)
        {
            std::cout << x[i] << "\t";  //printf("%f", phi_init[i][j]);
        }
        std::cout << "\n";
}

#endif // PRINTING_H_INCLUDED
