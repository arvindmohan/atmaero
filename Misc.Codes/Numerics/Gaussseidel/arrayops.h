#ifndef ARRAYOPS_H_INCLUDED
#define ARRAYOPS_H_INCLUDED

//*************** CLASS FOR ARRAY OPERATIONS ***************************//
// Rows = m,  Columns = n

class arrayops
{
    private:
    int m,n,asize,a,b;
    float init;

    public:
    const std::vector< std::vector<double> > array2dgen(const int&,const int&,const float&);
    const std::vector<double> array1dgen(const int&,const float&);
    //const std::vector< std::vector<double> > array2d_invert(const std::vector< std::vector<double> >&,const int&,const int&);
    const std::vector<double> sum(const std::vector<double>&, const std::vector<double>&);
    const std::vector<double> diff(const std::vector<double>&, const std::vector<double>&);
    const std::vector<double> mod(const std::vector<double>&);
    const double maxval(const std::vector<double>&);
    const double minval(const std::vector<double>&);
};

//******************* function matrixnewgen

const std::vector< std::vector<double> > arrayops::array2dgen(const int& m,const int& n,const float& init)
{

   const std::vector< std::vector<double> > array2d(m, std::vector<double>(n,init));

   return(array2d);

}

//******************** function arraynewgen

const std::vector<double> arrayops::array1dgen(const int& asize,const float& init)
{

  const std::vector<double> array1d(asize,init);

  return(array1d);

}


//******************** function sum

const std::vector<double> sum(const std::vector<double>& a, const std::vector<double>& b)
{
   int asize = b.size();
   std::cout << "size" << asize;
   std::vector<double> c(asize,0);

   for (int i=0;i<asize;i++)
   {
      c[i] = a[i] + b[i];
   }

    return(c);
}

//******************** function difference

const std::vector<double> diff(const std::vector<double>& a, const std::vector<double>& b)
{
   int asize = b.size();
   //std::cout << "size" << asize;
   std::vector<double> c(asize,0);

   for (int i=0;i<asize;i++)
   {
      c[i] = a[i] - b[i];
   }

    return(c);
}

//********************* function modulus

const std::vector<double> mod(const std::vector<double>& a)
{
   const int asize = a.size();
   std::vector<double> d(asize,0);

   for (int i=0;i<asize;i++)
   {
      d[i] = std::abs(a[i]);
   }

   return(d);

}

//********************* function maxval

const double maxval(const std::vector<double>& a)
{
   auto ptr = max_element(std::begin(a), std::end(a));
   return(*ptr);
}

//********************* function maxval

const double minval(const std::vector<double>& a)
{
   auto ptr = min_element(std::begin(a), std::end(a));
   return(*ptr);
}


//******************** function array2d_invert
/*
const std::vector< std::vector<double> > array2d_invert(const std::vector< std::vector<double> >& array,const int& m,const int& n)
{
    arrayops ob;
    const std::vector< std::vector<double> > array2d = ob.array2dgen(m,n,0);

}
*/






#endif // ARRAYOPS_H_INCLUDED
