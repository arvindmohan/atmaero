%module linearsolvers
%{
#include "linearsolvers.h"
%}

//  Wrapping Gauss Siedel Solver for Python and testing

%include "std_vector.i"
// Instantiate templates used by example
namespace std {
   %template(DoubleVector) vector<double>;
}

class linearsolvers
{
public:
     std::vector<double> gauss_seidel(const std::vector< std::vector<double> >,const std::vector<double>, const double);
};    

// Include the header file with above prototypes
%include "linearsolvers.h"
