#include<iostream>
#include<vector>
#include<cmath>
#include <algorithm>
#include "printing.h"
#include "arrayops.h"
#include "linearsolvers.h"

int main()
{
    //arrayops ob;
    linearsolvers oj;
    /*
    std::vector<double> k = ob.array1dgen(4,1.0);
    std::vector<double> l = ob.array1dgen(4,2.0);
    std::vector<double> d = mod(diff(k,l));
    */

    std::vector< std::vector<double> > eqn = {{10,-2,-1,-1},{-2,10,-1,-1},{-1,-1,10,-2},{-1,-1,-2,10}};
    std::vector<double> b = {3,15,27,-9};

    /*
    print_array2d(eqn,4,4);
    std::cout << "\n";
    print_array1d(b);
    */
    std::vector<double> sol = oj.gauss_seidel(eqn,b,0);

   // std::vector<double> modu = mod(d);
    //d = l;
    //std::cout << "mod" << mod << "\n";

    std::cout<< "The final solution is:" <"\n";
    print_array1d(sol);

    /*
    double e = minval(d);
    std::cout << "max is" << e << "\n";
    print_array1d(d);
    */
    return 0;
}
