#ifndef ARRAYOPS_H_INCLUDED
#define ARRAYOPS_H_INCLUDED

class arrayops
{
    int m,n,size_a;
    float init;
    public:
    float** matrixgen(int,int,float);
    float* arraygen(int,float);
};

//******************* function matrixgen

float** arrayops::matrixgen (int m, int n,float init)
{
  // Initialize memory
  float** array2d = new float* [m];

  for (int i=0;i< m; i++)
  {
      array2d[i] = new float [n];
  }

  // Initialize the array

    for (int i=0;i<m;i++)
    {
        for (int j=0;j<n;j++)
        {
            array2d[i][j] = init;
        }
    }

   return (array2d);

 //  Free memory
  for (int i=0;i < m;i++ )
  {
      delete [] array2d[i];
  }
  delete [] array2d;
}

//***************** function arraygen

float* arrayops::arraygen(int size_a,float init)
{
    // Init memory
    float* array1d = new float [size_a];

    // Init array
     for (int i=0;i<size_a;i++)
     {
         array1d[i] = init;
     }

    return(array1d);

    // Free memory
    delete [] array1d;


}









#endif // ARRAYOPS_H_INCLUDED
