
function [V,T,f] = lanczos(A,k,v)

n = length(v);
T = zeros(k);
V = zeros(n,k);

v1 = v/norm(v);

f = A*v1;

f = f - v1*(v1'*f);

    V(:,1) = v1; T(1,1) = (v1'*f);

    for j = 2:k,

        beta = norm(f); 
        v0 = v1; v1 = f/beta;
        
        f = A*v1 - v0*beta;
        alpha = v1'*f;
        f = f - v1*alpha;

        T(j,j-1) = beta; T(j-1,j) = beta; T(j,j) = alpha;
        V(:,j)   = v1;

    end 
        
end

