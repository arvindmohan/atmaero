% Conjugate Gradient call
% Arvind Mohan
clc;clear all;close all

% Load the Appropriate Test sparse matrix
%load nasa1824
load parabolic_fem
%load thermal2

A = Problem.A;
b = Problem.b;

[n,~] = size(b);

xinit = zeros(n,1);

%% CG Algo invocation
[AL,BE,x2,res] = conjgrad(A,b,xinit,1e-10);

% Test with MATLAB function
x1 = A\b; 

% Form CG coefficients for T matrix
[~,m] = size(AL);

eta = zeros(m-1,1);
delta = zeros(m,1);

delta(1) = 1/AL(1);


for j=2:m
   delta(j) = (1/AL(j)) + BE(j-1)/AL(j-1); 
   eta(j-1) = sqrt(BE(j-1))/AL(j-1); 
end

% Create T matrix

T = full(gallery('tridiag',m,eta,delta,eta));

% Computing Eigenvals
tic
lam = eig(T);
runtimeCG = toc;

tic
lam_act = eig(A);
runtimeMATLAB = toc;

disp('Min,Max Eigval,Runtime from Conjugate Gradient:')
min_lamCG = min(lam)
max_lamCG = max(lam)
disp('Min,Max Eigval,Runtime from MATLAB function:')
min_lamMATLAB = min(lam_act)
max_lamMATLAB = max(lam_act)

% Plot residual
loglog(res)
xlabel('Iterations')
ylabel('Residual')
title('Convergence of Conjugate Gradient Scheme')

% Plot errors
plot(x1 - x2)
xlabel('No. of elements')
ylabel('Error (X_{matlab} - X_{CG})')
title('Solution error of CG method at all points')

% Condition number
cond = max_lamCG/min_lamCG

save CGalgo -v7.3;
disp('Done!!!')

