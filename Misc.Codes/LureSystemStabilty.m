% Arvind T. Mohan
% Stability of a Lur'e System

clc
clear all 
close all

beta = 0.1;
alpha = 0.7;
A = [ 0,1,0;0,0,1;-1,-3,-3];
b = [0;0;1];
c = [1;0;0];

cvx_begin sdp
    variable P(3,3) symmetric;
    variable tau nonnegative;
    
    P>= eye(3);
    tau>=0;
    [ A'*P + P*A + beta*P + tau*(alpha^2)*c*c', P'*b;b'*P, -tau] <= 0;
cvx_end
cvx_status
cvx_optval

P
tau
    
