#  BLOB DETECTION TESTING ON AIRFOIL using LOG, DOG etc.
# Detect vortices in dynamic stall  
# Arvind T. Mohan - PhD Candidate, Aerospace Engineering, Ohio State U

%matplotlib qt
from matplotlib import pyplot as plt
from skimage import data
from skimage.feature import blob_dog, blob_log, blob_doh
from math import sqrt
from skimage.color import rgb2gray
import os
plt.rcParams['figure.figsize'] = (10.0, 8.0)

os.chdir('/media/ACADS/ResearchData/TurbulenceImageProc/')
print(os.getcwd())
image = data.imread('pnewCropMod.tif')
image_gray = image[:,:,0]
image_gray.shape
#plt.figure(10)
#plt.imshow(image_gray,cmap = 'Greys_r')
#plt.title('Single dim input image')

blobs_log = blob_log(image_gray, max_sigma=30, num_sigma=10, threshold=.1)
# Compute radii in the 3rd column.
blobs_log[:, 2] = blobs_log[:, 2] * sqrt(2)

blobs_dog = blob_dog(image_gray, max_sigma=30, threshold=.1)
blobs_dog[:, 2] = blobs_dog[:, 2] * sqrt(2)

blobs_doh = blob_doh(image_gray, max_sigma=30, threshold=.01)

blobs_list = [blobs_log, blobs_dog, blobs_doh]
colors = ['yellow', 'lime', 'red']
titles = ['Laplacian of Gaussian', 'Difference of Gaussian',
          'Determinant of Hessian']
sequence = zip(blobs_list, colors, titles)


fig,axes = plt.subplots(1, 3, sharex=True, sharey=True, subplot_kw={'adjustable':'box-forced'})
axes = axes.ravel()
for blobs, color, title in sequence:
    ax = axes[0]
    axes = axes[1:]
    ax.set_title(title)
    ax.imshow(image, interpolation='nearest')
    for blob in blobs:
        y, x, r = blob
        c = plt.Circle((x, y), r, color=color, linewidth=2, fill=False)
        ax.add_patch(c)

plt.show()
