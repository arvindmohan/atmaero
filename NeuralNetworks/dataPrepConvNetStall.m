%% Preparing data for Conv. Net classification of stalled and unstalled 
% for Dynamic stall
cd('/home/arvind/Research_northernwind/DeepLearning/Turbulence/AirfoilData/Dynstall/codes')
load('surfPHighResdynStall.mat');
numsnaps = 2501;
%load grid
x2d = squeeze(x(1:315,1,1:101));
z2d = squeeze(z(1:315,1,1:101));

%create truncated grid as a 24 x 24/64 X 64 size grid - pick here
x2dtr = squeeze(x(142:173,1,35:66));
z2dtr = squeeze(z(142:173,1,35:66));

%% plot surf Pressure point signal
figure;
sig = squeeze(surfP(300,51,:));
plot(1:2501,sig)

% unstalled flow: 1900 - 2300
% stalled flow: 880 - 1280
% sub-grid location x=146:169

%% Play Movie of Snapshots
frameRate = 5; % At reqd. frame rate
%for i=880:frameRate:1280
for i=1900:frameRate:2300    
    pcolor(x2d,z2d,surfP(:,:,i))
    shading interp;
    colormap jet;
    caxis([1.765 1.785])
    pause (1/12)
    disp(i)
end
disp('DONE!!!')

%% Play Movie of truncated Snapshots - Used for actual convNet training
frameRate = 2; % At reqd. frame rate
for i=880:frameRate:1100
%for i=1900:frameRate:2400   
    pcolor(x2dtr,z2dtr,surfP(142:173,35:66,i))
    shading interp;
    colormap jet;
    caxis([1.765 1.785])
    axis off
    pause (1/12)
    disp(i)
end
disp('DONE!!!')
%% save data

unStalled = surfP(142:173,35:66,1900:2400);
Stalled = surfP(142:173,35:66,880:1100);

%% save images for convNet

for i=1900:2400%frameRate:1100
    FigHandle = figure;
    pcolor(x2dtr,z2dtr,surfP(142:173,35:66,i))
    shading interp;
    colormap jet;
    caxis([1.765 1.785])
    axis off
    set(FigHandle, 'Position', [100, 100, 150, 150]);
    filename = ['airfpressure' num2str(i) '.png'];
    saveas(FigHandle,filename)  
    close all
end

%!mv airfpressure* convNetAirfoilImages

%% read image for testing
% image1 = imread('testAirfoil.png');
% 
% image2 = imread('airfpressure880.png');

clear imagesize
k=1;
for i=1900:2400
    filename = ['airfpressure' num2str(i) '.png'];
    imagetemp = imread(filename);
    imagesize(k) = (size(imagetemp,1)*size(imagetemp,2))/100;
    k=k+1;
end

plot(imagesize)

%% normalize raw snapshots for direct use in ConvNets

StalledNormalized = Stalled - min(Stalled);
StalledNormalized = StalledNormalized ./ max(StalledNormalized); % *

unStalledNormalized = unStalled - min(unStalled);
unStalledNormalized = unStalledNormalized ./ max(unStalledNormalized); % *


for i=1:221
   maxarray(i) = max(max(unStalledNormalized(:,:,i)));
   minarray(i) = min(min(unStalledNormalized(:,:,i)));
end

figure;
plot(maxarray)
hold on
plot(minarray)

% create final input dataset

airfoilNormConvData = NaN(32,32,722);

a = 0;
for i=1:221
    %airfoilNormConvData(:,:,i) = StalledNormalized(:,:,i);
    temp1 = min(min(min(airfoilNormConvData(:,:,i) - StalledNormalized(:,:,i))));
    a = a + temp1;
end

b=0;
for i=1:501
    j=221+i;
    %airfoilNormConvData(:,:,j) = unStalledNormalized(:,:,i);
    temp2 = min(min(min(airfoilNormConvData(:,:,j) - unStalledNormalized(:,:,i))));
    b = b + temp2;
end

% create input training labels 1 - stalled, 0 - unstalled

airfoilConvDataLabels = zeros(722,1);

airfoilConvDataLabels(1:221) = 1;

% shuffle dataset
shuffleID = 1:722;

shuffleID = shuffleID(randperm(size(shuffleID,2)));

airfoilNormConvDataShuffle = airfoilNormConvData(:,:,shuffleID);

airfoilConvDataLabelsShuffle = airfoilConvDataLabels(shuffleID);
 

%testing if shuffle is accurate
sum(sum(sum(airfoilNormConvData(:,:,608) - airfoilNormConvDataShuffle(:,:,2))))


save('airfoilStallConvNetTrainingData.mat','airfoilNormConvDataShuffle','airfoilConvDataLabelsShuffle','shuffleID','-v7.3');


