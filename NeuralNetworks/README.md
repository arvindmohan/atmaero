Some of my efforts using Neural Nets for Turbulence problems.

1) LSTM RNN for generating time series based on past inputs, for stall warning. LSTMs have been my prime focus of research
for time series forecasting in fluid mechanics, with a focus on predicting key indicators of the flow at future time steps
based on variables representing the current state of the flow.

2)  Convolutional NNs approach 1:  To classify airfoil as stalled/unstalled, based on a patch of 64 X 64 surface
pressure data from the wing. Accuracy on validation and test data has been 91% and 87% respectively.
This is an alternate approach to my efforts in 1), which is more of a continuous measure
of the airfoil state. Though 2) has had better accuracy, 1) seems to be more practically feasible
since it requires only 1-2 surface pressure probes, compared to an entire field for the ConvNet.

The reason why ConvNet accuracy is so high is because the unstalled wing surface pressure is fairly homogeneous
due to attached flow, unlike the stalled state. This makes training much easier, especially 
considering I have more unstalled snapshots than stalled.

Most of my NNs have 6-10 layers, which make them deep enough to learn several complex relationships - however,
the sheer computing cost is turning out to be a serious bottleneck, especially since I need to tune the hyperparameters
using some sort of grid search. Spearmint appears to be good, but right now computing costs are too prohibitive
without enough GPUs. Currently, I use a single Nvidia Kepler K20 GPU on Amazon AWS, in addition to my 64
core workstation at OSU. However, I expect to get significant resources soon.

Convolutional NNs approach 2: This approach consisted of a time-series classification problem in a given window by pre-processing
the time signals into wavelet scalograms. The scalograms are then processed as images, and CNNs are used to classify the images.
This greatly simplifies the problem and the approach has been adopted henceforth. Accuracy rates of over 85% are consistently obtained
for approach 2.

3) I was briefly enamored with Computer vision algorithms for `blob detection' to detect vortices and turbulent features from grayscale
images of airfoil in dynamic stall. I initially started playing with OpenCV (see code) but realized its complexity was
just overkill for something I could easily accomplish with the Python skimage library. I used 3 basic algos:
a) Laplacian of Gaussians b) Difference of Gaussians c) Determinant of Hessians

The results were quite interesting - check the .png image. The LOG identified several false positives but generally
picked most vortices accurately, surprisingly the smaller ones in the leading edge shear layer as well.

Most of the false positives in LOG and DOG were due to the airfoil surface/mesh interface which created artifical
gradients confusing the algos. Suitable pre-processing of the data could have removed most of these artifacts, but 
I did not work on this further due to more interesting results with the NNs.

===================

Though the ConvNets with approach 1 do not appear to be practically feasible (at least the way I do it) for stall warning, approach 2 seems
quite promising. Furthermore, they have
been beautiful for classifying images in ecommerce and other commercial applications - Indeed, the codes here
were directly adapted from those efforts. I am continuing to work on some variations
to the ConvNets, to adapt them to my problems better.








