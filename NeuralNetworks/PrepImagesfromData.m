%******* IMAGE PROCESSING and Data Prep for Blob detection from FDL3DI snapshots ********%
% Arvind Mohan

%% generate test images from turbulence data
x3dhead = 322; y3dhead = 148;
%xx = 1:x3dhead; yy = 1:y3dhead;

%[x2d,y2d] = meshgrid(xx,yy);

nsnaps=25;


for i=1:nsnaps
   filenum = 2647000 + (i-1)*125 
   Cp = importdata(['p' num2str(filenum) '.mat']);
   Cp = reshape(Cp,[x3dhead,y3dhead,size(Cp,2)]);
   pcolor(x,y,Cp)
   colormap gray
   shading interp
   colorbar 
   pause(1/24); 
   print(['p' num2str(i)],'-dtiffn')
end

%%
cd('/media/ACADS/ResearchData/TurbulenceImageProc/')

Cp = reshape(u,[x3dhead,y3dhead,1]);

pcolor(x,y,Cp)
colormap jet
shading interp
colorbar

K = mat2gray(Cp,[70.3609 71.8930]);

imshow(K)

K1 = mat2gray(Cp);

% swap dark pixels with bright
Kswap = abs(K - 1);

pcolor(x,y,Kswap)
colormap gray
shading interp
colorbar 

img = imread('p1.tif');

imshow(img(:,:,2))

a = img(:,:,1);

b = img(:,:,2);

sum(sum(img(:,:,1) - img(:,:,1)))



%% testing

clear a

a = rand(5,5);

S = mat2gray(a)
    
imshow(S)

%% play movie
cd('/home/arvind/Research_Aurora/DeepLearning/Turbulence/AirfoilData/Dynstall')
load surfaceGrid.mat
load surfPdynStall.mat

nsnaps=2501;
hFig = figure(1)
for i=1:nsnaps
   %filenum = 2647000 + (i-1)*125 
   %Cp = importdata(['p' num2str(filenum) '.mat']);
   %Cp = reshape(Cp,[x3dhead,y3dhead,size(Cp,2)]);
   pcolor(xx,zz,surfP(:,:,i))
   colormap gray
   shading interp
   colorbar 
   pause(1/24); 
   set(hFig, 'Position', [100 100 800 600])
   %print(['p' num2str(i)],'-dtiffn')
end

