# ConvNet for Airfoil Surface Pressure from Dynamic Stall of a plungi airfoil
# during downstroke (typically stalled) and upstroke (typically attached)
# Arvind T. Mohan
# PhD Candidate, Aerospace Engineering, Ohio State U
"""
Convolutional Neural Net to classify stalled/unstalled state
during dynamic stall of a plunging airfoil, using a small patch of surface pressure
on the airfoil, 64 X 64 patch.
"""

import numpy as np
import h5py
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras.constraints import maxnorm
from keras.optimizers import SGD
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.utils import np_utils
from keras import backend as K
K.set_image_dim_ordering('th')


# Load airfoil surface pressure patch from MAT HDF5 file
f1 = h5py.File('airfoilStallConvNetTrainingData.mat','r') 
f1.keys()
pFields = f1.get('airfoilNormConvDataShuffle')
pFieldLabels = f1.get('airfoilConvDataLabelsShuffle')
shuffleID = f1.get('shuffleID')

# Split training and test datasets 80/20
trainData = pFields[:577,:,:]
testData = pFields[578:722,:,:]

trainData = trainData.reshape((577,1,64,64))
testData = testData.reshape((144,1,64,64))

dataLabelTrain = pFieldLabels[:,:577]
dataLabelTest = pFieldLabels[:,578:722]

trainData = trainData.astype('float32')
testData = testData.astype('float32')

print(trainData.shape)
print(testData.shape)
print(dataLabelTrain.shape)
print(dataLabelTest.shape)

print('Loaded Dataset.')

# one hot encoding 
dataLabelTrain = np_utils.to_categorical(dataLabelTrain)
dataLabelTest = np_utils.to_categorical(dataLabelTest)
num_classes = dataLabelTest.shape[1]

# Define architecture (not more than 8 or the system will choke)
print('Building neural net...')
model = Sequential()
model.add(Conv2D(64, (3, 3), input_shape=(1, 64, 64), padding='same', activation='relu', kernel_constraint=maxnorm(3)))
model.add(Dropout(0.3))
model.add(Conv2D(64, (3, 3), activation='relu', padding='same', kernel_constraint=maxnorm(3)))
model.add(Dropout(0.3))
model.add(Conv2D(64, (3, 3), activation='relu', padding='same', kernel_constraint=maxnorm(3)))
model.add(Dropout(0.3))
model.add(Conv2D(64, (3, 3), activation='relu', padding='same', kernel_constraint=maxnorm(3)))
model.add(Dropout(0.3))
model.add(Conv2D(64, (3, 3), activation='relu', padding='same', kernel_constraint=maxnorm(3)))
model.add(Dropout(0.3))
model.add(Conv2D(64, (3, 3), activation='relu', padding='same', kernel_constraint=maxnorm(3)))
model.add(Dropout(0.3))
model.add(Conv2D(64, (3, 3), activation='relu', padding='same', kernel_constraint=maxnorm(3)))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Flatten())
model.add(Dense(256, activation='relu', kernel_constraint=maxnorm(3)))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))


epochs = 250
lrate = 0.04
decay = lrate/epochs
# Note to self: Nesterov speeds up convergence, but not always
sgd = SGD(lr=lrate, momentum=0.7, decay=decay, nesterov=True)
model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])
print((model.summary()))

# Start training
print('Begin training')
model.fit(trainData, dataLabelTrain, validation_data=(testData, dataLabelTest), epochs=epochs, batch_size=16)

# Evaluate model
scores = model.evaluate(testData, dataLabelTest, verbose=1)
print(("Accuracy: %.2f%%" % (scores[1]*100)))


