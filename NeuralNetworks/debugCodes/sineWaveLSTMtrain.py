#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Training on Dyn Stall Inception data
"""

import numpy as np
import h5py
import time
#import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils

start_time = time.time()

   
"""
Functions
"""  
"""
def plot_data(data):
    plt.plot(data,'o')
    plt.title('Signal')
    plt.xlabel('Time')
    plt.ylabel('Amplitude')
    plt.show()
"""

def downsample(data, n_points):
    """ downsample every 'n' points """
    new_data = data[0:-1:n_points]
    return new_data
    
def define_states(data, n_states):
    """ Define bounds for each state """
    data_lowbound = np.min(data)
    data_highbound = np.max(data)
    print('Signal Min/Max:', data_lowbound, data_highbound)
    data_span = data_highbound - data_lowbound
    state_window = data_span/n_states
    lbound = []
    ubound = []
    state_id = []
    for i in range(n_states):
        state_id.append(i)
        lbound.append(data_lowbound + (i*state_window))
        ubound.append(data_lowbound + ((i+1)*state_window))
        
    state_boundaries = list(zip(state_id,lbound,ubound))
    return state_boundaries
    
def create_states(data, state_boundaries):
    """ identify states in the signal """
    states = []
    sig_len = len(data)
    for i in range(sig_len):
        dataPoint = data[i]
        #print dataPoint
        for j in range(len(state_boundaries)):
            if (dataPoint > state_boundaries[j][1] \
                and dataPoint <= state_boundaries[j][2]):
                states.append(state_boundaries[j][0])
                break
    return states            
    
    
""" 
Read Data
 
"""

#inputData = np.load('NACA0012stalled.npy')
#print('Shape', inputData.shape)   

xinp = np.linspace(-np.pi, 8*np.pi, 801)
inputData = np.sin(xinp)
print('Shape', inputData.shape)  


"""
Main Code
"""
# INPUT
n_states = 100
seq_length = 10
num_epochs = 50
batch_size = 64
    
# plot data
print('input signal:')
#plt.plot(inputData)    
#plt.show()
    
state_boundaries = define_states(inputData, n_states)
states = create_states(inputData, state_boundaries)    
# prepare the dataset of input to output pairs encoded as integers
dataX = []
dataY = []
for i in range(0, len(states) - seq_length, 1):
	seq_in = states[i:i + seq_length]
	seq_out = states[i + seq_length]
	dataX.append(seq_in)
	dataY.append(seq_out)
n_patterns = len(dataX)
print("Total Patterns: ", n_patterns)
print("dataX and Y are", len(dataX), len(dataY))
    
    
# reshape X to be [samples, time steps, features]
X = np.reshape(dataX, (n_patterns, seq_length, 1))
# normalize
X = X / float(n_states)
# one hot encode the output variable
y = np_utils.to_categorical(dataY)


"""
# Train Neural Nets
# Use "return_sequences=True" only if using > 1 hidden layer.
# This snippet should be used to transfer information from one layer to the next
# Below, a 3 layer LSTM
print('Building LSTM network...')
model = Sequential()
model.add(LSTM(256, input_shape=(X.shape[1], X.shape[2]), return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256))
model.add(Dropout(0.2))
model.add(Dense(y.shape[1], activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam')    
"""
print('Building LSTM network...')
model = Sequential()
model.add(LSTM(256, input_shape=(X.shape[1], X.shape[2]), return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256))
model.add(Dropout(0.2))
model.add(Dense(y.shape[1], activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam') 

# define the checkpoint
filepath="training-weights-{epoch:02d}-{loss:.4f}.hdf5"
checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
callbacks_list = [checkpoint]

#print("X shape before fit is", X.shape)
#print("y shape before fit is", y.shape)
# fit the model
print('Training the net...')
model.fit(X, y, nb_epoch=num_epochs, batch_size=batch_size, callbacks=callbacks_list)
#print("X shape after fit is", X.shape)    
    
print(("---Run time %s seconds ---" % (time.time() - start_time)))
