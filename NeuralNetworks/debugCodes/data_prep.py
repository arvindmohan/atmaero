#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Data Preparation
Load Surface Pressure data and explore them
e.g. plotting, etc.
Created on Sat Mar  4 03:03:25 2017

@author: arvind
"""


import numpy as np
import h5py
import matplotlib.pyplot as plt

"""
Static Stall with and w/o control
"""

f1 = h5py.File('surfaceP_NC.mat','r') 
f2 = h5py.File('surfaceP_C.mat','r') 
f1.keys()
f2.keys()
# check keys name to get stored variable name
# Here, it is surfPNC
# Finally, extract the array data from the HDF5 files
spNC= f1.get('surfPNC')
spC = f2.get('surfPC')

""" Plot C and NC Surface pressures at axial location of choosing """
ax_loc = 100
plt.plot(spNC[:,ax_loc],label='NoControl')
plt.plot(spC[:,ax_loc],label='Control')
plt.title('Surface pressures at axial location')
plt.xlabel('Time')
plt.ylabel('Amplitude')
plt.legend(loc='upper left')
plt.show()

""" Plot NC Surface Pressures at different locations in the leading
edge, at adjacent grid points """

for i in range(10,86):
    plt.plot(spNC[:,i])

plt.title('Surface pressures at axial location')
plt.xlabel('Time')
plt.ylabel('Amplitude')
plt.legend(loc='upper left')
plt.show()

"""
Dynamic Stall of Plunging Airfoil
"""
g1 = h5py.File('surfPHighResdynStall.mat','r') 
g1.keys()

spDS= g1.get('surfP')
xloc = 30
zloc = 10
plt.plot(spDS[1250:,zloc,xloc])
plt.title('Surface pressures at axial location')
plt.xlabel('Time')
plt.ylabel('Amplitude')

""" Plot DS Surface Pressures at different locations in the leading
edge, at adjacent grid points """
xloc= 10
zloc = 10
for i in range(1,50):
    plt.plot(spDS[:,i,xloc])

plt.title('Surface pressures at axial location')
plt.xlabel('Time')
plt.ylabel('Amplitude')












