#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
LSTM for prediction of turbulent time series
Stateful net with memory and stacked LSTMs with memory
Created on Mon Nov 21 03:24:19 2016

@author: arvind
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas
import math
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
import os

# set Look_back

look_back = 1
num_epochs = 5
psize = 30

# convert an array of values into a dataset matrix
def create_dataset(dataset, look_back=1):
	dataX, dataY = [], []
	for i in range(len(dataset)-look_back-1):
		a = dataset[i:(i+look_back), 0]
		dataX.append(a)
		dataY.append(dataset[i + look_back, 0])
	return np.array(dataX), np.array(dataY)
 
#change current work dir
os.chdir('/home/arvind/Research_Aurora/DeepLearning/Turbulence/AirfoilData/Dynstall/')

# fix random seed for reproducibility
np.random.seed(7)

# load the dataset
dataframe = pandas.read_csv('airfoilTE.csv', usecols=[1], engine='python')
dataset = dataframe.values
dataset = dataset.astype('float32')

# normalize the dataset
scaler = MinMaxScaler(feature_range=(0, 1))
dataset = scaler.fit_transform(dataset)

# split into train and test sets
train_size = int(len(dataset) * 0.67)
test_size = len(dataset) - train_size
train, test = dataset[0:train_size,:], dataset[train_size:len(dataset),:]
print(len(train), len(test))

# reshape into X=t and Y=t+1
trainX, trainY = create_dataset(train, look_back)
testX, testY = create_dataset(test, look_back)

# reshape input to be [samples, time steps, features]
trainX = np.reshape(trainX, (trainX.shape[0], trainX.shape[1], 1))
testX = np.reshape(testX, (testX.shape[0], testX.shape[1], 1))

# create and fit the LSTM network
model = Sequential()
model.add(LSTM(4, input_dim=1))
model.add(Dense(1))
model.compile(loss='mean_squared_error', optimizer='adam')
model.fit(trainX, trainY, nb_epoch=num_epochs, batch_size=1, verbose=2)

# make predictions
trainPredict = model.predict(trainX)
testPredict = model.predict(testX)


# invert predictions
trainPredict = scaler.inverse_transform(trainPredict)
trainY = scaler.inverse_transform([trainY])
testPredict = scaler.inverse_transform(testPredict)
testY = scaler.inverse_transform([testY])

# calculate root mean squared error
trainScore = math.sqrt(mean_squared_error(trainY[0], trainPredict[:,0]))
print('Train Score: %.2f RMSE' % (trainScore))
#testScore = math.sqrt(mean_squared_error(testY[0], testPredict[:,0]))
#print('Test Score: %.2f RMSE' % (testScore))


trainPredictPlot = np.empty_like(dataset)
trainPredictPlot[:, :] = np.nan
trainPredictPlot[look_back:len(trainPredict)+look_back, :] = trainPredict
              
# shift test predictions for plotting
testPredictPlot = np.empty_like(dataset)
testPredictPlot[:, :] = np.nan
testPredictPlot[len(trainPredict)+(look_back*2)+1:len(dataset)-1, :] = testPredict                

# calculate unseen predictions
unknownTest = np.empty_like(trainX[len(trainX) - psize:len(trainX),])
unknownTest = trainX[len(trainX) - psize:len(trainX),]
unknownTest[:,0,:] = unknownTest[:,0,:] #+ 0.05*np.reshape(np.random.normal(0,1,len(testX)), [len(unknownTest[:,0,:]), 1])

unknownTestPredict = model.predict(unknownTest)               
            
unknownTestPredict = scaler.inverse_transform(unknownTestPredict)
unknownTestPredictPlot = np.zeros([len(trainX),1])
unknownTestPredictPlot[:, :] = np.nan
unknownTestPredictPlot[len(trainX) - psize:len(trainX),] = unknownTestPredict                 
                
# calculate root mean squared error
unknownCompareTrain = np.reshape(np.squeeze(trainX[len(trainX) - psize:len(trainX),:,:]), [1, psize])
unknownTestScore = math.sqrt(mean_squared_error(unknownCompareTrain[0], unknownTestPredict[:,0]))
print('Unknown test Score: %.2f RMSE' % (unknownTestScore))
                       
# plot baseline and predictions
#plt.plot(scaler.inverse_transform(dataset), label='dataset') #actual dataset
#trainPlot = 
temp = trainX[:(len(trainX) - psize),:]
trainPlot = np.reshape(temp, [148, 1])    

unknownInputPlot =  np.reshape(unknownTest, [psize, 1])  
unknownInputPlot = scaler.inverse_transform(unknownInputPlot)          
#plt.plot(trainPlot, label='train')
#plt.plot(testPredictPlot, label='test')
#plt.plot(testPredictPlot, label='test')
plt.plot(unknownInputPlot, label='input')
plt.plot(trainPredictPlot, label='train')
plt.plot(unknownTestPredictPlot,'o', label='predicted')
plt.legend(loc='upper right')
plt.xlabel('time')
plt.ylabel('signal')
plt.show()
                

