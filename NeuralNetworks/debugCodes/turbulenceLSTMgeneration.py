#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Test LSTM on artificial datasets
for turbulent flows
Generating predicted data
Created on Sat Dec  3 05:32:56 2016

@author: arvind
"""

# Load LSTM network and generate text
import sys
import numpy as np
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils

def generate_data(Fs, t, SNR, switch):
    data_length = Fs*t
    x = (np.linspace(0,t,data_length))*(1.0/Fs)
    if switch == 1:
        noise = np.random.randn(data_length)*(1.0/SNR)
    else:
        noise = 0
    data = 1.0 + np.sin(x+noise)
    return x, data
    

def plot_data(data):
    plt.plot(data,'o')
    plt.title('Signal')
    plt.xlabel('Time')
    plt.ylabel('Amplitude')
    plt.show()

def downsample(data, n_points):
    """ downsample every 'n' points """
    new_data = data[0:-1:n_points]
    return new_data
    
def define_states(data, n_states):
    """ Define bounds for each state """
    data_lowbound = np.min(data)
    data_highbound = np.max(data)
    print 'Signal Min/Max:', data_lowbound, data_highbound
    data_span = data_highbound - data_lowbound
    state_window = data_span/n_states
    lbound = []
    ubound = []
    state_id = []
    for i in range(n_states):
        state_id.append(i)
        lbound.append(data_lowbound + (i*state_window))
        ubound.append(data_lowbound + ((i+1)*state_window))
        
    state_boundaries = zip(state_id,lbound,ubound)
    return state_boundaries
    
def create_states(data, state_boundaries):
    """ identify states in the signal """
    states = []
    sig_len = len(data)
    for i in range(sig_len):
        dataPoint = data[i]
        for j in range(len(state_boundaries)):
            if (dataPoint > state_boundaries[j][1] \
                and dataPoint <= state_boundaries[j][2]):
                states.append(state_boundaries[j][0])
                break
    return states        
        
    
def map_to_signal(data, state_boundaries):
    """ Map predicted states back to signal form """
    signal = []
    for i in range(len(data)):
        dataPoint = data[i]
        #print dataPoint
        id = state_boundaries[dataPoint][0]
        try:
            assert(id == dataPoint)
        except AssertionError:
            print 'the offending index', dataPoint
        signal.append(0.5*(state_boundaries[dataPoint][1] \
                           + state_boundaries[dataPoint][2]))
    return signal

    
"""
Main Code
"""
# PREPARE DATA 
Fs = 100
t = 2000
SNR = 50
noise_switch = 0
n_states = 50

x_axis, signal = generate_data(Fs, t, SNR, noise_switch)
print 'Fs =', Fs,'and Noise', noise_switch

downsamp_signal = downsample(signal, 500)
plot_data(downsamp_signal)
state_boundaries = define_states(downsamp_signal, n_states)
states = create_states(downsamp_signal, state_boundaries)

# prepare the dataset of input to output pairs encoded as integers
seq_length = 5
dataX = []
dataY = []
for i in range(0, len(states) - seq_length, 1):
	seq_in = states[i:i + seq_length]
	seq_out = states[i + seq_length]
	dataX.append(seq_in)
	dataY.append(seq_out)
n_patterns = len(dataX)
print "Total Patterns: ", n_patterns

# reshape X to be [samples, time steps, features]
X = np.reshape(dataX, (n_patterns, seq_length, 1))
# normalize
X = X / float(n_states)
# one hot encode the output variable
y = np_utils.to_categorical(dataY)

# Train Neural Nets
model = Sequential()
model.add(LSTM(256, input_shape=(X.shape[1], X.shape[2])))
model.add(Dropout(0.2))
model.add(Dense(y.shape[1], activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam')    

# load the network weights
filename = "weights-improvement-19-2.9741.hdf5"
model.load_weights(filename)
model.compile(loss='categorical_crossentropy', optimizer='adam')
# pick a random seed
start = np.random.randint(0, len(dataX)-1)
pattern = dataX[start]
print "Seed:"
print "\"", pattern , "\""
# generate characters
for i in range(400):
	x = np.reshape(pattern, (1, len(pattern), 1))
	x = x / float(n_states)
	prediction = model.predict(x, verbose=0)
	index = np.argmax(prediction)
	#result = prediction[index]
	#print result
	pattern.append(index)
	pattern = pattern[1:len(pattern)]
print "\nDone."

predictionMod = prediction*float(n_states)
predictionMod = np.transpose(predictionMod.astype(int))
predicted_signal = map_to_signal(predictionMod, state_boundaries)

plot_data(predicted_signal)



