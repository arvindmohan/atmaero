"""
Predict time sequences using
Trained Dyn Stall Inception data
"""
import sys
import numpy as np
import h5py
import time
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils

def plot_data(data):
    plt.plot(data,'o')
    plt.title('Signal')
    plt.xlabel('Time')
    plt.ylabel('Amplitude')
    plt.show()

def downsample(data, n_points):
    """ downsample every 'n' points """
    new_data = data[0:-1:n_points]
    return new_data
    
def define_states(data, n_states):
    """ Define bounds for each state """
    data_lowbound = np.min(data)
    data_highbound = np.max(data)
    print('Signal Min/Max:', data_lowbound, data_highbound)
    data_span = data_highbound - data_lowbound
    state_window = data_span/n_states
    lbound = []
    ubound = []
    state_id = []
    for i in range(n_states):
        state_id.append(i)
        lbound.append(data_lowbound + (i*state_window))
        ubound.append(data_lowbound + ((i+1)*state_window))
        
    state_boundaries = list(zip(state_id,lbound,ubound))
    return state_boundaries
    
def create_states(data, state_boundaries):
    """ identify states in the signal """
    states = []
    sig_len = len(data)
    print(sig_len)
    for i in range(0,sig_len):
        print(i)
        dataPoint = data[i]
        print(dataPoint)
        for j in range(len(state_boundaries)):
            if (dataPoint > state_boundaries[j][1] \
                and dataPoint <= state_boundaries[j][2]):
                states.append(state_boundaries[j][0])
                print(state_boundaries[j][0])
                break
    return states        


def map_to_signal(data, state_boundaries):
    """ Map predicted states back to signal form """
    signal = []
    for i in range(len(data)):
        dataPoint = data[i]
        #print dataPoint
        id = state_boundaries[dataPoint][0]
        try:
            assert(id == dataPoint)
        except AssertionError:
            print('the offending index', dataPoint)
        signal.append(0.5*(state_boundaries[dataPoint][1] \
                           + state_boundaries[dataPoint][2]))
    return signal


def generate_noisy_seed(data, state_boundaries, SNR):
    signal = map_to_signal(data, state_boundaries)
    noise = np.random.normal(0, 1, len(signal))*(1.0/SNR)
    noisy_signal = signal + noise
    noise_state_signal = create_states(noisy_signal, state_boundaries)
    return noise_state_signal
    
    
    
""" 
Read Data
 
"""

inputData = np.load('NACA0012stalled.npy')
print(('Shape', inputData.shape))   

    
"""
Main Code
"""
# INPUT
n_states = 100
seq_length = 10


state_boundaries = define_states(inputData, n_states)
states = create_states(inputData, state_boundaries)    
# prepare the dataset of input to output pairs encoded as integers
dataX = []
dataY = []
for i in range(0, len(states) - seq_length, 1):
	seq_in = states[i:i + seq_length]
	seq_out = states[i + seq_length]
	dataX.append(seq_in)
	dataY.append(seq_out)
n_patterns = len(dataX)
print("Total Patterns: ", n_patterns)
    
    
# reshape X to be [samples, time steps, features]
X = np.reshape(dataX, (n_patterns, seq_length, 1))
# normalize
X = X / float(n_states)
# one hot encode the output variable
y = np_utils.to_categorical(dataY)
# Train Neural Nets
print('Building LSTM network...')
model = Sequential()
model.add(LSTM(256, input_shape=(X.shape[1], X.shape[2])))
model.add(Dropout(0.2))
model.add(Dense(y.shape[1], activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam')    

# load the network weights
filename = "training-weights-09-3.8428.hdf5"
model.load_weights(filename)
model.compile(loss='categorical_crossentropy', optimizer='adam')
# pick a random seed from input
#start = np.random.randint(0, len(dataX)-1)
#pattern = dataX[start]

print('Picking a noisy seed...')
#np.random.seed(42)
start = np.random.randint(0, len(dataX)-1)
purePattern = dataX[start]
#print 'Picking a noisy seed...'
#SNR = 10
#noisyPattern = generate_noisy_seed(purePattern, state_boundaries, SNR)
pattern = purePattern;
#print "Seed:"
#print "\"", pattern , "\""
print('Generating Sequence....')
predict_length = 400
# generate characters
predicted_signal=[]
for i in range(predict_length):
    x = np.reshape(pattern, (1, len(pattern), 1))
    x = x / float(n_states)
    prediction = model.predict(x, verbose=0)
    index = np.argmax(prediction)
    result = 0.5*(state_boundaries[index][1] + state_boundaries[index][2])
    #print result
    predicted_signal.append(result)
    pattern.append(index)
    pattern = pattern[1:len(pattern)]
print("\nDone.")

print('Original Signal:')
plot_data(inputData)
print('Generated Signal:')
plot_data(predicted_signal)



#a = map_to_signal(purePattern, state_boundaries)

#b = np.random.normal(0, 1, len(a))*(1.0/SNR)

# = a + b

#d = create_states(c, state_boundaries)






