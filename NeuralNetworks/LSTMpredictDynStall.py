"""
Predict time sequences using
Trained Dyn Stall Inception data
(training weights being generated elsewhere, like Godzilla)

Arvind Mohan 
PhD Candidate
Ohio State U
"""
import sys
import numpy as np
import h5py
import time
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils

def plot_data(data):
    plt.plot(data,'o')
    plt.title('Signal')
    plt.xlabel('Time')
    plt.ylabel('Amplitude')
    plt.show()

def downsample(data, n_points):
    """ downsample every 'n' points """
    new_data = data[0:-1:n_points]
    return new_data
    
def define_states(data, n_states):
    """ Define bounds for each state """
    data_lowbound = np.min(data)
    data_highbound = np.max(data)
    print('Signal Min/Max:', data_lowbound, data_highbound)
    data_span = data_highbound - data_lowbound
    state_window = data_span/n_states
    lbound = []
    ubound = []
    state_id = []
    for i in range(n_states):
        state_id.append(i)
        lbound.append(data_lowbound + (i*state_window))
        ubound.append(data_lowbound + ((i+1)*state_window))
        
    state_boundaries = list(zip(state_id,lbound,ubound))
    return state_boundaries
    
def create_states(data, state_boundaries):
    """ identify states in the signal """
    states = []
    sig_len = len(data)
    #print(sig_len)
    for i in range(0,sig_len):
        #print(i)
        dataPoint = data[i]
        #print(dataPoint)
        for j in range(len(state_boundaries)):
            if (dataPoint > state_boundaries[j][1] \
                and dataPoint <= state_boundaries[j][2]):
                states.append(state_boundaries[j][0])
                #print(state_boundaries[j][0])
                break
    return states        


def signal_convert(dat, state_boundaries):
    rawSignal=[]
    datlen = len(dat)
    #print('len', datlen)
    for i in range(0,datlen-1):
        #print('i is', i)
        ind = dat[i]
        #print('index', ind)
        temp = 0.5*(state_boundaries[ind][1] + state_boundaries[ind][2])
        rawSignal.append(temp)
        #print('aa is', temp)
    
    return rawSignal


def generate_noisy_seed(data, state_boundaries, SNR):
    signal = map_to_signal(data, state_boundaries)
    noise = np.random.normal(0, 1, len(signal))*(1.0/SNR)
    noisy_signal = signal + noise
    noise_state_signal = create_states(noisy_signal, state_boundaries)
    return noise_state_signal
    
    
    
""" 
Read Data
 
"""

inputData = np.load('dynStallInceptionDown3.npy')
print(('Shape', inputData.shape))   

    
"""
Main Code
"""
# INPUT
n_states = 250
seq_length = 75
# load the network weights
filename = "weights_case3_r5.hdf5"

state_boundaries = define_states(inputData, n_states)
states = create_states(inputData, state_boundaries)    
# prepare the dataset of input to output pairs encoded as integers
dataX = []
dataY = []
for i in range(0, len(states) - seq_length, 1):
	seq_in = states[i:i + seq_length]
	seq_out = states[i + seq_length]
	dataX.append(seq_in)
	dataY.append(seq_out)
n_patterns = len(dataX)
print("Total Patterns: ", n_patterns)
    
    
# reshape X to be [samples, time steps, features]
X = np.reshape(dataX, (n_patterns, seq_length, 1))
# normalize
X = X / float(n_states)
# one hot encode the output variable
y = np_utils.to_categorical(dataY)
# Train Neural Nets
print('Building LSTM network...')
model = Sequential()
model.add(LSTM(256, input_shape=(DataInput.shape[1], DataInput.shape[2]), return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256))
model.add(Dropout(0.2))
model.add(Dense(DataOut.shape[1], activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam')  


model.load_weights(filename)
model.compile(loss='categorical_crossentropy', optimizer='adam')
# pick a random seed from input
print('Picking a noisy seed...')
#np.random.seed(12)
start = np.random.randint(0, len(dataX)-1)
start=0
pattern = dataX[start]
print('INIT pattern is', pattern)
# convert pattern to signal
#patternSignal = signal_convert(pattern,state_boundaries)
#print('Input Seed Pattern:')
#plot_data(patternSignal)
#print 'Picking a noisy seed...'
#SNR = 10
#noisyPattern = generate_noisy_seed(purePattern, state_boundaries, SNR)
#print "Seed:"
#print "\"", pattern , "\""
print('Generating Sequence....')
predict_length = 110
# generate characters
predicted_signal=[]
for i in range(predict_length):
    x = np.reshape(pattern, (1, len(pattern), 1))
    #print('x is', x)
    x = x / float(n_states)
    #print('xmod is', x)
    prediction = model.predict(x, verbose=0)
    index = np.argmax(prediction)
    result = 0.5*(state_boundaries[index][1] + state_boundaries[index][2])
    #print result
    predicted_signal.append(result)
    pattern.append(index)
    pattern = pattern[1:len(pattern)]
print("\nDone.")

# convert pattern to signal
patternSignal = signal_convert(pattern,state_boundaries)

# final prediction
finalPrediction = patternSignal + predicted_signal

"""
print('Seed ID is', start)
print('FINAL pattern is', pattern)
print('data at Seed ID', dataX[start])
"""
print('Original Signal:')
plot_data(inputData)
print('Input Seed Pattern:')
plot_data(patternSignal)
print('Generated Signal:')
plot_data(predicted_signal)
print('Input + predicted signal')
plot_data(finalPrediction)
