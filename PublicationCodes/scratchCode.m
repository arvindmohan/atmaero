%% Scratch Code
% Put all junk code here
clc
clear

%% FFT for Lift
% tmp = CL;
% dt=50e-04;
% fs=1/dt;
% NFFT=256;
% tmp2 = squeeze(fft(tmp,NFFT)/length(tmp));
% pxx = 2*abs(tmp2(1:NFFT/2+1));
% f = fs/2*linspace(0,1,NFFT/2+1);
% 
% PSD_support=pxx;
% figure(10)
% plot(f,PSD_support)
% 
% 


%% Integration for Lift
clc
clear
% 
% x = -3:1:3; 
% y = -5:1:5; 
% [X,Y] = meshgrid(x,y); 
% F = X.^2 + Y.^2;
% %I = trapz(y,trapz(x,F,2))
% mesh(X,Y)


% [x,y,z,xl,yl,zl]=readgrid_VW;
% [X,Y] = meshgrid(x,y);
% %x = x';y=y';
% cd('/work/mohan.69/ModelReduction/VortexWingFullData/FOR_ARVIND/Pwallsnaps/');
% temp1 = reshape(importdata('rhoewall400100.mat'),[xl yl]);
% temp1 = 2.0*temp1*cos(4);
% A =  6.0127;
% %temp1 = temp1';
% %mesh(X,Y,temp1)
% %contourf(x,y,temp1)
% cd('/work/mohan.69/ModelReduction/VortexWingFullData/');
% plot_wh_meanc(y,x,temp1,temp1,21,2,1003)
% I = (trapz(y,trapz(x,temp1,1)))/A

% m = length(y) - 1; n = length(x) - 1;
% %y = y';
% P = zeros(m-1,n-1);
% dA = zeros(m-1,n-1);
% integ = 0;
% for i = 1:m
% 
%     for j = 1:n
%          P(i,j) = (F(i,j) + F(i+1,j) + F(i,j+1) + F(i+1,j+1))/4;
%          dx = abs(X(i,j+1) - X(i,j));
%          dy = abs(Y(i+1,j) - Y(i,j));
%          dA(i,j) = dx*dy;
%          integ = integ + P(i,j)*dA(i,j);
%     end
% 
% end
% 
% A = m*n;
% val = integ/A
% 
alpha = 4*(pi/180);
dim = [161 661 1];
[x,y,z,xl,yl,zl]=readgrid_VW;
[X,Y] = meshgrid(x,y);

cd('/work/mohan.69/ModelReduction/VortexWingFullData/FOR_ARVIND/Pwallsnaps/');
data = reshape(importdata(['rhoewall' num2str(400000) '.mat']),dim);

%data = data';
 A = 6.0;
% [y,x] = size(X);
% m = y - 1; n = x - 1;
% P = zeros(m-1,n-1);
% dA = zeros(m-1,n-1);
% integ = 0;
% for i = 1:m
% 
%     for j = 1:n
%          P(i,j) = (data(i,j) + data(i+1,j) + data(i,j+1) + data(i+1,j+1))/4;
%          dx = abs(X(i,j+1) - X(i,j));
%          dy = abs(Y(i+1,j) - Y(i,j));
%          dA(i,j) = dx*dy;
%          integ = integ + P(i,j)*dA(i,j);
%     end
% 
% end
% integQty = (2.0*integ*cos(alpha))/A

[Cl,P] = integArea(data,X,Y,A,alpha);
y1 = y(1:end-1); x1 = x(1:end-1);
contourf(y1,x1,P')
figure
contourf(y,x,data);

%% Flow Stats

% 
% figure(2)
% subplot(1,2,1)
% stdPoint = squeeze(fstd(Xi,Zi,:));
% heightVector = 0:1:111;
% heightVectorCoarse = NaN(1,111);
% temp = squeeze(fstdCoarse(Xi,Zi,:));
% stdPointCoarse = NaN(112,1);
% j=1;
% for i = 1:56
%     stdPointCoarse(j) =  temp(i);
% %     heightVectorCoarse(j) = heightVector(i);
%     j=j+2;
% end
% plot(stdPoint,heightVector,'+')
% hold on
% plot(stdPointCoarse,heightVector,'+')
% 
% xlabel('Standard Deviation')
% ylabel('Height')
% title('std variation with height - FINE vs COARSE')
% 
% clear temp
% subplot(1,2,2)
% meanPoint = squeeze(flowmean(Xi,Zi,:));
% % heightVector = 0:1:111;
% % heightVectorCoarse = NaN(1,111);
% temp = squeeze(flowmeanCoarse(Xi,Zi,:));
% meanPointCoarse = NaN(112,1);
% j=1;
% for i = 1:56
%     meanPointCoarse(j) =  temp(i);
% %     heightVectorCoarse(j) = heightVector(i);
%     j=j+2;
% end
% plot(meanPoint,heightVector,'+')
% hold on
% plot(meanPointCoarse,heightVector,'+')
% xlabel('Mean')
% ylabel('Height')
% title('Mean variation with height -FINE vs COARSE')
% 
% set(2,'Position',[230 250 1200 800])
% 
% 
% 







%% 2D dmd
% dim = [161 661 112];
% dimflat = [161*661 1];
% a = reshape(importdata('rhou200000.mat'),dim);
% a = reshape(squeeze(a(:,:,1)),dimflat);
% 
% b = importdata('rhouwall200000.mat');
% 
% 

%% Coarsen grid
% 
% %a = importdata('rhou200000.mat');
% dim = [161 661 112];
% a = reshape(importdata('rhou200000.mat'),dim);
% l = length(a);
% 
% il = dim(1); jl = dim(2); kl = dim(3);
% 
% dimCoarse = [81  331  56]
% b = zeros(dimCoarse);
% 
% ic=1;jc=1;kc=1;
% for k = 1:2:kl
%     for j = 1:2:jl
%         for i = 1:2:il
%            b(ic,jc,kc) = a(i,j,k);
%            ic = ic+1;
%         end
%         ic=1;
%         jc = jc+1;
%     end
%     jc=1;
%     kc = kc+1;
% end
% 
% 
% % Writing DMD modes to ASCII PLOT3D files
% disp('Writing DMD modes to Plot3D files');
% header = [81  331  56 1];
% for i=1:1
% fid = fopen(['rhouCoarse' num2str(i) '.dat'], 'w');
% fprintf(fid, '%i \t',header);
% fprintf(fid,'\n');
% fprintf(fid, '%f \t',b);
% fclose(fid);
% end
% disp('====================================================')
% 
% 
% 
% fid = fopen('probe-200000.q','r','ieee-le');
% ng = fread(fid,1,'int')
% dim = fread(fid,[3,ng],'int')
% dat = fread(fid,4,'double')
% rho = reshape(fread(fid,prod(dim),'double'),dim');
% rhou = reshape(fread(fid,prod(dim),'double'),dim');
% rhov = reshape(fread(fid,prod(dim),'double'),dim');
% rhow = reshape(fread(fid,prod(dim),'double'),dim');
% rhoe = reshape(fread(fid,prod(dim),'double'),dim');
% fclose(fid);
% 
% 
% 
% 
% 
% 


%% Basic Stats of flow
% clc;clear;close all
% 
% rng('default');
% rng(1,'twister');
% % Simple example
% % a = rand(3);
% % 
% % d = std(a)
% % 
% % for i=1:length(a)
% %     e(i,:) = (a(i,:) - mean(a)).^2;
% % end
% % 
% % fstd = sqrt(sum(e)/(length(a)-1))
% 
% % 3D example
% 
% a = rand(3,4,5,7);
% 
% d = std(a,0,4) % 3 is the dim. to operate along. 0 is norm. weight
%                 % suggested 0 as per documentation.
% meanval = mean(a,4);
% e = zeros(3,4,5);
% for i=1:length(a)
%     e(:,:,:) = e(:,:,:) + (a(:,:,:,i) - meanval).^2;
% end
% 
% fstd = sqrt(e/(length(a)-1))
% %fstd = sqrt(sum(e,4)/(length(a)-1))
% d - fstd
% 

