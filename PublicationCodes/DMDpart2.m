%% DMD Part 2
% Reconstruction and post-processing

%% Reconstruction

%Computing Scaling Coeffs for modes using 1st snapshot
disp('Computing Scaling Coeffs');
u1 = importdata('rhou200000.mat');
d = modes\u1;
clear u1

% Computing Scaled modes

modes_scaled = zeros(gridSize,nummodes);

for i=1:nummodes
   modes_scaled(:,i) =  modes(:,i)*d(i);
end

% Compute Scaled Mode Norms

mode_norms = zeros(nummodes,1);
for i=1:nummodes
   mode_norms(i) = norm(modes_scaled(:,i));
end

mode_ampl_unordered = mode_norms/(max(mode_norms));


% Order Modes and other vars by amplitude

[~,idx] = sort(mode_ampl_unordered,'descend');
mode_ampl = mode_ampl_unordered(idx);
clear mode_ampl_unordered
modes_scaled = modes_scaled(:,idx);
modes = modes(:,idx);
ritzOrder = ritz(idx);
floqvalsOrder = floqvals(idx);
dOrder = d(idx);

% Build Vandermonde Matrix using Ritz values

T = fliplr(vander(ritz));

T = T(idx,:);

% Computing Reconstructed Velocity
disp('Computing Reconstructed Data . . .');

dmdrecon = modes_scaled(:,1:reconmodes)*T(1:reconmodes,:);














