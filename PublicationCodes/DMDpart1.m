%% DMD Part 1
% Load Data and do eig decomposition
clc;clear;close all

%% USER INPUT
%cd('../Raw_data/');
pwd;
numofsnaps = 10;
delta = 50;
timestep = 10^-4; % From CFD solver - non-DIMENSIONAL TIME FOR EVERY 'DT'
init_file  = 200000;%  <== CHANGE if needed.
final_file = (init_file-delta) + ((numofsnaps)*delta);
reconmodes = numofsnaps-1;
meanfname = 'Umean';

% Header File info for Plot3d output
xhead = 161;
yhead = 661;
zhead = 112;
nvar = 1;

parpool();

%% DMD algorithm
dim = [xhead yhead zhead];
gridSize = prod(dim);
Nsnap = numofsnaps;

disp('Building Snapshot Mean...');

tic
% Compute Mean
temp = zeros(gridSize,1);
parfor i=1:Nsnap
   filenum = (init_file-delta) + i*delta;
   %disp(['Reading file rhou' num2str(filenum) '.mat']);
   temp = temp + importdata(['rhou' num2str(filenum) '.mat']);
end

Umean = temp/Nsnap;

clear temp
% Step 1: Building Matrix V1^n-1

V1 = zeros(gridSize,Nsnap-1);
parfor i=1:Nsnap-1
    filenum = (init_file-delta) + i*delta;
    V1(:,i) = importdata(['rhou' num2str(filenum) '.mat']);
end

% Computation Starts

% Step 3: Singular Value Decomposition of the V1 matrix (Economy)
disp('SVD in process...')
[U,E,W] = svd(V1,0);
disp('Operation Successful.');


clear V1
% Step 2: Building V2 Matrix

V2 = zeros(gridSize,Nsnap-1);
parfor i=1:Nsnap-1
    filenum = init_file + i*delta;
    V2(:,i) = importdata(['rhou' num2str(filenum+delta) '.mat']);
end

% Step 4: Computing S matrix

S = U'*V2*W/E;

clear V2 W E
% Step 5: Eigendecomposition of Matrix S
disp('Computing Eigendecomposition of S Matrix...')
[eigenvecs,eigenvals] = eig(S);
disp('Operation Successful.');
clear S

ritz = diag(eigenvals); % saving the Ritz values
clear eigenvals

time1 = toc;
time1 = time1/60

%% Post Processing of DMD data

% Computing Modes

disp('Computing DMD Modes...')
modes = U*eigenvecs;
clear U eigenvecs
[~,nummodes] = size(modes);
disp('Operation Successful.'); 

% Sorting Ritz values in descending order
[~,index] = sort(real(ritz),'descend');
ritz = ritz(index);
modes = modes(:,index);

% Computing Floquet values
dt = timestep*delta; % Actual time interval

floqvals = log(ritz)/dt;


