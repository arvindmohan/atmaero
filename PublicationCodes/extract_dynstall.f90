! Extract Data for a given region in Dynamic Stall case

program extract3D
      real,allocatable,dimension(:,:,:) :: x,y,z
      real,allocatable,dimension(:,:,:,:,:)::flow
      integer il,jl,kl,i,j,k,nfuncs,v,f,kl2d,gridfilenums,delta
      character (len=80) :: grid_fname
      gridfilenums = 360
      delta = 1
      il2d = 315
      jl2d = 315
      kl2d = 1
      
        open(10,file='grid_span1',form='unformatted',status='OLD')
		read(10) il,jl,kl
		print*,'the original grid size',il,jl,kl
		allocate(x(il,jl,kl))
		allocate(y(il,jl,kl))
		allocate(z(il,jl,kl))
		read(10) x,y,z
      print*,'max x val is',maxval(x(:,:,:)) 
      print*,'min x val is',minval(x(:,:,:))
      print*,'max y val is',maxval(y(:,:,:)) 
      print*,'min y val is',minval(y(:,:,:))
      print*,'max z val is',maxval(z(:,:,:)) 
      print*,'min z val is',minval(z(:,:,:)) 
         close(10)
      
      do f= 1,gridfilenums
		open(20,file='gridfilename',form='formatted')
		read(20,*) grid_fname
		print*,grid_fname
		open(30,file=grid_fname,form='unformatted',status='OLD')
		read(30) il,jl,kl
		read(30) x,y,z
		close(30)

        grid_fname = 'gridslice_span'
        lenfname = len_trim(grid_fname)
        print*,'writing file', f
        write(grid_fname(lenfname+1:lenfname+3),'(I3.3)')  init+(f)*delta
        open(40,file=grid_fname,form='unformatted')
        write(40) il2d,jl2d,1
        write(40) (((x(i,j,k),i=1,il2d),j=1,jl2d),k=1,kl2d),&
				  &(((y(i,j,k),i=1,il2d),j=1,jl2d),k=1,kl2d),&
                  &(((z(i,j,k),i=1,il2d),j=1,jl2d),k=1,kl2d)        		 
        close(40)
      enddo 
      
      
      
      
     
end program extract3D


