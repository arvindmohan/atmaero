% Post Processing 2D DMD VW case

cd('/work/mohan.69/ModelReduction/VortexWingFullData/');
%load('dmdPwallVW.mat');

%% Plot modes
numofsnaps = 4394;
nummodes = numofsnaps - 1;
delta = 50;
timestep = 10^-4; % From CFD solver - non-DIMENSIONAL TIME FOR EVERY 'DT'
init_file  = 200000;%  <== CHANGE if needed.
final_file = (init_file-delta) + ((numofsnaps)*delta);
% Define basic params
header = [161 661 1 1];

nmodes = 68;
i = 2;
% Write to p3d files
parfor i=1:2:nmodes
    savep3d(['DMDmode' num2str(i)],header,modes(:,i));
end

% % Plot DMD modes in MATLAB
% 
% [x,y,z,xl,yl,zl]=readgrid_VW;
% Xmean = real(reshape(modes(:,modeID),dim));
% plot_wh_meanc(y,x,Xmean,Xmean,21,2,1003)

%% Reconstruction
gridSize = prod(header);
reconmodes = 15;
modes_scaled = zeros(gridSize,nummodes);
parfor i=1:nummodes
   modes_scaled(:,i) =  modes(:,i)*dOrder(i);
end

T = fliplr(vander(ritzOrder));

dmdrec = modes_scaled(:,1:reconmodes)*T(1:reconmodes,:);


%% Error calculation
cd('/work/mohan.69/ModelReduction/VortexWingFullData/FOR_ARVIND/Pwallsnaps/');
disp('computing error')
parfor i=1:nummodes
 filenum = (init_file-delta) + i*delta;
 %disp(['Reading file rhou' num2str(filenum) '.mat']);
 u(:,i) = importdata(['rhoewall' num2str(filenum) '.mat']);
 error_recon(:,i) = dmdrec(:,i) - u(:,i);
 error_sum(i) = sum(error_recon(:,i).^2);
 u_sum(i) = sum(u(:,i).^2);
 error_total(i) = sqrt(error_sum(i))/sqrt(u_sum(i));
 error(i) = abs(error_total(i))*100;
end
disp('done!!')
%total error

total_error = abs(sum(error_total))































