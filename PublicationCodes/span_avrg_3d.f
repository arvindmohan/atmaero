      PROGRAM SPAN_AVRG
C
      PARAMETER (IDM=466,JDM=395,KDM=101)
C
      common/rstin/ X(IDM,JDM,KDM),Y(IDM,JDM,KDM),Z(IDM,JDM,KDM)
      common/qplt/ q(IDM,JDM,KDM,5),q2d(IDM,JDM,3,5)
c
      open(unit=1,file='grid.d',status='old',form='unformatted')
      open(unit=2,file='flow.d',status='old',form='unformatted')
C
      open(unit=42,file='grid_span.d',form='unformatted')
      open(unit=43,file='flow_span.d',form='unformatted')
c
c
c INPUT DATA
      xm1=0.1
      re=60000.
      alpha=8.0
      GAM=1.4
      pinf=1./(gam*xm1*xm1)
c
C READ GRID AND Q 3D FILES
c
      read(1) idp,jdp,kdp
      read(1) (((x(i,j,k),i=1,idp),j=1,jdp),k=1,kdp),
     1         (((y(i,j,k),i=1,idp),j=1,jdp),k=1,kdp),
     2         (((z(i,j,k),i=1,idp),j=1,jdp),k=1,kdp)
c
      read(2) idp,jdp,kdp
      read(2) xm1,alpha,re,tau
      read(2) ((((q(i,j,k,n),i=1,idp),j=1,jdp),
     1 k=1,kdp),n=1,5)
c
c
      write(*,*) 'time=', tau
c
c average in the spanwise direction
c
      do i=1,idm
      do j=1,jdm
c
      q2d(i,j,1,1)=0.0
      q2d(i,j,1,2)=0.0
      q2d(i,j,1,3)=0.0
      q2d(i,j,1,4)=0.0
      q2d(i,j,1,5)=0.0
c
      do k=3,kdm-2
c
      q2d(i,j,1,1)=q2d(i,j,1,1)+q(i,j,k,1)
      q2d(i,j,1,2)=q2d(i,j,1,2)+q(i,j,k,2)
      q2d(i,j,1,3)=q2d(i,j,1,3)+q(i,j,k,3)
      q2d(i,j,1,4)=q2d(i,j,1,4)+q(i,j,k,4)
      q2d(i,j,1,5)=q2d(i,j,1,5)+q(i,j,k,5)
c
      end do
c
      q2d(i,j,1,1)=q2d(i,j,1,1)/float(kdm-4)
      q2d(i,j,1,2)=q2d(i,j,1,2)/float(kdm-4)
      q2d(i,j,1,3)=q2d(i,j,1,3)/float(kdm-4)
      q2d(i,j,1,4)=q2d(i,j,1,4)/float(kdm-4)
      q2d(i,j,1,5)=q2d(i,j,1,5)/float(kdm-4)
c
      end do
      end do
c
      do l=1,5
      do k=2,3
      do i=1,idm
      do j=1,jdm
c
      q2d(i,j,k,l)=q2d(i,j,1,l)
c
      end do
      end do
      end do
      end do
c
c output
c
      write(42) idm,jdm,3
      write(42) (((x(i,j,k),i=1,idm),j=1,jdm),k=1,3),
     1         (((y(i,j,k),i=1,idm),j=1,jdm),k=1,3),
     2         (((z(i,j,k),i=1,idm),j=1,jdm),k=1,3)
c
      write(43) idm,jdm,3
      write(43) xm1,alpha,re,tau
      write(43) ((((q2d(i,j,k,n),i=1,idm),j=1,jdm),
     1 k=1,3),n=1,5)
c
 1000 format(4(e16.9,1x))
c
      stop
      end
