function [xx,yy,zz,xl,yl,zl]=readgrid_VW

if 1==2
% Read  grid
fid = fopen('/work/mohan.69/ModelReduction/VortexWingFullData/FOR_ARVIND/Esnaps/suction_side.x','r','ieee-le');
ng = fread(fid,1,'int');
dim = fread(fid,[3,ng],'int')
x = reshape(fread(fid,prod(dim),'double'),dim');
y = reshape(fread(fid,prod(dim),'double'),dim');
z = reshape(fread(fid,prod(dim),'double'),dim');
fclose(fid);
xx=squeeze(x(:,1,1));
yy=squeeze(y(1,:,1))';
zz=squeeze(z(1,1,:));

xl=length(xx);
yl=length(yy);
zl=length(zz);
else
    
   load('/work/mohan.69/ModelReduction/VortexWingFullData/FOR_ARVIND/Esnaps/grid.mat')
   xx=x;
   yy=y;
   zz=z;
end

