! Extract Data for a given region in Dynamic Stall case

program extract3D
      real,allocatable,dimension(:,:,:) :: x,y,z
      real,allocatable,dimension(:,:,:,:)::flow
      integer il,jl,kl,i,j,k,nfuncs,v,f,kl2d,flowfilenums,delta
      real fsmach,alpha,re,time
      character (len=80) :: flow_fname
      flowfilenums = 5
      delta = 1
      nfuncs = 5 ! No. of Q parameters
      
     !the flow params are  0.1000000       8.000000       60000.00       78.57472
     
     open(10,file='flow_span1',form='unformatted',status='OLD')
	 read(10) il,jl,kl
	 print*,'the original flow grid size',il,jl,kl
	 read(10) fsmach,alpha,rey,time
	 print*,'the flow params are',fsmach,alpha,rey,time
	 allocate(flow(il,jl,kl,nfuncs))
	 !read(10) ((((flow(i,j,k,v),i=1,il),j=1,jl),k=1,kl),v=1,nfuncs)
  
	 il2d = 315
	 jl2d = 315
     kl2d = 1 ! taking 2d slice out of 3 planes saved in Dyn. stall case
      
      do f= 1,flowfilenums
		open(20,file='flowfilename',form='formatted')
		read(20,*) flow_fname
		print*,flow_fname
		open(30,file=flow_fname,form='unformatted',status='OLD')
		read(30) il,jl,kl
		read(30) fsmach,alpha,rey,time
		read(30) ((((flow(i,j,k,v),i=1,il),j=1,jl),k=1,kl),v=1,nfuncs)
		close(30)

        flow_fname = 'flowsliceasc_span'
        lenfname = len_trim(flow_fname)
        write(flow_fname(lenfname+1:lenfname+3),'(I3.3)')  init+(f)*delta
        open(40,file=flow_fname,form='formatted')
		write(40,*) il2d,jl2d,1
		write(40,*) fsmach,alpha,rey,time
		write(40,*) ((((flow(i,j,k,v),i=1,il2d),j=1,jl2d),k=1,kl2d),v=1,nfuncs)  
		print*,'written file',flow_fname      		 
		close(40)
      enddo
       
     
end program extract3D


