% Further DMD analysis based on Dr. Roy's AFRL report

clc
clear all
close all

% load data

cd('./Usnapsnew/PODresults/');
disp('Loading Data file...')
load poddynstall_u.mat

% Compute Single sided Amp. spectrum for mode(s)
disp('Computing Single sided Amp. spectrum for every mode ');
cd('../../');

for j=1:nummodes
    [freq,fftamp(:,j)] = probe_fft(aPOD(:,j),dt);
    fftamp_norm(:,j) = fftamp(:,j)/max(fftamp(:,j));
end

cd('./Usnapsnew/PODresults/');
%% Plotting

% Plotting Time Trace

trace = figure(1);
set(gcf, 'Position', [pos(1) pos(2) 10*100, 5*100]); %<- Set size
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
plot(time,aPOD(:,1),'b', ...
   time,aPOD(:,2),'r',...
   time,aPOD(:,3),'k',...
   time,aPOD(:,4),'c',...
   time,aPOD(:,5),'g','LineWidth',2)
xlabel('Time(s)','Fontsize',14,'FontWeight','bold')
ylabel('Real(a(t))','Fontsize',14,'FontWeight','bold')
title('Time trace of POD Amplitude ','Fontsize',14,'FontWeight','bold')
grid on
hleg1 = legend('Mode 1','Mode 2','Mode 3','Mode 4','Mode 5');
set(hleg1,'Location','NorthEast');
xlim([0 26])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Plotting FFTs
slice = 55;

for i=1:5
figure
plot(freq(1:slice),fftamp_norm(1:slice,i),'k')
xlabel('St. no');
ylabel('Amplitude');
title(['Single sided Amplitude spectrum for POD mode ' ...
       num2str(i)])
end

init_mode = 1;
final_mode = 5;

for i = init_mode:final_mode
     h = figure('Units', 'pixels', ...
    'Position', [100 100 500 375]);
     hold on;
     probef = line(freq(1:slice),fftamp_norm(1:slice,i));
     
     % Adjust Line Properties (Functional)
     set(probef,'LineStyle', '-','Color','b');
     
     % Adjust Line Properties (Aesthetics)
     set(probef,'LineWidth',0.5);

     % Add Legend and Labels

%      hTitle  = title(['Single sided Amplitude spectrum of Probe ' ...
%        num2str(i) ' vs Global DMD spectrum']);
     hXLabel = xlabel('St. no');
     hYLabel = ylabel('Normalized Amplitude');
    % hLegend = legend([probef,dmdf],['Probe','',num2str(i)],'DMD Frequency','location', 'NorthEast');
     
     % Adjust Font and Axes Properties

    set( gca                       , ...
        'FontName'   , 'Helvetica' );
    set([hXLabel, hYLabel], ...
        'FontName'   , 'AvantGarde');
   % set([hLegend, gca]             , ...
   %     'FontSize'   , 8           );
    set([hXLabel, hYLabel]  , ...
        'FontSize'   , 10          );
%     set( hTitle                    , ...
%         'FontSize'   , 10         , ...
%         'FontWeight' , 'bold'      );

    set(gca, ...
      'Box'         , 'off'     , ...
      'TickDir'     , 'out'     , ...
      'TickLength'  , [.02 .02] , ...
      'XMinorTick'  , 'on'      , ...
      'YMinorTick'  , 'on'      , ...
      'YGrid'       , 'off'      , ...
      'XColor'      , [.3 .3 .3], ...
      'YColor'      , [.3 .3 .3], ...
      'XTick'       , 0:0.5:4, ...
      'YTick'       , 0:0.1:1, ...
      'LineWidth'   , 1         );
  
%     % Export to EPS
        epsname = [ num2str(i) 'modefft' 'POD' '.eps'];
        %epsname = [ num2str(i) 'u' 'DMD' '.jpg'];
        set(gcf, 'PaperPositionMode', 'auto');
        print(h,'-depsc2',epsname)
        %print(h,'-djpeg ',epsname)
       close;
      
%       % Postprocess
% 
%       fixPSlinestyle(epsname,epsname);
       
end


disp('DONE!!!');

disp('******************************')
disp('Run Complete.')
cd('../../');
disp('******************************')