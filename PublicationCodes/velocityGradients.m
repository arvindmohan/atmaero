%% Velocity Gradients 3D flowfield
% Vortex-Wing interaction 
clear
clc

%% Choose parameters
component = 1; % u-1,v-2,w-3,U-4
height = 4; % 1-112 units from surface
direction = 1; % x-1,y-2


%% Create Field of operation

fieldU = rhou(:,:,height);
fieldV = rhov(:,:,height);
y = y(1,:,height);
x = x(:,1,height);

% Pick initial point
indx = -0.4523;
indy = 2.107;

ind=find(y<=indy);
Zi=ind(end);

ind=find(x<=indx);
Xi=ind(end);


%% Compute Gradients - U
% X-dir
clear dudx
dudx = NaN(dim(1),dim(2));

for i=1:(dim(1) - 1)
    for j = 1:dim(2)
        dudx(i,j) = (fieldU(i+1,j) - fieldU(i,j))/(x(i+1) - x(i));
    end
end

% Y-dir
clear dudy
dudy = NaN(dim(1),dim(2));

for i=1:dim(1)
    for j = 1:(dim(2)-1)
        dudy(i,j) = (fieldU(i,j+1) - fieldU(i,j))/(y(j+1) - y(j));
    end
end

%% Compute Gradients - V
% X-dir
clear dvdx
dvdx = NaN(dim(1),dim(2));

for i=1:(dim(1) - 1)
    for j = 1:dim(2)
        dvdx(i,j) = (fieldV(i+1,j) - fieldV(i,j))/(x(i+1) - x(i));
    end
end

% Y-dir
clear dvdy
dvdy = NaN(dim(1),dim(2));

for i=1:dim(1)
    for j = 1:(dim(2)-1)
        dvdy(i,j) = (fieldV(i,j+1) - fieldV(i,j))/(y(j+1) - y(j));
    end
end


%% Plot
% X-dir - U
figure(1)
set(1,'Position',[100 20 1800 900])
pcolor(y,x,dudx), shading interp
%contour(y,x,dudx)
colorbar
axis equal tight
colormap('jet')

% Y-dir - U
figure(2)
set(2,'Position',[100 20 1800 900])
pcolor(y,x,dudy), shading interp
%contour(y,x,dudx)
colorbar
axis equal tight
colormap('jet')

% X-dir - V
figure(3)
set(3,'Position',[100 20 1800 900])
pcolor(y,x,dvdx), shading interp
%contour(y,x,dudx)
colorbar
axis equal tight
colormap('jet')

% Y-dir - V
figure(4)
set(4,'Position',[100 20 1800 900])
pcolor(y,x,dvdy), shading interp
%contour(y,x,dudx)
colorbar
axis equal tight
colormap('jet')




