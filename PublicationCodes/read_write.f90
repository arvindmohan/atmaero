program read_write

   implicit none

   character(len=40) :: prbfile1, prbfile2
   real(kind=8), dimension(:), allocatable :: var1
   real(kind=4), dimension(:), allocatable :: var2
   real(kind=8) :: dat1(4)
   real(kind=4) :: dat2(4)
   integer, dimension(:,:), allocatable :: dims
   integer :: ng, n, si

   write(*,*) 'Specify probe file:'
   read (*,*) prbfile1
   prbfile2 = trim(prbfile1) // '.sp'

   open(1,file=prbfile1,form='unformatted',access='stream')
   open(2,file=prbfile2,form='unformatted',access='stream')
   read (1) ng
   write(2) ng
   allocate(dims(3,ng))
   read (1) dims
   write(2) dims
   write(*,*) dims
   do n = 1, ng
      write(*,*) 'Block ', n
      si = product(dims(:,n))*5
      allocate(var1(si))
      allocate(var2(si))
      
      ! Read double precision header and variables
      read (1) dat1
      read (1) var1

      ! Convert to single precision and write
      dat2 = dat1
      var2 = var1
      write(2) dat2
      write(2) var2
      deallocate(var1,var2)
   end do

end program
