%% Spatial Analysis Incipient Stall Stages
clear;clc
cd('/home/arvind/Research_Aurora/DynStallHighResolved/codes/chordMEMDresults/')
% Define params and extract chronogram at given height
xl = 315; yl = 315; zspan = 51;
dim = [xl,yl];
Fs_chord =  315; dt_chord = 1/Fs_chord; 
height = 20;

%%  Load data and start analysis
load('./chordMEMDresults/chordStage0.mat');
load('./chordMEMDresults/chordStage1.mat');
load('./chordMEMDresults/chordStage2.mat');
load('./chordMEMDresults/IMFspanStage0.mat');
load('./chordMEMDresults/IMFspanStage1.mat');
load('./chordMEMDresults/IMFspanStage2.mat');

nIMFspanStage0_U = size(IMFspanStage0_U,2);

%plot time signature of IMFs to decide which IMF to study in depth
figure;
IMFid = 1;
for i=1:31
   plot(squeeze(IMFspanAllstages_U(i,IMFid,:)));
   hold on
end


% Spectra of signals
figure;
for i=1:31
      sig = squeeze(IMFspanStage1_U(20,1,:)); 
      NFFT = floor(length(sig)/2);
      [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_chord);
      plot(fsig(2:end),specsig(2:end));
      hold on
end

chordChannelsU = squeeze(dataU(:,height,:));

% V velocity
%plot signals

j=1;
for i=50:2:110
   chordStage0V(:,j) = chordChannelsU(:,i);
   j=j+1;
end


j=1;
for i=110:2:170
   chordStage1V(:,j) = chordChannelsU(:,i);
   j=j+1;
end


j=1;
for i=170:2:230
   chordStage2V(:,j) = chordChannelsU(:,i);
   j=j+1;
end

j=1;
for i=290:2:350
   chordStage3(:,j) = chordChannelsU(:,i);
   j=j+1;
end

%% Matching pursuit TF analysis

% code used to write files for analysis have been commented out
%after use.

% %write channels all along the chord to bin file for EMPI/MP5
% for i=1:2:31
%    str = ['t' num2str(i+62) '.bin'];
%    signal = chordStage2(:,i);
%    file = fopen(str,'w');
%    fwrite(file, signal,'float32');
%    fclose(file); 
% end

% NOTE: Use the standalone Python program ~runMP.py~ which calls 
%another python function (which writes the config file
% for MP5) and runs it. This part is not in this code, and
% must be done externally. Run the program as SUDO.
% 
% IMFid = 1;
% % Write files to analyze IMFs via MP
% for i=10:10:31
%    str = ['t_S2IMF1_' num2str(i+60) '.bin'];
%    signal = squeeze(IMFspanStage2_U(i,IMFid,:));
%    file = fopen(str,'w');
%    fwrite(file, signal,'float32');
%    fclose(file); 
% end

% MEMD of chordwise signals

tic
disp('Computing Multivariate EMD')
n_proj = size(chordStage0V,2)*10;
IMFspanStage0_V = memd_mod(chordStage0V,n_proj);
toc
disp('In minutes:')
disp(toc/60);

tic
disp('Computing Multivariate EMD')
n_proj = size(chordStage1V,2)*10;
IMFspanStage1_V = memd_mod(chordStage1V,n_proj);
toc
disp('In minutes:')
disp(toc/60);

tic
disp('Computing Multivariate EMD')
n_proj = size(chordStage2V,2)*10;
IMFspanStage2_V = memd_mod(chordStage2V,n_proj);
toc
disp('In minutes:')
disp(toc/60);

tic
disp('Computing Multivariate EMD')
n_proj = size(chordStage3,2)*10;
IMFspanStage3_U = memd_mod(chordStage3,n_proj);
toc
disp('In minutes:')
disp(toc/60);
nIMFspanStage3_U = size(IMFspanStage3_U,2);

% Line IMF plot all channels - U
figure;
for j=1:nIMFspanStage3_U
    subplot(nIMFspanStage3_U,1,j)
    plot(1:315,squeeze(IMFspanStage3_U(:,j,:)))
    hold on
    xlabel(num2str(j));
end

% IMF spectra all channels

figure;
for j=1:nIMFspanStage3_U
    
    subplot(nIMFspanStage3_U,1,j)
    for i=1:31
          sig = squeeze(IMFspanStage3_U(i,j,:)); 
          NFFT = floor(length(sig)/2);
          [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_chord);
          plot(fsig,specsig);
          hold on
    end
    xlabel(num2str(j));
end


% compare similarity of IMFs across time
IMFCorrsV_Allstages = zeros(31,1);
IMFid = 1;
for i=1:31
    temp = corrcoef(squeeze(IMFspanAllstages_V(1,IMFid,:)),squeeze(IMFspanAllstages_V(i,IMFid,:)));
    IMFCorrsV_Allstages(i) = temp(1,2);
end

figure;
plot(1:31,IMFCorrsV_Allstages(1:31))
axis([0 +inf 0.2 1])
grid on

% compute MEMD of Stage 0,1 and 2 combined
% sampled at every '3' datapoints

%generate concatenated dataset
chordallstagesV = zeros(315,31);
temp = [chordStage0V,chordStage1V,chordStage2V];
j=1;
for i=1:3:93
    chordallstagesV(:,j) = temp(:,i);
    j=j+1;
end

%memd
tic
disp('Computing Multivariate EMD')
n_proj = size(chordallstagesV,2)*10;
IMFspanAllstages_V = memd_mod(chordallstagesV,n_proj);
toc
disp('In minutes:')
disp(toc/60);
nIMFallstages_V = size(IMFspanAllstages_V,2);

% Line IMF plot all channels - U
figure;
for j=1:nIMFallstages_U
    subplot(nIMFallstages_U,1,j)
    plot(1:315,squeeze(IMFspanAllstages_U(:,j,:)))
    hold on
    xlabel(num2str(j));
end

% IMF spectra all channels

figure;
for j=1:nIMFallstages_V
    
    subplot(nIMFallstages_V,1,j)
    for i=1:31
          sig = squeeze(IMFspanAllstages_V(i,j,:)); 
          NFFT = floor(length(sig)/2);
          [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_chord);
          plot(fsig,specsig);
          hold on
    end
    xlabel(num2str(j));
end

%% Joint PDFs

chordChannelsU = squeeze(dataU(:,height,:));
chordChannelsU = squeeze(dataV(:,height,:));
chordChannelsP = squeeze(dataP(:,height,1:600));

close all
% sig1 = chordStage1(:,30);
% sig2 = chordStage1V(:,30);
bins1=10; bins2=10;
nfig=11;
WJ_pdf_plot_deploy(sig1,sig2,bins1,bins2,nfig,1)
corrcoef(sig1,sig2)

%% test PDF
% this is the actual code to generate a pdf
IMFid=1;
sig1 = squeeze(IMFspanStage2_U(20,IMFid,:));
sig2 = squeeze(IMFspanStage2_V(20,IMFid,:));
data = [sig1,sig2]';
% hist3 will bin the data
xi = linspace(min(data(1,:)), max(data(1,:)), 10);
yi = linspace(min(data(2,:)), max(data(2,:)), 10);
hst = hist3(data',{xi yi});

% normalize the histogram data
dx = xi(2)-xi(1);
dy = yi(2)-yi(1);
area = dx*dy;
pdfData = hst/sum(sum(hst))/area;

% plot pdf
figure; 
contourf(xi,yi,pdfData);
colorbar
corrcoef(sig1,sig2)

%% surface pressure analysis

surfaceP = squeeze(dataP(:,1,1:600));
chordP = squeeze(dataP(:,height,1:600));
Stage0P = surfaceP(:,50:110);
Stage1P = surfaceP(:,110:170);
Stage2P = surfaceP(:,170:230);

Stage0Ph = chordP(:,50:2:110);
Stage1Ph = chordP(:,110:2:170);
Stage2Ph = chordP(:,170:2:230);

figure;
% diff(pressure)
for i=1:31
   diffP = diff(Stage2P(:,i));
   plot(1:314,diffP);
   hold on
end

% diff(velocity)
figure;
for i=1:31
   diffV = diff(chordStage2(:,i));
   plot(1:314,diffV);
   hold on
end

figure;
for i=122:180
   plot(chordP(:,i));
   hold on
end

%write channels all along the chord to bin file for EMPI/MP5
for i=1:10:31
   str = ['tPh' num2str(i+62) '.bin'];
   signal = Stage2Ph(:,i);
   file = fopen(str,'w');
   fwrite(file, signal,'float32');
   fclose(file); 
end




      
      
      
      
