%% Velocity Gradients 3D flowfield - Multiple Time Snapshots
% Vortex-Wing interaction 

clear
clc

%% Choose parameters
component = 1; % u-1,v-2,w-3,U-4
height = 4; % 1-112 units from surface
direction = 1; % x-1,y-2
nsnaps = 3;

%% Create Field of operation
% Read Grid
fid = fopen('suction_side.x','r','ieee-le');
ng = fread(fid,1,'int');
dim = fread(fid,[3,ng],'int')
x = reshape(fread(fid,prod(dim),'double'),dim');
y = reshape(fread(fid,prod(dim),'double'),dim');
z = reshape(fread(fid,prod(dim),'double'),dim');
fclose(fid);

fieldU = NaN(dim(1),dim(2),nsnaps);
fieldV = NaN(dim(1),dim(2),nsnaps);


for i=1:nsnaps
    val = 200000 + (i-1)*50;
    fname = ['probe-' num2str(val) '.q'];
    fid = fopen(fname,'r','ieee-le');
    ng = fread(fid,1,'int');
    dim = fread(fid,[3,ng],'int')
    dat = fread(fid,4,'double');
    rho = reshape(fread(fid,prod(dim),'double'),dim');
    rhou = reshape(fread(fid,prod(dim),'double'),dim');
    rhov = reshape(fread(fid,prod(dim),'double'),dim');
    rhow = reshape(fread(fid,prod(dim),'double'),dim');
    rhoe = reshape(fread(fid,prod(dim),'double'),dim');
    fclose(fid);
    fieldU(:,:,i) = rhou(:,:,height);
    fieldV(:,:,i) = rhov(:,:,height);
end

y = y(1,:,height);
x = x(:,1,height);

% Pick initial point
indx = -0.4523;
indy = 2.107;

ind=find(y<=indy);
Zi=ind(end);

ind=find(x<=indx);
Xi=ind(end);


%% Compute Gradients - U
% X-dir
clear dudx
dudx = NaN(dim(1),dim(2),nsnaps);

for k=1:nsnaps
    for i=1:(dim(1) - 1)
        for j = 1:dim(2)
            dudx(i,j,k) = (fieldU(i+1,j,k) - fieldU(i,j,k))/(x(i+1) - x(i));
        end
    end
end

% Y-dir
clear dudy
dudy = NaN(dim(1),dim(2),nsnaps);

for k=1:nsnaps
    for i=1:dim(1)
        for j = 1:(dim(2)-1)
            dudy(i,j,k) = (fieldU(i,j+1,k) - fieldU(i,j,k))/(y(j+1) - y(j));
        end
    end
end


%% Compute Gradients - V
% X-dir
clear dvdx
dvdx = NaN(dim(1),dim(2),nsnaps);

for k=1:nsnaps
    for i=1:(dim(1) - 1)
        for j = 1:dim(2)
            dvdx(i,j,k) = (fieldV(i+1,j,k) - fieldV(i,j,k))/(x(i+1) - x(i));
        end
    end
end

% Y-dir
clear dvdy
dvdy = NaN(dim(1),dim(2),nsnaps);

for k=1:nsnaps
    for i=1:dim(1)
        for j = 1:(dim(2)-1)
            dvdy(i,j,k) = (fieldV(i,j+1,k) - fieldV(i,j,k))/(y(j+1) - y(j));
        end
    end
end

%% Plot

% Play Movie of Results
figure(1)
set(1,'Position',[100 20 1800 900])
frameRate = 1; % At reqd. frame rate
for i=1:frameRate:nsnaps
    pcolor(y,x,dudx(:,:,i)), shading interp
    %contour(y,x,dudx)
    colorbar
    axis equal tight
    colormap('jet')
    caxis([1.765 1.785])
    %pause (1/12)
end
disp('DONE!!!')



% X-dir - U
figure(1)
set(1,'Position',[100 20 1800 900])
pcolor(y,x,dudx(:,:,2)), shading interp
%contour(y,x,dudx)
colorbar
axis equal tight
colormap('jet')

% Y-dir - U
figure(2)
set(2,'Position',[100 20 1800 900])
pcolor(y,x,dudy(:,:,2)), shading interp
%contour(y,x,dudx)
colorbar
axis equal tight
colormap('jet')

% X-dir - V
figure(3)
set(3,'Position',[100 20 1800 900])
pcolor(y,x,dvdx(:,:,2)), shading interp
%contour(y,x,dudx)
colorbar
axis equal tight
colormap('jet')

% Y-dir - V
figure(4)
set(4,'Position',[100 20 1800 900])
pcolor(y,x,dvdy(:,:,2)), shading interp
%contour(y,x,dudx)
colorbar
axis equal tight
colormap('jet')

