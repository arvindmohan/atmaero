% Classical DMD with Vandermonde Reconstruction
% Arvind Mohan - PhD Student, Ohio State
close all 
clear all
clc
disp('Dynamic Mode Decomposition and Reconstruction of Flow Data')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
disp('Reconstruction by Rowley - Vandermonde Matrix based method')
disp('High Fidelity Computational Multiphysics Lab - The Ohio State University')
disp('Arvind Mohan - PhD Candidate, Ohio State')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

%% USER INPUT
cd('../Raw_data/');
pwd;
numofsnaps = 20;
file_delta = 125;
delta = 125;
timestep = 10^-4; % From CFD solver - non-DIMENSIONAL TIME FOR EVERY 'DT'
init_file  = 3039000;%  <== CHANGE if needed.
final_file = init_file + ((numofsnaps)*file_delta) - file_delta;
coherence = 'no';
reconmodes = numofsnaps-1;
meanfname = 'Umean';

% Header File info for Plot3d output
xhead = 160;
yhead = 148;
zhead = 1;
nvar = 1;

%% DMD algorithm 
disp('Reading In Snapshot Data...');
clear temp
m=1;

for i=init_file:file_delta:final_file  
    u(:,m)=importdata(['u' num2str(i) '.mat']);
    m=m+1;
    %disp(i)
end

disp('Operation Successful.');


% Setting up matrices
[gridsize,Nsnap] = size(u); 

% Calculate mean of data
Umean = mean(u,2);

% Step 1: Building Matrix V1^n-1

V1 = zeros(gridsize,Nsnap-1);
for i=1:Nsnap-1
    V1(:,i) = u(:,i);
end

% Step 2: Building V2 Matrix

V2 = zeros(gridsize,Nsnap-1);

for i=1:Nsnap-1
    V2(:,i) = u(:,i+1);
end

% Computation Starts

% Step 3: Singular Value Decomposition of the V1 matrix (Economy)
disp('SVD in process...')
[U,E,W] = svd(V1,0);
disp('Operation Successful.');

% Step 4: Computing S matrix

S = U'*V2*W/E;

% Step 5: Eigendecomposition of Matrix S
disp('Computing Eigendecomposition of S Matrix...')
[eigenvecs,eigenvals] = eig(S);
disp('Operation Successful.');
ritz = diag(eigenvals); % saving the Ritz values

%% Post Processing of DMD data

% Computing Modes

disp('Computing DMD Modes...')
modes = U*eigenvecs;
[~,nummodes] = size(modes);
disp('Operation Successful.'); 

% Sorting Ritz values in descending order
[~,index] = sort(real(ritz),'descend');
ritz = ritz(index);
modes = modes(:,index);

% Computing Floquet values
dt = timestep*delta; % Actual time interval

floqvals = log(ritz)/dt;


%% Reconstruction

%Computing Scaling Coeffs for modes using 1st snapshot
disp('Computing Scaling Coeffs');
d = modes\u(:,1);

% Computing Scaled modes

modes_scaled = zeros(gridsize,nummodes);

for i=1:nummodes
   modes_scaled(:,i) =  modes(:,i)*d(i);
end

% Compute Scaled Mode Norms

mode_norms = zeros(nummodes,1);
for i=1:nummodes
   mode_norms(i) = norm(modes_scaled(:,i));
end

mode_ampl_unordered = mode_norms/(max(mode_norms));


% Order Modes and other vars by amplitude

[~,idx] = sort(mode_ampl_unordered,'descend');
mode_ampl = mode_ampl_unordered(idx);
modes_scaled = modes_scaled(:,idx);
modes = modes(:,idx);
ritzOrder = ritz(idx);
floqvalsOrder = floqvals(idx);
dOrder = d(idx);

% Build Vandermonde Matrix using Ritz values

T = fliplr(vander(ritz));

T = T(idx,:);

% Computing Reconstructed Velocity
disp('Computing Reconstructed Data . . .');

dmdrecon = modes_scaled(:,1:reconmodes)*T(1:reconmodes,:);

cd('../sample_results/');
pwd

% Writing Mean of Snapshots to ASCII PLOT3D files
header = [xhead yhead zhead nvar];
disp('Writing Mean flow to Plot3D file');
fid = fopen([meanfname '.dat'], 'w');
fprintf(fid, '%i \t',header);
fprintf(fid,'\n');
fprintf(fid, '%f \t',Umean);
fclose(fid);
disp('====================================================')

% Writing Scaled DMD modes to ASCII PLOT3D files
% disp('Writing Scaled DMD modes to Plot3D files');
% header = [xhead yhead zhead nvar];
% for i=1:nummodes
% fid = fopen(['DMDmodeScaled' num2str(i) '.dat'], 'w');
% fprintf(fid, '%i \t',header);
% fprintf(fid,'\n');
% fprintf(fid, '%f \t',modes_scaled(:,i));
% fclose(fid);
% end
% disp('====================================================')

% Writing DMD modes to ASCII PLOT3D files
disp('Writing DMD modes to Plot3D files');
header = [xhead yhead zhead nvar];
for i=1:nummodes
fid = fopen(['DMDmode' num2str(i) '.dat'], 'w');
fprintf(fid, '%i \t',header);
fprintf(fid,'\n');
fprintf(fid, '%f \t',modes(:,i));
fclose(fid);
end
disp('====================================================')

% Writing Reconstructed data to ASCII PLOT3D files
fname = strcat('DMD',num2str(reconmodes),'reconstructed');
disp('Writing Reconstructed Data to Plot3D files');
header = [xhead yhead zhead nvar];
for i=1:nummodes
fid = fopen([fname num2str(i) '.dat'], 'w');
fprintf(fid, '%i \t',header);
fprintf(fid,'\n');
fprintf(fid, '%f \t',dmdrecon(:,i));
fclose(fid);
end
disp('====================================================')

% Overall Reconstruction Error

for i=1:nummodes
 error_recon(:,i) = dmdrecon(:,i) - u(:,i);
 error_sum(i) = sum(error_recon(:,i).^2);
 u_sum(i) = sum(u(:,i).^2);
 error_total(i) = sqrt(error_sum(i))/sqrt(u_sum(i));
 error(i) = abs(error_total(i))*100;
end

%% Plotting
% Plot Error Distribution

modenumbers = 1:1:nummodes;

errordistribution = figure(1);
pos = get(gcf,'Position');
set(gcf, 'Position', [pos(1) pos(2) 10*100, 5*100]); %<- Set size
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
s = 25; % size of circle
scatter(modenumbers,error,s,'MarkerEdgeColor','b','MarkerFaceColor','c','LineWidth',1.5)
xlabel('Snapshot No.','Fontsize',14,'FontWeight','bold')
ylabel('Percent Error','Fontsize',14,'FontWeight','bold')
grid on
plotvals = nummodes;
disp('The minimum error is:')
disp(min(error))
disp('The maximum error is:')
disp(max(error))

% Export to EPS
disp('Plotting Error Distribution...')
epsname = ['errordistribution' '.eps'];
set(gcf, 'PaperPositionMode', 'auto');
print(errordistribution,'-depsc2',epsname)
close;

% Plot Stable and Unstable Floquet exponents

numvals = nummodes;
initvals = 1;
real_floq = real(floqvalsOrder(initvals:numvals)); 
imag_floq = imag(floqvalsOrder(initvals:numvals));
floquetplot = figure(2);
pos = get(gcf,'Position');
set(gcf, 'Position', [pos(1) pos(2) 10*100, 5*100]); %<- Set size
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
s = 25; % size of circle
scatter(imag_floq(1:plotvals),real_floq(1:plotvals),s,'MarkerEdgeColor','b','MarkerFaceColor','c','LineWidth',1.5)
xlabel('Imaginary Part','Fontsize',14,'FontWeight','bold')
ylabel('Real Part','Fontsize',14,'FontWeight','bold')
grid on


% Export to EPS
disp('Plotting Eigenvalues...')
epsname = ['floquetstability' '.eps'];
set(gcf, 'PaperPositionMode', 'auto');
print(floquetplot,'-depsc2',epsname)
close;

% Plotting Mode_norms for every mode
rmsval = figure(3);
pos = get(gcf, 'Position');
slice = numofsnaps-1;
set(gcf, 'Position', [pos(1) pos(2) 10*100, 5*100]); %<- Set size
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
s = 25;
scatter(modenumbers(1:slice),mode_ampl(1:slice),s,'MarkerEdgeColor','b','MarkerFaceColor','c','LineWidth',1.5)
xlabel('Mode(s)','Fontsize',14,'FontWeight','bold')
ylabel('Mode amplitude','Fontsize',14,'FontWeight','bold')
grid on

% Export to EPS
disp('Plotting Mode Amplitudes...')
epsname = ['Modenorms' '.eps'];
set(gcf, 'PaperPositionMode', 'auto');
print(rmsval,'-depsc2',epsname)
close;


%% Save and Exit.
disp('====================================================')
disp('Saving Complete Analysis MAT file to: ');
disp(pwd); 

save dmdfull_u -v7.3   % Save entire workspace to access later

disp('DONE!!!');

disp('******************************')
disp('Run Complete.')
cd('../Codes/');
disp('******************************')
