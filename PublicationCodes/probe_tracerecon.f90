PROGRAM Probe_trace ! Extract 2D grid probe time-traces
      implicit none
      real,allocatable,dimension(:,:,:) :: x,y,z
      real,allocatable,dimension(:,:,:,:)::flow
      real,allocatable,dimension(:,:):: probedata
      real fsmach,alpha,rey,time
      integer il,jl,kl,i,j,k,iprobe,jprobe,probevar,nfuncs,v,f,kl2d,filenums,delta
      integer ildmy,jldmy,kldmy
      character (len=80) :: fname
      
      filenums = 719
      delta = 1
      probevar = 1 ! rho-1,rhou-2,rhov-3,rhow,-4,p-5
  
      !kl = 1
      nfuncs = 1
      open(10,file='gridslice_span1',form='unformatted')
      read(10) il,jl,kl
      close(10)
      
  
      print*,'Enter the value of jl location'
      read*,jprobe
      print*,'Enter the value of il location'
      read*,iprobe
  
      allocate(flow(il,jl,kl,nfuncs))
      allocate(probedata(filenums,1))
      
      do f=1,filenums
        open(20,file='flowfilerecon',form='formatted')
        read(20,*) fname
        print*,fname
        open(30,file=fname,form='formatted')
        read(30,*) ildmy,jldmy,kldmy
        !print*,ildmy,jldmy,kldmy
        !read(30) fsmach,alpha,rey,time
        !print*,fsmach,alpha,rey,time
        read(30,*) ((((flow(i,j,k,v),i=1,ildmy),j=1,jldmy),k=1,kldmy),v=1,nfuncs)
        probedata(f,1) = flow(iprobe,jprobe,kldmy,probevar)
        print*, probedata(f,1)
        close(30)
      enddo
      !print*,probedata
      ! .prb refers to a data file containing time trace - notation is
      ! Probe number followed by variable
      open(40,file='1urecon.prb',form='formatted')
      write(40,*) probedata
      print*,'Written Probe signal to file'
      close(40) 
      
      close(20)
  
  
END PROGRAM
