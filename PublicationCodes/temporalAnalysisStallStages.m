%% Temporal Analysis Incipient Stall Stages
% refer ____.m for stage identification

% Define params and Extract chronogram at given height
xl = 315; yl = 315; zspan = 51;
dim = [xl,yl];
dt_temporal = 0.005; Fs_temporal = 1/dt_temporal;
height = 20;

%%  Load data and start analysis
load dataU.mat

temporalData = squeeze(dataU(:,height,1:270));
t = (1:270)*dt_temporal;
%plot chronogram
h=figure;
contourf(1:xl,t,temporalData');
colormap jet
colorbar
savefig(h,'chronoh34.fig')
% extract channels from x=200 to 300, dx=10
j=1;
for i=200:10:315
    temporalChannels(j,:) = temporalData(i,:);
    j=j+1;
end
clear j;
nChannels = size(temporalChannels,1);
% apply MEMD to temporal Channels
clear IMFtemporal*
tic
disp('Computing Multivariate EMD')
n_proj = nChannels*10;
IMFtemporal_U = memd_mod(temporalChannels,n_proj);
toc
disp('In minutes:')
disp(toc/60);
nIMFtemporal_U = size(IMFtemporal_U,2);

% Line IMF plot all channels - U
f1 = figure;
for j=1:nIMFtemporal_U
    subplot(nIMFtemporal_U,1,j)
    plot(t,squeeze(IMFtemporal_U(:,j,:)))
    hold on
    xlabel(num2str(j));
end
title('MEMD IMFs')
savefig(f1,'memdIMFs.fig');

% IMF spectra all channels
f2 = figure;
for j=1:nIMFtemporal_U
    
    subplot(nIMFtemporal_U,1,j)
    for i=1:nChannels
          sig = squeeze(IMFtemporal_U(i,j,:)); 
          NFFT = floor(length(sig)/2);
          [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_temporal);
          plot(fsig,specsig);
          hold on
    end
    xlabel(num2str(j));
end
title('MEMD IMF spectra')
savefig(f2,'memdIMFspectra.fig');

%% Matching pursuit TF analysis

%write channels all along the chord to bin file for EMPI/MP5

for i=10:10:190
   str = ['x' num2str(i) '.bin'];
   signal = temporalData(i,:);
   file = fopen(str,'w');
   fwrite(file, signal,'float32');
   fclose(file); 
end

% NOTE: Call the python function in terminal to write the config file
% for MP5 and run it. This part is manual and not in this code!
%system('ipython','-echo')
%! sed -i 's/demo.dat/x200.bin/g' demo2.set
%!empi submit_empi

% DMD analysis of every temporal stage - 0,1,2

%extract stage snapshots
Stage0snaps = reshape(dataU(:,:,50:110),[xl*yl,61]);
Stage1snaps = reshape(dataU(:,:,110:170),[xl*yl,61]);
Stage2snaps = reshape(dataU(:,:,170:230),[xl*yl,61]);

%perform DMD
dmd_function(Stage0snaps,dt_temporal,'Stage0');

dmd_function(Stage1snaps,dt_temporal,'Stage1');

dmd_function(Stage2snaps,dt_temporal,'Stage2');

disp('Finished DMD');

% visualize DMD modes in grid
load('grid3D.mat');
xmax = size(x,1);
ymax = size(x,2);
zmax = size(x,3);
dim_max = [xmax,ymax,zmax];
ix= 1;jx = 315;
x2d = x(ix:xl,1:jx,zspan);
y2d = y(ix:xl,1:jx,zspan);
disp('Loaded Grid')

%modes = reshape(modes,[xl,yl,60]);
flow  = reshape(Stage2snaps,[xl,yl,61]);

id = 61;
figure;
%pcolor(x2d,y2d,real(modes(:,:,id)))
pcolor(x2d,y2d,real(flow(:,:,id)))
colormap jet
shading interp
colorbar
set(gcf, 'Position',[100, 100, 1389, 700])

% play movie
figure;
j=1;
for i=1:10
    pcolor(x2d,y2d,real(modes(:,:,i)))
    shading interp
    colormap jet    
    drawnow
    K(j) = getframe(gcf);   
    %colorbar
    j = j+1;
    pause(1/4);
    set(gcf, 'Position',[100, 100, 1389, 700])
    disp(i)
end
disp('DONE')


% plot freqs vs ampl for all stages
load freqAmpStages.mat
figure;
subplot(3,1,1)
semilogy(s0,amp0,'ko')
axis([0 +inf 0 +inf]);
subplot(3,1,2)
semilogy(s1,amp1,'ko')
axis([0 +inf 0 +inf]);
subplot(3,1,3)
semilogy(s2,amp2,'ko')
axis([0 +inf 0 +inf]);



%extract stage snapshots
Stage0snapsV = reshape(dataV(:,:,50:110),[xl*yl,61]);
Stage1snapsV = reshape(dataV(:,:,110:170),[xl*yl,61]);
Stage2snapsV = reshape(dataV(:,:,170:230),[xl*yl,61]);
Stage12snapsU = reshape(dataU(:,:,110:230),[xl*yl,121]);

%perform DMD
dmd_function(Stage0snapsV,dt_temporal,'Stage0V');

dmd_function(Stage1snapsV,dt_temporal,'Stage1V');

dmd_function(Stage12snapsU,dt_temporal,'Stage12U');

disp('Finished DMD');

% visualize DMD modes in grid
load('grid3D.mat');
xmax = size(x,1);
ymax = size(x,2);
zmax = size(x,3);
dim_max = [xmax,ymax,zmax];
ix= 1;jx = 315;
x2d = x(ix:xl,1:jx,zspan);
y2d = y(ix:xl,1:jx,zspan);
disp('Loaded Grid')

nummodes = size(modes,2);
% extract unique modes (not conjugates)
atemp = freqs >=0;
modes = modes(:,atemp);
modes = reshape(modes,[xl,yl,size(modes,2)]);

%Plotting first N UNIQUE modes --> MODE IDs
% may change due to neglecting -ve freqs!
figure;
j=1;
%clev=linspace(-5,5,10);
%clev=clev*0.005;
for i=1:1:9
    subplot(3,3,j)
    pcolor(x2d,y2d,real(modes(:,:,i)));
    colormap jet
    shading interp
    colorbar
    j=j+1;
    title(['St=' num2str(s2(i))])
end
xlabel('DMD modes Stage 2')

modes = reshape(modes,[xl,yl,size(modes,2)]);

id = 17;
figure;
contour(x2d,y2d,real(modes(:,:,id)))
%pcolor(x2d,y2d,real(modes(:,:,id)))
colormap jet
shading interp
colorbar
%set(gcf, 'Position',[100, 100, 1389, 700])

atemp = find(s0 >=0);
s0 = s0(atemp);
amp0 = amp0(atemp);
atemp = find(s1 >=0);
s1 = s1(atemp);
amp1 = amp1(atemp);
atemp = find(s2 >=0);
s2 = s2(atemp);
amp2 = amp2(atemp);

% plot freqs vs ampl for all stages
load freqAmpStages.mat
figure;
subplot(3,1,1)
semilogy(s0,amp0,'ko')
axis([0 +inf 0 +inf]);
subplot(3,1,2)
semilogy(s1,amp1,'ko')
axis([0 +inf 0 +inf]);
subplot(3,1,3)
semilogy(s2,amp2,'ko')
axis([0 +inf 0 +inf]);

figure;
for i=1:31
   plot(1:315,chordStage2(:,i))
   hold on
end

% study stabilities
figure;
for i=1:3
   fdata = importdata(['Stage' num2str(i-1) 'DMD.mat']);
   plot(real(fdata.floqvalsOrder))
   hold on
   grid on
end

% harmonics of St=6.19 in flapping

harm_id =[16,17,53];
figure;
for i=1:3
    subplot(3,1,i)
    id = harm_id(i);
    contour(x2d,y2d,real(modes(:,:,id)))
    colormap jet
    shading interp
    colorbar
    title(['Mode ' num2str(id)])
end

% extract data from DMD mode and chordwise FFT
dmdrec = reshape(dmdrec,[315,315,60]);

figure;
for i=1:nummodes
    chordDat = real(modes(:,height,i));
    %chordDat = real(dmdrec(:,height,i));
    sig = chordDat; 
    NFFT = floor(length(sig)/2);
    [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_chord);
    plot(fsig,specsig);
    hold on
end


%compute time evolution and phase of modes
a_t =zeros(nummodes,nummodes);
phase_modes = zeros(nummodes,nummodes);
t = (1:nummodes)*dt_temporal;
for i=1:nummodes
   a_t(:,i) = exp(floqvalsOrder(i)*t); 
   phase_modes(:,i) = (atan(imag(a_t(:,i))./real(a_t(:,i))));
   temp = diff(phase_modes(:,i));
   ang_velo(i) = temp(3);
end

%plot time evolution
figure;
for i=1:1:25
    subplot(5,5,i)
    plot(t,real(a_t(:,i)))
    %title(['St=' num2str(s1(i))])
    title(['Mode' num2str(i)])
end
xlabel('DMD modes Stage 2 time traces (real)')

%plot phase evolution
figure;
j=1;
for i=1:1:25
    subplot(5,5,j)
    temp = unwrap(phase_modes(:,i));
    plot(t,temp)
    j=j+1;
    %title(['St=' num2str(s1(i))])
    title(['Mode' num2str(i)])
end
xlabel('DMD modes Stage 2 phase evolution')

% Lissajous figures

id1 =  5; id2 = 11;
sig1 = sin(real(a_t(:,id1))); sig2 = sin(real(a_t(:,id2)));
figure;
plot(sig1,sig2);

figure;
j=1;
id1 = 20; %source mode
for i=31:2:60
   sig1 = sin(real(a_t(:,id1))); sig2 = sin(real(a_t(:,i))); 
   subplot(5,3,j)
   %temp = sig1./sig2;
   %plot(temp);
   plot(sig1,sig2);
   %plot(phase_modes(:,id1))
   %hold on
   %plot(phase_modes(:,i))
   title(['Mode' num2str(i)])
   j=j+1;
end

%angular velocity ratios
for i=1:nummodes
   %sig1 = sin(real(a_t(:,id1))); sig2 = sin(real(a_t(:,i))); 
   ang_vel_ratio(i) = ang_velo(i)/ang_velo(id1); 
   %temp = corrcoef(sig1,sig2); %correlation between modes
   %mcor(i) = temp(1,2);
end
%figure;
%plot(mcor)

figure;
sig1 = sin(real(a_t(:,id1))); sig2 = sin(real(a_t(:,9))); 
plot(sig1,sig2);

%=========== RECONSTRUCTION=============%

% Plot modes
nummodes = size(modes,2);

% Reconstruction
gridSize = 315*315;
reconmodes = 20;
modes_scaled = zeros(gridSize,nummodes);
parfor i=1:nummodes
   modes_scaled(:,i) =  modes(:,i)*dOrder(i);
end

T = fliplr(vander(ritzOrder));

dmdrec = modes_scaled(:,1:reconmodes)*T(1:reconmodes,:);


% Error calculation
disp('computing error')
parfor i=1:nummodes
 %disp(['Reading file rhou' num2str(filenum) '.mat']);
 temp = dataU(:,:,i+110);
 u=  reshape(temp,[315*315,1]);
 error_recon(:,i) = dmdrec(:,i) - u;
 error_sum(i) = sum(error_recon(:,i).^2);
 u_sum(i) = sum(u.^2);
 error_total(i) = sqrt(error_sum(i))/sqrt(u_sum(i));
 error(i) = abs(error_total(i))*100;
end
disp('done!!')
%total error

total_error = abs(sum(error_total))


figure;
plot(error)

















%% Extent of influence by TE flapping

%generate data
j=1;
for i=250:2:310
   temporalChannelsU(:,j) =  squeeze(dataU(i,height,:));
   j=j+1;
end
%temporalChannelsU = temporalChannelsU';
nChannels = size(temporalChannelsU,2);
%analyze

clear IMFtemporal*
tic
disp('Computing Multivariate EMD')
n_proj = nChannels*10;
IMFtemporal_U = memd_mod(temporalChannelsU(1:230,:),n_proj);
toc
disp('In minutes:')
disp(toc/60);
nIMFtemporal_U = size(IMFtemporal_U,2);

t = (1:230)*dt_temporal;

% Line IMF plot all channels - U
f1 = figure;
for j=1:nIMFtemporal_U
    subplot(nIMFtemporal_U,1,j)
    plot(t,squeeze(IMFtemporal_U(:,j,:)))
    hold on
    xlabel(num2str(j));
end


% IMF spectra all channels
f2 = figure;
for j=1:nIMFtemporal_U
    
    subplot(nIMFtemporal_U,1,j)
    for i=1:nChannels
          sig = squeeze(IMFtemporal_U(i,j,:)); 
          NFFT = floor(length(sig)/2);
          [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_temporal);
          plot(fsig,specsig);
          hold on
    end
    xlabel(num2str(j));
end


%plot time signature of IMFs to decide which IMF to study in depth
figure;
IMFid = 1;
for i=1:31
   plot(squeeze(IMFtemporal_U(i,IMFid,:)));
   hold on
end

% compare similarity of IMFs across time
IMFCorrsU_temporal = zeros(31,1);
IMFid = 2;
for i=31:-1:1
    temp = corrcoef(squeeze(IMFtemporal_U(31,IMFid,:)),squeeze(IMFtemporal_U(i,IMFid,:)));
    IMFCorrsU_temporal(i) = temp(1,2);
end

figure;
plot(1:31,IMFCorrsU_temporal(1:31))
%axis([0 +inf 0 1])
grid on

%% Spectra of LSB from Visbal's paper
% x/c = 0.05

close all
LSB = squeeze(dataU(60,height,1:300));
%LSB = surfaceP(100,1:300);
t = (1:300)*dt_temporal;
figure;
plot(t,LSB)

figure;
sig = LSB; 
NFFT = floor(length(sig)/2);
[specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_temporal);
plot(fsig,specsig);

%write channels all along the chord to bin file for EMPI/MP5
for i=60:2:100
   str = ['x' num2str(i) '.bin'];
   signal = squeeze(dataU(i,height,1:300));
   file = fopen(str,'w');
   fwrite(file, signal,'float32');
   fclose(file); 
end

%% POD Analysis

%perform POD
pod_function(Stage0snaps,dt_temporal,'Stage0');

pod_function(Stage1snaps,dt_temporal,'Stage1');

pod_function(Stage2snaps,dt_temporal,'Stage2');

disp('Finished POD');


% POD mode energy
figure;
semilogy(1:61,lambda);
title('POD mode energies Stage 2')

% visualize DMD modes in grid
load('grid3D.mat');
xmax = size(x,1);
ymax = size(x,2);
zmax = size(x,3);
dim_max = [xmax,ymax,zmax];
ix= 1;jx = 315;
x2d = x(ix:xl,1:jx,zspan);
y2d = y(ix:xl,1:jx,zspan);
disp('Loaded Grid')

modes = reshape(PODmodes,[xl,yl,61]);

%Plotting first 20 modes
figure;
j=1;
%clev=linspace(-5,5,10);
%clev=clev*0.005;
for i=1:1:20
    subplot(5,4,j)
    pcolor(x2d,y2d,real(modes(:,:,i)));
    colormap jet
    shading interp
    colorbar
    j=j+1;
    title(num2str(i))
end
xlabel('POD modes Stage 2')


id = 4;
figure;
pcolor(x2d,y2d,real(modes(:,:,id)))
colormap jet
shading interp
colorbar
set(gcf, 'Position',[100, 100, 1189, 500])


% analyzing POD mode evolution
figure;
j=1;
for i=1:1:15
    subplot(5,3,j)
    plot(1:61,aPOD(:,i));
    j=j+1;
    title(num2str(i))
end
xlabel('POD time coeffs Stage 2')

% POD spectra

figure;
j=1;
for i=1:1:15
    subplot(5,3,j)
    sig = aPOD(:,i); 
    NFFT = floor(length(sig)/2);
    [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_temporal);
    plot(fsig,specsig);
    j=j+1;
    title(num2str(i))
end
xlabel('POD Spectra Stage 2')

figure;
plot(Energy)


%write channels all along the chord to bin file for EMPI/MP5
for i=1:1:15
   str = ['PODs0_time' num2str(i) '.bin'];
   signal = aPOD(:,i);
   file = fopen(str,'w');
   fwrite(file, signal,'float32');
   fclose(file); 
end




