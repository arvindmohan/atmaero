%% DMD Vortex Wing 2D slice
% Load Data and do eig decomposition
clc;clear;close all

%% DMD Part 1
%% USER INPUT
cd('./Pwallsnaps/');
pwd;
numofsnaps = 4394;
delta = 50;
timestep = 10^-4; % From CFD solver - non-DIMENSIONAL TIME FOR EVERY 'DT'
init_file  = 200000;%  <== CHANGE if needed.
final_file = (init_file-delta) + ((numofsnaps)*delta);
reconmodes = numofsnaps-1;
meanfname = 'Pwallmean';

% Header File info for Plot3d output
xhead = 161;
yhead = 661;
zhead = 1;
nvar = 1;

%parpool();

%% DMD algorithm
dim = [xhead yhead zhead];
gridSize = prod(dim);
Nsnap = numofsnaps;

disp('Building Snapshot Mean...');

tic
% Compute Mean
temp = zeros(gridSize,1);
parfor i=1:Nsnap
   filenum = (init_file-delta) + i*delta;
   %disp(['Reading file rhou' num2str(filenum) '.mat']);
   temp = temp + importdata(['rhoewall' num2str(filenum) '.mat']);
end

Umean = temp/Nsnap;

clear temp
% Step 1: Building Matrix V1^n-1

V1 = zeros(gridSize,Nsnap-1);
parfor i=1:Nsnap-1
    filenum = (init_file-delta)+ i*delta;
    V1(:,i) = importdata(['rhoewall' num2str(filenum) '.mat']);
end

% Computation Starts

% Step 3: Singular Value Decomposition of the V1 matrix (Economy)
disp('SVD in process...')
[U,E,W] = svd(V1,0);
disp('Operation Successful.');


clear V1
% Step 2: Building V2 Matrix

V2 = zeros(gridSize,Nsnap-1);
parfor i=1:Nsnap-1
    filenum = (init_file-delta) + i*delta;
    V2(:,i) = importdata(['rhoewall' num2str(filenum+delta) '.mat']);
end

% Step 4: Computing S matrix

S = U'*V2*W/E;

clear V2 W E
% Step 5: Eigendecomposition of Matrix S
disp('Computing Eigendecomposition of S Matrix...')
[eigenvecs,eigenvals] = eig(S);
disp('Operation Successful.');
clear S

ritz = diag(eigenvals); % saving the Ritz values
clear eigenvals


%% Post Processing of DMD data

% Computing Modes

disp('Computing DMD Modes...')
modes = U*eigenvecs;
clear U eigenvecs
[~,nummodes] = size(modes);
disp('Operation Successful.'); 

% Sorting Ritz values in descending order
[~,index] = sort(real(ritz),'descend');
ritz = ritz(index);
modes = modes(:,index);

% Computing Floquet values
dt = timestep*delta; % Actual time interval

floqvals = log(ritz)/dt;

%% DMD Part 2
% Reconstruction and post-processing

%% Reconstruction

%Computing Scaling Coeffs for modes using 1st snapshot
disp('Computing Scaling Coeffs');
u1 = importdata('rhoewall200000.mat');
d = modes\u1;
clear u1

% Computing Scaled modes

modes_scaled = zeros(gridSize,nummodes);

parfor i=1:nummodes
   modes_scaled(:,i) =  modes(:,i)*d(i);
end

% Compute Scaled Mode Norms

mode_norms = zeros(nummodes,1);
parfor i=1:nummodes
   mode_norms(i) = norm(modes_scaled(:,i));
end

mode_ampl_unordered = mode_norms/(max(mode_norms));


% Order Modes and other vars by amplitude

[~,idx] = sort(mode_ampl_unordered,'descend');
mode_ampl = mode_ampl_unordered(idx);
clear mode_ampl_unordered
modes_scaled = modes_scaled(:,idx);
modes = modes(:,idx);
ritzOrder = ritz(idx);
floqvalsOrder = floqvals(idx);
dOrder = d(idx);

% Build Vandermonde Matrix using Ritz values

T = fliplr(vander(ritz));

T = T(idx,:);

% Computing Reconstructed Velocity
disp('Computing Reconstructed Data . . .');

dmdrecon = modes_scaled(:,1:reconmodes)*T(1:reconmodes,:);

time1 = toc;
time1 = time1/60
disp('saving results')
save('dmdPwallVW.mat','modes','floqvalsOrder','ritzOrder','dOrder','mode_ampl','dmdrecon','time1','-v7.3');




