% Ancilliary post processing on DMD/POD data
cd('/home/mohan.69/work/ModelReduction/ASME_Data/analysis/nocontrol/')
%% Random stuff
clc;close all;
% Plot Stable and Unstable Floquet exponents

numvals = nummodes;
initvals = 1;
hold on

floquetplot1 = figure(1);
set(gcf, 'Position', [pos(1) pos(2) 10*100, 5*100]); %<- Set size
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
s = 50; % size of circle
xlabel('Imaginary Part','Fontsize',14,'FontWeight','bold')
ylabel('Real Part','Fontsize',14,'FontWeight','bold')
title('Floquet Multipliers of DMD Eigenvalues - All Modes','Fontsize',14,'FontWeight','bold')
grid on
plotvals = 50;
scatter(imag_floq(1:plotvals),real_floq(1:plotvals),s,'MarkerEdgeColor','b','MarkerFaceColor','c','LineWidth',1.5)
savefig(floquetplot1,'floquetstability1')
saveas(floquetplot1,'floquetstability1','png')

floquetplot2 = figure(2);
modenumbers = 1:1:nummodes-rankloss;

scatter([1:plotvals],real_floq(1:plotvals),s,'MarkerEdgeColor','b','MarkerFaceColor','c','LineWidth',1.5)
xlabel('Mode number','Fontsize',14,'FontWeight','bold')
ylabel('Real Part of Modes','Fontsize',14,'FontWeight','bold')
title('Floquet Multipliers of DMD Eigenvalues','Fontsize',14,'FontWeight','bold')
grid on
savefig(floquetplot2,'floquetstability2')
saveas(floquetplot2,'floquetstability2','png')

floquetplot3 = figure(3);
modenumbers = 1:1:nummodes-rankloss;

scatter([1:plotvals],imag_floq(1:plotvals),s,'MarkerEdgeColor','b','MarkerFaceColor','c','LineWidth',1.5)
xlabel('Mode number','Fontsize',14,'FontWeight','bold')
ylabel('Imag Part of Modes','Fontsize',14,'FontWeight','bold')
title('Floquet Multipliers of DMD Eigenvalues','Fontsize',14,'FontWeight','bold')
grid on
savefig(floquetplot3,'floquetstability3')
saveas(floquetplot3,'floquetstability3','png')




%% FFT Analysis of Modes from DMD and POD
clc
close all
signal = aPOD(:,5);
%signal = real(CmOrder(:,8));
N = length(signal);
Fs = 1/dt;
T = 1/Fs;
t = (0:N-1)*T;
NFFT = 2^nextpow2(N);


K = fft(signal,NFFT)/N;

f = Fs/2*linspace(0,1,NFFT/2+1);
fftplot = figure(1);
plot(f,2*abs(K(1:NFFT/2+1)),'b','LineWidth',1)
title('Single-Sided Amplitude Spectrum of Mode evolution')
xlabel('Frequency (Hz)')
ylabel('Cm')
grid on
savefig(fftplot,'fftplotPOD')
saveas(fftplot,'fftplotPOD','png')








