%% Dynamic Mode Decomposition and Reconstruction - ALL operations
% Computing Amplitude,Modes, Reconstruction. --> NACA 0015 Airfoil
% Arvind T. Mohan
% Aerospace Engineering,HFCMPL - The Ohio State University
% TYPE 2 :- SVD (More Stable for noisy data)
% Refer Schmid J. Fluid. Mech (2010), vol.656, pp, 5-28

close all 
clear all
clc
disp('Dynamic Mode Decomposition and Reconstruction of Flow Data')
disp('DYNAMIC STALL CASE')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
disp('High Fidelity Computational Multiphysics Lab - The Ohio State University')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

%% USER INPUT
%cd('~/Research_Aurora/nocontrol_snaps/');
cd('/work/mohan.69/ModelReduction/DYNAMICSTALL/')
pwd
numofsnaps = 359;
file_delta = 1;
delta = 78/360;
timestep = 10^-4; % From CFD solver - non-DIMENSIONAL TIME FOR EVERY 'DT'
init_file  = 1;%  <== CHANGE if needed.
final_file = init_file + ((numofsnaps)*file_delta);
coherence = 'no';
reconmodes = numofsnaps;

% Header File info for Plot3d output
xhead = 315;
yhead = 315;
zhead = 1;
nvar = 1;

%% DMD algorithm 
disp('Reading In Snapshot Data...');
clear temp
m=1;

for i=init_file:file_delta:final_file  
    u(:,m)=importdata(['rhou' num2str(i) '.mat']);
    m=m+1;
    %disp(i)
end

disp('Operation Successful.');


% Setting up matrices
[gridsize,Nsnap] = size(u); 

% Calculate mean of data
Umean=zeros(size(u,1),1);
Umean =(mean(u'))';

% Step 1: Building Matrix V1^n-1

V1 = zeros(gridsize,Nsnap-1);
for i=1:Nsnap-1
    
    V1(:,i) = u(:,i);
    
end

% Step 2: Building V2 Matrix

V2 = zeros(gridsize,Nsnap-1);

for i=1:Nsnap-1
    V2(:,i) = u(:,i+1);
end

% Computation Starts

% Step 3: Singular Value Decomposition of the V1 matrix (Economy)
disp('SVD in process...')
[U,E,Wstar] = svd(V1,0);
disp('Operation Successful.');

% Step 4: Computing S matrix

S = U'*V2*Wstar/E;

% Step 5: Eigendecomposition of Matrix S
disp('Computing Eigendecomposition of S Matrix...')
[eigenvecs,eigenvals] = eig(S);
disp('Operation Successful.');
ritz = diag(eigenvals); % saving the Ritz values


%% Post Processing of DMD data
%clear timestep delta dt floqvals floqvalsOrder vecOrder modes real_floq imag_floq resid r datarows nummodes

% Convergence Residual

r = V2 - V1*S;
resid = norm(r,2)/length(V2); % L2 Norm of the Residual matrix i.e. Max Sing. Val
disp('No. of Snapshots:')
disp(numofsnaps)
disp('The L2 Norm Convergence Residual "r" is:')
disp(resid)

% Calculate Floquet Values and Compute modes

dt = timestep*delta; % Actual time interval

floqvals = log(ritz)/dt;
[~,idx]  = sort(real(floqvals),'descend'); % Sorting by High --> Low Real part
floqvalsOrder = floqvals(idx);
vecOrder = eigenvecs(:,idx); % Ordering Eigenvecs by Eigenvalue


% Computing Modes

disp('Computing DMD Modes...')
modes = U*vecOrder;
[~,nummodes] = size(modes);
disp('Operation Successful.'); 

% Writing Mean of Snapshots to ASCII PLOT3D files
header = [xhead yhead zhead nvar];
fname = 'Umean';
disp('Writing Mean flow to Plot3D file');
fid = fopen([fname '.dat'], 'w');
fprintf(fid, '%i \t',header);
fprintf(fid,'\n');
fprintf(fid, '%f \t',Umean);
fclose(fid);
disp('====================================================')

% Compute Coherence of DMD modes

if strcmp('yes',coherence) == 1 
Cmodes = zeros(size(U));
disp('Computing Coherence of DMD modes...'); 
for i = 1:nummodes
   Cmodes(:,i) = U*inv(U'*U)*U'*modes(:,i); 
end
disp('Operation Successful.');
end

%save timestep delta dt floqvals floqvalsOrder vecOrder modes real_floq imag_floq resid r datarows nummodes


%% Computing Amplitudes at all time instants
disp('Computing "Am" at all time instants...')
[datarows,nummodes] = size(modes);
rankloss = 0; % USE ONLY WHEN YOU REALLY have Rank Deficiency Issues

% Time traces of Amplitudes (C)
total = (nummodes-1)*dt;
time = (0:dt:total);
Am = zeros(nummodes,nummodes-rankloss); % Am(row = time evolution,column = mode)

modes_timeinst = zeros(datarows,nummodes);
for i = 1:nummodes
    for j=1:nummodes
    modes_timeinst(:,j) = modes(:,j)*exp(floqvalsOrder(j)*dt);
    end
    modes_timeinst =  modes_timeinst(:,1:end-rankloss);
    Am(i,:) = modes_timeinst\u(:,i);
end

disp('Operation Successful.');
disp('====================================================')
% Computing Time trace "Cn"
Cm = zeros(size(Am));
for j=1:nummodes-rankloss
    for i=1:nummodes
        Cm(i,j) = Am(i,j)*exp(floqvalsOrder(j)*dt);
    end
end

% Computing RMS value of Time trace for every mode to get |Am|

for i=1:nummodes-rankloss
   Am_rms(i) = norm(Cm(:,i))/sqrt(length(time));
end
% Sorting Am_rms and modes from highest to lowest
[~,index]  = sort(Am_rms,'descend'); % Sorting by High --> Low 
Am_rmsOrder = Am_rms(index);
modes = modes(:,index); 
CmOrder = Cm(:,index);% - real(Cm);
AmOrder = Am(:,index);
floqvalsFinalOrder = floqvalsOrder(index);
disp('====================================================')


% Writing DMD modes to ASCII PLOT3D files
disp('Writing DMD modes to Plot3D files');
header = [xhead yhead zhead nvar];
for i=1:nummodes-rankloss
fid = fopen(['dynstallDMDmode_u' num2str(i) '.dat'], 'w');
fprintf(fid, '%i \t',header);
fprintf(fid,'\n');
fprintf(fid, '%f \t',modes(:,i));
fclose(fid);
end
disp('====================================================')

%% Velocity Reconstruction
%cd('/home/mohan.69/work/ModelReduction/DMDtesting/');
reconmodes = 35;
disp('Computing Reconstructed Data . . .');
dmdrecon = zeros(datarows,length(time));
dmdmode_evolve = zeros(datarows,length(time));

for j = 1:reconmodes
    for i=1:nummodes
        dmdmode_evolve(:,i) = AmOrder(i,j)*modes(:,j)*exp(floqvalsFinalOrder(j)*dt);
    end
    dmdrecon = dmdrecon + dmdmode_evolve;
end

disp('Operation Successful.');
disp('====================================================')
% Writing Reconstructed data to ASCII PLOT3D files
fname = strcat('DMDdynstall',num2str(reconmodes),'reconstructed');
disp('Writing Reconstructed Data to Plot3D files');
header = [xhead yhead zhead nvar];
for i=1:nummodes
fid = fopen([fname num2str(i) '.dat'], 'w');
%disp(['Writing file ' fname num2str(i) '.dat']);
fprintf(fid, '%i \t',header);
fprintf(fid,'\n');
fprintf(fid, '%f \t',dmdrecon(:,i));
fclose(fid);
end
disp('====================================================')
% Overall Reconstruction Error

for i=1:nummodes
 error_recon(:,i) = dmdrecon(:,i) - u(:,i);
end

error_sum = sum(sum(error_recon.^2));
u_sum = sum(sum(u.^2));
error_total = sqrt(error_sum)/sqrt(u_sum);
error = abs(error_total)*100;
fprintf('Total Reconstruction Error (percent) using %d modes is:\n',reconmodes);
disp(error);

modeerror = zeros(1,reconmodes);
for i = 1:reconmodes
    temp1 = sum(error_recon(:,i).^2);
    u_mode = sum(u(:,i).^2);
    temp2 = sqrt(temp1)/sqrt(u_mode);
    modeerror(i) = abs(temp2)*100;
end
disp('Average Error:')
disp(mean(modeerror))
disp('Std. Deviation of Error:')
disp(std(modeerror))
disp('====================================================')
% % Output Reconstruction Error for every mode to ASCII PLOT3D files
% fname = strcat('DMDError',num2str(reconmodes),'reconstructed');
% disp('Writing DMDError to Plot3D files');
% header = [xhead yhead zhead nvar];
% for i=1:nummodes
% fid = fopen([fname num2str(i) '.dat'], 'w');
% %disp(['Writing file ' fname num2str(i) '.dat']);
% fprintf(fid, '%i \t',header);
% fprintf(fid,'\n');
% fprintf(fid, '%f \t',error_recon(:,i));
% fclose(fid);
% end

disp('Plotting Data...')
%% Plotting
% Plot Stable and Unstable Floquet exponents

numvals = nummodes;
initvals = 1;
hold on

real_floq = real(floqvalsFinalOrder(initvals:numvals)); % Use only floqvalsOrder to order by Stability
imag_floq = imag(floqvalsFinalOrder(initvals:numvals));

floquetplot = figure(1);
set(gcf, 'Position', [pos(1) pos(2) 10*100, 5*100]); %<- Set size
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
s = 50; % size of circle
xlabel('Imaginary Part','Fontsize',14,'FontWeight','bold')
ylabel('Real Part','Fontsize',14,'FontWeight','bold')
title('Floquet Multipliers of DMD Eigenvalues - All Modes','Fontsize',14,'FontWeight','bold')
grid on
plotvals = nummodes;
scatter(imag_floq(1:plotvals),real_floq(1:plotvals),s,'MarkerEdgeColor','b','MarkerFaceColor','c','LineWidth',1.5)
%scatter(imag_floq(5),real_floq(5),s,'MarkerEdgeColor','b','MarkerFaceColor','c','LineWidth',1.5)

savefig(floquetplot,'floquetstability')
saveas(floquetplot,'floquetstability','png')

% Plotting Am_rms for every mode
rmsval = figure(2);
%pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) 10*100, 5*100]); %<- Set size
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
modenumbers = 1:1:nummodes-rankloss;
s = 50;
scatter(modenumbers,Am_rmsOrder,s,'MarkerEdgeColor','b','MarkerFaceColor','c','LineWidth',1.5)
xlabel('Mode(s)','Fontsize',14,'FontWeight','bold')
ylabel('|Am|','Fontsize',14,'FontWeight','bold')
title('Plotting Am_{rms} contribution of every mode','Fontsize',14,'FontWeight','bold')
grid on
savefig(rmsval,'A_rmsmodes')
saveas(rmsval,'A_rmsmodes','png')

% Plotting Time Trace

trace = figure(3);
set(gcf, 'Position', [pos(1) pos(2) 10*100, 5*100]); %<- Set size
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
%subplot(2,1,1)
plot(time,real(CmOrder(:,1)),'b', ...
    time,real(CmOrder(:,2)),'r',...
    time,real(CmOrder(:,4)),'k',...
    time,real(CmOrder(:,6)),'c',...
    time,real(CmOrder(:,8)),'g','LineWidth',2)
xlabel('Time(s)','Fontsize',14,'FontWeight','bold')
ylabel('Real Part of Cm','Fontsize',14,'FontWeight','bold')
title('Time trace of DMD Amplitude Real(Cn)','Fontsize',14,'FontWeight','bold')
grid on
hleg1 = legend('Mode 1 (Mean)','Mode 2','Mode 3','Mode 4','Mode 5');
set(hleg1,'Location','NorthEastOutside');

% subplot(2,1,2)
% plot(time,imag(CmOrder(:,1)),'b', ...
%     time,imag(CmOrder(:,2)),'r',...
%     time,imag(CmOrder(:,4)),'k',...
%     time,imag(CmOrder(:,6)),'c',...
%     time,imag(CmOrder(:,8)),'g','LineWidth',2)
% xlabel('Time(s)','Fontsize',14,'FontWeight','bold')
% ylabel('Imag Part of Cm','Fontsize',14,'FontWeight','bold')
% title('Time trace of DMD Amplitude Imag(Cn)','Fontsize',14,'FontWeight','bold')
% grid on
savefig(trace,'mode_timetrace')
saveas(trace,'mode_timetrace','png')

% Plotting Snapshot errors

modalerror = figure(4);
set(gcf, 'Position', [pos(1) pos(2) 10*100, 5*100]); %<- Set size
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
modenumbers = 1:1:nummodes-rankloss;
s = 50;
scatter(modenumbers,modeerror,s,'MarkerEdgeColor','b','MarkerFaceColor','c','LineWidth',1.5)
xlabel('Mode(s)','Fontsize',14,'FontWeight','bold')
ylabel('% Error','Fontsize',14,'FontWeight','bold')
title(' Reconstruction % error for every mode','Fontsize',14,'FontWeight','bold')
grid on
savefig(rmsval,'Err_modes')
saveas(rmsval,'Err_modes','png')


%% Save and Exit.
disp('====================================================')
disp('Saving Complete Analysis MAT file to: ');
disp(pwd); 

save dmddynstall_u -v7.3  %modes_nocontrol modes 

disp('DONE!!!');

disp('******************************')
disp('Run Complete.')
disp('******************************')


%cd('/home/mohan.69/work/ModelReduction/ASME_Data/control/Udata/')
