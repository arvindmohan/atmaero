function savep3d(filename,header,data)
% SAVE data into ASCII Plot3D files for Fielview
disp('Writing to Plot3D file');
fid = fopen([filename '.dat'], 'w');
fprintf(fid, '%i \t',header);
fprintf(fid,'\n');
fprintf(fid, '%f \t',data);
fclose(fid);
disp('====================================================')

end

