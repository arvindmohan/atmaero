
clc
clear all
close all
dir = pwd;
index = 1;
disp('Converting Snapshots to MAT files')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
disp('Arvind Mohan')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

%% Transferring Data
 f='/work/mohan.69/ModelReduction/DYNAMICSTALL/';
 init_file  = 1;
 delta = 1;
 numsnaps = 360;
 final_file = (init_file-delta) + (numsnaps*delta);
 
header1 = [315 315 1];
header2 = [0.1000000  8.000000  60000.00  78.57472];
    
disp('Converting snapshots to .MAT files');
 
 for i=init_file:delta:final_file
     index = index + 1;
   %  disp(['Reading file snapshot_u' num2str(i) '.dat']);
   %  temp = dlmread(fullfile(f,['snapshot_u' num2str(i) '.dat']));
   %  save(fullfile(f,['u' num2str(i) '.mat']),'temp');
 
   %  disp(['Reading file snapshot_v' num2str(i) '.dat']);
   %  temp = dlmread(fullfile(f,['snapshot_v' num2str(i) '.dat']));
   %  save(fullfile(f,['v' num2str(i) '.mat']),'temp');
     
     disp(['Reading file snapshot_rhou' num2str(i) '.dat']);
     temp = dlmread(fullfile(f,['snapshot_rhou' num2str(i) '.dat']));
     save(fullfile(f,['rhou' num2str(i) '.mat']),'temp');
     
    % Converting MAT files to Plot3d Q files
% 
%     disp('Writing MAT files to Plot3D Q files');
% 
%     fid = fopen(['rho' num2str(i)], 'w');
%     fprintf(fid, '%i \t',header1);
%     fprintf(fid,'\n');
%     fprintf(fid, '%6.4f \t',header2);
%     fprintf(fid,'\n');
%     fprintf(fid, '%d \t',temp);
%     fclose(fid);
% 
%     disp('====================================================')

 end


disp('Done!!!')

