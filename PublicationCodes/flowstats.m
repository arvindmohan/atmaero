%% Basic Stats of flow
clc;clear;close all

init_file  = 200000;
delta = 50;
numsnaps = 5;
final_file = (init_file-delta) + (numsnaps*delta);

dim = [161 661 112];
% 
%a = reshape(importdata('rhou200000.mat'),dim);

% Compute Mean
temp = zeros(dim);
for i=1:numsnaps
   filenum = init_file + i*delta;
   disp(['Reading file rhou' num2str(filenum) '.mat']);
   temp = temp + reshape(importdata(['rhou' num2str(filenum) '.mat']),dim);
end

flowmean = temp/numsnaps;

% Compute Standard Deviation
e = zeros(dim);
for i=1:numsnaps
   filenum = init_file + i*delta;
   disp(['Reading file rhou' num2str(filenum) '.mat']);
   temp1 = reshape(importdata(['rhou' num2str(filenum) '.mat']),dim);
   e(:,:,:) = e(:,:,:) + (temp1 - flowmean).^2;
end

fstd = sqrt(e/(numsnaps-1));
%fstd = sqrt(sum(e,4)/(length(a)-1))
%d - fstd

% Plotting
xi = 80; yi = 120;
figure(1)
subplot(1,2,1)
stdPoint = squeeze(fstd(xi,yi,:));
heightVector = [0:1:111];
plot(stdPoint,heightVector)
xlabel('Standard Deviation')
ylabel('Height')
title('std variation with height')
subplot(1,2,2)
meanPoint = squeeze(flowmean(xi,yi,:));
plot(meanPoint,heightVector)
xlabel('Standard Deviation')
ylabel('Height')
title('Mean variation with height')


