function [yn,yp,ycmor] = morlet(xh,z0,ff,kf)
% Morlet wavelet trasnform of a signal
% xh: FFT of signal
% ff: vector of frequencies (linear)
% kf: vector of frequencies (logarithmic)
% z0: parameter
% ycmor, yn, yp: Morlet transform arrays (complex, norm and phase)
  n=length(xh); xh = reshape(xh,1,n);
  ns=length(kf); sq2pi = sqrt(2*pi);
  if z0 == 5; cps= 1.4406;
  elseif z0 == 10; cps= 2.83527;
  else cps = 1;  end  %  no normalization, can be fixed for specific cases.
  for is=1:ns
    z=ff/kf(is);  
    morh= z0/kf(is)/sq2pi * (exp(-(z0^2*(1-z).^2)/2) ...
       - exp(-z0^2*(1+z.^2)/2) ) +zeros(1,n)*i;
    morh = morh * kf(is)/sqrt(cps) ; % my normalization
    
    yh = xh.*morh;
%     for ii=1:n
%       z=ff(ii)/kf(is);
%       morh= z0/kf(is)/sq2pi * (exp(-(z0^2*(1-z)^2)/2) ...
%        - exp(-z0^2*(1+z^2)/2) ) +0*i;
%       morh = morh * kf(is)/sqrt(cps) ; % my normalization
%       yh(ii) = xh(ii)*morh;  
%     end
    ycmor(is,:) = ifft(yh); % the complex inverse transform
  end
  yn = abs(ycmor); % its norm
  yp = angle(ycmor); % and its phase
return