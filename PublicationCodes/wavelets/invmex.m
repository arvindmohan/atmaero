function y0 = invmex(z,drs)
% inverse mexhat transform, multiscale
% z: array of wavelet coefficients
% drs: ratio of successive scales
% y0: reconstructed 1-D signal
  [ns,nx] = size(z); dls = log(drs); 
  for ix=1:nx; y0(ix) = 0.; 
    for is=2:2:ns-1 ; % truncated loop would give filtered inverse
      y0(ix) = y0(ix) + (z(is-1,ix)+4*z(is,ix)+z(is+1,ix)) ;% Simpson
    end
  y0(ix) = y0(ix)*dls/3; 
  end   
return