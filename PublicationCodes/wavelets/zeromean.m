function x = zeromean(x)
% removes the mean from a signal
  x = x - mean(x);
return