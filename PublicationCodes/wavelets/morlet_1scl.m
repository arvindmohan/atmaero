function [yn,yc] = morlet(xh,z0,ff,kf)
% 1-scale Morlet wavelet transform of a signal
  n=length(xh); sq2pi = sqrt(2*pi);
  for ii=1:n
    z=ff(ii)/kf;
    morh= z0/kf/sq2pi * (exp(-(z0^2*(1-z)^2)/2) ...
       - exp(-z0^2*(1+z^2)/2) ) +0*i;
    morh = morh * kf ;
    yh(ii) = xh(ii)*morh;  
  end
  yc = ifft(yh); % the complex inverse transform
  yn = abs(yc); % the complex inverse transform
return