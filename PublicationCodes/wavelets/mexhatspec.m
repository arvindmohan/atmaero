function sEs = mexhatspec(z,dx)
% calculates the mean mexhat compensated spectrum
% from the wavelet coefficients
  [ns,nx] = size(z); sEs = zeros(1,ns);
  for is=1:ns ; 
    for ix=1:nx ; sEs(is) = sEs(is) + z(is,ix)^2 ; end
    sEs(is) = 2*sEs(is) /nx/dx ; %energy per unit time
  end
return