function ff=fftfreq(x)
% constructs the linear 2-sided scale of frequencies for FFT
    n=length(x); ndx = n*(x(2)-x(1));
    ff = zeros(1,n);
    ff(1:n/2) = 0:n/2-1;
    ff(n/2+1:n) = -n/2:-1;
%     for ii=1:(n/2) ; ff(ii)= (ii-1); end
%     for ii=(1+n/2):n ; ff(ii)= -(n-ii+1); end
    ff = ff/ndx;
return