function [skew,kurt] = skewkurt(map)
    [ns,nx] = size(map);
    skew = zeros(1,ns); kurt=zeros(1,ns);
    for is=1:ns; ave=0.; var = 0.;
        for ix=1:nx;  ave = ave + map(is,ix); end
        ave = ave/nx;
        for ix=1:nx; 
            z = map(is,ix) - ave;  var = var + z^2; 
            skew(is)= skew(is) +z^3 ; kurt(is) = kurt(is) + z^4;
        end
        std = sqrt(var/(nx-1)); 
        skew(is) = skew(is)/nx / std^3;
        kurt(is) = kurt(is)/nx / std^4;
    end
return 

