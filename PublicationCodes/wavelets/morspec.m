function kEk = morspec(z,dx)
% calculates the mean morlet compensated spectrum
% from the wavelet coefficients
  [ns,nx] = size(z); kEk = zeros(1,ns);
  for is=1:ns ;
    for ix=1:nx ; kEk(is) = kEk(is) + z(is,ix)^2 ; end
    kEk(is) = kEk(is) /nx/dx/2 ;
  end
return