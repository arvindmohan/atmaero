function [xmex] = mexhat(xh,ff,s)
% Mexhat transform of a signal
% xh: FFT of the signal
% ff: vector of frequencies (linear) for FFT
% s: vector of scales (logarithmic) for end-use or display
% xmex: array of coefficients in the wavelet half-plane
  n=length(xh); xh = reshape(xh,1,n); ns=length(s);
  for is=1:ns; 
    omsq=(2*pi*ff).^2; 
    z=omsq*s(is);
    g2=-omsq.*exp(-z)+zeros(1,n)*i; % Fourier transform of Mexican hat
    yh = xh.*g2; % convolution in Fourier spaceS
%     for ii=1:n;
%       omsq=(2*pi*ff(ii))^2; 
%       z=omsq*s(is); 
%       g2=-omsq*exp(-z)+0*i; % Fourier transform of Mexican hat
%       yh(ii) = xh(ii)*g2;  % convolution in Fourier space
%     end 
    xmex(is,:) = -real(ifft(yh))*s(is); % the transform, multiscale normalization
  end
return
