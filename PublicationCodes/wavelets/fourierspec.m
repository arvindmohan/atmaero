function [nf,fEf,freq] = fourierspec(dx,ff,yh)
% calculation of mean Fourier spectrum f.E(f) 
  nx = length(ff);
  nf = nx/2 - 1 ;% not including zero frequency for log-scale plotting
  freq(1:nf) = ff(2:nf+1);
  Ef(1:nf) = (abs(yh(2:nf+1)).^2 + abs(yh(nx-(nf-1:-1:0))).^2) /nx/2;
  fEf = Ef.*freq; 
%   for jf =1:nf ;
%     freq(jf) = ff(jf+1);
%     Ef(jf) = (abs(yh(jf+1))^2 + abs(yh(nx-jf+1))^2) /nx/2;
%     fEf(jf) = Ef(jf)*freq(jf);
%   end
return