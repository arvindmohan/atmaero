function y0 = morletfilter(xh,z0,ff,kf,drs)
%    THIS FUNCTION HAS NOT BEEN TESTED YET __ USER BEWARE
% Morlet-filtered signal: forward and inverse wrapped in one; 
% xh: DFFT of signal
% ff: vector of frequencies (linear)
% kf: vector of frequencies (logarithmic) (odd number for Simpson)
% z0: parameter
  n=length(xh); ns=length(kf); sq2pi = sqrt(2*pi);
  dls = log(drs);
  if z0 == 5; cps= 1.4406;
  elseif z0 == 10; cps= 2.83527;
  else cps = 1;  end  %  no normalization, can be fixed for specific cases.
  
  for is=1:ns
    for ii=1:n
      z=ff(ii)/kf(is);
      morh= z0/kf(is)/sq2pi * (exp(-(z0^2*(1-z)^2)/2) ...
       - exp(-z0^2*(1+z^2)/2) ) +0*i;
      morh = morh * kf(is)/sqrt(cps);
      yh(ii) = xh(ii)*morh*morh;  
    end
    yinv(is,:) = real(ifft(yh));
  end
% Simpson integration over all scales
  for ix=1:n; y0(ix) = 0. ; 
    for is=2:2:ns-1 ;      
      y0(ix) = y0(ix) + (yinv(is-1,ix)+4*yinv(is,ix)+yinv(is+1,ix)) ;
    end
    y0(ix) = y0(ix)*dls/3; 
  end     
return