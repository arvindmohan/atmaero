function yh = scramblephase(yh)
% Scramble the phase of an FFT signal
  ynrm = abs(yh);
  n = length(ynrm);
  r = rand(n,1)*2*pi; %uniformly distributed 
  for j=1:n;  yh(j) = ynrm(j)*exp(i*r(j)); end;
return
  