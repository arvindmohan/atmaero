
function [u2s] = mexinvs1D(u2,s,ffx)
% slow inverse
   [ns,nx] = size(u2); 
   us = zeros(ns,nx);
   dsl = log(s(1)/s(2));
   pisq = pi*pi;   
   for ix=1:nx ; u2s(ix) = 0.; end ; 
% now the convolution    
   for is=1:ns; 
      u = u2(is,:) ; u=zeromean(u);  uf = fft(u);
      omsq = 4*pisq*ffx.^2; 
      f2 = -omsq.*exp(-omsq*s(is));
      xh2s = uf.*f2;
%       for ix=1:nx ;
%         omsq = 4*pisq*ffx(ix)^2; 
%         f2 = -omsq*exp(-omsq*s(is));
%         xh2s(ix) = uf(ix) * f2;
%       end ; 
      uu2 = -s(is)* real(ifft(xh2s)); us(is,:) = 4*uu2(:);
   end
     u2s = zeros(1,nx);
     for is=2:2:ns;
       u2s(1:nx) = u2s(1:nx) + (us(is+1,1:nx)+us(is-1,1:nx)+4*us(is,1:nx));
     end
     u2s = u2s*dsl/3.; 
   
%   for ix=1:nx ; 
%      for is=1:ns; us(is) = +4*u2(is,ix); end;
%      for is=2:2:ns;
%          u2s(ix) = u2s(ix) + (us(is+1)+us(is-1)+4*us(is));
%      end
%      u2s(ix) = u2s(ix)*dsl/3.; 
%    end; 
return
   