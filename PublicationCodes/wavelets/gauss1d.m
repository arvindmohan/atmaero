function yf = gauss1d(x,ff,s)
% Gaussian filtering of a signal
% x: the signal, zero mean
% ff: vector of frequencies (linear) for FFT
% s: scale --- see its definition!!!
    n=length(x); 
    xh = fft(x);
    omsq = 2*pi*ff; omsq = omsq.^2; z = omsq*s; % s is scalar
    g2 = exp(-z) + zeros(1,n)*i; % Fourier transform of Mexican hat
    yh = xh.*g2;
    yf = real(ifft(yh)); % 
return