function y0 = invmor(zmor,z0,ff,kf,drs)
% inverse Morlet transform
% zmor: complex wavelet coefficients
% ff: vector of frequencies (linear)
% kf: vector of frequencies (logarithmic) (length should be odd number)
% drs: ratio of successive scales
% y0: reconstructed signal
  [ns,n] = size(zmor); dls = log(drs);
  sq2pi = sqrt(2*pi); 
  if z0 == 5; cps= 1.4406;
  elseif z0 == 10; cps= 2.83527;
  else cps = 1;  end  %  no normalization, can be fixed for specific cases.
  
  for is=1:ns;  ymor = zmor(is,:); xh = fft(ymor);
      z=ff/kf(is);
      morh= z0/kf(is)/sq2pi * (exp(-(z0^2*(1-z).^2)/2) ...
       - exp(-z0^2*(1+z.^2)/2) ) +zeros(1,n)*i;
      morh = morh * kf(is)/sqrt(cps); 
      yh = xh.*morh;  
%     for ii=1:n ; z=ff(ii)/kf(is);
%       morh= z0/kf(is)/sq2pi * (exp(-(z0^2*(1-z)^2)/2) ...
%        - exp(-z0^2*(1+z^2)/2) ) +0*i;
%       morh = morh * kf(is)/sqrt(cps); 
%       yh(ii) = xh(ii)*morh;  end
    yinv(is,:) = real(ifft(yh));
  end
% Simpson integration over all scales
  y0 = zeros(1,n) ; 
  for is=2:2:ns-1 ;      
    y0(1:n) = y0(1:n) + (yinv(is-1,1:n)+4*yinv(is,1:n)+yinv(is+1,1:n)) ;
  end
  y0 = y0*dls/3; 
       
return