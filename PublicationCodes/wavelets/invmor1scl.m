function y0 = invmor1scl(ymor,z0,ff,kf)
% single scale "inverse Morlet transform" (no normalization)
% ymor: complex wavelet coefficients
% ff: vector of frequencies (linear)
% kf: frequency 
% y0: reconstructed signal
  [ns,n] = size(ymor); 
  sq2pi = sqrt(2*pi); 
  
  xh = fft(ymor);
    for ii=1:n ; z=ff(ii)/kf;
      morh= z0/kf/sq2pi * (exp(-(z0^2*(1-z)^2)/2) ...
       - exp(-z0^2*(1+z^2)/2) ) +0*i;
      morh = morh * kf; 
      yh(ii) = xh(ii)*morh;  
    y0 = real(ifft(yh));
  end
 
return