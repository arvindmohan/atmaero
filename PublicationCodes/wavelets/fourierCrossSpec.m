function [nf,fxcor,freq] = fouriercrossspec(dx,ff,yh,zh)
% calculation of mean Fourier spectrum f.E(f) 
  nx = length(ff);
  nf = nx/2 - 1 ;% not including zero frequency for log-scale plotting
  for jf =1:nf ;
    freq(jf) = ff(jf+1);
    x = conj(yh).*zh;
    Ef(jf) = (real(x(jf+1)) + real(x(nx-jf+1))) /nx/2;
    fxcor(jf) = Ef(jf)*freq(jf);
  end
return