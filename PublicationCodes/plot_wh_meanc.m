function []=plot_wh_meanc(y,x,Xval,Xmean,npct,conf,nbfig)

figure(nbfig)
set(nbfig,'Position',[100 20 1800 900])

if conf==1
 pcolor(y,x,Xval), shading interp
   
else
contourf(y,x,Xval,npct)
end
hold on

mini=min(Xmean(:));
maxi=max(Xmean(:));

contour(y,x,Xmean,mini:(maxi-mini)/3 :maxi,'k', 'linewidth',3)
contour(y,x,Xmean,mini:(maxi-mini)/2 :maxi,'k', 'linewidth',3)

axis equal tight
colormap('jet')

caxis([ min(Xval(:))   max(Xval(:)) ])