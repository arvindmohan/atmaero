% Processing Probe Signals
% For Scitech 2015 Dynamic Stall paper
% For SD7003 Airfoil from Visbal Paper

clc
clear all
close all

cd('./Usnapsnew/results/');
%% Load in one file to get basic info about all other probe files
prb1name = '1u.prb';
u = dlmread(prb1name);
init_probe = 1;
num_probes = 12;
dt=0.035;

%% Compute Global Spectrum from DMD modes

%load modes 
%load dabs
load dnorm
%load u1 
load floqvalsOrder % Load data from DMD analysis
% already arranged by A_rms values

%Computing Scaling Coeffs for modes using 1st snapshot

%d = modes\u1;

% Mode norms
%dabs = abs(d);
%dnorm = dabs/max(dabs);

% Extract only positive frequencies 
idx = find(imag(floqvalsOrder) >= 0);
mode_St = (imag(floqvalsOrder(idx)))/(2*pi);
mode_normsPos = dnorm(idx);

%save dabs dabs -v7.3


%% Compute Probe FFT for all probes and Plot
data_len = length(u);
t = 0:dt:(data_len*dt - dt);
%nfft = data_len;
fft_len = (data_len/2) + 1;%((2^nextpow2(data_len))/2) + 1;  

data = NaN(data_len,num_probes);
f = NaN(fft_len,num_probes);
amp = NaN(fft_len,num_probes);
amp_norm = NaN(fft_len,num_probes);

plotstyle = ['r','+','-','o','k','c'];

% Plot time signal for every probe

for i=init_probe:num_probes    
    fname = [ num2str(i) 'u' '.prb'];
    data(:,i)=dlmread(fname);
    disp('Time signal for')
    disp(fname)
end

% Compute Single sided Amp. spectrum for every probe

for i=init_probe:num_probes
    [f(:,i),amp(:,i)] = probe_fft(data(:,i),dt);
    amp_norm(:,i) = amp(:,i)/max(amp(:,i));
end
    
%% Comparing the two    
    
freq = f(:,1);

load DMD_amps.mat
% To ****Manually**** create DMD_amps for the FIRST TIME ONLY
%- Comment out after use.
% DMD_amps = zeros(fft_len,1);
% for i=1:361
%     if DMD_amps(i) == 0;
%         DMD_amps(i) = NaN;
%     end
% end

% Find no. of significant modes used for comparison
% i.e. find the no. of non-NaN values
count = 0;
for i=1:fft_len
   if isnan(DMD_amps(i)) 
       DMD_amps(i) = 0;
   else
       count = count + 1;
   end
end
disp('No. of DMD modes used for comparison:');disp(count);

% Computing Error b/w DMD and Probe freq for all probes
ProbeL2Error = NaN(num_probes,1);
ProbeError = NaN(fft_len,num_probes);
for i=1:num_probes
    ProbeError(:,i) = DMD_amps - amp_norm(:,i);
    ProbeL2Error(i) = norm(ProbeError(:,i));
end

% Find which Probe has minimum error
[min_error,index] = min(ProbeL2Error);
disp('Min. Error occurs at Mode no.:');
disp(index);

%% Publication Quality Plots

% To plot only upto a certain x-range
% for better clarity
% Default set to full length
slice = 82;
clear DMD_amps
load DMD_amps.mat
for i = init_probe:num_probes
     h = figure('Units', 'pixels', ...
    'Position', [100 100 500 375]);
     hold on;
     probef = line(freq(1:slice),amp_norm(1:slice,i));
     dmdf = stem(freq(1:slice),DMD_amps(1:slice));
     
     % Adjust Line Properties (Functional)
     set(probef,'LineStyle', '-','Color','b');
     set(dmdf,'LineStyle','--','Color','r');
     
     % Adjust Line Properties (Aesthetics)
     set(probef,'LineWidth',0.5);
     set(dmdf                            , ...
    'LineWidth'       , 0.5           , ...
    'Marker'          , 'o'         , ...
    'MarkerSize'      , 4           , ...
    'MarkerEdgeColor' , 'k'  , ...
    'MarkerFaceColor' , 'c'  );

     % Add Legend and Labels

%      hTitle  = title(['Single sided Amplitude spectrum of Probe ' ...
%        num2str(i) ' vs Global DMD spectrum']);
     hXLabel = xlabel('St. no');
     hYLabel = ylabel('Normalized Amplitude');
     hLegend = legend([probef,dmdf],['Probe','',num2str(i)],'DMD Frequency','location', 'NorthEast');
     
     % Adjust Font and Axes Properties

    set( gca                       , ...
        'FontName'   , 'Helvetica' );
    set([hXLabel, hYLabel], ...
        'FontName'   , 'AvantGarde');
    set([hLegend, gca]             , ...
        'FontSize'   , 8           );
    set([hXLabel, hYLabel]  , ...
        'FontSize'   , 10          );
%     set( hTitle                    , ...
%         'FontSize'   , 10         , ...
%         'FontWeight' , 'bold'      );

    set(gca, ...
      'Box'         , 'off'     , ...
      'TickDir'     , 'out'     , ...
      'TickLength'  , [.02 .02] , ...
      'XMinorTick'  , 'on'      , ...
      'YMinorTick'  , 'on'      , ...
      'YGrid'       , 'off'      , ...
      'XColor'      , [.3 .3 .3], ...
      'YColor'      , [.3 .3 .3], ...
      'XTick'       , 0:0.5:14.5, ...
      'YTick'       , 0:0.1:1, ...
      'LineWidth'   , 1         );
  
%     % Export to EPS
        epsname = [ num2str(i) 'u' 'DMD' '.eps'];
        %epsname = [ num2str(i) 'u' 'DMD' '.jpg'];
        set(gcf, 'PaperPositionMode', 'auto');
        print(h,'-depsc2',epsname)
        %print(h,'-djpeg ',epsname)
       close;
      
%       % Postprocess
% 
%       fixPSlinestyle(epsname,epsname);
       
end

%% Plot full spectrum
slice = 25;
   h = figure('Units', 'pixels', ...
    'Position', [100 100 500 375]);
     hold on;
     dmdf = stem(mode_St(1:slice),mode_normsPos(1:slice));
     
     % Adjust Line Properties (Functional)
     set(dmdf,'LineStyle','--','Color','r');
     
     % Adjust Line Properties (Aesthetics)
     set(dmdf                            , ...
    'LineWidth'       , 0.5           , ...
    'Marker'          , 'o'         , ...
    'MarkerSize'      , 2           , ...
    'MarkerEdgeColor' , 'k'  , ...
    'MarkerFaceColor' , 'c'  );

     % Add Legend and Labels

%      hTitle  = title(['Single sided Amplitude spectrum of Probe ' ...
%        num2str(i) ' vs Global DMD spectrum']);
     hXLabel = xlabel('St. no');
     hYLabel = ylabel('Normalized Amplitude');
     hLegend = legend(dmdf,'DMD Frequency','location', 'NorthEast');
     
     % Adjust Font and Axes Properties

    set( gca                       , ...
        'FontName'   , 'Helvetica' );
    set([hXLabel, hYLabel], ...
        'FontName'   , 'AvantGarde');
    set([hLegend, gca]             , ...
        'FontSize'   , 8           );
    set([hXLabel, hYLabel]  , ...
        'FontSize'   , 10          );
%     set( hTitle                    , ...
%         'FontSize'   , 10         , ...
%         'FontWeight' , 'bold'      );

    set(gca, ...
      'Box'         , 'off'     , ...
      'TickDir'     , 'out'     , ...
      'TickLength'  , [.02 .02] , ...
      'XMinorTick'  , 'on'      , ...
      'YMinorTick'  , 'on'      , ...
      'YGrid'       , 'off'      , ...
      'XColor'      , [.3 .3 .3], ...
      'YColor'      , [.3 .3 .3], ...
      'LineWidth'   , 1);
  
%     % Export to EPS
        epsname = [ 'DMDfullspectrum' '.eps'];
        %epsname = [ num2str(i) 'u' 'DMD' '.jpg'];
        set(gcf, 'PaperPositionMode', 'auto');
        print(h,'-depsc2',epsname)
        %print(h,'-djpeg ',epsname)
       close;
      
%       % Postprocess
% 
%       fixPSlinestyle(epsname,epsname);

%% plot time trace

slice = 82;

for i = init_probe:num_probes
     h = figure('Units', 'pixels', ...
    'Position', [100 100 500 375]);
     hold on;
     probef = line(t,data(:,i));
     
     % Adjust Line Properties (Functional)
     set(probef,'LineStyle', '-','Color','b');
     
     % Adjust Line Properties (Aesthetics)
     set(probef,'LineWidth',0.5);

     % Add Legend and Labels

%      hTitle  = title(['Single sided Amplitude spectrum of Probe ' ...
%        num2str(i) ' vs Global DMD spectrum']);
     hXLabel = xlabel('Time (s)');
     hYLabel = ylabel('Amplitude');
     %hLegend = legend([probef,dmdf],['Probe','',num2str(i)],'DMD Frequency','location', 'NorthEast');
     
     % Adjust Font and Axes Properties

    set( gca                       , ...
        'FontName'   , 'Helvetica' );
    set([hXLabel, hYLabel], ...
        'FontName'   , 'AvantGarde');
   % set([hLegend, gca]             , ...
   %     'FontSize'   , 8           );
    set([hXLabel, hYLabel]  , ...
        'FontSize'   , 10          );
%     set( hTitle                    , ...
%         'FontSize'   , 10         , ...
%         'FontWeight' , 'bold'      );

    set(gca, ...
      'Box'         , 'off'     , ...
      'TickDir'     , 'out'     , ...
      'TickLength'  , [.02 .02] , ...
      'XMinorTick'  , 'on'      , ...
      'YMinorTick'  , 'on'      , ...
      'YGrid'       , 'off'      , ...
      'XColor'      , [.3 .3 .3], ...
      'YColor'      , [.3 .3 .3], ...
      'LineWidth'   , 1         );
  
%     % Export to EPS
        epsname = [ num2str(i) 'u' 'Probe' '.eps'];
        %epsname = [ num2str(i) 'u' 'DMD' '.jpg'];
        set(gcf, 'PaperPositionMode', 'auto');
        print(h,'-depsc2',epsname)
        %print(h,'-djpeg ',epsname)
       close;
      
%       % Postprocess
% 
%       fixPSlinestyle(epsname,epsname);
       
end


