%% Stat Analysis for VW case
% For only the 2D suction side of wing
% Arvind T. Mohan
clc;clear;close all

%% Initialization and Load grid
disp('Init')
init_file  = 200000;
timestep = 10^-4; % From CFD solver - non-DIMENSIONAL TIME FOR EVERY 'DT'
delta = 50;
numsnaps = 4394;
final_file = (init_file-delta) + (numsnaps*delta);

% load  grid
load('grid2D.mat');
dim = [xl yl];


%% Specify Point location
indy = 1.8124;
indx = 0.1181;

header = [xl yl 1 1];
gridSize = prod(header);
disp('DONE!!!')
%% Load Data into Memory and Compute Mean
cd('/mpidir_godzilla/mohan.69/VortexWingAnalysis/Pwallsnaps/');
disp('Loading Data...')
% ncores = 30;
% parpool(ncores);

Xfield = zeros(xl,yl,numsnaps);
parfor i=1:numsnaps
   filenum = (init_file-delta) + i*delta; 
   Xfield(:,:,i) = reshape(importdata(['rhoewall' num2str(filenum) '.mat']),dim);
end

cd('/mpidir_godzilla/mohan.69/VortexWingAnalysis/StatAnalysisVW/ATMCode/'); % Loaded Data, leaving Dir.

disp('Compute Mean')
% Compute Mean
Xmean = mean(Xfield,3);

plot_wh_meanc(y,x,Xmean,Xmean,21,2,1000)
colorbar
title('Mean Flow Profile')
savep3d('Meanflow',header,reshape(Xmean,[gridSize 1]));
% cmax=1;
% cmin=cmax;
% grey_plot(cmin,cmax,1,1)
disp('DONE!!!')
%% Play Movie of Snapshots
% frameRate = 5; % At reqd. frame rate
% for i=1:frameRate:numsnaps
%     plot_wh_meanc(y,x,Xfield(:,:,i),Xmean,21,1,1001)
%     caxis([1.765 1.785])
%     pause (1/12)
% end
% disp('DONE!!!')

%% Compute RMS for full domain
disp('Compute RMS')
e = zeros(xl,yl);
parfor i=1:numsnaps
  temp = (Xfield(:,:,i) - Xmean).^2;
  e = e + temp;
end

Xstd_Full = sqrt(e/(numsnaps-1));
clear e temp
disp('DONE!!!')
%% Do FFT Analysis at any point
clear probeS* pxx* f IMF tmp val*
close all
disp('FFT Analysis at any point')
% Pick point from plot

plot_wh_meanc(y,x,Xstd_Full,Xmean,21,2,1003)
colorbar
title('RMS of the field')

% clear indy indx
% M=1;
% sprintf('choose %2d point(s) on the plot ', M)
% [indy,indx]= ginput(M);

ind=find(y<=indy);
Zi=ind(end);

ind=find(x<=indx);
Xi=ind(end);

hold on
plot(y(Zi),x(Xi),'o','MarkerSize',10,'MarkerEdgeColor','r','MarkerFaceColor','y')


probeSignal = squeeze(Xfield(Xi,Zi,1:1:end));
    
% FFT
dt = timestep*delta;
Fs = 1/dt;

tx=length(probeSignal);
NFFT=2^nextpow2(tx);
mtx=0;
while NFFT>tx(1)
    NFFT=2^nextpow2(tx(1)-mtx);
    mtx=mtx+1;
end
%

%%%  perform spectrum raw signal
Overlap = NFFT/4;
[pxx,f] = cpsd(probeSignal, probeSignal, hanning(NFFT),Overlap, NFFT, Fs);

 %%%  plot spectrum raw signal
figure(4)  
nn=1;
semilogx(f(nn:end),pxx(nn:end).*f(nn:end));
title('Spectra of Raw Signal at Probe')
disp('DONE!!!')    
%% Perform EMD on Probe Signal
disp('Perform EMD on Probe Signal')
tmp = probeSignal;
IMF = emdH(tmp-mean(tmp));

%%%  perform & plot  imf + psd of imf
[nImf,~]=size(IMF);
bandT=(1:1:tx);
vec = 1:numsnaps;
for i=1:nImf
    figure(1)
    subplot(round(nImf/2),2,i )
%     plot(nT(bandT),tmp(bandT)-mean(tmp),'r')
%     hold on
    sig=IMF(i,:);
    plot(vec,sig(bandT)) 


    [pxx,f] = cpsd(sig, sig, hanning(NFFT), NFFT/2 , NFFT, Fs);
    nn=3;

    figure(3)
    semilogx(f(nn:end),pxx(nn:end).*f(nn:end));
    hold on
    title('Premul. Power Spectra of EMD Modes at Probe');

end
disp('DONE!!!')
%% Premul. Spectra of Raw Probe Signal and its various modifications
disp('Premul. Spectra of Raw Probe Signal')
% Reconstruct Probe Sig. with reqd. IMFs
AA = 1.*IMF(1,:);

% Subtracting mean and small/noisy EMD time scale from Probe Signal?
valref=(tmp(bandT)-mean(tmp))-AA';
       
       
figure(10)
subplot(311)
% Plot Mean subtracted Probe Sig.
plot(vec,tmp(bandT)-mean(tmp),'r')
hold on
% Plot small scale EMD mode(s) chosen earlier
plot(vec,AA,'b')
% Plot Mean subtracted Probe Sig. - EMD small scales
plot(vec,valref,'m')
title('Plot all 3 signals for comparison')


subplot(312)
plot(vec,AA,'b')
title('Plot small scale EMD mode(s)');

subplot(313)
plot(vec,valref,'m')
title('Plot Mean subtracted Probe Sig. - EMD small scales')

% Spectrum for EMD Small scales
[pxx,f] = cpsd(AA, AA, hanning(NFFT), NFFT/2 , NFFT, Fs);
% Spectrum for Probe Sig. - EMD small scale - Mean
[pxxr,f] = cpsd(valref, valref, hanning(NFFT), NFFT/2 , NFFT, Fs);
% Spectrum for Probe Sig. - Mean
[pxxt,f] = cpsd(tmp(bandT)-mean(tmp), tmp(bandT)-mean(tmp), hanning(NFFT), NFFT/2 , NFFT, Fs);
  nn=3;

figure(11)
% Spectrum for Probe Sig. - Mean
semilogx(f(nn:end),pxxt(nn:end).*f(nn:end),'r');
hold on
% Spectrum for EMD Small scales
semilogx(f(nn:end),pxx(nn:end).*f(nn:end),'b');
hold on
% Spectrum for Probe Sig. - EMD small scale - Mean
semilogx(f(nn:end),pxxr(nn:end).*f(nn:end),'m');
title('% Spectrum for Probe Sig. - EMD small scale - Mean')
disp('DONE!!!')

%% 2 Point Correlation with Probe EMD Signal and Raw Field
clear Pwall_* valref
% Pick which IMF you want to correlate with
IMFid = 4;
valref=IMF(IMFid,:);
pas = 1;
nbins=200;

Pwall_corr=zeros(xl,yl);
Pwall_coh_var=zeros(xl,yl);
Pwall_corr_dt=zeros(xl,yl,2*nbins+1);
disp('Computing 2D Correlation:')
parfor i=1:xl
    for j=1:yl

        tmp=squeeze(Xfield(i,j,1:pas:end));
        
        val1=(tmp-mean(tmp));

        % Correlating EMD mode with Mean sub. Raw Signal 
        % At every point in the field
        RR = corrcoef(valref,val1);
        coeff_corr=RR(1,2);

        Pwall_corr(i,j)=coeff_corr ;
        % nbins specifies min. and max. lag
        r=xcorr(valref,val1,nbins,'coeff');
        Pwall_corr_dt(i,j,:)=(fliplr(r));

        rmax=max(sqrt(r.*r));
        Pwall_coh_var(i,j)=rmax;

    end
    i
end
disp('DONE!!!')    
%% Plot with no lag
%pcolor(y,x,Pwall_corr), shading interp

Xval=Pwall_corr;
plot_wh_meanc(y,x,Xval,Xmean,pi,1,1005)
colorbar
title('2 Point Correlation of probe with RAW field')

hold on
plot(y(Zi),x(Xi),'o','MarkerSize',10,'MarkerEdgeColor','r','MarkerFaceColor','y')
str = sprintf('Correlation without Lag - For IMF %d',IMFid);
title(str)
cmax=0.8;
cmin=cmax;
grey_plot(cmin,cmax,4,1)

figure(4)
plot( ((1:1:2*nbins+1).*dt)-mean( (1:1:2*nbins+1).*dt),squeeze(Pwall_corr_dt(Xi,Zi,:)))
str2 = sprintf('Corr. Coeff plot - For IMF %d',IMFid);
title(str2)
    
%%  plot with lag

parfor i=nbins:1:2*nbins+1
    i
    filename = ['CorrLag' num2str(IMFid) 'IMFa' num2str(i)]
    data = reshape(Pwall_corr_dt(:,:,i),[gridSize 1]);
    savep3d(filename,header,data);
%     pas = 4;    
    %pcolor( y,x,squeeze(Pwall_corr_dt(:,:,i))), shading interp
%     Xval=squeeze(Pwall_corr_dt(:,:,i));
%     plot_wh_meanc(y,x,Xval,Xmean,pi,1,1006)
% 
%     hold on
%     plot(y(Zi),x(Xi),'o','MarkerSize',10,'MarkerEdgeColor','r','MarkerFaceColor','y')
%     axis equal tight
%     colormap('jet')
%     cmax=0.8;
%     cmin=cmax;
%     title('Correlation with Lag')
%     grey_plot(cmin,cmax,pas,1)
% 
%     pause(1/24)
end
disp('DONE!!!')









%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Analyze DMD Reconstructed Data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Load Data into Memory and Compute Mean
cd('/mpidir_godzilla/mohan.69/VortexWingAnalysis/dmd50pwallsnaps/');
disp('Starting Analysis on DMD recon flow:')
disp('Loading Data...')
nummodes = numsnaps - 1;

XfieldDMD = zeros(xl,yl,nummodes);
parfor i=1:nummodes
   filenum = (init_file-delta) + i*delta; 
   XfieldDMD(:,:,i) = reshape(importdata(['dmdpwall50recon' num2str(filenum) '.mat']),dim);
end
XfieldDMD = real(XfieldDMD);
cd('/mpidir_godzilla/mohan.69/VortexWingAnalysis/StatAnalysisVW/ATMCode/'); % Loaded Data, leaving Dir.

disp('Compute Mean')
% Compute Mean
XmeanDMD = mean(XfieldDMD,3);

plot_wh_meanc(y,x,XmeanDMD,XmeanDMD,21,2,1006)
colorbar
title('DMD Mean Flow Profile')
savep3d('MeanflowDMD',header,reshape(XmeanDMD,[gridSize 1]));
% cmax=1;
% cmin=cmax;
% grey_plot(cmin,cmax,1,1)
disp('DONE!!!')

%% Compute RMS for DMD flow
disp('Compute RMS')

e = zeros(xl,yl);
parfor i=1:nummodes
  temp = (XfieldDMD(:,:,i) - XmeanDMD).^2;
  e = e + temp;
end

Xstd_FullDMD = sqrt(e/(nummodes-1));

disp('DONE!!!')


%% Do FFT Analysis at any point for DMD flow

%close all
disp('FFT Analysis at any point')
% Pick point from plot

plot_wh_meanc(y,x,Xstd_FullDMD,XmeanDMD,21,2,1006)
colorbar
title('RMS of the DMD field')

probeSignalDMD = squeeze(XfieldDMD(Xi,Zi,1:1:end));
    
% FFT
% dt = timestep*delta;
% Fs = 1/dt;

tx=length(probeSignalDMD);
NFFT=2^nextpow2(tx);
mtx=0;
while NFFT>tx(1)
    NFFT=2^nextpow2(tx(1)-mtx);
    mtx=mtx+1;
end
%

%%%  perform spectrum raw signal
Overlap = NFFT/4;
[pxxDMD,fDMD] = cpsd(probeSignalDMD, probeSignalDMD, hanning(NFFT),Overlap, NFFT, Fs);

 %%%  plot spectrum raw signal
figure(12)  
nn=1;
semilogx(fDMD(nn:end),pxxDMD(nn:end).*fDMD(nn:end));
title('Spectra of Raw Signal at DMD Probe')
disp('DONE!!!')    

%% Perform EMD on DMD Probe Signal
disp('Perform EMD on Probe Signal')
tmpDMD = probeSignalDMD;
IMFDMD = emdH(tmpDMD-mean(tmpDMD));

%%%  perform & plot  imf + psd of imf
[nImfDMD,~]=size(IMFDMD);
bandT=(1:1:nummodes);
vec = 1:nummodes;
for i=1:nImfDMD
    figure(13)
    subplot(round(nImfDMD/2),2,i )
%     plot(nT(bandT),tmp(bandT)-mean(tmp),'r')
%     hold on
    sig=IMFDMD(i,:);
    plot(vec,sig(bandT))


    [pxx,f] = cpsd(sig, sig, hanning(NFFT), NFFT/2 , NFFT, Fs);
    nn=3;

    figure(14)
    semilogx(f(nn:end),pxx(nn:end).*f(nn:end));
    hold on
    title('Premul. Power Spectra of EMD Modes at DMD Probe');

end
disp('DONE!!!')

%% 2 Point Correlation with Probe Original EMD Signal and DMD field
% Pick which IMF you want to correlate with
IMFid = 2;
valref=IMF(IMFid,1:nummodes);
pas = 1;
nbins=200;

Pwall_corrDMD=zeros(xl,yl);
Pwall_coh_varDMD=zeros(xl,yl);
Pwall_corr_dtDMD=zeros(xl,yl,2*nbins+1);
disp('Computing 2D Correlation:')
parfor i=1:xl
    for j=1:yl

        tmp=squeeze(XfieldDMD(i,j,1:pas:end));
        
        val1=(tmp-mean(tmp) - AA(1:nummodes)');

        % Correlating EMD mode with Mean sub. Raw Signal 
        % At every point in the field
        RR = corrcoef(valref,val1);
        coeff_corr=RR(1,2);

        Pwall_corrDMD(i,j)=coeff_corr ;
        % nbins specifies min. and max. lag
        r=xcorr(valref,val1,nbins,'coeff');
        Pwall_corr_dtDMD(i,j,:)=(fliplr(r));

        rmax=max(sqrt(r.*r));
        Pwall_coh_varDMD(i,j)=rmax;

    end
    i
end
disp('DONE!!!')    

%% Plot with no lag
%pcolor(y,x,Pwall_corr), shading interp

XvalDMD=Pwall_corrDMD;
plot_wh_meanc(y,x,XvalDMD,XmeanDMD,pi,1,1005)
colorbar
title('2 Point Correlation of probe with DMD field')

hold on
plot(y(Zi),x(Xi),'o','MarkerSize',10,'MarkerEdgeColor','r','MarkerFaceColor','y')
str = sprintf('Correlation without Lag DMD - For IMF %d',IMFid);
title(str)
cmax=0.8;
cmin=cmax;
grey_plot(cmin,cmax,4,1)

figure(15)
plot( ((1:1:2*nbins+1).*dt)-mean( (1:1:2*nbins+1).*dt),squeeze(Pwall_corr_dtDMD(Xi,Zi,:)))
str2 = sprintf('Corr. Coeff plot DMD- For IMF %d',IMFid);
title(str2)
    