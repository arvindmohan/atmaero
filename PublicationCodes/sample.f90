! Testing stuff
program test
      real,allocatable,dimension(:,:,:) :: x,y,z
      integer il,jl,kl,i,j,k,nfuncs,v,f,kl2d,gridfilenums,delta
      character (len=80) :: grid_fname
      gridfilenums = 360
      delta = 1
   
      open(10,file='grid_span1',form='unformatted')
      read(10) il,jl,kl
      print*,'grid size is',il,jl,kl      
      allocate(x(il,jl,kl))
	  allocate(y(il,jl,kl))
	  allocate(z(il,jl,kl))
      read(10) x,y,z 
      print*,'max x val is',maxval(x(:,:,:)) 
      print*,'min x val is',minval(x(:,:,:))
      print*,'max y val is',maxval(y(:,:,:)) 
      print*,'min y val is',minval(y(:,:,:))
      print*,'max z val is',maxval(z(:,:,:)) 
      print*,'min z val is',minval(z(:,:,:))   
      
      !print*,shape(x)

      
      close(10)


end program test

