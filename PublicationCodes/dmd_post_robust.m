% Further DMD analysis based on Dr. Roy's AFRL report

clc
clear all
close all

% load data

cd('./Usnapsnew/results/');
disp('Loading Data file...')
load dmddynstall_u.mat


% Build Time coefficients 'a' for every mode 

disp('Building Time coefficients for every mode ');
a = zeros(nummodes,nummodes);
for j = 1:nummodes
    for i=1:nummodes
       a(i,j) = dOrder(j)*(ritzOrder(j))^(i);
    end
end

endtime = numofsnaps*dt;
time = linspace(0,endtime,nummodes);

% Compute Single sided Amp. spectrum for mode(s)
disp('Computing Single sided Amp. spectrum for every mode ');
cd('../../');

for j=1:nummodes
    [real_freq,real_fftamp(:,j)] = probe_fft(real(a(:,j)),dt);
    real_fftampnorm(:,j) = real_fftamp(:,j)/max(real_fftamp(:,j));
    [imag_freq,imag_fftamp(:,j)] = probe_fft(imag(a(:,j)),dt);
    imag_fftampnorm(:,j) = imag_fftamp(:,j)/max(imag_fftamp(:,j));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Extract only positive frequencies 
idx = find(imag(floqvalsOrder) >= 0);
mode_St = (imag(floqvalsOrder(idx)))/(2*pi);

% Compute Phase Angle for every mode

theta = zeros(nummodes,nummodes);

for i=1:nummodes
   theta(:,i) = atan2(imag(a(:,i)),real(a(:,i))); 
end

% % Compute difference between Imag parts to 
% % Search for harmonics
% diff_harmonics = NaN((length(ritzOrder)-1),1);
% for i=1:(length(ritzOrder)-1)
%    diff_harmonics(i) = abs(imag(ritzOrder(i))) - abs(imag(ritzOrder(i+1))); 
% end


% % Combining Harmonic Modes to a Single Structure
% harmonicmode = zeros(gridsize,1);
% for i=3:11
%    harmonicmode = harmonicmode + modes(:,i); 
% end
% 
% % Scaled Version
% 
% harmonicmode_scaled = zeros(gridsize,1);
% for i=3:11
%    harmonicmode_scaled = harmonicmode_scaled + modes_scaled(:,i); 
% end
% 
% 
% % Writing Harmonic Mode to ASCII PLOT3D file
% header = [xhead yhead zhead nvar];
% disp('Writing Harmonic Modeto Plot3D file');
% fid = fopen(['Harmonicmode_scaled' '.dat'], 'w');
% fprintf(fid, '%i \t',header);
% fprintf(fid,'\n');
% fprintf(fid, '%f \t',harmonicmode_scaled);
% fclose(fid);
% disp('====================================================')

% % Find Coupled Modes
% 
% % Lissajous Figures
% lins = 1:200;
% modex =  sin(theta(:,3)); %sin(lins); 
% modey =  sin(theta(:,5)); %sin(3*lins); 
% close all
% 
% figure
% plot(modex,modey)
% 
% figure
% subplot(2,1,1)
% plot(time,modex)
% subplot(2,1,2)
% plot(time,modey)
% 
% figure
% cor = xcorr(imag(a(:,3)),imag(a(:,7)));
% 
% plot(cor)

cd('./Usnapsnew/results/');
%% Plotting

% Plotting Time Trace

trace = figure(1);
set(gcf, 'Position', [pos(1) pos(2) 8*100, 5*100]); %<- Set size
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties

plot(time,real(a(:,1)),'+', ...
   time,real(a(:,3)),'r',...
   time,real(a(:,5)),'k',...
   time,real(a(:,7)),'c',...
   time,real(a(:,9)),'g',...
   time,real(a(:,11)),'-','LineWidth',2)
xlabel('Time(s)','Fontsize',14,'FontWeight','bold')
ylabel('Real(a(t))','Fontsize',14,'FontWeight','bold')
%title('Time trace of DMD Amplitude Real(Cn)','Fontsize',14,'FontWeight','bold')
grid on
hleg1 = legend('Mode 1','Mode 2','Mode 3','Mode 4','Mode 5','Mode 6');
set(hleg1,'Location','NorthEast');
xlim([0 26])

savefig(trace,'mode_timetrace')

% Export to EPS
epsname = ['DMDtrace' '.eps'];
set(gcf, 'PaperPositionMode', 'auto');
print(trace,'-depsc2',epsname)
close;
%saveas(trace,'mode_timetrace','png')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Plotting FFTs

slice = 91;
init_mode = 1;
final_mode = 11;

for i = init_mode:2:final_mode
     h = figure('Units', 'pixels', ...
    'Position', [100 100 500 375]);
     hold on;
     probef = line(real_freq(1:slice),real_fftampnorm(1:slice,i));
     
     % Adjust Line Properties (Functional)
     set(probef,'LineStyle', '-','Color','b');
     
     % Adjust Line Properties (Aesthetics)
     set(probef,'LineWidth',0.5);

     % Add Legend and Labels

%      hTitle  = title(['Single sided Amplitude spectrum of Probe ' ...
%        num2str(i) ' vs Global DMD spectrum']);
     hXLabel = xlabel('St. no');
     hYLabel = ylabel('Normalized Amplitude');
    % hLegend = legend([probef,dmdf],['Probe','',num2str(i)],'DMD Frequency','location', 'NorthEast');
     
     % Adjust Font and Axes Properties

    set( gca                       , ...
        'FontName'   , 'Helvetica' );
    set([hXLabel, hYLabel], ...
        'FontName'   , 'AvantGarde');
   % set([hLegend, gca]             , ...
   %     'FontSize'   , 8           );
    set([hXLabel, hYLabel]  , ...
        'FontSize'   , 10          );
%     set( hTitle                    , ...
%         'FontSize'   , 10         , ...
%         'FontWeight' , 'bold'      );

    set(gca, ...
      'Box'         , 'off'     , ...
      'TickDir'     , 'out'     , ...
      'TickLength'  , [.02 .02] , ...
      'XMinorTick'  , 'on'      , ...
      'YMinorTick'  , 'on'      , ...
      'YGrid'       , 'off'      , ...
      'XColor'      , [.3 .3 .3], ...
      'YColor'      , [.3 .3 .3], ...
      'XTick'       , 0:0.5:15, ...
      'YTick'       , 0:0.1:1, ...
      'LineWidth'   , 1         );
  
%     % Export to EPS
        epsname = [ num2str(i) 'modefft' 'DMD' '.eps'];
        %epsname = [ num2str(i) 'u' 'DMD' '.jpg'];
        set(gcf, 'PaperPositionMode', 'auto');
        print(h,'-depsc2',epsname)
        %print(h,'-djpeg ',epsname)
        close;
      
%       % Postprocess
% 
%       fixPSlinestyle(epsname,epsname);
     
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Phase Portraits
slice = 500;
init_mode = 1;
final_mode = 11;

for i=init_mode:2:final_mode

     h = figure('Units', 'pixels', ...
    'Position', [100 100 500 375]);
     hold on;
     probef = line(real(a(1:slice,i)),imag(a(1:slice,i)));
     
     % Adjust Line Properties (Functional)
     set(probef,'LineStyle', '-','Color','b');
     
     % Adjust Line Properties (Aesthetics)
     set(probef,'LineWidth',0.5);

     % Add Legend and Labels

%      hTitle  = title(['Single sided Amplitude spectrum of Probe ' ...
%        num2str(i) ' vs Global DMD spectrum']);
     hXLabel = xlabel('Real(a(t))');
     hYLabel = ylabel('Imag(a(t))');
    % hLegend = legend([probef,dmdf],['Probe','',num2str(i)],'DMD Frequency','location', 'NorthEast');
     
     % Adjust Font and Axes Properties

    set( gca                       , ...
        'FontName'   , 'Helvetica' );
    set([hXLabel, hYLabel], ...
        'FontName'   , 'AvantGarde');
   % set([hLegend, gca]             , ...
   %     'FontSize'   , 8           );
    set([hXLabel, hYLabel]  , ...
        'FontSize'   , 10          );
%     set( hTitle                    , ...
%         'FontSize'   , 10         , ...
%         'FontWeight' , 'bold'      );

    set(gca, ...
      'Box'         , 'off'     , ...
      'TickDir'     , 'out'     , ...
      'TickLength'  , [.02 .02] , ...
      'XMinorTick'  , 'on'      , ...
      'YMinorTick'  , 'on'      , ...
      'YGrid'       , 'off'      , ...
      'XColor'      , [.3 .3 .3], ...
      'YColor'      , [.3 .3 .3], ...
      'LineWidth'   , 1         );

%         'XTick'       , 0:100:1000, ...
%       'YTick'       , 0:0.1:1, ...
%     % Export to EPS
        epsname = [ num2str(i) 'modePhasePortrait' 'DMD' '.eps'];
        %epsname = [ num2str(i) 'u' 'DMD' '.jpg'];
        set(gcf, 'PaperPositionMode', 'auto');
        print(h,'-depsc2',epsname)
        %print(h,'-djpeg ',epsname)
       close;
      
    
end

% Comparing time trace

h = figure('Units', 'pixels', ...
'Position', [100 100 500 375]);
hold on;
probecompare = plot(time,orig1prb(1:end-1),time,recon1prb(1:end));
% Adjust Line Properties (Aesthetics)
set(probecompare,'LineWidth',0.75);

rms(recon1prb(1:end)-orig1prb(1:end-1))

% Add Legend and Labels
hXLabel = xlabel('Time(s)');
hYLabel = ylabel('Signal');
hLegend = legend(probecompare,'Original Signal','Reconstructed Signal','location', 'NorthEast');

% Adjust Font and Axes Properties

set( gca                       , ...
'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel], ...
'FontName'   , 'AvantGarde');
set([hLegend, gca]             , ...
     'FontSize'   , 8           );
set([hXLabel, hYLabel]  , ...
'FontSize'   , 10          );


set(gca, ...
'Box'         , 'off'     , ...
'TickDir'     , 'out'     , ...
'TickLength'  , [.02 .02] , ...
'XMinorTick'  , 'on'      , ...
'YMinorTick'  , 'on'      , ...
'YGrid'       , 'off'      , ...
'XColor'      , [.3 .3 .3], ...
'YColor'      , [.3 .3 .3], ...
'LineWidth'   , 1         );

%         'XTick'       , 0:100:1000, ...
%       'YTick'       , 0:0.1:1, ...
%     % Export to EPS
epsname = [ 'Reconcompare' 'DMD' '.eps'];
%epsname = [ num2str(i) 'u' 'DMD' '.jpg'];
set(gcf, 'PaperPositionMode', 'auto');
print(h,'-depsc2',epsname)
%print(h,'-djpeg ',epsname)
close;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


disp('DONE!!!');

disp('******************************')
disp('Run Complete.')
cd('../../');
disp('******************************')