function [f,amp] = probe_fft(x,dt)
x = x';
fs=1/dt;
L = length(x);
NFFT = 720; %2^nextpow2(L); % Next power of 2 from length of y
Y = fft(x,NFFT)/L;
f = fs/2*linspace(0,1,NFFT/2+1);
amp = 2*abs(Y(1:NFFT/2+1));

end