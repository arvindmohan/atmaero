%% Explore data 
cd('/mpidir_godzilla/mohan.69/ModelReduction/DynStallHighRes/JournalData/AnalysisData/codes/')

%slice domain
xl = 315; yl = 315; zspan = 51;
dim = [xl,yl];
nsnaps = 1200;
dt = 0.005; Fs = 1/dt;
init_file  = 1081;
height = 20;
% load field
load('grid3D.mat');
xmax = size(x,1);
ymax = size(x,2);
zmax = size(x,3);
dim_max = [xmax,ymax,zmax];
% load data
load dataU 
load dataV

ix= 1;jx = 315;
x2d = x(ix:xl,1:jx,zspan);
y2d = y(ix:xl,1:jx,zspan);

xd = xl - ix+1; yd = jx;


disp('Loaded Grid')
figure;
pcolor(x2d,y2d,dataU(:,:,280)) %NaN(xl,jx)
colormap jet
shading interp
set(gcf, 'Position',[100, 100, 1389, 700])
cd('/mpidir_godzilla/mohan.69/ModelReduction/DynStallHighRes/JournalData/AnalysisData/codes/')

%% load solution
cd('/mpidir_godzilla/mohan.69/ModelReduction/DynStallHighRes/FULL3D/MATsnaps/')
dataUf = zeros(xl,yl,nsnaps);dataVf = zeros(xl,yl,nsnaps);
tic
for i=1:nsnaps
   filenum = (init_file-1) + i*1; 
   %load(['DSHighRes3D' num2str(filenum) '.mat'],'U');
   flow = importdata(['DSHighRes3D' num2str(filenum) '.mat']);
   temp = reshape(flow.U,dim_max);
   dataU(:,:,i) = temp(1:xl,1:yl,zspan);
   temp = reshape(flow.V,dim_max);
   dataV(:,:,i) = temp(1:xl,1:yl,zspan);   
   disp(i)
end
disp('Loaded solution')
cd('/mpidir_godzilla/mohan.69/ModelReduction/DynStallHighRes/JournalData/AnalysisData/codes/')
toc 
load dataU dataV
%% play movie
figure;
j=1;
for i=1:nsnaps
    pcolor(x2d,y2d,dataU(:,:,i))
    shading interp
    colormap jet    
    drawnow
    K(j) = getframe(gcf);   
    %colorbar
    j = j+1;
    pause;
    set(gcf, 'Position',[100, 100, 1389, 700])
    disp(i)
end
disp('DONE')

%% Mean subtracted data

dataUmean = mean(dataU,3);
dataVmean = mean(dataV,3);
dataUfluc = zeros(xl,yl,600);
dataVfluc = zeros(xl,yl,600);

for i=1:600
   dataUfluc(:,:,i) = dataU(:,:,i) - dataUmean;   
   dataVfluc(:,:,i) = dataV(:,:,i) - dataVmean;   
end


%% Spectra at KHFR
% 
% M=1;
% sprintf('choose %2d point(s) on the plot ', M)
% [indy,indx]= ginput(M);
% 
% ind=find(y2d<=indx);
% Zi=ind(end);
% 
% ind=find(x2d<=indy);
% Xi=ind(end);
% 
% hold on
% plot(y2d(Zi),x2d(Xi),'o','MarkerSize',10,'MarkerEdgeColor','r','MarkerFaceColor','k')
% 
% idXi = [ceil(Xi/xd),mod(Xi,xd)];
% idZi = [ceil(Zi/xd),mod(Zi,xd)];
% 
% Ud = dataU(ix:xl,1:jx,300);
% Udfull = dataU(ix:xl,1:jx,300);

xid=262; yid=69;
sig = squeeze(dataU(xid,yid,1:1:end));


NFFT = floor(nsnaps/2);
[specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs);
figure;
plot(fsig(1:end),specsig(1:end))


%% Plot fieldplanes chronogram
chlen = 260;
channels = squeeze(dataVfluc(:,20,1:chlen));

[xg,yg] = meshgrid(1:xl,1:chlen);
hFig = figure;
%contourf(xg,yg,fieldplane')
% dat = squeeze(IMF(:,1,:));
%pcolor(xg,yg,channels')
contourf(xg,yg,channels',20)
%axis([-inf inf -inf inf -0.05 0.05])
shading interp
colormap jet
colorbar
xlabel('Chord Length')
ylabel('Time')
title('h=40')
set(hFig, 'Position', [800 800 1500 1000])

%% Analysis with Probe along chord
nprobes = 20;
% Generate Probes
% probeDataU = zeros(nprobes,600);
% probeDataV = zeros(nprobes,600);


xloc = 25;
probeDataUfluc(20,:) = squeeze(dataUfluc(xloc,height,:));
probeDataVfluc(20,:) = squeeze(dataVfluc(xloc,height,:));

% plot probes
figure;
for i=1:nprobes
   plot(1:chlen,probeDataUfluc(i,1:chlen));
   hold on
end

figure;
for i=1:nprobes
   plot(1:chlen,probeDataVfluc(i,1:chlen));
   hold on
end

% 2-point correlation

probeCorrsU = zeros(nprobes,(chlen*2)-1);
probeCorrsV = zeros(nprobes,(chlen*2)-1);

figure;
for i=1:nprobes
    [probeCorrsU(i,:),lags] = xcorr(probeDataU(20,1:chlen),probeDataU(i,1:chlen),'coeff');
    plot(lags,probeCorrsU(i,:))
    hold on
end

figure;
for i=1:nprobes
    [probeCorrsV(i,:),lags] = xcorr(probeDataV(20,1:chlen),probeDataV(i,1:chlen),'coeff');
    plot(lags,probeCorrsV(i,:))
    hold on
end

% MEMD for all probes
tic
disp('Computing Multivariate EMD')
n_proj = nprobes*10;
IMF_U = memd_mod(probeDataU(:,1:chlen),n_proj);
IMF_V = memd_mod(probeDataV(:,1:chlen),n_proj);
toc
disp('In minutes:')
disp(toc/60);
nIMF_U = size(IMF_U,2);
nIMF_V = size(IMF_V,2);
% Plot MEMD results

% Line IMF plot all channels - U
figure;
for j=1:nIMF_U
    subplot(nIMF_U,1,j)
    plot(1:chlen,squeeze(IMF_U(:,j,:)))
    hold on
    xlabel(num2str(j));
end

% IMF spectra all channels

figure;
for j=1:nIMF_U
    
    subplot(nIMF_U,1,j)
    for i=1:nprobes
          sig = squeeze(IMF_U(i,j,:)); 
          NFFT = floor(length(sig)/2);
          [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs);
          plot(fsig,specsig);
          hold on
    end
    xlabel(num2str(j));
end


% Line IMF plot all channels - V
figure;
for j=1:nIMF_V
    subplot(nIMF_V,1,j)
    plot(1:chlen,squeeze(IMF_V(:,j,:)))
    hold on
    xlabel(num2str(j));
end

% IMF spectra all channels

figure;
for j=1:nIMF_V
    
    subplot(nIMF_V,1,j)
    for i=1:nprobes
          sig = squeeze(IMF_V(i,j,:)); 
          NFFT = floor(length(sig)/2);
          [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs);
          plot(fsig,specsig);
          hold on
    end
    xlabel(num2str(j));
end


%% Correlate MEMD IMFs

% 2-point correlation

probeIMFCorrsU = zeros(nprobes,nIMF_U,(chlen*2)-1);
probeIMFCorrsV = zeros(nprobes,nIMF_V,(chlen*2)-1);
IMFid = 8;
figure;
for i=1:nprobes
    [probeIMFCorrsU(i,1,:),lags] = xcorr(squeeze(IMF_U(20,IMFid,1:chlen)),squeeze(IMF_U(i,IMFid,1:chlen)),'coeff');
    plot(lags,squeeze(probeIMFCorrsU(i,1,:)))
    hold on
end
title(num2str(IMFid))


IMFid = 6;
figure;
for i=1:nprobes
    [probeIMFCorrsV(i,1,:),lags] = xcorr(squeeze(IMF_V(20,IMFid,1:chlen)),squeeze(IMF_V(i,IMFid,1:chlen)),'coeff');
    plot(lags,squeeze(probeIMFCorrsV(i,1,:)))
    hold on
end
title(num2str(IMFid))


% 2-point correlation coeffs
clear probeIMFCorrCoeffs*
probeIMFCorrCoeffsU = zeros(nprobes,nIMF_U);
probeIMFCorrCoeffsV = zeros(nprobes,nIMF_V);

% U vel
for i=1:nIMF_U
    for j=1:nprobes
        temp =  corrcoef(squeeze(IMF_U(1,i,1:chlen)),squeeze(IMF_U(j,i,1:chlen)));
        probeIMFCorrCoeffsU(j,i) = temp(1,2);
    end
end

figure;
for i=1:nIMF_U
    plot(1:nprobes,probeIMFCorrCoeffsU(:,i),'-o')
    hold on
end

% V vel
for i=1:nIMF_V
    for j=1:nprobes
        temp =  corrcoef(squeeze(IMF_V(1,i,1:chlen)),squeeze(IMF_V(j,i,1:chlen)));
        probeIMFCorrCoeffsV(j,i) = temp(1,2);
    end
end

figure;
for i=1:nIMF_V
    plot(1:nprobes,probeIMFCorrCoeffsV(:,i),'-o')
    hold on
end


%% MEMD on different sections of the flow-field

nprobesSection = 11;
chlenSection = 75;
% 
% probeDataSectionU = probeDataU(1:11,76:(chlenSection*2));
% probeDataSectionV = probeDataV(1:11,76:(chlenSection*2));

% plot probes
figure;
for i=1:nprobesSection
   plot(1:chlenSection,probeDataSectionU(i,:));
   hold on
end
title('U velocity')

figure;
for i=1:nprobesSection
   plot(1:chlenSection,probeDataSectionV(i,:));
   hold on
end
title('V velocity')

% MEMD for all probes
tic
disp('Computing Multivariate EMD')
n_proj = nprobesSection*10;
IMFSection_U = memd_mod(probeDataSectionU,n_proj);
IMFSection_V = memd_mod(probeDataSectionV,n_proj);
toc
disp('In minutes:')
disp(toc/60);
nIMFSection_U = size(IMFSection_U,2);
nIMFSection_V = size(IMFSection_V,2);

% Line IMF plot all channels - U
figure;
for j=1:nIMFSection_U
    subplot(nIMFSection_U,1,j)
    plot(1:chlenSection,squeeze(IMFSection_U(:,j,:)))
    hold on
    xlabel(num2str(j));
end
title('MEMD IMFs')

% IMF spectra all channels
figure;
for j=1:nIMFSection_U
    
    subplot(nIMFSection_U,1,j)
    for i=1:nprobesSection
          sig = squeeze(IMFSection_U(i,j,:)); 
          NFFT = floor(length(sig)/2);
          [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs);
          plot(fsig,specsig);
          hold on
    end
    xlabel(num2str(j));
end
title('MEMD IMF spectra')

% Line IMF plot all channels - V
figure;
for j=1:nIMFSection_V
    subplot(nIMFSection_V,1,j)
    plot(1:chlenSection,squeeze(IMFSection_V(:,j,:)))
    hold on
    xlabel(num2str(j));
end
title('MEMD IMFs')

% IMF spectra all channels
figure;
for j=1:nIMFSection_V
    
    subplot(nIMFSection_V,1,j)
    for i=1:nprobesSection
          sig = squeeze(IMFSection_V(i,j,:)); 
          NFFT = floor(length(sig)/2);
          [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs);
          plot(fsig,specsig);
          hold on
    end
    xlabel(num2str(j));
end
title('MEMD IMF spectra')


%% Joint PDFs
% sig1 = probeDataSectionU(1,:);
% sig2 = probeDataSectionV(1,:);
sig1 = squeeze(dataU(50,50,:));
sig2 = squeeze(dataV(50,50,:));
bins1=10; bins2=5;
nfig=11;
WJ_pdf_plot_deploy(sig1,sig1,bins1,bins2,nfig,1)
corrcoef(sig1,sig2)
%% Chordwise spatial statistics
close all;
clear chordChannels
clc
%define chordwise dt and Fs
Fs_chord =  315; dt_chord = 1/Fs_chord; 
chordChannels = squeeze(dataU(:,height,:));
spanLength = size(chordChannels,1);

% U velocity
%plot signals
figure;
j=1;
for i=170:2:230
   %plot(1:spanLength,chordChannels(:,i));
   plot(chordChannels(:,i));
   chordStage2(:,j) = chordChannels(:,i);
   hold on
   j=j+1;
end

% plot stats to check disturbance presence
distStats = zeros(3,41);
j=1; 
for i=50:1:110
   temp = chordChannels(:,i);
   %temp = temp - mean(temp); same result
   distStats(1,j) = std(temp);
   distStats(2,j) = skewness(temp);
   distStats(3,j) = kurtosis(temp);
   j=j+1;
end

figure;
subplot(3,1,1)
plot(50:1:110,distStats(1,:));
title('Std')
subplot(3,1,2)
plot(50:1:110,distStats(2,:));
title('Skewness')
subplot(3,1,3)
plot(50:1:110,distStats(3,:));
title('Kurtosis')

figure;hist(distStats(2,:))

% first 25 timesteps
figure;
for i=50:1:110
      sig = squeeze(chordChannels(:,i)); 
      NFFT = floor(length(sig)/2);
      [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_chord);
      plot(fsig(2:end),specsig(2:end));
      hold on
end

% MEMD of chordwise signals
clear IMFspan*
tic
disp('Computing Multivariate EMD')
n_proj = 20*10;
IMFspan_U = memd_mod(chordChannels(:,170:190),n_proj);
toc
disp('In minutes:')
disp(toc/60);
nIMFspan_U = size(IMFspan_U,2);

% Line IMF plot all channels - U
figure;
for j=1:nIMFspan_U
    subplot(nIMFspan_U,1,j)
    plot(1:xl,squeeze(IMFspan_U(:,j,:)))
    hold on
    xlabel(num2str(j));
end
title('MEMD IMFs')

% IMF spectra all channels
figure;
for j=1:nIMFspan_U
    
    subplot(nIMFspan_U,1,j)
    for i=1:11
          sig = squeeze(IMFspan_U(i,j,:)); 
          NFFT = floor(length(sig)/2);
          [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_chord);
          plot(fsig,specsig);
          hold on
    end
    xlabel(num2str(j));
end
title('MEMD IMF spectra')

%**********CHORD CHANNEL MIX****************************%
% Create channels with component in t=130-170
% STAGE 1: t=130-170, STAGE 2: t=170:210 ?? (bounds subject to change
% slightly)
%chordChannelMix = zeros(xl,20);
j=1;
for i=170:5:210
   chordChannelMix(:,j) =  chordChannels(:,i);
   j=j+1;
   i
end


%plot signals
figure;
for i=1:9
   %plot(1:spanLength,chordChannels(:,i));
   plot(chordChannelMix(:,i));
   hold on
end

% MEMD of chordwise signals
clear IMFspan*
tic
disp('Computing Multivariate EMD')
n_proj = 9*10;
IMFspanMix_U = memd_mod(chordChannelMix,n_proj);
toc
disp('In minutes:')
disp(toc/60);
nIMFspanMix_U = size(IMFspanMix_U,2);


% Line IMF plot all channels - U
figure;
for j=1:nIMFspanMix_U
    subplot(nIMFspanMix_U,1,j)
    plot(1:xl,squeeze(IMFspanMix_U(:,j,:)))
    hold on
    xlabel(num2str(j));
end
title('MEMD IMFs')

% IMF spectra all channels
figure;
for j=1:nIMFspanMix_U
    
    subplot(nIMFspanMix_U,1,j)
    for i=1:9
          sig = squeeze(IMFspanMix_U(i,j,:)); 
          NFFT = floor(length(sig)/2);
          [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_chord);
          plot(fsig,specsig);
          hold on
    end
    xlabel(num2str(j));
end
title('MEMD IMF spectra')


% Wavelet Analysis
signal = squeeze(IMFspanMix_U(1,2,:));
%figure;plot(1:xl,signal)
sig = struct('val',signal,'period',dt_chord);
cwtS1 = cwtft(sig);
scales = cwtS1.scales;
MorletFourierFactor = 4*pi/(6+sqrt(2+6^2));
freq = 1./(scales.*MorletFourierFactor);
figure;
contourf(1:xl,freq,real(cwtS1.cfs));
colorbar
xlabel('Chord-distance'); ylabel('Pseudo-Wavenumber');

% %Spectrogram Analysis
% signal = squeeze(IMFspanMix_U(1,1,:));
% NFFT = floor(length(signal)/2);
% [s,f,t] = spectrogram(signal,hanning(NFFT),ceil(NFFT/2),NFFT,Fs_chord);
% figure; plot(f,imag(s(:,1)))

% HHT Instantaneous Frequency
[Amp,Freq]  = INST_FREQ_mod(signal',Fs_chord);
figure;plot(2:xl,Freq(2:end))

% % Write into text file for Tecplot
% output = [(1:xl);freq;real(cwtS1.cfs)];
% fname = 'cwtDynStall.dat';
% techeader = ['xl,','freq','coeffs'];
% fileID = fopen(fname,'w');
% fprintf(fileID,techeader);
% fprintf(fileID,'\n');
% fprintf(fileID,'%d %f %f\n',output);
% fclose(fileID);
% disp('written tecplot data!!!')


%==============================================================%
%******* STAGE 0 PRE-STALL ANALYSIS *****************%
%==============================================================%

% U velocity
%plot signals
close all
figure;
%clear chordStage1
for i=1:31
   plot(chordStage0(:,i));
   hold on
end

% Spectra of signals
figure;
for i=1:31
      sig = squeeze(chordStage0(:,i)); 
      NFFT = floor(length(sig)/2);
      [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_chord);
      plot(fsig(2:end),specsig(2:end));
      hold on
end

% MEMD of chordwise signals
clear IMFspan*
tic
disp('Computing Multivariate EMD')
n_proj = size(chordStage0,2)*10;
IMFspanStage0_U = memd_mod(chordStage0,n_proj);
toc
disp('In minutes:')
disp(toc/60);
nIMFspanStage0_U = size(IMFspanStage0_U,2);

% Line IMF plot all channels - U
figure;
for j=1:nIMFspanStage0_U
    subplot(nIMFspanStage0_U,1,j)
    plot(1:xl,squeeze(IMFspanStage0_U(:,j,:)))
    hold on
    xlabel(num2str(j));
end
title('MEMD IMFs')

% IMF spectra all channels
figure;
for j=1:nIMFspanStage0_U
    
    subplot(nIMFspanStage0_U,1,j)
    for i=1:31
          sig = squeeze(IMFspanStage0_U(i,j,:)); 
          NFFT = floor(length(sig)/2);
          [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_chord);
          plot(fsig,specsig);
          hold on
    end
    xlabel(num2str(j));
end
title('MEMD IMF spectra')

% HHT Instantaneous Frequency
figure;
IMFid = 1;
for i=1:31
   [~,Freq]  = INST_FREQ_mod(squeeze(IMFspanStage0_U(i,IMFid,:))',Fs_chord);
   plot(3:313,Freq(3:end-2))
   hold on
end

%=====Flow Physics========%

%analyze surface pressure with time
pres = squeeze(dataP(:,1,1:600));

figure;
for i=110:2:170
    plot(1:xl,pres(:,i))
    hold on
end













%==============================================================%
%******* STAGE 1 STALL INITIATION ANALYSIS *****************%
%==============================================================%

% U velocity
%plot signals
close all
figure;
%clear chordStage1
for i=1:31
   plot(chordStage1(:,i));
   hold on
end


% Spectra of signals
figure;
for i=1:31
      sig = squeeze(chordStage1(:,i)); 
      NFFT = floor(length(sig)/2);
      [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_chord);
      plot(fsig(2:end),specsig(2:end));
      hold on
end


% MEMD of chordwise signals
% clear IMFspan*
tic
disp('Computing Multivariate EMD')
n_proj = size(chordStage1,2)*10;
IMFspanStage1_U = memd_mod(chordStage1,n_proj);
toc
disp('In minutes:')
disp(toc/60);
nIMFspanStage1_U = size(IMFspanStage1_U,2);

% Line IMF plot all channels - U
figure;
for j=1:nIMFspanStage1_U
    subplot(nIMFspanStage1_U,1,j)
    plot(1:xl,squeeze(IMFspanStage1_U(:,j,:)))
    hold on
    xlabel(num2str(j));
end
title('MEMD IMFs')

% IMF spectra all channels
figure;
for j=1:nIMFspanStage1_U
    
    subplot(nIMFspanStage1_U,1,j)
    for i=1:31
          sig = squeeze(IMFspanStage1_U(i,j,:)); 
          NFFT = floor(length(sig)/2);
          [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_chord);
          plot(fsig,specsig);
          hold on
    end
    xlabel(num2str(j));
end
title('MEMD IMF spectra')

% HHT Instantaneous Frequency
figure;
IMFid = 1;
for i=1:31
   [~,Freq]  = INST_FREQ_mod(squeeze(IMFspanStage1_U(i,IMFid,:))',Fs_chord);
   plot(3:313,Freq(3:end-2))
   hold on
end

%==============================================================%
%******* STAGE 2 STALL PROPAGATION ANALYSIS *****************%
%==============================================================%

% U velocity
%plot signals
close all
figure;
%clear chordStage1
for i=1:31
   plot(chordStage2(:,i));
   hold on
end


% Spectra of signals
figure;
for i=1:31
      sig = squeeze(chordStage2(:,i)); 
      NFFT = floor(length(sig)/2);
      [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_chord);
      plot(fsig(2:end),specsig(2:end));
      hold on
end


% MEMD of chordwise signals
clear IMFspan*
tic
disp('Computing Multivariate EMD')
n_proj = size(chordStage2,2)*10;
IMFspanStage2_U = memd_mod(chordStage2,n_proj);
toc
disp('In minutes:')
disp(toc/60);
nIMFspanStage2_U = size(IMFspanStage2_U,2);

% Line IMF plot all channels - U
figure;
for j=1:nIMFspanStage2_U
    subplot(nIMFspanStage2_U,1,j)
    plot(1:xl,squeeze(IMFspanStage2_U(:,j,:)))
    hold on
    xlabel(num2str(j));
end
title('MEMD IMFs')

% IMF spectra all channels
figure;
for j=1:nIMFspanStage2_U
    
    subplot(nIMFspanStage2_U,1,j)
    for i=1:31
          sig = squeeze(IMFspanStage2_U(i,j,:)); 
          NFFT = floor(length(sig)/2);
          [specsig,fsig] = cpsd(sig, sig, hanning(NFFT),NFFT/2, NFFT, Fs_chord);
          plot(fsig,specsig);
          hold on
    end
    xlabel(num2str(j));
end
title('MEMD IMF spectra')

% HHT Instantaneous Frequency
figure;
IMFid = 1;
for i=1:31
   [~,Freq]  = INST_FREQ_mod(squeeze(IMFspanStage2_U(i,IMFid,:))',Fs_chord);
   plot(3:313,Freq(3:end-2))
   hold on
end





























































