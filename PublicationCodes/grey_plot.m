function [suffix2]=grey_plot(cmin,cmax,pas,mino)

            suffix2='';
            cmap=colormap;   
            cmap2=cmap;
            tt=size(cmap,1);
                  
        
              
              
             ht=cmin/(cmax+cmin);
             
            
              if (ht)<=0.05;
                  ht=0;
                   cc=1:round(ht*tt)+pas+1;
              else
                  cc=round(ht*tt)-pas:round(ht*tt)+pas+1; 
              end
   
            caxis([-cmin cmax])
           
       
  
            
            if mino==1
            
            suffix2='_grey';   
            %cmap2(cc,:)=0.5;
            cmap2(cc,:)=1;
            colormap(cmap2)
            %colorbar
            end
end