program readgrid
  ! program to read in the grid
  real, dimension(:,:,:), allocatable :: x,y,z

  open(10,file='grid_span1',form='UNFORMATTED',status='OLD')
  read(10) il,jl,kl
  print*,'il,jl,kl=',il,jl,kl
  allocate (x(il,jl,kl))
  allocate (y(il,jl,kl))
  allocate (z(il,jl,kl))
  read(10) x,y,z
  close(10)

  print*,'minval x ',minval(x),maxval(x)
  print*,'minval y ',minval(y),maxval(y)
  print*,'minval z ',minval(z),maxval(z)
  !print x
  stop
end program readgrid
