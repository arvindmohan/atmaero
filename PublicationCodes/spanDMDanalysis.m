%% Spanwise DMD

clc;clear;close all

%slice domain
xl = 315; yl = 315; zspan = 51;
dim = [xl,yl];
nsnaps = 600;
dt = 0.005;
init_file  = 1081;

% load field
load('grid3D.mat');
xmax = size(x,1);
ymax = size(x,2);
zmax = size(x,3);
dim_max = [xmax,ymax,zmax];

% height
height = 14;
z2d = squeeze(z(1:xl,height,1:zmax));
x2d = squeeze(x(1:xl,height,1:zmax));
% Load data
data = zeros(xl,zmax,2);
cd('/mpidir_godzilla/mohan.69/ModelReduction/DynStallHighRes/FULL3D/MATsnaps/')

for i=1:nsnaps
   filenum = (init_file-1) + i*1; 
   load(['DSHighRes3D' num2str(filenum) '.mat'],'U');
   temp = reshape(U,dim_max);
   data(:,:,i) = squeeze(temp(1:xl,height,1:zmax));
   disp(i)
end
disp('DONE')

cd('/mpidir_godzilla/mohan.69/ModelReduction/DynStallHighRes/JournalData/AnalysisData/codes/')

% play movie

j=1;
for i=1:nsnaps
    pcolor(x2d,z2d,data(:,:,i))
    shading interp
    colormap jet    
    drawnow
    K(j) = getframe(gcf);   
    colorbar
    j = j+1;
    %pause(1/24);
    set(gcf, 'Position',[100, 100, 1389, 700])
end
disp('DONE')

%********** perform DMD**************%
data2d = reshape(data,[xl*zmax,nsnaps]);
[modes,mode_ampl,floqvalsOrder,freqs,dOrder] = dmd_function(data2d,dt);
nummodes = size(modes,2);

%***** Viz. DMD results
temp = reshape(modes,[xl,zmax,nsnaps-1]);

figure;
pcolor(x2d,z2d,real(temp(:,:,3)))
shading interp
colormap jet    
set(gcf, 'Position',[100, 100, 1389, 700])

% % Viz. modes
% j=1;
% for i=1:nsnaps
%     pcolor(x2d,z2d,real(temp(:,:,1)))
%     shading interp
%     colormap jet    
%     drawnow
%     K(j) = getframe(gcf);   
%     colorbar
%     j = j+1;
%     pause;
%     set(gcf, 'Position',[100, 100, 1389, 700])
% end
% disp('DONE')

IndPos = find(freqs >= 0);
modesPos = modes(:,IndPos);
freqsPos = freqs(IndPos);
[~,ind] = sort(freqsPos,'descend');
freqsHigh = freqsPos(ind);
modesHighFreq = modesPos(:,ind);
numPos = size(IndPos,1);
ampsPos = mode_ampl(IndPos);

figure;
modeProfile = zeros(numPos,xl);
finit = 1;
fend = 300; %numPos;
for i = finit:fend
   temp = real(reshape(modesHighFreq(:,i),[xl,zmax]));
   modeProfile(i,:) = temp(:,zspan);
   %set(hFig6, 'Position', [800 800 1500 1000])
   plot(1:xl,modeProfile(i,:))
   title(['St No.',num2str(freqsHigh(finit)),'to',num2str(freqsHigh(fend))])
   hold on
end

% convert grid location to actual chord length location
chordDx = 1/xl;
% figure;
% pcolor(x2d(1:xl,1:height),y2d(1:xl,1:height),NaN(xl,height))


%% ******** identify TE KH spatial support
TEbegin = 279; TEend = xl;
figure;
plot(1:xl,modeProfile(275,:))
limits = [1 xl -0.05 0.05];
axis(limits)

clear modesDiff modesTE En TEid

% Extract TE

std(modeProfile(1,TEbegin:TEend))
TEtol = 8e-04;


j=1;
for i=1:numPos
   En(i) = std(modeProfile(i,TEbegin:TEend));
   
   if En(i) >= TEtol
      TEid(j) = i;
       j=j+1;
   end
end

figure;
plot(En)

% plot modes with strong TE KHFR
figure;
for i=1:length(TEid)
   id = TEid(i);
   plot(1:xl,modeProfile(id,:))
   hold on
end
axis(limits)
title('strong TE KHFR modes')

% compare
modesTE = zeros(numPos,xl);

for i=1:length(TEid)
   id = TEid(i);
   modesTE(id,:) = modeProfile(id,:); 
end

modesDiff = modeProfile - modesTE;

figure;
for i=1:numPos
   plot(1:xl,modesDiff(i,:))
   hold on 
end
axis(limits)
title('Modes without strong TE KHFR')

% identify freqs and ampl. in strong KHFR

for i=1:length(TEid)
   id = TEid(i);
   KHFRfreq(i) = freqsPos(id); 
   KHFRamp(i) = ampsPos(id);
end

figure;
scatter(1:length(TEid),KHFRfreq)
title('Freq. distribution of Strong KHFR modes')

figure;
semilogy(KHFRfreq,KHFRamp,'o')
title('Freq.-Amp distribution of Strong KHFR modes')





