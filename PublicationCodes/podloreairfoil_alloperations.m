%% Proper Orthogonal Decomposition and Reconstruction - ALL operations
% Computing Amplitude,Modes, Reconstruction. --> NACA 0015 Airfoil
% Snapshot Method , Sirovich et. al./Lumley et. al.
% Arvind T. Mohan
% Aerospace Engineering,HFCMPL - The Ohio State University

close all 
clear all
clc
disp('Proper Orthogonal Decomposition of Flow Data')
disp('DYNAMIC STALL CASE')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
disp('High Fidelity Computational Multiphysics Lab - The Ohio State University')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

%% USER INPUT
cd('./Usnaps');
pwd
numofsnaps = 720;
file_delta = 1;
delta = 1;
timestep = 1; % From CFD solver - non-DIMENSIONAL TIME FOR EVERY 'DT'
init_file  = 1;%  <== CHANGE if needed.
final_file = init_file + ((numofsnaps)*file_delta) - file_delta;
coherence = 'no';
reconmodes = numofsnaps;
meanfname = 'Umean';

% Header File info for Plot3d output
xhead = 315;
yhead = 315;
zhead = 1;
nvar = 1;

%% POD algorithm 
disp('Reading In Snapshot Data...');
clear temp

m=1;
for i=init_file:file_delta:final_file
    u(:,m)=importdata(['u' num2str(i) '.mat']);
    m=m+1;
end
disp('Operation Successful.');

[gridsize,Nsnap] = size(u);

Umean(:,1)=(mean(u'))';

%*********  Let's build the Snapshot POD kernel C  ************
C = zeros(Nsnap,Nsnap);

disp('Calculating Covariance matrix');
C = u'*u; 
C = C/Nsnap ; 

disp('Computing Eigendecomposition of Covariance Matrix...')
[EigenVect,EigenValues] = eig(C);  
disp('Operation Successful.');

clear C
lambda = flipud(diag(EigenValues));  
clear EigenValues
aPOD = fliplr(EigenVect); 
clear EigenVect
disp('====================================================')
%******* aPOD is the POD temporal coefficient
[times,modes]=size(aPOD); 

for j = 1:modes,
    aPOD(:,j) = aPOD(:,j)*sqrt(lambda(j)*Nsnap);
end
save aPOD_testing aPOD;

phiU = zeros(gridsize,modes);

phiU = u*aPOD;

disp('Calculating Modes');
for j=1:modes,  
    phiU(:,j)=phiU(:,j)/(lambda(j)*Nsnap); 
end
disp('Operation Successful.');
disp('====================================================')
phi=[phiU];
save Phi phi
clear phi

total = sum(lambda);
individual = lambda*100/total;
Energy = zeros(modes,1);
E = 0;

disp('Calculating Energy')
for j = 1:modes,    
    Energy(j) = E + individual(j);
    E = Energy(j);
end
disp('Operation Successful.');
disp('====================================================')

cd('./PODresults');

%% Writing modes to ASCII PLOT3D files
disp('Writing POD modes to Plot3D files');
header = [xhead yhead zhead nvar];
for i=1:modes
fid = fopen(['dynstallPODmode_u' num2str(i) '.dat'], 'w');
%disp(['Writing file nocontrolUPODmode' num2str(i) '.dat']);
fprintf(fid, '%i \t',header);
fprintf(fid,'\n');
fprintf(fid, '%f \t',phiU(:,i));
fclose(fid);
end

disp('====================================================')
%% POD reconstructed data to ASCII PLOT3D files
disp('Computing Reconstructed Data . . .');
nummodes = modes;
reconmodes = nummodes;
podrecon = phiU(:,1:reconmodes)*aPOD(:,1:reconmodes)';
disp('Operation Successful.');

% disp('====================================================')
% % Writing Reconstructed data to ASCII PLOT3D files
% fname = strcat('PODdynstall',num2str(reconmodes),'reconstructed');
% disp('Writing Reconstructed Data to Plot3D files');
% header = [xhead yhead zhead nvar];
% for i=1:nummodes
% fid = fopen([fname num2str(i) '.dat'], 'w');
% %disp(['Writing file ' fname num2str(i) '.dat']);
% fprintf(fid, '%i \t',header);
% fprintf(fid,'\n');
% fprintf(fid, '%f \t',podrecon(:,i));
% fclose(fid);
% end

disp('====================================================')

% Overall Reconstruction Error
error_recon = zeros(gridsize,nummodes);
for i=1:nummodes
 error_recon(:,i) = podrecon(:,i) - u(:,i);
end

error_sum = sum(sum(error_recon.^2));
u_sum = sum(sum(u.^2));
error_total = sqrt(error_sum)/sqrt(u_sum);
error = abs(error_total)*100;
fprintf('Total Reconstruction Error (percent) using %d modes is:\n',reconmodes);
disp(error);
disp('====================================================')

% Output Reconstruction Error for every mode to ASCII PLOT3D files
% 
% fname = strcat('PODError',num2str(reconmodes),'recons');
% disp('Writing PODError to Plot3D files');
% header = [xhead yhead zhead nvar];
% for i=1:nummodes
% fid = fopen([fname num2str(i) '.dat'], 'w');
% %disp(['Writing file ' fname num2str(i) '.dat']);
% fprintf(fid, '%i \t',header);
% fprintf(fid,'\n');
% fprintf(fid, '%f \t',error_recon(:,i));
% fclose(fid);
% end

%% Plotting

% Plotting Energy for every mode
energyplot = figure(1);
%pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) 10*100, 5*100]); %<- Set size
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
modenumbers = 1:modes;
s = 25;
scatter(modenumbers,Energy,s,'MarkerEdgeColor','b','MarkerFaceColor','r','LineWidth',1.5)
xlabel('Mode(s)','Fontsize',14,'FontWeight','bold')
ylabel('Energy','Fontsize',14,'FontWeight','bold')
title('Plotting Energy contribution of every mode','Fontsize',14,'FontWeight','bold')
grid on
savefig(energyplot,'Energy_PODmodes')
saveas(energyplot,'Energy_PODmodes','png')


% Plotting Time Trace
dt = timestep*delta; % Actual time interval
total = (nummodes-1)*dt;
time = (0:dt:total);

trace = figure(2);
set(gcf, 'Position', [pos(1) pos(2) 10*100, 5*100]); %<- Set size
set(gca, 'FontSize', 11, 'LineWidth', 0.75); %<- Set properties
plot(time,aPOD(:,1),'b', ...
    time,aPOD(:,2),'r',...
    time,aPOD(:,3),'k',...
    time,aPOD(:,4),'c',...
    time,aPOD(:,5),'g','LineWidth',2)
xlabel('Time(s)','Fontsize',14,'FontWeight','bold')
ylabel('Cm','Fontsize',14,'FontWeight','bold')
title('Time trace of POD Amplitude','Fontsize',14,'FontWeight','bold')
grid on
hleg1 = legend('Mode 1 (Mean)','Mode 2','Mode 3','Mode 4','Mode 5');
set(hleg1,'Location','NorthEast');

savefig(trace,'PODmode_timetrace')
saveas(trace,'PODmode_timetrace','png')


%% Save and Exit.
disp('====================================================')
disp('Saving Complete Analysis MAT file to: ');
disp(pwd); 

save poddynstall_u -v7.3  %modes_nocontrol modes 

disp('DONE!!!');

disp('******************************')
disp('Run Complete.')
disp('******************************')

cd('../../')