% Read Vortex Wing Final Data

% Read Grid
fid = fopen('suction_side.x','r','ieee-le');
ng = fread(fid,1,'int');
dim = fread(fid,[3,ng],'int')
x = reshape(fread(fid,prod(dim),'double'),dim');
y = reshape(fread(fid,prod(dim),'double'),dim');
z = reshape(fread(fid,prod(dim),'double'),dim');
fclose(fid);

% Read Solution

fid = fopen('probe-200000.q','r','ieee-le');
ng = fread(fid,1,'int');
dim = fread(fid,[3,ng],'int')
dat = fread(fid,4,'double');
rho = reshape(fread(fid,prod(dim),'double'),dim');
rhou = reshape(fread(fid,prod(dim),'double'),dim');
rhov = reshape(fread(fid,prod(dim),'double'),dim');
rhow = reshape(fread(fid,prod(dim),'double'),dim');
rhoe = reshape(fread(fid,prod(dim),'double'),dim');
fclose(fid);

i=
fname = ['probe-' num2str(i*50) '.q'];
fid = fopen('probe-200000.q','r','ieee-le');
ng = fread(fid,1,'int');
dim = fread(fid,[3,ng],'int')
dat = fread(fid,4,'double');
rho = reshape(fread(fid,prod(dim),'double'),dim');
rhou = reshape(fread(fid,prod(dim),'double'),dim');
rhov = reshape(fread(fid,prod(dim),'double'),dim');
rhow = reshape(fread(fid,prod(dim),'double'),dim');
rhoe = reshape(fread(fid,prod(dim),'double'),dim');
fclose(fid);