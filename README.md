**Arvind T. Mohan**

_PhD Candidate_

Aerospace Engineering

The Ohio State University

2013  Master of Science, Aerospace Engineering, The Ohio State University, Columbus, Ohio


2011  Bachelor of Engineering, Mechanical Engineering, Government College of Engineering, Salem, Tamil Nadu, India

This repo includes several of my work samples, which are categorized into multiple folders. 

**Links to my publications/presentations**

1) Model reduction and analysis of deep dynamic stall on a plunging airfoil 
http://www.sciencedirect.com/science/article/pii/S0045793016300093

2) Analysis of Airfoil Stall Control Using Dynamic Mode Decomposition
https://arc.aiaa.org/doi/abs/10.2514/1.C034044?journalCode=ja

3) Model Reduction and Analysis of Deep Dynamic Stall on a Plunging Airfoil using Dynamic Mode Decomposition
https://arc.aiaa.org/doi/abs/10.2514/6.2015-1058

4) Statistical Analysis and Model Reduction of Surface Pressure for Interaction of a Streamwise-Oriented Vortex with a Wing
https://arc.aiaa.org/doi/abs/10.2514/6.2015-3412

5) A Preliminary Spectral Decomposition and Scale Separation Analysis of a High-Fidelity Dynamic Stall Dataset
https://arc.aiaa.org/doi/abs/10.2514/6.2016-1352

6) Contrasting Modal Decompositions of Flow Fields with & without Control
 https://arc.aiaa.org/doi/abs/10.2514/6.2015-0301
 
7) Model Reduction and Analysis of NS-DBD Based Control of Stalled NACA0015 Airfoil
http://proceedings.asmedigitalcollection.asme.org/proceeding.aspx?articleid=2086807

8) Analysis of non-stationary turbulent flows using Multivariate EMD and Matching Pursuits
http://adsabs.harvard.edu/abs/2016APS..DFDA35007M

**Upcoming Publication**

Non-Stationary Signal Processing Techniques for Turbulent Flow Control (Follow up to the work in (8) at APS)

**Current Work**

_A predictive tool for imminent stall warning based on surface pressure probes using LSTM Recurrent Neural networks_

With: AFRL Collaborative Center for Aeronautical Sciences, The Ohio State University

This project uses LSTM RNNs to predict the surface pressure at future times based on data at current and previous
time instants. Since airfoil surface pressure exhibits subtle variations prior to onset of stall, the LSTM net 
trained on un-stalled flow can predict future data considerably different from the expected trends for an
unstalled airfoil. The deviation in the predicted state from expected state can be used as a continous metric to 
forewarn imminent flow stall onset. An application of such a warning mechanism would be to activate
flow control actuators just before stall onset, potentially resulting in a smoother transition 
from an uncontrolled to controlled state.


**Projects currently underway**

1) _A Machine Learning based predictive modeling of near wall inner layer scales based on the dynamics of the outer layer scales in DNS of a Re__{\tau} = 4000 channel flow_

With: Dr. Lionel Agostini, Imperial College, London.

The interaction between near wall scales and outer layer scales in a turbulent wall bounded flow  are an intense topic of study for drag reduction. 
Several physical and statistical models exist to model the interaction between outer and inner scales, specifically,
the `footprinting' of the outer scales on the walls [1]. This project attempts to use multilayered neural networks to
create a model of this interaction, to predict the near wall dynamics based on reduced order models of the large scale 
structures from the outer layer. A high resolution DNS of 40 Terabytes is available, which would provide ample training and test datasets.

[1] Agostini, L., and M. A. Leschziner. "On the influence of outer large-scale structures on near-wall turbulence in channel flow." Physics of Fluids 26.7 (2014): 075107.

2) _Reduced Order Modeling of Fluid Mechanics using LSTM Recurrent Neural Networks_

  Reduced Order Modeling (ROM) can be used as surrogates to prohibitively expensive simulations to model flow behavior for long time periods. 
  ROM is predicated on extracting dominant spatio-temporal features of the flow from CFD or experimental datasets. The objective is to explore ROM development with a deep learning approach for predictive modeling. 
  Although deep learning and related artificial intelligence based predictive modeling techniques have shown varied success in other fields, 
  such approaches are in their initial stages of application to fluid dynamics. Here, the application of the LSTM neural network to sequential data is explored 
  specifically to predict the time coefficients of Proper Orthogonal Decomposition (POD) modes of the flow for future timesteps, by training it on data at previous timesteps. 
  The approach is demonstrated by constructing ROMs of several canonical flows. Additionally, the work will seek to demonstrate that statistical estimates of stationarity in the training data can indicate
  a priori how amenable a given flow-field is to this approach. 
