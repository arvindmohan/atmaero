"""
Decompose Square Cavity Mesh for FDL3DI
in the axial direction
A.T.M Oct 9 2013

The "bounds" vector contains the imin and imax
for every block along the axial direction.
e.g. [imin1,imax1,imin2,imax2,.....iminN,imaxN]
This vector is written to file in the same format.
"""
import numpy as np
import math
import os
import sys

# USER INPUT - Vary as needed

L = 1000 # Length of Square Cavity
n = 9  # Support filter for FDL3DI Compact Diff. Scheme
c1 = 250 # IBB1 point
c2 = 750 # IBB2 point
#x_doms = (nprocs)/(y_doms*z_doms) + (nprocs)%(y_doms*z_doms) # Total no.of axial blocks in mesh
x_doms=16

#nprocs = 64 # No. of processors in machine
#y_doms = 4  # No. of blocks in y-direction
#z_doms = 3 # No. of blocks in z-direction
fname = 'cavityblockbdys.dat' # Name of file to save output
fname1 = 'cavityileach' # Name of file to save output

# COMPUTATION STARTS
print 'Axial domain decomposition for Square Cavity Case: \n'
# Compute IBB1 and IBB2
imin_global = 1
imax_global = L
print 'The total no. of axial blocks (no. of processors for x-direction) are',x_doms
d = L/x_doms # average width of every block
reg_bloc = (x_doms-2)/3 # Average No. of blocks in each region of mesh
if reg_bloc is 1:
	print 'No. of blocks in each region is',reg_bloc,'\n'
	print 'Not enough processors in x direction - need atleast 6 for efficiency'
	sys.exit()
# In case of odd-valued x_doms, the additional blocks will be
# added to the last region of the mesh (Region 3)
print 'The average width of every block is:',d
#print 'Average No. of blocks in each region',reg_bloc
print 'IBB1 origin point is ',c1
print 'IBB2 origin point is ',c2
imin_IBB1 = math.floor(c1 - d/2.0)
imax_IBB1 = math.floor(c1 + d/2.0)
imin_IBB2 = math.floor(c2 - d/2.0)
imax_IBB2 = math.floor(c2 + d/2.0)
bounds=[]
print 'For a filter support of ',n,':'
print 'The [min,max] bounds for IBB1 and IBB2 are:',imin_IBB1,imax_IBB1,'and',imin_IBB2,imax_IBB2

# Operating locally on each block
imin_reg1 = imin_global
imax_reg1 = imin_IBB1-1
imin_bloc1 = imin_reg1
l1= imax_reg1 - imin_reg1
regbloc1 = math.floor(l1/d)
reg_bloc1 = int(regbloc1)

imin_reg2 = imax_IBB1+1
imax_reg2 = imin_IBB2-1
imin_bloc2 = imin_reg2
l2 = imax_reg2 - imin_reg2
regbloc2 = math.floor(l2/d)
reg_bloc2 = int(regbloc2)

imin_reg3 = imax_IBB2+1
imax_reg3 = imax_global
imin_bloc3 = imin_reg3
l3 = imax_reg3 - imin_reg3
regbloc3 = math.floor(l3/d)
reg_bloc3 = int(regbloc3)


#regbloc2 = math.floor(x_doms/3)
print 'No. of blocks in Reg-1 is', reg_bloc1
print 'No. of blocks in Reg-2 is', reg_bloc2
print 'No. of blocks in Reg-3 is', reg_bloc3

extra = x_doms - reg_bloc1 - reg_bloc2 - reg_bloc3
print 'EXTRA is', extra

# Reallocating Extra blocks to each region one at a time
if extra > 0:
	j=1
	while j < extra+1:
		regbloc1 = regbloc1+1
		j=j+1
		if j > extra:
			break
		regbloc2 = regbloc2+1
		j=j+1
		if j > extra:
			break
		regbloc3 = regbloc3+1
		j=j+1
		if j > extra:
			break

print 'NEW No. of blocks after reallocation in Reg-1 is', regbloc1
print 'NEW No. of blocks after reallocation in Reg-2 is', regbloc2
print 'NEW No. of blocks after reallocation in Reg-3 is', regbloc3

# Region 1 (from left side to imin_IBB1)- Compute block bounds
reg_bloc = int(regbloc1)-1
for i in xrange(1,reg_bloc+1):
    delta = int(math.floor(l1/float(reg_bloc)))
    if (i==reg_bloc):
       delta = imax_reg1 - imax_bloc1
    imax_bloc1 =  imin_bloc1 + delta
    bounds.append(imin_bloc1)
    bounds.append(imax_bloc1)
    imin_bloc1 = imax_bloc1+1  # updating min bound of next block

bounds.append(bounds[-1]+1)
bounds.append(imax_IBB1)

# Region 2 (between imax_IBB1 and imin_IBB2) - Compute block bounds
reg_bloc = int(regbloc2)
for i in xrange(1,reg_bloc+1):
    delta = int(math.floor(l2/float(reg_bloc)))
    if (i==reg_bloc):
       delta = imax_reg2 - imax_bloc2
    imax_bloc2 =  imin_bloc2 + delta
    bounds.append(imin_bloc2)
    bounds.append(imax_bloc2)
    imin_bloc2 = imax_bloc2+1  # updating min bound of next block

bounds.append(bounds[-1]+1)
bounds.append(imax_IBB2)

# Region 3 (between imax_IBB2 and imax_global) - Compute block bounds
reg_bloc = int(regbloc3)-1
for i in xrange(1,reg_bloc+1):
    delta = int(math.floor(l3/float(reg_bloc)))
    imax_bloc3 =  imin_bloc3 + delta
    if (i==reg_bloc):
       imax_bloc3 = imax_reg3
       print 'imax_reg3',imax_reg3
    bounds.append(imin_bloc3)
    bounds.append(imax_bloc3)
    imin_bloc3 = imax_bloc3+1  # updating min bound of next block

print 'Run Complete.The bounds of all the axial blocks are:\n',bounds
np.savetxt(fname,bounds,'%d')
print 'Data also saved to file -', fname,'\n'
# Block widths
width = []
size = len(bounds)
i=0
for i in xrange(0,size-1,2):
	w = bounds[i+1] - bounds[i] +1
	width.append(w)

print 'Printing width of each successive block:'
print width

with open(fname1,'a') as f:
        f.write(str(x_doms))
with open(fname1,'a') as f:
        f.write(str(width))
# np.savetxt(fname1,x_doms,'%d')
# np.savetxt(fname1,width,'%d')
print 'length is',len(width)
print 'sum is',sum(width)
print 'Gluck mit der Simulation! Bye.'

"""
print 'Local No. of blocks after reallocation in Reg-1 is', regbloc1
print 'Local No. of blocks after reallocation in Reg-2 is', regbloc2
print 'Local No. of blocks after reallocation in Reg-3 is', regbloc3
"""









