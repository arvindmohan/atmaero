#ifndef XLINKS_H_INCLUDED
#define XLINKS_H_INCLUDED
#include "includes.h"


std::tuple<vec2d,vec2d,vec2d,vec2d,vec2d,vec2d> xlinks(vec2d& ue,vec2d& ve,vec2d& uw,vec2d& vw,vec2d& un,vec2d& vn,vec2d& us,
                                                 vec2d& vs,vec2d& p,vec2d& uC,vec2d& vC,const double& dx,const double& dy,const double& u_inl,
                                                 const int& Istep,const int& Jstep,const int& M,const int& N)
{
   double dxdy,dydx,ce,cw,cn,cs;
   double NCE,PCE,NCW,PCW,NCN,PCN,NCS,PCS;
   double gamE,gamW,gamN,gamS;
   double u_wall = 0.0;
   double rhoE,rhoW,rhoN,rhoS;
   double ut,ub,vt,vb;
   dydx = dy/dx; dxdy = dx/dy;
   ut = ub = u_wall;
   vt = vb = u_wall;
   rhoE=rhoW=rhoN =rhoS=1000;
   gamE=gamW=gamN=gamS=0.001;

   // DEFINING LINK COEFFICIENTS
   vec2d aex(M,vec1d(N,0.0));
   vec2d awx(M,vec1d(N,0.0));
   vec2d anx(M,vec1d(N,0.0));
   vec2d asx(M,vec1d(N,0.0));
   vec2d aox(M,vec1d(N,0.0));
   vec2d Sx(M,vec1d(N,0.0));


/*** Interior Cells ***/

// Region 1

for (int i=1;i<=Istep-2;i++)
{
   for (int j=1;j<=N-2;j++)
   {
    cw = rhoW*uw[i][j];
    ce = rhoE*ue[i][j];
    cn = rhoN*vn[i][j];
    cs = rhoS*vs[i][j];

    NCE = (std::abs(ce)-ce)/2.0;
    NCW = (std::abs(cw)-cw)/2.0;
    NCN = (std::abs(cn)-cn)/2.0;
    NCS = (std::abs(cs)-cs)/2.0;

    PCE = (std::abs(ce)+ce)/2.0;
    PCW = (std::abs(cw)+cw)/2.0;
    PCN = (std::abs(cn)+cn)/2.0;
    PCS = (std::abs(cs)+cs)/2.0;

  aex[i][j] = -NCE*dy - gamE*dydx;
  awx[i][j] = -PCW*dy - gamW*dydx;
  aox[i][j] = PCE*dy + NCW*dy  + PCN*dx + NCS*dx
              + gamE*dydx + gamW*dydx + gamN*dxdy + gamS*dxdy;
  anx[i][j] = -NCN*dx - gamN*dxdy;
  asx[i][j] = -PCS*dx - gamS*dxdy;
  Sx[i][j] = 0.5*(p[i][j-1] - p[i][j+1])*dy;

   }
}

// Region 2

for (int i=Istep-1;i<=M-2;i++)
{
   for (int j=Jstep+1;j<=N-2;j++)
   {
    cw = rhoW*uw[i][j];
    ce = rhoE*ue[i][j];
    cn = rhoN*vn[i][j];
    cs = rhoS*vs[i][j];

    NCE = (std::abs(ce)-ce)/2.0;
    NCW = (std::abs(cw)-cw)/2.0;
    NCN = (std::abs(cn)-cn)/2.0;
    NCS = (std::abs(cs)-cs)/2.0;

    PCE = (std::abs(ce)+ce)/2.0;
    PCW = (std::abs(cw)+cw)/2.0;
    PCN = (std::abs(cn)+cn)/2.0;
    PCS = (std::abs(cs)+cs)/2.0;

  aex[i][j] = -NCE*dy - gamE*dydx;
  awx[i][j] = -PCW*dy - gamW*dydx;
  aox[i][j] = PCE*dy + NCW*dy  + PCN*dx + NCS*dx
              + gamE*dydx + gamW*dydx + gamN*dxdy + gamS*dxdy;
  anx[i][j] = -NCN*dx - gamN*dxdy;
  asx[i][j] = -PCS*dx - gamS*dxdy;
  Sx[i][j] = 0.5*(p[i][j-1] - p[i][j+1])*dy;
   }
}

// Setting Step interior to zero!! - Just like that ;)
  for (int i=Istep;i<=M-1;i++)
  {
     for (int j=0;j<=Jstep-1;j++)
     {
         aex[i][j] = 0.0;
         awx[i][j] = 0.0;
         aox[i][j] = 0.0;
         anx[i][j] = 0.0;
         asx[i][j] = 0.0;
         Sx[i][j] = 0.0;
     }
  }



/*** Edges ***/

// Inlet Interior Cells

  int j = 0;

  for (int i=1;i<=Istep-2;i++)
  {
    cw = rhoW*u_inl;
    ce = rhoE*ue[i][j];
    cn = rhoN*vn[i][j];
    cs = rhoS*vs[i][j];

    NCE = (std::abs(ce)-ce)/2.0;
    NCW = (std::abs(cw)-cw)/2.0;
    NCN = (std::abs(cn)-cn)/2.0;
    NCS = (std::abs(cs)-cs)/2.0;

    PCE = (std::abs(ce)+ce)/2.0;
    PCW = (std::abs(cw)+cw)/2.0;
    PCN = (std::abs(cn)+cn)/2.0;
    PCS = (std::abs(cs)+cs)/2.0;

  aex[i][j] = -NCE*dy - gamE*dydx - (gamW/3.0)*dydx;
  awx[i][j] = 0.0;
  aox[i][j] = PCE*dy + gamE*dydx + 3.0*gamW*dydx
             + NCS*dx + PCN*dx + gamN*dxdy + gamS*dxdy;
  anx[i][j] = -NCN*dx - gamN*dxdy;
  asx[i][j] = -PCS*dx - gamS*dxdy;
  Sx[i][j] = 0.5*(p[i][j] - p[i][j+1])*dy + (8.0/3.0)*gamW*u_inl*dydx + rhoW*u_inl*u_inl*dy
             + (8.0/3.0)*gamS*ub*dxdy + rhoS*vb*ub*dx;

  }


  // Top Wall Interior cells

  int i = 0;
  for (int j=1;j<=N-2;j++)
  {
    cw = rhoW*uw[i][j];
    ce = rhoE*ue[i][j];
    cn = rhoN*vn[i][j];
    cs = rhoS*vs[i][j];

    NCE = (std::abs(ce)-ce)/2.0;
    NCW = (std::abs(cw)-cw)/2.0;
    NCN = (std::abs(cn)-cn)/2.0;
    NCS = (std::abs(cs)-cs)/2.0;

    PCE = (std::abs(ce)+ce)/2.0;
    PCW = (std::abs(cw)+cw)/2.0;
    PCN = (std::abs(cn)+cn)/2.0;
    PCS = (std::abs(cs)+cs)/2.0;

  aex[i][j] = -NCE*dy - gamE*dydx;
  awx[i][j] = -PCW*dy - gamW*dydx;
  aox[i][j] = PCE*dy + gamE*dydx + gamW*dydx
             + NCS*dx + NCW*dy + 3.0*gamN*dxdy + gamS*dxdy;
  anx[i][j] = 0.0;
  asx[i][j] = -PCS*dx - (gamN/3.0)*dxdy - gamS*dxdy;
  Sx[i][j] = 0.5*(p[i][j-1] - p[i][j+1])*dy + (8.0/3.0)*gamN*ut*dxdy - rhoN*vt*ut*dy;

  }


// Bottom Edge Interior Cells

  i = M-1;
  for (int j=Jstep+1;j<=N-2;j++)
  {
    cw = rhoW*uw[i][j];
    ce = rhoE*ue[i][j];
    cn = rhoN*vn[i][j];
    cs = rhoS*vs[i][j];

    NCE = (std::abs(ce)-ce)/2.0;
    NCW = (std::abs(cw)-cw)/2.0;
    NCN = (std::abs(cn)-cn)/2.0;
    NCS = (std::abs(cs)-cs)/2.0;

    PCE = (std::abs(ce)+ce)/2.0;
    PCW = (std::abs(cw)+cw)/2.0;
    PCN = (std::abs(cn)+cn)/2.0;
    PCS = (std::abs(cs)+cs)/2.0;

  aex[i][j] = -NCE*dy - gamE*dydx;
  awx[i][j] = -PCW*dy - gamW*dydx;
  aox[i][j] = PCE*dy + gamE*dydx + gamW*dydx
             + PCN*dx + NCW*dy + 3.0*gamS*dxdy + gamN*dxdy;
  anx[i][j] = -NCN*dx - gamN*dxdy;
  asx[i][j] = 0.0;
  Sx[i][j] = 0.5*(p[i][j-1] - p[i][j+1])*dy + (8.0/3.0)*gamS*ub*dxdy + rhoS*vb*ub*dx;

  }


// Bottom Step Interior cells

  i = Istep-1;
  for (int j=1;j<=Jstep-1;j++)
  {
    cw = rhoW*uw[i][j];
    ce = rhoE*ue[i][j];
    cn = rhoN*vn[i][j];
    cs = rhoS*vs[i][j];

    NCE = (std::abs(ce)-ce)/2.0;
    NCW = (std::abs(cw)-cw)/2.0;
    NCN = (std::abs(cn)-cn)/2.0;
    NCS = (std::abs(cs)-cs)/2.0;

    PCE = (std::abs(ce)+ce)/2.0;
    PCW = (std::abs(cw)+cw)/2.0;
    PCN = (std::abs(cn)+cn)/2.0;
    PCS = (std::abs(cs)+cs)/2.0;

  aex[i][j] = -NCE*dy - gamE*dydx;
  awx[i][j] = -PCW*dy - gamW*dydx;
  aox[i][j] = PCE*dy + gamE*dydx + gamW*dydx
             + PCN*dx + NCW*dy + 3.0*gamS*dxdy + gamN*dxdy;
  anx[i][j] = -NCN*dx - gamN*dxdy;
  asx[i][j] = 0.0;
  Sx[i][j] = 0.5*(p[i][j-1] - p[i][j+1])*dy + (8.0/3.0)*gamS*ub*dxdy + rhoS*vb*ub*dx;

  }

// Left wall Step Interior cells

  j = Jstep;
  for (int i=Istep;i<=M-2;i++)
  {
    cw = rhoW*uw[i][j];
    ce = rhoE*ue[i][j];
    cn = rhoN*vn[i][j];
    cs = rhoS*vs[i][j];

    NCE = (std::abs(ce)-ce)/2.0;
    NCW = (std::abs(cw)-cw)/2.0;
    NCN = (std::abs(cn)-cn)/2.0;
    NCS = (std::abs(cs)-cs)/2.0;

    PCE = (std::abs(ce)+ce)/2.0;
    PCW = (std::abs(cw)+cw)/2.0;
    PCN = (std::abs(cn)+cn)/2.0;
    PCS = (std::abs(cs)+cs)/2.0;

  aex[i][j] = -NCE*dy - gamE*dydx - (gamW/3.0)*dydx ;
  awx[i][j] = 0.0;
  aox[i][j] = PCE*dy + gamE*dydx + gamS*dxdy
             + PCN*dx + NCS*dx + 3.0*gamW*dydx + gamN*dxdy;
  anx[i][j] = -NCN*dx - gamN*dxdy;
  asx[i][j] = -PCS*dx - gamS*dxdy;
  Sx[i][j] = 0.5*(p[i][j-1] - p[i][j+1])*dy + (8.0/3.0)*gamW*u_wall*dydx + rhoW*u_wall*u_wall*dy;

  }

  // Right Outlet Interior Cells

  j = N-1;
  for (int i=1;i<=M-2;i++)
  {
    cw = rhoW*uw[i][j];
    ce = rhoE*uC[i][j];
    cn = rhoN*vn[i][j];
    cs = rhoS*vs[i][j];

    NCE = (std::abs(ce)-ce)/2.0;
    NCW = (std::abs(cw)-cw)/2.0;
    NCN = (std::abs(cn)-cn)/2.0;
    NCS = (std::abs(cs)-cs)/2.0;

    PCE = (std::abs(ce)+ce)/2.0;
    PCW = (std::abs(cw)+cw)/2.0;
    PCN = (std::abs(cn)+cn)/2.0;
    PCS = (std::abs(cs)+cs)/2.0;

  aex[i][j] = 0.0;
  awx[i][j] = -PCW*dy - gamW*dydx;
  aox[i][j] = NCW*dy + gamW*dydx + gamS*dxdy
             + PCN*dx + NCS*dx + gamN*dxdy;
  anx[i][j] = -NCN*dx - gamN*dxdy;
  asx[i][j] = -PCS*dx - gamS*dxdy;
  Sx[i][j] = 0.5*(p[i][j-1] - p[i][j])*dy - rhoE*uC[i][j]*uC[i][j]*dy;

  }

/*** Corners ***/

  // Inlet Top Corner
 i = 0;
 j = 0;

  cw = rhoW*u_inl;
  ce = rhoE*ue[i][j];
  cn = rhoN*vt;
  cs = rhoS*vs[i][j];

  NCE = (std::abs(ce)-ce)/2.0;
  NCW = (std::abs(cw)-cw)/2.0;
  NCN = (std::abs(cn)-cn)/2.0;
  NCS = (std::abs(cs)-cs)/2.0;

  PCE = (std::abs(ce)+ce)/2.0;
  PCW = (std::abs(cw)+cw)/2.0;
  PCN = (std::abs(cn)+cn)/2.0;
  PCS = (std::abs(cs)+cs)/2.0;


  aex[i][j] = -NCE*dy - gamE*dydx - (gamW/3.0)*dydx;
  awx[i][j] = 0.0;
  aox[i][j] = PCE*dy + gamE*dydx + 3.0*gamW*dydx
             + NCS*dx + 3.0*gamN*dxdy + gamS*dxdy;
  anx[i][j] = 0.0;
  asx[i][j] = -PCS*dx - (gamN/3.0)*dxdy - gamS*dxdy;
  Sx[i][j] = 0.5*(p[i][j] - p[i][j+1])*dy + (8.0/3.0)*gamN*ut*dxdy
             + (8.0/3.0)*gamW*u_inl*dydx + rhoW*u_inl*u_inl*dx - rhoN*vt*ut*dx;


  // Inlet Bottom Corner

 j = 0;
 i = Istep-1;

    cw = rhoW*u_inl;
    ce = rhoE*ue[i][j];
    cn = rhoN*vn[i][j];
    cs = rhoS*vb;

    NCE = (std::abs(ce)-ce)/2.0;
    NCW = (std::abs(cw)-cw)/2.0;
    NCN = (std::abs(cn)-cn)/2.0;
    NCS = (std::abs(cs)-cs)/2.0;

    PCE = (std::abs(ce)+ce)/2.0;
    PCW = (std::abs(cw)+cw)/2.0;
    PCN = (std::abs(cn)+cn)/2.0;
    PCS = (std::abs(cs)+cs)/2.0;

  aex[i][j] = -NCE*dy - gamE*dydx - (gamW/3.0)*dydx;
  awx[i][j] = 0.0;
  aox[i][j] = PCE*dy + gamE*dydx + 3.0*gamW*dydx
             + NCS*dx + PCN*dx + gamN*dxdy + gamS*dxdy;
  anx[i][j] = -NCN*dx - gamN*dxdy - (gamS/3.0)*dxdy;
  asx[i][j] = 0.0;
  Sx[i][j] = 0.5*(p[i][j] - p[i][j+1])*dy + (8.0/3.0)*gamW*u_inl*dydx - rhoW*u_inl*u_inl*dy;


// Outlet top Corner

 j = N-1;
 i = 0;

    cw = rhoW*uw[i][j];
    ce = rhoE*uC[i][j];
    cn = rhoN*vt;
    cs = rhoS*vs[i][j];

    NCE = (std::abs(ce)-ce)/2.0;
    NCW = (std::abs(cw)-cw)/2.0;
    NCN = (std::abs(cn)-cn)/2.0;
    NCS = (std::abs(cs)-cs)/2.0;

    PCE = (std::abs(ce)+ce)/2.0;
    PCW = (std::abs(cw)+cw)/2.0;
    PCN = (std::abs(cn)+cn)/2.0;
    PCS = (std::abs(cs)+cs)/2.0;

  aex[i][j] = 0.0;
  awx[i][j] = -PCW*dy - gamW*dydx;
  aox[i][j] = NCW*dy + gamW*dydx + NCS*dx
             + 3.0*gamN*dxdy + gamS*dxdy;
  anx[i][j] = 0.0;
  asx[i][j] = -PCS*dx - gamS*dxdy - (gamN/3.0)*dxdy;
  Sx[i][j] = 0.5*(p[i][j-1] - p[i][j])*dy - rhoE*uC[i][j]*uC[i][j]*dy
             - rhoN*vt*ut*dx + (8.0/3.0)*gamN*ut*dxdy;


//  Outlet Bottom Corner

j = N-1;
i = M-1;

    cw = rhoW*uw[i][j];
    ce = rhoE*uC[i][j];
    cn = rhoN*vn[i][j];
    cs = rhoS*vb;

    NCE = (std::abs(ce)-ce)/2.0;
    NCW = (std::abs(cw)-cw)/2.0;
    NCN = (std::abs(cn)-cn)/2.0;
    NCS = (std::abs(cs)-cs)/2.0;

    PCE = (std::abs(ce)+ce)/2.0;
    PCW = (std::abs(cw)+cw)/2.0;
    PCN = (std::abs(cn)+cn)/2.0;
    PCS = (std::abs(cs)+cs)/2.0;

  aex[i][j] = 0.0;
  awx[i][j] = -PCW*dy - gamW*dydx;
  aox[i][j] = NCW*dy + gamW*dydx + 3.0*gamS*dxdy
             + PCN*dx + gamN*dxdy;
  anx[i][j] = -NCN*dx - gamN*dxdy - (gamS/3.0)*dxdy;
  asx[i][j] = 0.0;
  Sx[i][j] = 0.5*(p[i][j-1] - p[i][j])*dy - rhoE*uC[i][j]*uC[i][j]*dy
             + rhoS*vb*ub*dx + (8.0/3.0)*gamS*ub*dxdy;

// Bottom Left Wall Corner

 j = Jstep;
 i = M-1;

    cw = rhoW*u_wall;
    ce = rhoE*ue[i][j];
    cn = rhoN*vn[i][j];
    cs = rhoS*vb;

    NCE = (std::abs(ce)-ce)/2.0;
    NCW = (std::abs(cw)-cw)/2.0;
    NCN = (std::abs(cn)-cn)/2.0;
    NCS = (std::abs(cs)-cs)/2.0;

    PCE = (std::abs(ce)+ce)/2.0;
    PCW = (std::abs(cw)+cw)/2.0;
    PCN = (std::abs(cn)+cn)/2.0;
    PCS = (std::abs(cs)+cs)/2.0;

  aex[i][j] = -NCE*dy - gamE*dydx - (gamW/3.0)*dydx;
  awx[i][j] = 0.0;
  aox[i][j] = PCE*dy + gamE*dydx + 3.0*gamW*dydx
             + NCS*dx + NCN*dx + gamN*dxdy + 3.0*gamS*dxdy;
  anx[i][j] = -NCN*dx - gamN*dxdy - (gamS/3.0)*dxdy;
  asx[i][j] = 0.0;
  Sx[i][j] = 0.5*(p[i][j] - p[i][j+1])*dy + (8.0/3.0)*gamW*u_wall*dydx + rhoW*u_wall*u_wall*dy
             + (8.0/3.0)*gamS*ub*dxdy + rhoS*vb*ub*dx;

 // Return Link Coeffs

  return std::make_tuple(aex,awx,anx,asx,aox,Sx);
}



#endif // XLINKS_H_INCLUDED
