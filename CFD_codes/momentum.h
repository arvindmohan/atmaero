#ifndef MOMENTUM_H
#define MOMENTUM_H
#include "includes.h"

class xmomentum
{
    private:
    public:
        double rho,gam,ut,ul,ur,ub,vl,vt,vr,vb;
        double alpha_x; vec2d aox;
        std::tuple<vec2d,vec2d,double> xlinks(const vec2d& ,const vec2d& ,const vec2d& ,const int&,const int&);
        std::tuple<vec2d,double> xADI(const vec2d&,const vec2d&,const vec2d&,
                   const vec2d&,const vec2d&,const vec2d&,
                   vec2d&,const int&,const int&);
        double xresid(const vec2d&,const vec2d&,const vec2d&,
                   const vec2d&,const vec2d&,const vec2d&,
                   vec2d&,const int&,const int&);
    protected:

};


class ymomentum
{
    private:
    public:
        double rho,gam,ut,ul,ur,ub,vl,vt,vr,vb;
        double alpha_y;
        std::tuple<vec2d,vec2d,double> ylinks(const vec2d& ,const vec2d& ,const vec2d& ,const int&,const int&);
        std::tuple<vec2d,double> yADI(const vec2d&,const vec2d&,const vec2d&,
                   const vec2d&,const vec2d&,const vec2d&,
                   vec2d&,const int&,const int&);
        double yresid(const vec2d&,const vec2d&,const vec2d&,
                   const vec2d&,const vec2d&,const vec2d&,
                   vec2d&,const int&,const int&);
    protected:

};
#endif // MOMENTUM_H
