// Extrudes 2D mesh into 3D and outputs mesh file in plot3D format - Arvind Mohan, Oct 20,2012
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

int main ()
{
  ofstream gridfile ("3dMesh.dat",ios::app);
 // ifstream oldfile("old.dat");
  string line;
// copy first set of xy values
//while (!oldfile.eof())
 //   {
  //      while (getline( oldfile,line ) )
    //       {
     //       gridfile << line << endl;
          //  cout << line << endl;
       //    }
   // }


  float dz = 0.0241;
  for (int j=0; j<187; j++)
// Copies x,t mesh data for every plane    
  { 
        ifstream oldfile("old.dat");
        while (getline( oldfile,line ) )
           {
            gridfile << line << endl;
          //  cout << line << endl;
           }
   
       oldfile.close();

 
   // to generate mesh in the z direction and write it to the new mesh file.
  int i = 0;
  float z = j*dz;
    while (i<5083)
   {
    gridfile  << setw(11) <<   z  << setw(15)  <<    z   << setw(15)  <<    z   << setw(15)  <<      z    << setw(15)  <<    z  << setw(15) << endl;
    i++;
   }
    gridfile  << setw(11) <<    z  << setw(15)  <<    z   << setw(15)  << endl;
   
   }
   //gridfile.close();
  
 // oldfile.close();
  return 0;
}
