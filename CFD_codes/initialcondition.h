#ifndef INITIALCONDITION_H_INCLUDED
#define INITIALCONDITION_H_INCLUDED
#include "includes.h"

std::tuple<vec2d,vec2d,vec2d,vec2d,vec2d,vec2d,vec2d,vec2d,vec2d,vec2d,vec2d> initialcond(vec2d& ue,vec2d& ve,vec2d& uw,vec2d& vw,
                                               vec2d& un,vec2d& vn,vec2d& us,vec2d& vs,vec2d& p,vec2d& uC,vec2d& vC,
                                               const double& u_inl,const int& Istep,const int& Jstep,
                                               const int& M,const int& N)
{
// Whole Rectangle
  for (int i=0;i<=M-1;i++)
  {
     for (int j=0;j<=N-1;j++)
     {
         uC[i][j] = u_inl;
     }
  }

// Setting Step interior to zero!!
  for (int i=Istep;i<=M-1;i++)
  {
     for (int j=0;j<=Jstep-1;j++)
     {
         uC[i][j] = 0.0;
     }
  }

// Setting everything equal

  ue=ve=uw=vw=un=vn=us=vs=vC=uC;


 return std::make_tuple(ue,ve,uw,vw,un,vn,us,vs,p,uC,vC);

}

#endif // INITIALCONDITION_H_INCLUDED
