#ifndef FACEVELS_H_INCLUDED
#define FACEVELS_H_INCLUDED
#include "includes.h"

std::tuple<vec2d,vec2d> facevels(const vec2d& ae,const vec2d& ao,vec2d& p,vec2d& uC,vec2d& ue,vec2d& uw,
                                 const double& dx,const double& dy,const int& Istep,
                                 const int& Jstep,const int& M,const int& N)
{
   double dxdyb2 = dx*dy*0.5;
   double dpdxO = 0.0;
   double dpdxE = 0.0;
   double dpdxe = 0.0;
   double p_out = 0.0;

   // Inlet domain
   for (int i=0;i<=Istep-1;i++)
   {
       for (int j=0;j<Jstep;j++)
       {

        if (j==0)
        {
           dpdxO = 0.5*(p[i][j+1] - p[i][j])*dy;
           dpdxE = 0.5*(p[i][j+2] - p[i][j])*dy;
           dpdxe = 0.5*(p[i][j+1] - p[i][j])*dy;
           ue[i][j] = 0.5*(uC[i][j] + uC[i][j+1]) + dxdyb2*((1/ao[i][j])*dpdxO + (1/ae[i][j])*dpdxE - (1/ae[i][j] + 1/ao[i][j])*dpdxe);
           uw[i][j+1] = ue[i][j];
        }

         dpdxO = 0.5*(p[i][j+1] - p[i][j-1])*dy;
         dpdxE = 0.5*(p[i][j+2] - p[i][j])*dy;
         dpdxe = 0.5*(p[i][j+1] - p[i][j])*dy;
         ue[i][j] = 0.5*(uC[i][j] + uC[i][j+1]) + dxdyb2*((1/ao[i][j])*dpdxO + (1/ae[i][j])*dpdxE - (1/ae[i][j] + 1/ao[i][j])*dpdxe);
         uw[i][j+1] = ue[i][j];
       }
   }


  // Remaining domain

   for (int i=0;i<=M-1;i++)
   {
       for (int j=Jstep;j<=N-1;j++)
       {

        if (j=Jstep && i>=Istep)
        {
           dpdxO = 0.5*(p[i][j+1] - p[i][j])*dy;
           dpdxE = 0.5*(p[i][j+2] - p[i][j])*dy;
           dpdxe = 0.5*(p[i][j+1] - p[i][j])*dy;
           ue[i][j] = 0.5*(uC[i][j] + uC[i][j+1]) + dxdyb2*((1/ao[i][j])*dpdxO + (1/ae[i][j])*dpdxE - (1/ae[i][j] + 1/ao[i][j])*dpdxe);
           uw[i][j+1] = ue[i][j];
        }

        if (j==N-2)
        {
           dpdxO = 0.5*(p[i][j+1] - p[i][j-1])*dy;
           dpdxE = 0.5*(p_out - 0.5*(p[i][j] + p[i][j]))*dy;
           dpdxe = 0.5*(p[i][j+1] - p[i][j])*dy;
           ue[i][j] = 0.5*(uC[i][j] + uC[i][j+1]) + dxdyb2*((1/ao[i][j])*dpdxO + (1/ae[i][j])*dpdxE - (1/ae[i][j] + 1/ao[i][j])*dpdxe);
           uw[i][j+1] = ue[i][j];
        }

         dpdxO = 0.5*(p[i][j+1] - p[i][j-1])*dy;
         dpdxE = 0.5*(p[i][j+2] - p[i][j])*dy;
         dpdxe = 0.5*(p[i][j+1] - p[i][j])*dy;
         ue[i][j] = 0.5*(uC[i][j] + uC[i][j+1]) + dxdyb2*((1/ao[i][j])*dpdxO + (1/ae[i][j])*dpdxE - (1/ae[i][j] + 1/ao[i][j])*dpdxe);
         uw[i][j+1] = ue[i][j];
       }
   }

    return std::make_tuple(ue,uw);
}


#endif // FACEVELS_H_INCLUDED
