#ifndef INCLUDES_H_INCLUDED
#define INCLUDES_H_INCLUDED

#include<iostream>
#include<cmath>
#include<array>
#include<vector>
#include<algorithm>
#include<tuple>
#include "tdma.h"

typedef std::vector<double> vec1d;
typedef std::vector< std::vector<double> > vec2d;

#endif // INCLUDES_H_INCLUDED
