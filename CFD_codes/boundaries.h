#ifndef BOUNDARIES_H_INCLUDED
#define BOUNDARIES_H_INCLUDED
#include "includes.h"

std::tuple<vec2d,vec2d,vec2d,vec2d,vec2d,vec2d,vec2d,vec2d,vec2d> boundaries(vec2d& ue,vec2d& ve,vec2d& uw,vec2d& vw,
                                               vec2d& un,vec2d& vn,vec2d& us,vec2d& vs,vec2d& p,const double& u_inl,
                                               const int& Istep,const int& Jstep,const int& M,const int& N)
{

double u_out,u_wall;
double v_inl,v_out,v_wall;

u_wall = v_wall = 0.0;
v_inl = 0.0;
u_out = v_out = 0.0;

/** Apply BCs  **/

// INLET

int j=0;
for (int i=0;i<=Istep-1;i++)
{
  uw[i][j] = u_inl;
  //std::cout << "uw is" << uw[i][j] << "\n";
  vw[i][j] = v_inl;
}

//print_array2d(uw);

// Step bottom wall

int i = Istep-1;
for(int j=0;j<=Jstep-1;j++)
{
  us[i][j] = u_wall;
  vs[i][j] = v_wall;
}

// Step left wall

j = Jstep;
for(int i=Istep;i<=M-1;i++)
{
  uw[i][j] = u_wall;
  vw[i][j] = v_wall;
}

// OUTLET

j=N-1;
for (int i=0;i<=M-1;i++)
{
  ue[i][j] = u_out;
  ve[i][j] = v_out;
}

// Top Wall

i=0;
for (int j=0;j<=N-1;j++)
{
  un[i][j] = u_wall;
  vn[i][j] = v_wall;
}

// Bottom Wall

i=M-1;
for (int j=Jstep;j<=N-1;j++)
{
  us[i][j] = u_wall;
  vs[i][j] = v_wall;
}

return std::make_tuple(ue,ve,uw,vw,un,vn,us,vs,p);

}

// Pressure at outlet



#endif // BOUNDARIES_H_INCLUDED



















