/********************************************************************/
// 	                                          C++ MATH LIBRARY
//  Contains functions for most common math operations on arrays - frequently updated
//  Heavily templated class and functions for all data types.
// Arvind Mohan
/*******************************************************************/
#ifndef ARRAYOPS_H_INCLUDED
#define ARRAYOPS_H_INCLUDED
#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <chrono>
#include <random>
#include <cassert>
#include <time.h>
#include "printing.h"

// Rows = m,  Columns = n
template <class T>
class arrayops
{
    private:
    int m,n,asize;
    T a,b;
    float init;

    public:
    const std::vector< std::vector<T> > array2dgen(const int&,const int&,const float&);
    const std::vector<T> array1dgen(const int&,const float&);
    const std::vector<T> sum(const std::vector<T>&, const std::vector<T>&);
    const T sum_vector(const std::vector< std::vector<T> >&);
    const std::vector< std::vector<T> > sum(const std::vector< std::vector<T> >&,const std::vector< std::vector<T> >&,const int& ,const int&);
    const std::vector<T> diff(const std::vector<T>&, const std::vector<T>&);
    const std::vector< std::vector<T> > diff(const std::vector< std::vector<T> >&,const std::vector< std::vector<T> >&);
    const std::vector<T> mod(const std::vector<T>&);
    const std::vector<T> mul(const std::vector<T>&, const std::vector<T>&);
    const std::vector<T> mul(const T& ,const std::vector<T>&);
    const std::vector< std::vector<T> > mul(const T& ,const std::vector< std::vector<T> >&,const int&,const int&);
    const std::vector<T> sin(const std::vector<T>&);
    const std::vector<T> cos(const std::vector<T>&);
    const std::vector<T> tan(const std::vector<T>&);
    const std::vector<T> sqrt(const std::vector<T>&);
    const std::vector< std::vector<T> > sqrt(std::vector< std::vector<T> >&);
    const std::vector< std::vector<T> > sqr(std::vector< std::vector<T> >&);
    const T maxval(const std::vector<T>&);
    const T minval(const std::vector<T>&);
    const std::vector<T> ln(const std::vector<T>&);
    const std::vector<T> convert1d(const std::vector< std::vector<T> >&,const int&,const int&);
    const std::vector< std::vector<T> >convert2d(const std::vector<T>&,const int&,const int&);

};

//******************* function matrixnewgen

template <class T>
const std::vector< std::vector<T> > arrayops<T>::array2dgen(const int& m,const int& n,const float& init)
{

   const std::vector< std::vector<T> > array2d(m, std::vector<T>(n,init));

   return(array2d);

}


//******************** function arraynewgen
template <class T>
const std::vector<T> arrayops<T>::array1dgen(const int& asize,const float& init)
{

  const std::vector<T> array1d(asize,init);

  return(array1d);

}

//******************** function sum
template <class T>
const std::vector<T> sum(const std::vector<T>& a, const std::vector<T>& b)
{
   int asize = b.size();
   //std::cout << "size" << asize;
   std::vector<T> c(asize,0);

   for (int i=0;i<asize;i++)
   {
      c[i] = a[i] + b[i];
   }

    return(c);
}

template <class T>
const std::vector< std::vector<T> > sum(const std::vector< std::vector<T> >& a,const std::vector< std::vector<T> >& b,const int& My,const int& Mz)
{
   std::vector< std::vector<T> > c(My, std::vector<T>(Mz,0));
   for (int i=0;i<My;i++)
   {
       for (int j=0;j<Mz;j++)
       {
            c[i][j] = a[i][j] + b[i][j];
       }
   }
   return(c);
}

// Sum elements of vector
template <class T>
const T sum_vector(const std::vector< std::vector<T> >& a)
{
   int N = a.size();
   int M = a[0].size();
   double sum = 0;
   //std::cout << i << "and" <<j << "\n";
   for (int i=0;i<N;i++)
   {
       for (int j=0;j<M;j++)
       {
           sum += a[i][j];
       }
   }

   return(sum);
}

//******************** function difference
template <class T>
const std::vector<T> diff(const std::vector<T>& a, const std::vector<T>& b)
{
   int asize = b.size();

   std::vector<T> c(asize,0);

   for (int i=0;i<asize;i++)
   {
      c[i] = a[i] - b[i];
   }

    return(c);
}

template <class T>
const std::vector< std::vector<T> > diff(const std::vector< std::vector<T> >& a,const std::vector< std::vector<T> >& b)
{
   int bsize = b.size();
   std::vector< std::vector<T> > c(bsize, std::vector<T>(bsize,0));
   for (int i=0;i<bsize;i++)
   {
       for (int j=0;j<bsize;j++)
       {
            c[i][j] = a[i][j] - b[i][j];
       }
   }
   return(c);
}
//********************* function modulus
template <class T>
const std::vector<T> mod(const std::vector<T>& a)
{
   const int asize = a.size();
   std::vector<T> d(asize,0);

   for (int i=0;i<asize;i++)
   {
      d[i] = std::abs(a[i]);
   }

   return(d);

}

//********************* function maxval
template <class T>
const T maxval(const std::vector<T>& a)
{
   auto ptr = max_element(std::begin(a), std::end(a));
   return(*ptr);
}

//********************* function maxval
template <class T>
const T minval(const std::vector<T>& a)
{
   auto ptr = min_element(std::begin(a), std::end(a));
   return(*ptr);
}

template <class T>
const std::vector<T> ln(const std::vector<T>& a)
{
   const int asize = a.size();
   std::vector<T> b(asize,0);
   for (int i=0;i<asize;++i)
   {
       b[i] = log(a[i]);
   }
   return(b);
}

template <class T>
const std::vector<T> mul(const std::vector<T>& a, const std::vector<T>& b)
{
   int asize = a.size();
   int bsize = b.size();
   assert(asize = bsize);

   std::vector<T> c(bsize,0);

   for (int i=0;i<bsize;i++)
   {
      c[i] = a[i]*b[i];
   }

    return(c);
}

template <class T>
const std::vector<T> mul(const T& a, const std::vector<T>& b)
{
   int bsize = b.size();

   std::vector<T> c(bsize,0);

   for (int i=0;i<bsize;i++)
   {
      c[i] = a*b[i];
   }

    return(c);
}


template <class T>  // DO THE TYPE TRAITS!!!!!
const std::vector< std::vector<T> > mul(const T& a,const std::vector< std::vector<T> >& b,const int& My,const int& Mz)
{
   std::vector< std::vector<T> > c(My, std::vector<T>(Mz,0.0));

   for (int i=0;i<My;i++)
   {
       for (int j=0;j<Mz;j++)
       {
            c[i][j] = a*b[i][j];
       }
   }
    return(c);
}

template <class T>
const std::vector<T> sin(const std::vector<T>& a)
{
   const int asize = a.size();
   std::vector<T> d(asize,0);

   for (int i=0;i<asize;i++)
   {
      d[i] = sin(a[i]);
   }

   return(d);
}


template <class T>
const std::vector<T> cos(const std::vector<T>& a)
{
   const int asize = a.size();
   std::vector<T> d(asize,0);

   for (int i=0;i<asize;i++)
   {
      d[i] = cos(a[i]);
   }

   return(d);
}


template <class T>
const std::vector<T> tan(const std::vector<T>& a)
{
   const int asize = a.size();
   std::vector<T> d(asize,0);

   for (int i=0;i<asize;i++)
   {
      d[i] = tan(a[i]);
   }

   return(d);
}

template <class T>
const std::vector<T> sqrt(const std::vector<T>& a)
{
   const int asize = a.size();
   std::vector<T> d(asize,0);

   for (int i=0;i<asize;i++)
   {
      d[i] = sqrt(a[i]);
   }

   return(d);
}


template <class T>
const std::vector< std::vector<T> > sqrt(std::vector< std::vector<T> >& a)
{
   int N = a.size();
   int M = a[0].size();
   for (int i=0;i<N;i++)
   {
       for (int j=0;j<M;j++)
       {
           a[i][j] = sqrt(a[i][j]);
       }
   }

   return(a);
}

template <class T>
const std::vector< std::vector<T> > sqr(std::vector< std::vector<T> >& a)
{
   int N = a.size();
   int M = a[0].size();
   for (int i=0;i<N;i++)
   {
       for (int j=0;j<M;j++)
       {
           a[i][j] = pow(a[i][j],2);
       }
   }

   return(a);
}



template <class T>
const std::vector<T> convert1d(const std::vector < std::vector<T> >& a,const int& m,const int& n)
{
  const int vecsize = m*n;
  std::vector<T> d(vecsize,0);
  int k=0;

  for (int i=0;i<m;i++)
  {
    for (int j=0;j<n;j++)
    {
      d[k] = a[i][j];
      k++;
    }

  }

  return(d);

}


template <class T>
const std::vector< std::vector<T> > convert2d(const std::vector<T>& a,const int& m,const int& n)
{
  std::vector< std::vector<T> > d(m, std::vector<T>(n,0.0));
  int k=0;

  for (int i=0;i<m;i++)
  {
    for (int j=0;j<n;j++)
    {
      d[i][j] = a[k];
      k++;
    }

  }

  return(d);

}



#endif // ARRAYOPS_H_INCLUDED
