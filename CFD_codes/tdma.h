/*
Arvind Mohan
PhD student
Aerospace Engineering
The Ohio State University
*/
/*  TDMA Solver */

#include<iostream>
#include<cmath>
#include<array>
#include<vector>
#include<algorithm>
#include "arrayops.h"
#include "printing.h"


inline std::vector<double> TDMA(const std::vector<double>& a,const std::vector<double>& b,const std::vector<double>& c,
                         const std::vector<double>& d)

{
    int n = d.size();
    //std::cout << "size" << n << "\n";

    std::vector<double> A = a;
    std::vector<double> B = b;
    std::vector<double> C = c;
    std::vector<double> D = d;
    std::vector<double> cp(n,0.0);
    std::vector<double> dp(n,0.0);
    std::vector<double> X(n,0.0);
/*
    std::cout << "A is" << "\n";
    print_array1d(A);
    std::cout << "B is" << "\n";
    print_array1d(B);
    std::cout << "C is" << "\n";
    print_array1d(C);
    std::cout << "D is" << "\n";
    print_array1d(D);
*/
    double term = 0.0;

    // 1st row points
    cp[0] = C[0]/B[0]; //std::cout << "the C[0] is"<< C[0] << "\n";
    dp[0] = D[0]/B[0]; //std::cout << "the D[0] is"<< D[0] << "\n";

    for(int i=1;i<n;i++)
    {
       //std::cout << "cp and A and B are at i=" << i << "\t" << cp[i-1] << "\t" << A[i] << "\t" << B[i] << "\n";
       term = B[i] - cp[i-1]*A[i];
       //std::cout << "the term is"<< term << "\n";
       cp[i] = C[i]*(1.0/term);
       //std::cout << "the cp[i] is"<< cp[i] << "\n";
       dp[i] = (D[i] - dp[i-1]*A[i])*(1.0/term);
       //std::cout << "the dp[i] is"<< dp[i] << "\n";
    }

    X[n-1] = dp[n-1];
   //std::cout << "the X[n-1] is"<< X[n-1] << "\n";
    for (int i=n-2;i>-1;i--)
    {
       X[i] =dp[i] - cp[i]*X[i+1];
       //std::cout << "the X[i] is"<< X[i] << "\n";
    }
    //print_array1d(X);
return(X);

}
/*
 ch_1[0]=c_r[0]/b_r[0];
   dh_1[0]=d_r[0]/b_r[0];
   for (id=1;id<=Mn1;id++)
   {
      ch_1[id]=c_r[id]/(b_r[id]-ch_1[id-1]*a_r[id]);
      dh_1[id]=(d_r[id]-dh_1[id-1]*a_r[id])/(b_r[id]-ch_1[id-1]*a_r[id]);
   }

   ans_r[Mn1]=dh_1[Mn1];
   for (id=Mn1-1;id>-1;id--)
   {
       ans_r[id]=dh_1[id]-ch_1[id]*ans_r[id+1];
   }
*/
/*
   ch_1[0]=c_c[0]/b_c[0];
   dh_1[0]=d_c[0]/b_c[0];
   for (id=1;id<M;id++)
   {
      ch_1[id]=c_c[id]/(b_c[id]-ch_1[id-1]*a_c[id]);
      dh_1[id]=(d_c[id]-dh_1[id-1]*a_c[id])/(b_c[id]-ch_1[id-1]*a_c[id]);
   }

   udiff[M-1][j]=dh_1[M];
   for (int i=M-2;i>-1;i--)
   {
       udiff[i][j]=dh_1[id]-ch_1[id]*udiff[i+1][j];
   }
*/
