#ifndef RESIDUAL_H_INCLUDED
#define RESIDUAL_H_INCLUDED
#include "includes.h"

double resid(const vec2d& ae,const vec2d& aw,const vec2d& ao,const vec2d& an,
              const vec2d& as,const vec2d& S,vec2d& uC,const int& Istep,const int& Jstep,
              const int& M,const int& N)
{
    vec2d resid(M,vec1d(N,0.0));
    double residroot = 0.0; double temp=0.0;

  // Inlet region

   // Interior

   for (int j=1;j<Jstep;j++)
   {
       for (int i=1;i<Istep-1;i++)
       {
         resid[i][j] =  S[i][j] - ao[i][j]*uC[i][j] - an[i][j]*uC[i-1][j]
                        - as[i][j]*uC[i+1][j] - ae[i][j]*uC[i][j+1] - aw[i][j]*uC[i][j-1];
       }
   }

   // Inlet top corner

   int j=0;
   int i=0;

   resid[i][j] =  S[i][j] - ao[i][j]*uC[i][j] - as[i][j]*uC[i+1][j] - ae[i][j]*uC[i][j+1];

   // Inlet bottom corner

   i = Istep-1;
   j = 0;

   resid[i][j] =  S[i][j] - ao[i][j]*uC[i][j] - an[i][j]*uC[i-1][j] - ae[i][j]*uC[i][j+1];


   // Inlet column interior

   j=0;
   for (int i=1;i<Istep-1;i++)
   {
    resid[i][j] =  S[i][j] - ao[i][j]*uC[i][j] - an[i][j]*uC[i-1][j]
                        - as[i][j]*uC[i+1][j] - ae[i][j]*uC[i][j+1];
   }

   // Top edge

   i=0;
   for (int j=1;j<Jstep;j++)
   {
   resid[i][j] =  S[i][j] - ao[i][j]*uC[i][j] - as[i][j]*uC[i+1][j] - ae[i][j]*uC[i][j+1]
                  - aw[i][j]*uC[i][j-1];
   }

   // Bottom edge

   i=Istep-1;
   for (int j=1;j<Jstep;j++)
   {
   resid[i][j] =  S[i][j] - ao[i][j]*uC[i][j] - an[i][j]*uC[i-1][j] - ae[i][j]*uC[i][j+1]
                  - aw[i][j]*uC[i][j-1];
   }


  // Remaining Domain

  // Bottom Interior - 1

  for (int i = Istep;i<=M-2;i++)
  {
      for (int j=Jstep+1;j<=N-2;j++)
      {
       resid[i][j] =  S[i][j] - ao[i][j]*uC[i][j] - an[i][j]*uC[i-1][j]
                        - as[i][j]*uC[i+1][j] - ae[i][j]*uC[i][j+1] - aw[i][j]*uC[i][j-1];
      }
  }

  // left wall - 2

  j = Jstep;
  for (int i = Istep;i<=M-2;i++)
  {
   resid[i][j] =  S[i][j] - ao[i][j]*uC[i][j] - an[i][j]*uC[i-1][j]
                        - as[i][j]*uC[i+1][j] - ae[i][j]*uC[i][j+1];
  }


  // left wall corner - 3
  i = M-1;
  j = Jstep;
  resid[i][j] =  S[i][j] - ao[i][j]*uC[i][j] - an[i][j]*uC[i-1][j]
                         - ae[i][j]*uC[i][j+1];

  // Bottom Edge interior - 4
  i = M-1;
  for (int j=Jstep+1;j<=N-2;j++)
  {
  resid[i][j] =  S[i][j] - ao[i][j]*uC[i][j] - an[i][j]*uC[i-1][j]
                         - ae[i][j]*uC[i][j+1] - aw[i][j]*uC[i][j-1];
  }

  // outlet bottom corner - 5

  i = M-1;
  j = N-1;
  resid[i][j] =  S[i][j] - ao[i][j]*uC[i][j] - an[i][j]*uC[i-1][j]
                          - aw[i][j]*uC[i][j-1];

 // Outlet interior - 6

 j = N-1;
 for (int i = Istep;i<=M-2;i++)
  {
   resid[i][j] =  S[i][j] - ao[i][j]*uC[i][j] - an[i][j]*uC[i-1][j]
                        - as[i][j]*uC[i+1][j] - aw[i][j]*uC[i][j-1];
  }

 // Outlet top corner - 7
 j = N-1;
 i = 0;
 resid[i][j] =  S[i][j] - ao[i][j]*uC[i][j] - as[i][j]*uC[i+1][j]
                        - aw[i][j]*uC[i][j-1];

 // Top Edge - 8

 i = 0;
 for (int j=Jstep;j<=N-2;j++)
  {
  resid[i][j] =  S[i][j] - ao[i][j]*uC[i][j] - as[i][j]*uC[i+1][j]
                         - ae[i][j]*uC[i][j+1] - aw[i][j]*uC[i][j-1];
  }

 // Upper interior region

  for (int i = 1;i<=Istep-1;i++)
  {
      for (int j=Jstep;j<=N-2;j++)
      {
       resid[i][j] =  S[i][j] - ao[i][j]*uC[i][j] - an[i][j]*uC[i-1][j]
                        - as[i][j]*uC[i+1][j] - ae[i][j]*uC[i][j+1] - aw[i][j]*uC[i][j-1];
      }
  }


  // Computing L2 norm residual

  // Inlet domain

   for (int i=0;i<=Istep-1;i++)
   {
       for (int j=0;j<Jstep;j++)
       {
          temp = resid[i][j];
          residroot = residroot + pow(temp,2);
       }
   }

   // Remaining domain

   for (int i=0;i<=M-1;i++)
   {
       for (int j=Jstep;j<=N-1;j++)
       {
          temp = resid[i][j];
          residroot = residroot + pow(temp,2);
       }
   }

  residroot = sqrt(residroot);


 return(residroot);
}



#endif // RESIDUAL_H_INCLUDED
