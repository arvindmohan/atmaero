#include "momentum.h"
#include "includes.h"
#include "arrayops.h"
#include "printing.h"
//#include "tdma.h"

std::tuple<vec2d,vec2d,double> ymomentum::ylinks(const vec2d& u,const vec2d& v,const vec2d& p,const int& N,const int& M)
{
  //std::cout << "This is y momentum" << "\n";
    rho = 1000; gam = 0.001;
    ut = 0.01; ul=ur=ub=0.0;
    vl=vt=vb=0.0; vr=0.0;
    int id=0;
    double res = 1.0;
   double dx,dy,dxdy,dydx,ce,cw,cn,cs,B,H;
   double NCE,PCE,NCW,PCW,NCN,PCN,NCS,PCS;
   vec1d x(N,0.0); vec1d y(M,0.0);
   vec2d ae(M,vec1d(N,0.0));
   vec2d aw(M,vec1d(N,0.0));
   vec2d an(M,vec1d(N,0.0));
   vec2d as(M,vec1d(N,0.0));
   vec2d ao(M,vec1d(N,0.0));
   vec2d S(M,vec1d(N,0.0));
   vec2d vcorr(M+1,vec1d(N,0.0));
   vec2d vcorrnew(M+1,vec1d(N,0.0));


   B=0.01;H=0.01;
   dx = B/N;
   dy = H/M;
   dydx = dy/dx; dxdy = dx/dy;

 // DEFINING LINK COEFFICIENTS

  // TOP LEFT cell
  int i=0;
  int j=0;

  ce = rho*(u[i][j+1] + u[i+1][j+1])*0.5;
  cn = rho*(v[i][j] + v[i+1][j])*0.5;
  cs = rho*(v[i+1][j] + v[i+2][j])*0.5;

  NCE = (std::abs(ce)-ce)/2.0;
  NCN = (std::abs(cn)-cn)/2.0;
  NCS = (std::abs(cs)-cs)/2.0;

  PCE = (std::abs(ce)+ce)/2.0;
  PCN = (std::abs(cn)+cn)/2.0;
  PCS = (std::abs(cs)+cs)/2.0;


  ae[i][j] = -NCE*dy - gam*dydx - gam*dydx/3.0;
  aw[i][j] = 0.0;
  ao[i][j] = PCE*dy + gam*dydx + gam*dydx
             + PCN*dx + NCS*dx + 3.0*gam*dydx + gam*dxdy;
  an[i][j] = 0.0;
  as[i][j] = -PCS*dx - gam*dxdy;
  S[i][j]= (p[i+1][j] - p[i][j])*dy + (8.0/3.0)*gam*vl*dydx
            + rho*vl*ul*dy + gam*vt*dxdy + NCN*vt*dx;

  // Top Edge
  i = 0;
  for (int j=1;j<N-1;j++)
  {
      ce = rho*(u[i][j+1] + u[i+1][j+1])*0.5;
      cw = rho*(u[i][j] + u[i+1][j])*0.5;
      cn = rho*(v[i][j] + v[i+1][j])*0.5;
      cs = rho*(v[i+1][j] + v[i+2][j])*0.5;

      NCE = (std::abs(ce)-ce)/2.0;
      NCW = (std::abs(cw)-cw)/2.0;
      NCN = (std::abs(cn)-cn)/2.0;
      NCS = (std::abs(cs)-cs)/2.0;

      PCE = (std::abs(ce)+ce)/2.0;
      PCW = (std::abs(cw)+cw)/2.0;
      PCN = (std::abs(cn)+cn)/2.0;
      PCS = (std::abs(cs)+cs)/2.0;

      ae[i][j] = -NCE*dy - gam*dydx;
      aw[i][j] = -PCW*dy - gam*dydx;
      ao[i][j] = PCE*dy + gam*dydx + gam*dydx + gam*dxdy
                 + PCN*dx + NCS*dx + NCW*dy + gam*dxdy;
      an[i][j] = 0.0;
      as[i][j] = -PCS*dx - gam*dxdy;
      S[i][j]= (p[i+1][j] - p[i][j])*dy +gam*vt*dxdy + NCN*vt*dx;
  }

// Top Right
      i = 0;
      j = N-1;

      ce = rho*(u[i][j+1] + u[i+1][j+1])*0.5;
      cw = rho*(u[i][j] + u[i+1][j])*0.5;
      cn = rho*(v[i][j] + v[i+1][j])*0.5;
      cs = rho*(v[i+1][j] + v[i+2][j])*0.5;

      NCE = (std::abs(ce)-ce)/2.0;
      NCW = (std::abs(cw)-cw)/2.0;
      NCN = (std::abs(cn)-cn)/2.0;
      NCS = (std::abs(cs)-cs)/2.0;

      PCE = (std::abs(ce)+ce)/2.0;
      PCW = (std::abs(cw)+cw)/2.0;
      PCN = (std::abs(cn)+cn)/2.0;
      PCS = (std::abs(cs)+cs)/2.0;

      ae[i][j] = 0.0;
      aw[i][j] = -PCW*dy - gam*dydx -  gam*dydx/3.0 ;
      ao[i][j] = 3.0*gam*dydx + gam*dydx + gam*dydx
                 + PCN*dx + NCS*dx + NCW*dy + gam*dxdy;
      an[i][j] = 0.0;
      as[i][j] = -PCS*dx - gam*dxdy;
      S[i][j]= (p[i+1][j] - p[i][j])*dy +gam*vt*dxdy + NCN*vt*dx
                - rho*ur*vr*dy + (8.0/3.0)*gam*vr*dydx;


// Left Edge Interiors

   j = 0;

   for (int i=1;i<M-2;i++)
   {
      ce = rho*(u[i][j+1] + u[i+1][j+1])*0.5;
      //cw = rho*(u[i][j] + u[i+1][j])*0.5;
      cn = rho*(v[i][j] + v[i-1][j])*0.5;
      cs = rho*(v[i+1][j] + v[i][j])*0.5;

      NCE = (std::abs(ce)-ce)/2.0;
      NCN = (std::abs(cn)-cn)/2.0;
      NCS = (std::abs(cs)-cs)/2.0;

      PCE = (std::abs(ce)+ce)/2.0;
      PCN = (std::abs(cn)+cn)/2.0;
      PCS = (std::abs(cs)+cs)/2.0;

      ae[i][j] = -NCE*dy - gam*dydx - gam*dydx/3.0;
      aw[i][j] = 0.0;
      ao[i][j] = PCE*dy + gam*dydx + gam*dydx
                 + PCN*dx + NCS*dx + 3.0*gam*dydx + gam*dxdy;
      an[i][j] = -NCN*dx - gam*dxdy;
      as[i][j] = -PCS*dx - gam*dxdy;
      S[i][j]= (p[i+1][j] - p[i][j])*dy + rho*ul*vl*dy + (8.0/3.0)*gam*vl*dydx;
   }


// Interior Cells

  for (int i=1;i<M-2;i++)
  {
      for (int j=1;j<N-1;j++)
      {
      ce = rho*(u[i][j+1] + u[i+1][j+1])*0.5;
      cw = rho*(u[i][j] + u[i+1][j])*0.5;
      cn = rho*(v[i][j] + v[i-1][j])*0.5;
      cs = rho*(v[i+1][j] + v[i][j])*0.5;

      NCE = (std::abs(ce)-ce)/2.0;
      NCW = (std::abs(cw)-cw)/2.0;
      NCN = (std::abs(cn)-cn)/2.0;
      NCS = (std::abs(cs)-cs)/2.0;
     // std::cout << "NCE is" << NCE << "\n";
      PCE = (std::abs(ce)+ce)/2.0;
      PCW = (std::abs(cw)+cw)/2.0;
      PCN = (std::abs(cn)+cn)/2.0;
      PCS = (std::abs(cs)+cs)/2.0;

      ae[i][j] = -NCE*dy - gam*dydx;
      aw[i][j] = -PCW*dy - gam*dydx;
      ao[i][j] = PCE*dy + gam*dydx + gam*dydx + gam*dxdy
                 + PCN*dx + NCS*dx + NCW*dy + gam*dxdy;
      an[i][j] = -NCN*dx - gam*dxdy;
      as[i][j] = -PCS*dx - gam*dxdy;
      S[i][j]= (p[i+1][j] - p[i][j])*dy;
      }

  }

// Right Edge Interior

   j = N-1;

   for (int i=1;i<M-2;i++)
   {

      //ce = rho*(u[i][j+1] + u[i+1][j+1])*0.5;
      cw = rho*(u[i][j] + u[i+1][j])*0.5;
      cn = rho*(v[i][j] + v[i-1][j])*0.5;
      cs = rho*(v[i+1][j] + v[i][j])*0.5;

      //NCE = (std::abs(ce)-ce)/2.0;
      NCW = (std::abs(cw)-cw)/2.0;
      NCN = (std::abs(cn)-cn)/2.0;
      NCS = (std::abs(cs)-cs)/2.0;

      //PCE = (std::abs(ce)+ce)/2.0;
      PCW = (std::abs(cw)+cw)/2.0;
      PCN = (std::abs(cn)+cn)/2.0;
      PCS = (std::abs(cs)+cs)/2.0;

      ae[i][j] = 0.0;
      aw[i][j] = -PCW*dy - gam*dydx - gam*dydx/3.0;
      ao[i][j] = NCW*dy + gam*dydx + gam*dydx
                 + PCN*dx + NCS*dx + 3.0*gam*dydx + gam*dxdy;
      an[i][j] = -NCN*dx - gam*dxdy;
      as[i][j] = -PCS*dx - gam*dxdy;
      S[i][j]= (p[i+1][j] - p[i][j])*dy - rho*ur*vr*dy + (8.0/3.0)*gam*vr*dydx;
   }


// Bottom Left cell (2nd row from bottom wall)

    i = M-2;
    j = 0;
      ce = rho*(u[i+1][j+1] + u[i][j+1])*0.5;
      cn = rho*(v[i][j] + v[i-1][j])*0.5;
      cs = rho*(v[i][j] + v[i+1][j])*0.5;

      NCE = (std::abs(ce)-ce)/2.0;
      NCN = (std::abs(cn)-cn)/2.0;
      NCS = (std::abs(cs)-cs)/2.0;

      PCE = (std::abs(ce)+ce)/2.0;
      PCN = (std::abs(cn)+cn)/2.0;
      PCS = (std::abs(cs)+cs)/2.0;

      ae[i][j] = -NCE*dy - gam*dydx - gam*dydx/3.0;
      aw[i][j] = 0.0;
      ao[i][j] = PCE*dy + gam*dydx + gam*dydx
                 + PCN*dx + NCS*dx + 3.0*gam*dydx + gam*dxdy;
      an[i][j] = -NCN*dx - gam*dxdy;
      as[i][j] = 0.0;
      S[i][j]= (p[i-1][j] - p[i][j])*dy + PCS*vb*dx + gam*vb*dxdy
                + rho*ul*vl*dy + (8.0/3.0)*gam*vl*dydx;



//Bottom Edge Interior

    i = M-2;

    for (int j=1;j<N-1;j++)
    {
      ce = rho*(u[i][j+1] + u[i+1][j+1])*0.5;
      cw = rho*(u[i][j] + u[i+1][j])*0.5;
      cn = rho*(v[i][j] + v[i-1][j])*0.5;
      cs = rho*(v[i][j] + v[i+1][j])*0.5;

      NCE = (std::abs(ce)-ce)/2.0;
      NCW = (std::abs(cw)-cw)/2.0;
      NCN = (std::abs(cn)-cn)/2.0;
      NCS = (std::abs(cs)-cs)/2.0;

      PCE = (std::abs(ce)+ce)/2.0;
      PCW = (std::abs(cw)+cw)/2.0;
      PCN = (std::abs(cn)+cn)/2.0;
      PCS = (std::abs(cs)+cs)/2.0;

      ae[i][j] = -NCE*dy - gam*dydx;
      aw[i][j] = -PCW*dy - gam*dydx;
      ao[i][j] = PCE*dy + gam*dydx + gam*dydx + gam*dxdy
                 + PCN*dx + NCS*dx + NCW*dy + gam*dxdy;
      an[i][j] = -NCN*dx - gam*dxdy;
      as[i][j] = 0.0;
      S[i][j]= (p[i-1][j] - p[i][j])*dy + PCS*vb*dx + gam*vb*dxdy;
    }

// Bottom Right cell
    i = M-2;
    j = N-1;
      ce = rho*(u[i+1][j+1] + u[i][j+1])*0.5;
      cw = rho*(u[i][j] + u[i+1][j])*0.5;
      cn = rho*(v[i][j] + v[i-1][j])*0.5;
      cs = rho*(v[i][j] + v[i+1][j])*0.5;

      NCE = (std::abs(ce)-ce)/2.0;
      NCW = (std::abs(cw)-cw)/2.0;
      NCN = (std::abs(cn)-cn)/2.0;
      NCS = (std::abs(cs)-cs)/2.0;

      PCE = (std::abs(ce)+ce)/2.0;
      PCW = (std::abs(cw)+cw)/2.0;
      PCN = (std::abs(cn)+cn)/2.0;
      PCS = (std::abs(cs)+cs)/2.0;

      ae[i][j] = 0.0;
      aw[i][j] = -PCW*dy - gam*dydx - gam*dydx/3.0;
      ao[i][j] = 3.0*gam*dydx + gam*dydx + gam*dydx
                 + PCN*dx + NCS*dx + NCW*dy + gam*dxdy;
      an[i][j] = -NCN*dx - gam*dxdy;
      as[i][j] = 0.0;
      S[i][j]= (p[i-1][j] - p[i][j])*dy + PCS*vb*dx + gam*vb*dxdy
               - rho*ur*vr*dy + (8.0/3.0)*gam*vr*dydx;
/*
    std::cout << "aw is:" << "\n";
    print_array2d(aw);
    std::cout << "ae is:" << "\n";
    print_array2d(ae);
    std::cout << "an is:" << "\n";
    print_array2d(an);
    std::cout << "as is:" << "\n";
    print_array2d(as);
    std::cout << "ao is:" << "\n";
    print_array2d(ao);
    std::cout << "Source is:" << "\n";
    print_array2d(S);
*/
    ymomentum f;
    std::tie(vcorrnew,res) = f.yADI(ae,aw,ao,an,as,S,vcorr,M,N);
   // std::cout << "final v is" << "\n";
   // print_array2d(v);

return std::make_tuple(vcorrnew,ao,res);
}


std::tuple<vec2d,double> ymomentum::yADI(const vec2d& ae,const vec2d& aw,const vec2d& ao,const vec2d& an,
                     const vec2d& as,const vec2d& S,vec2d& vcorr,
                     const int& M,const int& N)
{

   ymomentum f;
   // TDMA Coeffs for row and column sweep (different lengths)
   vec1d a_r(N,0.0);vec1d b_r(N,0.0);vec1d c_r(N,0.0);
   vec1d d_r(N,0.0);vec1d ans_r(N,0.0);
   vec1d a_c(M-1,0.0);vec1d b_c(M-1,0.0);vec1d c_c(M-1,0.0);
   vec1d d_c(M-1,0.0);vec1d ans_c(M-1,0.0);
   vec2d vdiff(M+1,vec1d(N,0.0)); int id=0;
   alpha_y=0.2;
  std::cout << "vinit is" << "\n";
  print_array2d(vcorr);
double res = 1.0;
//while (res > 1e-11)
for (int kk=0;kk<2;kk++)
{
    // ROW SWEEP
   for (int i=0;i<M-1;i++)
   {
       for (int j=0;j<N;j++)
       {
          a_r[j] = aw[i][j];
          b_r[j] = ao[i][j]*(1.0+alpha_y);
          c_r[j] = ae[i][j];

        if (i==0)
        {
          d_r[j] = S[i][j] - as[i][j]*vcorr[i+1][j];
        }
        else if (i==M-2)
        {
          d_r[j] = S[i][j] - an[i][j]*vcorr[i-1][j];
        }
        else
        {
          d_r[j] = S[i][j] - as[i][j]*vcorr[i+1][j] - an[i][j]*vcorr[i-1][j];
        }

        // Correction
       if (j==0)
       {
         d_r[j] = d_r[j] - ao[i][j]*vcorr[i][j] - ae[i][j]*vcorr[i][j+1];
       }
       else if (j==N-1)
       {
         d_r[j] = d_r[j] - ao[i][j]*vcorr[i][j] - aw[i][j]*vcorr[i][j-1];
       }
       else
       {
         d_r[j] = d_r[j] - ao[i][j]*vcorr[i][j] - aw[i][j]*vcorr[i][j-1] - ae[i][j]*vcorr[i][j+1];
       }
       }
  /* std::cout << "The input arrays row sweep are" <<"\n";
   print_array1d(a_r);print_array1d(b_r);
   print_array1d(c_r);
   print_array1d(d_r); */
    ans_r = TDMA(a_r,b_r,c_r,d_r);
   // std::cout << "ans_r is" << "\n";
   // print_array1d(ans_r);

       for (int q=0;q<N;q++)
       {
         vdiff[i][q] = ans_r[q];
       }

   }

   //std::cout << "vdiff after row sweep is" << "\n";
   //print_array2d(vdiff);

   vcorr = sum(vcorr,vdiff,M+1,N);

   //std::cout << "vcorr after row sweep is" << "\n";
   //print_array2d(vcorr);

  // COLUMN SWEEP

   for (int j=0;j<N;j++)
   {
       for (int i=0;i<M-1;i++)
       {
          id = i;//i-1;
          a_c[id] = as[i][j];
          b_c[id] = ao[i][j]*(1.0+alpha_y);
          c_c[id] = an[i][j];

         if (j==0)
         {
           d_c[id] = S[i][j] - ae[i][j]*vcorr[i][j+1];
         }
         else if (j==N-1)
         {
           d_c[id] = S[i][j] - aw[i][j]*vcorr[i][j-1];
         }
         else
         {
           d_c[id] = S[i][j] - ae[i][j]*vcorr[i][j+1] - aw[i][j]*vcorr[i][j-1];
         }

         // Correction
         if (i==M-2)
         {
           d_c[id] = d_c[id] - ao[i][j]*vcorr[i][j] - an[i][j]*vcorr[i-1][j];
         }
         else if (i==0)
         {
           d_c[id] = d_c[id] - ao[i][j]*vcorr[i][j] - as[i][j]*vcorr[i+1][j];
         }
         else
         {
           d_c[id] = d_c[id] - ao[i][j]*vcorr[i][j] - an[i][j]*vcorr[i-1][j] - as[i][j]*vcorr[i+1][j];
         }
       }

   // TDMA
 /*  std::cout << "The input arrays column sweep are" <<"\n";
   print_array1d(a_c);print_array1d(b_c);
   print_array1d(c_c);
   print_array1d(d_c); */
   ans_c = TDMA(a_c,b_c,c_c,d_c);
   //std::cout << "ans_c is" << "\n";
   //print_array1d(ans_c);
   for (int q=1;q<M-1;q++)
   {
     vdiff[q][j] = ans_c[q-1];
   }

   }

   //std::cout << "vdiff after column sweep is" << "\n";
   //print_array2d(vdiff);
   vcorr = sum(vcorr,vdiff,M+1,N);
   //std::cout << "vcorr after column sweep is" << "\n";
   //print_array2d(vcorr);

   res = f.yresid(ae,aw,ao,an,as,S,vcorr,M,N);

   //std::cout << "The Y-MOMENTUM residual is" << res << "\n";
}
   return std::make_tuple(vcorr,res);

}




double ymomentum::yresid(const vec2d& ae,const vec2d& aw,const vec2d& ao,const vec2d& an,
                     const vec2d& as,const vec2d& S,vec2d& vcorr,
                     const int& M,const int& N)
{
    vec2d resid(M,vec1d(N,0.0));
    double residroot = 0.0; double temp=0.0;
   // Interior Cells

   for (int j=1;j<N-1;j++)
   {
       for (int i=1;i<M-2;i++)
       {
         resid[i][j] =  S[i][j] - ao[i][j]*vcorr[i][j] - an[i][j]*vcorr[i-1][j]
                        - as[i][j]*vcorr[i+1][j] - ae[i][j]*vcorr[i][j+1] - aw[i][j]*vcorr[i][j-1];
       }
   }

   // Left vertical cells

   int j=0;

   for (int i=1;i<M-2;i++)
   {
     resid[i][j] =  S[i][j] - ao[i][j]*vcorr[i][j] - an[i][j]*vcorr[i-1][j]
                    - as[i][j]*vcorr[i+1][j] - ae[i][j]*vcorr[i][j+1];
   }

  // Right cell vertical

    j=N-1;

   for (int i=1;i<M-2;i++)
   {
     resid[i][j] =  S[i][j] - ao[i][j]*vcorr[i][j] - an[i][j]*vcorr[i-1][j]
                    - as[i][j]*vcorr[i+1][j] - aw[i][j]*vcorr[i][j-1];
   }

  // Top Edge

    int i=0;

   for (int j=1;j<N-1;j++)
   {
     resid[i][j] =  S[i][j] - ao[i][j]*vcorr[i][j] - as[i][j]*vcorr[i+1][j]
                    - ae[i][j]*vcorr[i][j+1] - aw[i][j]*vcorr[i][j-1];
   }

   // Bottom Edge

    i=M-2;

   for (int j=1;j<N-1;j++)
   {
     resid[i][j] =  S[i][j] - ao[i][j]*vcorr[i][j] - an[i][j]*vcorr[i-1][j]
                    - ae[i][j]*vcorr[i][j+1] - aw[i][j]*vcorr[i][j-1];
   }


   // Top Left Corner

    i=0;

    j =0;

    resid[i][j] =  S[i][j] - ao[i][j]*vcorr[i][j] - as[i][j]*vcorr[i+1][j]
                    - ae[i][j]*vcorr[i][j+1];


   // Top Right Corner

    i=0;

    j =N-1;

    resid[i][j] =  S[i][j] - ao[i][j]*vcorr[i][j] - as[i][j]*vcorr[i+1][j]
                    - aw[i][j]*vcorr[i][j-1];

   // Bottom Left Corner

    i=M-2;

    j =0;

    resid[i][j] =  S[i][j] - ao[i][j]*vcorr[i][j] - an[i][j]*vcorr[i-1][j]
                    - ae[i][j]*vcorr[i][j+1];

   // Bottom Right Corner

    i=M-2;

    j =N-1;

    resid[i][j] =  S[i][j] - ao[i][j]*vcorr[i][j] - an[i][j]*vcorr[i-1][j]
                    - aw[i][j]*vcorr[i][j-1];

   // Compute Residual

   for (int i=0;i<M-1;i++)
   {
       for (int j=0;j<N;j++)
       {
          temp = resid[i][j];
          residroot = residroot + pow(temp,2);
       }
   }

   residroot = sqrt(residroot);

   return(residroot);

}







