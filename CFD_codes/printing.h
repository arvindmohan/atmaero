/* Library to just print arrays out .... Just to make life easier from for loops */

#ifndef PRINTING_H_INCLUDED
#define PRINTING_H_INCLUDED
#include <iostream>
#include <fstream>
#include <string>

template <class T>
void print_array2d (const std::vector< std::vector<T> >& a)
{
   int m = a.size();
   int n = a[0].size();
    for (int i=0;i<m;i++)
    {
        for (int j=0;j<n;j++)
        {
            std::cout << a[i][j] << "\t";  //printf("%f", phi_init[i][j]);
        }
        std::cout << "\n";
    }
    //std::cout << "size is" << asize << "\n";
}

template <class T>
void print_array1d (const std::vector<T>& x)
{
    const int asize = x.size();
  //std::cout << "The 1d array is:" << "\n";
    for (int i=0;i<asize;i++)
        {
            std::cout << x[i] << "\t";  //printf("%f", phi_init[i][j]);
        }
        std::cout << "\n";
}

/* To write data to file */

template <class T>
void writefile_array2d(const std::vector< std::vector<T> >& myarr, const int& m, const int& n,const std::string& fname)
{
    std::ofstream outfile;
    outfile.open(fname);
    std::cout << "Writing" <<  fname << " \n ";
    for (int i=0;i<m;i++)
    {
        for (int j=0;j<n;j++)
        {
            outfile << myarr[i][j] << "\t";  //printf("%f", phi_init[i][j]);
        }
        outfile << "\n";
    }
     outfile.close();
     std::cout << "Done" << "\n";
}

template <class T>
void writefile_array1d(const std::vector<T>& myarr,const std::string& fname)
{
    const int n = myarr.size();
    std::ofstream outfile;
    outfile.open(fname);
    std::cout << "Writing" <<  fname << " \n ";
        for (int j=0;j<n;j++)
        {
            outfile << myarr[j] << "\t";
        }
    //outfile << "\n";

     outfile.close();
     std::cout << "Done" << "\n";
}



#endif // PRINTING_H_INCLUDED
