#ifndef SOLVE_MOM_H_INCLUDED
#define SOLVE_MOM_H_INCLUDED
#include "includes.h"
#include "residual.h"

std::tuple<vec2d,double> ADI(const vec2d& ae,const vec2d& aw,const vec2d& ao,const vec2d& an,
                     const vec2d& as,const vec2d& S,vec2d& uC,const int& Istep,const int& Jstep,
                     const int& M,const int& N)
{
 int inX=0;
 int inY=0;
 int remX=0;
 int remY=0;
 double alpha_x = 0.2;
 double maxiter_x = 10;
 double res=0.0;
 vec2d udiff(M,vec1d(N,0.0));

    /*** ROW SWEEP **/

    // Inlet square domain

   inX = Jstep; inY = Istep;
   //std::cout << inX << "\t" << inY << "\n";
   vec1d a_in(inX,0.0);vec1d b_in(inX,0.0);vec1d c_in(inX,0.0);
   vec1d d_in(inX,0.0);vec1d ans_in(inX,0.0);
   //print_array1d(a_in);

for (int k=0;k<maxiter_x;k++)
{

for (int i=0;i<inY;i++)
{
    for (int j=0;j<inX;j++)
    {
      a_in[j] = aw[i][j];
      b_in[j] = ao[i][j]*(1.0+alpha_x);
      c_in[j] = ae[i][j];

     if (i==0)
     {
       d_in[j] = S[i][j] - as[i][j]*uC[i+1][j];
     }
      else if (i==inY)
     {
       d_in[j] = S[i][j] - an[i][j]*uC[i-1][j];
     }
     else
     {
       d_in[j] = S[i][j] - an[i][j]*uC[i-1][j] - as[i][j]*uC[i+1][j];
     }

     // Correction
     if (j==0)
     {
       d_in[j] = d_in[j] - ao[i][j]*uC[i][j] - ae[i][j]*uC[i][j+1];
     }
     else
     {
       d_in[j] = d_in[j] - ao[i][j]*uC[i][j] - ae[i][j]*uC[i][j+1] - aw[i][j]*uC[i][j-1];
     }
    }
   // TDMA

   ans_in = TDMA(a_in,b_in,c_in,d_in);

   for (int q=0;q<inX;q++)
   {
     udiff[i][q] = ans_in[q];
   }

}

   // Remaining domain (Mapping Reqd.)

   remX = N-Jstep; remY = M-1;
   //std::cout << remX << "\t" << remY << "\n";
   vec1d a_rem(remX,0.0);vec1d b_rem(remX,0.0);vec1d c_rem(remX,0.0);
   vec1d d_rem(remX,0.0);vec1d ans_rem(remX,0.0);
   //print_array1d(a_rem);
   double jd=0;double qd=0;


for (int i=0;i<M;i++)
{
    for (int j=0;j<remX;j++)
    {
      jd =  j + Jstep;
      a_rem[j] = aw[i][jd];
      b_rem[j] = ao[i][jd]*(1.0+alpha_x);
      c_rem[j] = ae[i][jd];

     if (i==0)
     {
       d_rem[j] = S[i][jd] - as[i][jd]*uC[i+1][jd];
     }
      else if (i==M-1)
     {
       d_rem[j] = S[i][jd] - an[i][jd]*uC[i-1][jd];
     }
     else
     {
       d_rem[j] = S[i][jd] - an[i][jd]*uC[i-1][jd] - as[i][jd]*uC[i+1][jd];
     }

     // Correction
     if (j==Jstep && i >= Istep)
     {
           d_rem[j] = d_rem[j] - ao[i][jd]*uC[i][jd] - ae[i][jd]*uC[i][jd+1];
     }

     else if (jd==N-1)
     {
          d_rem[j] = d_rem[j] - ao[i][jd]*uC[i][jd]  - aw[i][jd]*uC[i][jd-1];
     }
     else
     {
          d_rem[j] = d_rem[j] - ao[i][jd]*uC[i][jd]  - aw[i][jd]*uC[i][jd-1] - ae[i][jd]*uC[i][jd+1];
     }

    }
   // TDMA

   ans_rem = TDMA(a_rem,b_rem,c_rem,d_rem);
   //std::cout << "ans_rem is" << "\n";
   //print_array1d(ans_rem);
   for (int q=0;q<remX;q++)
   {
     qd = q + Jstep;
     udiff[i][qd] = ans_rem[q];
   }

}

 uC = sum(uC,udiff,M,N);

   /*** COLUMN SWEEP **/

   // Inlet square domain (Mapping Reqd.)
   inX = Jstep; inY = Istep;
   //std::cout << inX << "\t" << inY << "\n";
   vec1d ac_in(inY,0.0);vec1d bc_in(inY,0.0);vec1d cc_in(inY,0.0);
   vec1d dc_in(inY,0.0);vec1d ansc_in(inY,0.0);
   //print_array1d(ac_in);

for (int j=0;j<inX;j++)
{
    for (int i=0;i<inY;i++)
    {
      ac_in[i] = aw[i][j];
      bc_in[i] = ao[i][j]*(1.0+alpha_x);
      cc_in[i] = ae[i][j];

     if (j==0)
     {
       dc_in[i] = S[i][j] - ae[i][j]*uC[i][j+1];
     }
     else
     {
       dc_in[i] = S[i][j] - ae[i][j]*uC[i][j+1] - aw[i][j]*uC[i][j-1];
     }

     // Correction
     if (i==inY-1)
     {
       dc_in[i] = dc_in[j] - ao[i][j]*uC[i][j] - an[i][j]*uC[i-1][j];
     }
     else if (i==0)
     {
       dc_in[i] = dc_in[j] - ao[i][j]*uC[i][j]  - as[i][j]*uC[i+1][j];
     }
     else
     {
       dc_in[i] = dc_in[j] - ao[i][j]*uC[i][j] - an[i][j]*uC[i-1][j] - as[i][j]*uC[i+1][j];
     }
    }

   // TDMA

   ansc_in = TDMA(ac_in,bc_in,cc_in,dc_in);

   for (int q=0;q<inY;q++)
   {
     udiff[q][j] = ansc_in[q];
   }

}


   // Remaining domain (Mapping Reqd.)

   remX = N-Jstep; remY = M;
   //std::cout << remX << "\t" << remY << "\n";
   vec1d ac_rem(M,0.0);vec1d bc_rem(M,0.0);vec1d cc_rem(M,0.0);
   vec1d dc_rem(M,0.0);vec1d ansc_rem(M,0.0);
   //print_array1d(a_rem);

for (int j=Jstep;j<remX;j++)
{
    for (int i=0;i<M;i++)
    {
      ac_rem[i] = aw[i][j];
      bc_rem[i] = ao[i][j]*(1.0+alpha_x);
      cc_rem[i] = ae[i][j];

     if (j==Jstep && i>= Istep)
     {
       dc_rem[i] = S[i][j] - ae[i][j]*uC[i][j+1];
     }
      else if (j==N-1)
     {
       dc_rem[i] = S[i][j] - aw[i][j]*uC[i][j-1];
     }
     else
     {
       dc_rem[i] = S[i][j] - ae[i][j]*uC[i][j+1] - aw[i][j]*uC[i][j-1];
     }

     // Correction
     if (i==0)
     {
           dc_rem[i] = dc_rem[i] - ao[i][j]*uC[i][j] - as[i][j]*uC[i+1][j];
     }

     else if (i==M-1)
     {
          dc_rem[i] = dc_rem[i] - ao[i][j]*uC[i][j]  - an[i][j]*uC[i-1][j];
     }
     else
     {
          dc_rem[i] = dc_rem[i] - ao[i][j]*uC[i][j]  - as[i][j]*uC[i+1][j] - an[i][j]*uC[i-1][j];
     }

    }
   // TDMA

   ansc_rem = TDMA(ac_rem,bc_rem,cc_rem,dc_rem);
   //std::cout << "ansc_rem is" << "\n";
   //print_array1d(ansc_rem);
   for (int q=0;q<M;q++)
   {
     //qd = q + Istep;
     udiff[q][j] = ansc_rem[q];
   }

}

//std::cout << "udiff is" << "\n";
//print_array2d(udiff);
uC = sum(uC,udiff,M,N);

res = resid(ae,aw,ao,an,as,S,uC,Istep,Jstep,M,N);

}


return std::make_tuple(uC,alpha_x);


}








#endif // SOLVE_MOM_H_INCLUDED
