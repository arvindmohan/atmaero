#include "momentum.h"
#include "includes.h"
#include "arrayops.h"
#include "printing.h"


std::tuple<vec2d,vec2d,double> xmomentum::xlinks(const vec2d& u,const vec2d& v,const vec2d& p,const int& N,const int& M)
{

   //std::cout << "This is X momentum" << "\n";
    rho = 1000; gam = 0.001;
    ut = 0.01; ul=ur=ub=0.0;
    vl=vt=vb=0.0; vr=0.0;
    int id=0;
    double res = 1.0;
   double dx,dy,dxdy,dydx,ce,cw,cn,cs,B,H;
   double NCE,PCE,NCW,PCW,NCN,PCN,NCS,PCS;
   vec1d x(N,0.0); vec1d y(M,0.0);
   vec2d ae(M,vec1d(N,0.0));
   vec2d aw(M,vec1d(N,0.0));
   vec2d an(M,vec1d(N,0.0));
   vec2d as(M,vec1d(N,0.0));
   vec2d aox(M,vec1d(N,0.0));
   vec2d S(M,vec1d(N,0.0));
   vec2d ucorr(M,vec1d(N+1,0.0));
   vec2d ucorrnew(M,vec1d(N+1,0.0));

   B=0.01;H=0.01;
   dx = B/N;
   dy = H/M;
   dydx = dy/dx; dxdy = dx/dy;

   // DEFINING LINK COEFFICIENTS

  // TOP LEFT 2nd cell
  cw = rho*(u[0][1] + ul)*0.5;
  ce = rho*(u[0][1] + u[0][2])*0.5;
  cn = rho*vt;
  cs = rho*(v[0][1] + v[1][1])*0.5;

  NCE = (std::abs(ce)-ce)/2.0;
  NCW = (std::abs(cw)-cw)/2.0;
  NCN = (std::abs(cn)-cn)/2.0;
  NCS = (std::abs(cs)-cs)/2.0;

  PCE = (std::abs(ce)+ce)/2.0;
  PCW = (std::abs(cw)+cw)/2.0;
  PCN = (std::abs(cn)+cn)/2.0;
  PCS = (std::abs(cs)+cs)/2.0;


  ae[0][1] = -NCE*dy - gam*dydx;
  aw[0][1] = 0.0;
  aox[0][1] = PCE*dy + gam*dydx + gam*dydx
             + NCS*dx + NCW*dy + 3.0*gam*dxdy + gam*dxdy;
  an[0][1] = 0.0;
  as[0][1] = -PCS*dx - gam*dxdy - (gam/3.0)*dxdy;
  S[0][1] = (p[0][0] - p[0][1])*dy + (8.0/3.0)*gam*ut*dxdy
            - rho*vt*ut*dx + gam*ul*dydx + PCW*ul*dy;

  // TOP EDGE interior

 int i = 0;

 for (int j=2;j<N;j++)
 {
  cw = rho*(u[i][j] + u[i][j-1])*0.5;
  ce = rho*(u[i][j] + u[i][j+1])*0.5;
  cn = rho*vt;
  cs = rho*(v[i][j] + v[i+1][j])*0.5;

  NCE = (std::abs(ce)-ce)/2.0;
  NCW = (std::abs(cw)-cw)/2.0;
  NCN = (std::abs(cn)-cn)/2.0;
  NCS = (std::abs(cs)-cs)/2.0;

  PCE = (std::abs(ce)+ce)/2.0;
  PCW = (std::abs(cw)+cw)/2.0;
  PCN = (std::abs(cn)+cn)/2.0;
  PCS = (std::abs(cs)+cs)/2.0;

  ae[i][j] = -NCE*dy - gam*dydx;
  aw[i][j] = -PCW*dy - gam*dydx;
  aox[i][j] = PCE*dy + gam*dydx + gam*dydx
             + NCS*dx + NCW*dy
             + 3.0*gam*dxdy + gam*dxdy;
  an[i][j] = 0.0;
  as[i][j] = -PCS*dx - gam*dxdy - (gam/3.0)*dxdy;
  S[i][j] = (p[i][j-1] - p[i][j])*dy + (8.0/3.0)*gam*ut*dxdy
            - rho*vt*ut*dx;
 }

// Top Right cell

  cw = rho*(u[0][N-1] + u[0][N-2])*0.5;
  ce = rho*(u[0][N-1] + ur)*0.5;
  cn = rho*vt;
  cs = rho*(v[0][N-1] + v[1][N-1])*0.5;

  NCE = (std::abs(ce)-ce)/2.0;
  NCW = (std::abs(cw)-cw)/2.0;
  NCN = (std::abs(cn)-cn)/2.0;
  NCS = (std::abs(cs)-cs)/2.0;

  PCE = (std::abs(ce)+ce)/2.0;
  PCW = (std::abs(cw)+cw)/2.0;
  PCN = (std::abs(cn)+cn)/2.0;
  PCS = (std::abs(cs)+cs)/2.0;

  ae[0][N-1] = 0.0;
  aw[0][N-1] = -PCW*dy - gam*dydx;
  aox[0][N-1] = PCE*dy + gam*dydx + gam*dydx
             + NCS*dx + NCW*dy
             + 3.0*gam*dxdy + gam*dxdy;
  an[0][N-1] = 0.0;
  as[0][N-1] = -PCS*dx - gam*dxdy - (gam/3.0)*dxdy;
  S[0][N-1] = (p[0][N-2] - p[0][N-1])*dy + (8.0/3.0)*gam*ut*dxdy
            - rho*vt*ut*dx + gam*ur*dydx + NCE*ur*dy;
// INTERIOR Cells

// Left Edge 2nd cell

int j = 1;

for (int i=1;i<N-1;i++)
{
   cw = rho*(u[i][j-1] + u[i][j])*0.5;
   ce = rho*(u[i][j] + u[i][j+1])*0.5;
   cn = rho*(v[i][j-1] + v[i][j])*0.5;
   cs = rho*(v[i+1][j-1] + v[i+1][j])*0.5;

  NCE = (std::abs(ce)-ce)/2.0;
  NCW = (std::abs(cw)-cw)/2.0;
  NCN = (std::abs(cn)-cn)/2.0;
  NCS = (std::abs(cs)-cs)/2.0;

  PCE = (std::abs(ce)+ce)/2.0;
  PCW = (std::abs(cw)+cw)/2.0;
  PCN = (std::abs(cn)+cn)/2.0;
  PCS = (std::abs(cs)+cs)/2.0;

   ae[i][j] = -NCE*dy - gam*dydx;
   aw[i][j] = 0.0;
   aox[i][j] = PCE*dy + gam*dydx + gam*dxdy
             + NCS*dx + NCW*dy/2.0
             + gam*dydx + gam*dxdy + PCN*dx;
   an[i][j] = -NCN*dy - gam*dxdy;
   as[i][j] = -PCS*dx - gam*dxdy;
   S[i][j] = (p[i][j-1] - p[i][j])*dy + (PCW*dy - gam*dydx)*ul; // + gam*dydx?
}


// Interior nodes

for (int i=1;i<M-1;i++)
{
    for (int j=2;j<N;j++)
    {
       cw = rho*(u[i][j-1] + u[i][j])*0.5;
       ce = rho*(u[i][j] + u[i][j+1])*0.5;
       cn = rho*(v[i][j-1] + v[i][j])*0.5;
       cs = rho*(v[i+1][j-1] + v[i+1][j])*0.5;

      NCE = (std::abs(ce)-ce)/2.0;
      NCW = (std::abs(cw)-cw)/2.0;
      NCN = (std::abs(cn)-cn)/2.0;
      NCS = (std::abs(cs)-cs)/2.0;

      PCE = (std::abs(ce)+ce)/2.0;
      PCW = (std::abs(cw)+cw)/2.0;
      PCN = (std::abs(cn)+cn)/2.0;
      PCS = (std::abs(cs)+cs)/2.0;

       ae[i][j] = -NCE*dy - gam*dydx;
       aw[i][j] = -PCW*dy - gam*dydx;
       aox[i][j] = PCE*dy + gam*dydx + gam*dxdy
                 + NCS*dx + NCW*dy
                 + gam*dydx + gam*dxdy + PCN*dx;
       an[i][j] = -NCN*dy - gam*dxdy;
       as[i][j] = -PCS*dx - gam*dxdy;
       S[i][j] = (p[i][j-1] - p[i][j])*dy;
    }
}

// Right Edge

j = N-1;

for (int i=1;i<N-1;i++)
{
   cw = rho*(u[i][j-1] + u[i][j])*0.5;
   ce = rho*(u[i][j] + u[i][j+1])*0.5;
   cn = rho*(v[i][j-1] + v[i][j])*0.5;
   cs = rho*(v[i+1][j-1] + v[i+1][j])*0.5;

  NCE = (std::abs(ce)-ce)/2.0;
  NCW = (std::abs(cw)-cw)/2.0;
  NCN = (std::abs(cn)-cn)/2.0;
  NCS = (std::abs(cs)-cs)/2.0;

  PCE = (std::abs(ce)+ce)/2.0;
  PCW = (std::abs(cw)+cw)/2.0;
  PCN = (std::abs(cn)+cn)/2.0;
  PCS = (std::abs(cs)+cs)/2.0;

   ae[i][j] = 0.0;
   aw[i][j] = -PCW*dy - gam*dydx;
   aox[i][j] = PCE*dy+ gam*dydx + gam*dxdy
             + NCS*dx + NCW*dy
             + gam*dydx + gam*dxdy + PCN*dx;
   an[i][j] = -NCN*dy - gam*dxdy;
   as[i][j] = -PCS*dx - gam*dxdy;
   S[i][j] = (p[i][j-1] - p[i][j])*dy - (NCE*dy - gam*dydx)*ur; // + NVE?
}


// Bottom Edge 2nd Left cell

  cw = rho*(u[N-1][1] + u[N-1][0])*0.5;
  ce = rho*(u[N-1][1] + u[N-1][2])*0.5;
  cn = rho*(v[N-1][1] + v[N-2][1])*0.5;
  cs = rho*vb;

  NCE = (std::abs(ce)-ce)/2.0;
  NCW = (std::abs(cw)-cw)/2.0;
  NCN = (std::abs(cn)-cn)/2.0;
  NCS = (std::abs(cs)-cs)/2.0;

  PCE = (std::abs(ce)+ce)/2.0;
  PCW = (std::abs(cw)+cw)/2.0;
  PCN = (std::abs(cn)+cn)/2.0;
  PCS = (std::abs(cs)+cs)/2.0;

  ae[N-1][1] = -NCE*dy - gam*dydx;
  aw[N-1][1] = 0.0;
  aox[N-1][1] = PCE*dy + gam*dydx + gam*dydx
             + PCN*dx + NCW*dy
             + 3.0*gam*dxdy + gam*dxdy;
  an[N-1][1] = -NCN*dy - gam*dxdy - (gam/3.0)*dxdy;
  as[N-1][1] = 0.0;
  S[N-1][1] = (p[0][0] - p[0][1])*dy + (8.0/3.0)*gam*ub*dxdy
              + rho*vb*ub*dx + gam*ul*dydx + PCW*ul*dy;

// Bottom Edge Interior
 i = N-1;

 for (int j=2;j<N-1;j++)
 {
  cw = rho*(u[i][j] + u[i][j-1])*0.5;
  ce = rho*(u[i][j] + u[i][j+1])*0.5;
  cn = rho*(v[i-1][j] + v[i][j])*0.5;
  cs = rho*vb;

  NCE = (std::abs(ce)-ce)/2.0;
  NCW = (std::abs(cw)-cw)/2.0;
  NCN = (std::abs(cn)-cn)/2.0;
  NCS = (std::abs(cs)-cs)/2.0;

  PCE = (std::abs(ce)+ce)/2.0;
  PCW = (std::abs(cw)+cw)/2.0;
  PCN = (std::abs(cn)+cn)/2.0;
  PCS = (std::abs(cs)+cs)/2.0;

  ae[i][j] = -NCE*dy - gam*dydx;
  aw[i][j] = -PCW*dy - gam*dydx;
  aox[i][j] = PCE*dy + gam*dydx + gam*dydx
             + PCN*dx + NCW*dy
             + 3.0*gam*dxdy + gam*dxdy;
  an[i][j] = -NCN*dx - gam*dxdy - (gam/3.0)*dxdy;
  as[i][j] = 0.0;
  S[i][j] = (p[i][j-1] - p[i][j])*dy + (8.0/3.0)*gam*ub*dxdy
            + rho*vb*ub*dx;
 }

// Bottom Right cell

  cw = rho*(u[0][N-1] + u[0][N-2])*0.5;
  ce = rho*(u[0][N-1] + ur)*0.5;
  cn = rho*(v[N-1][N-1] + v[N-2][N-1])*0.5;
  cs = rho*vb;

  NCE = (std::abs(ce)-ce)/2.0;
  NCW = (std::abs(cw)-cw)/2.0;
  NCN = (std::abs(cn)-cn)/2.0;
  NCS = (std::abs(cs)-cs)/2.0;

  PCE = (std::abs(ce)+ce)/2.0;
  PCW = (std::abs(cw)+cw)/2.0;
  PCN = (std::abs(cn)+cn)/2.0;
  PCS = (std::abs(cs)+cs)/2.0;

  ae[N-1][N-1] = 0.0;
  aw[N-1][N-1] = -PCW*dy - gam*dydx;
  aox[N-1][N-1] = PCE*dy + gam*dydx + gam*dxdy
             + PCN*dx + NCW*dy
             + 3.0*gam*dxdy + gam*dxdy;
  an[N-1][N-1] = -NCN*dx - gam*dxdy - (gam/3.0)*dxdy;
  as[N-1][N-1] = 0.0;
  S[N-1][N-1] = (p[N-1][N-2] - p[N-1][N-1])*dy + (8.0/3.0)*gam*ub*dxdy
            + rho*vb*ub*dx + gam*ur*dydx + NCE*ur*dy; // -rho*vb
/*
std::cout << "aw is:" << "\n";
print_array2d(aw);
std::cout << "ae is:" << "\n";
print_array2d(ae);
std::cout << "an is:" << "\n";
print_array2d(an);
std::cout << "as is:" << "\n";
print_array2d(as);
std::cout << "aox is:" << "\n";
print_array2d(aox);
std::cout << "Source is:" << "\n";
print_array2d(S);
*/
xmomentum f;
std::tie(ucorrnew,res) = f.xADI(ae,aw,aox,an,as,S,ucorr,M,N);
//std::cout << "final u is" << "\n";
//print_array2d(ucorr);

return std::make_tuple(ucorrnew,aox,res);
}




std::tuple<vec2d,double> xmomentum::xADI(const vec2d& ae,const vec2d& aw,const vec2d& aox,const vec2d& an,
                     const vec2d& as,const vec2d& S,vec2d& ucorr,
                     const int& M,const int& N)
{
//    arrayops <double> f;
   xmomentum f;
   // TDMA Coeffs for row and column sweep (different lengths)
   vec1d a_r(N-1,0.0);vec1d b_r(N-1,0.0);vec1d c_r(N-1,0.0);
   vec1d d_r(N-1,0.0);vec1d ans_r(N-1,0.0);vec1d a_c(N,0.0);vec1d ch_1(N,0.0);vec1d dh_1(N,0.0);
   vec1d b_c(N,0.0);vec1d c_c(N,0.0);vec1d d_c(N,0.0);vec1d ans_c(N,0.0);
   vec2d udiff(M,vec1d(N+1,0.0)); int id=0;
   alpha_x=0.2;

  //std::cout << "uinit is" << "\n";
  //print_array2d(ucorr);

double res=1.0;
for (int k=0;k<2;k++)
//while(res > 1.0e-11)
{
//std::cout << "ucorr is" <<"\n";
//print_array2d(ucorr);
//std::cout << "S[M-1][1] is" << S[M-1][2] <<"\n";

/***** ROW SWEEP *****/
// Set TDMA coefficients
for (int i=0;i<M;i++)
{
    for (int j=1;j<N;j++)
    {
      id = j-1;
      a_r[id] = aw[i][j];
      b_r[id] = aox[i][j]*(1.0+alpha_x);
      c_r[id] = ae[i][j];
     //std::cout << "S[M-1][1] inside is" << S[M-1][1] <<"\n";
     if (i==0)
     {
      d_r[id] = S[i][j] - as[i][j]*ucorr[i+1][j];
      /*std::cout << "this thing i==0 works" << "\n";
      std::cout << "dr i==0 is" << d_r[id] << "\n";
      std::cout << "S[i][j] is" << S[i][j] << "\n";
      std::cout << "as[i][j]" << as[i][j] << "\n";
      std::cout << "ucorr[i+1][j] is" << ucorr[i+1][j] << "\n";*/
     }
      else if (i==M-1)
     {
      d_r[id] = S[i][j] - an[i][j]*ucorr[i-1][j];
     /* std::cout << "i is" << i << "j is" << j << "\n";
      std::cout << "this thing i==M-1 works" << "\n";
      std::cout << "S[i][j] is" << S[i][j] << "\n";
      std::cout << "an[i][j]" << an[i][j] << "\n";
      std::cout << "ucorr[i-1][j] is" << ucorr[i-1][j] << "\n";
      std::cout << "dr i==M-1 is" << d_r[id] << "\n";*/
     }
     else
     {
       d_r[id] = S[i][j] - an[i][j]*ucorr[i-1][j] - as[i][j]*ucorr[i+1][j];
     }
     //std::cout << "Intermediate d_r" << "\n";
     //print_array1d(d_r);
     // Correction
     if (j==1)
     {
       d_r[id] = d_r[id] - aox[i][j]*ucorr[i][j] - ae[i][j]*ucorr[i][j+1];
     }
     else if (j==N-1)
     {
       d_r[id] = d_r[id] - aox[i][j]*ucorr[i][j] - aw[i][j]*ucorr[i][j-1];
     }
     else
     {
       d_r[id] = d_r[id] - aox[i][j]*ucorr[i][j] - ae[i][j]*ucorr[i][j+1] - aw[i][j]*ucorr[i][j-1];
     }
    }
   // TDMA
   //std::cout << "The input arrays row sweep are" <<"\n";
   //print_array1d(a_r);print_array1d(b_r);print_array1d(c_r);print_array1d(d_r);
   ans_r = TDMA(a_r,b_r,c_r,d_r);
   //std::cout << "ans_r is" << "\n";
   //print_array1d(ans_r);

   for (int q=1;q<N;q++)
   {
     udiff[i][q] = ans_r[q-1];
   }

}
   //std::cout << "udiff ROW sweep is" << "\n";
   //print_array2d(udiff);

   ucorr = sum(ucorr,udiff,M,N+1);

   //std::cout << "ucorr ROW sweep is" << "\n";
   //print_array2d(ucorr);

/***** COLUMN SWEEP *****/

for (int j=1;j<N;j++)
{
    for (int i=0;i<M;i++)
    {
      id = i;
      a_c[id] = as[i][j];
      b_c[id] = aox[i][j]*(1.0+alpha_x);
      c_c[id] = an[i][j];

     if (j==1)
     {
       d_c[id] = S[i][j] - ae[i][j]*ucorr[i][j+1];
     }
     else if (j==N-1)
     {
       d_c[id] = S[i][j] - aw[i][j]*ucorr[i][j-1];
     }
     else
     {
       d_c[id] = S[i][j] - ae[i][j]*ucorr[i][j+1] - aw[i][j]*ucorr[i][j-1];
     }
     //std::cout << "Intermediate d_c" << "\n";
     //print_array1d(d_c);
     // Correction
     if (i==M-1)
     {
       d_c[id] = d_c[id] - aox[i][j]*ucorr[i][j] - an[i][j]*ucorr[i-1][j];
     }
     else if (i==0)
     {
       d_c[id] = d_c[id] - aox[i][j]*ucorr[i][j] - as[i][j]*ucorr[i+1][j];
     }
     else
     {
       d_c[id] = d_c[id] - aox[i][j]*ucorr[i][j] - an[i][j]*ucorr[i-1][j] - as[i][j]*ucorr[i+1][j];
     }
    }
   // TDMA
  /* std::cout << "The input arrays column sweep are" <<"\n";
   print_array1d(a_c);print_array1d(b_c);
   print_array1d(c_c);
   print_array1d(d_c);
   std::cout << "\n";*/
   ans_c = TDMA(a_c,b_c,c_c,d_c);
   //std::cout << "ans_c is" << "\n";
   //print_array1d(ans_c);

   for (int q=0;q<M;q++)
   {
     udiff[q][j] = ans_c[q];
   }

}
   //std::cout << "udiff COLUMN sweep is" << "\n";
   //print_array2d(udiff);

   ucorr = sum(ucorr,udiff,M,N+1);

   //std::cout << "ucorr COLUMN sweep is" << "\n";
   //print_array2d(ucorr);

   res = f.xresid(ae,aw,aox,an,as,S,ucorr,M,N);

   //std::cout << "The X-MOMENTUM residual is" << res << "\n";
}



return std::make_tuple(ucorr,res);

}

double xmomentum::xresid(const vec2d& ae,const vec2d& aw,const vec2d& aox,const vec2d& an,
                     const vec2d& as,const vec2d& S,vec2d& ucorr,
                     const int& M,const int& N)
{
    vec2d resid(M,vec1d(N,0.0));
    double residroot = 0.0; double temp=0.0;
   // Interior Cells

   for (int j=2;j<N-1;j++)
   {
       for (int i=1;i<M-1;i++)
       {
         resid[i][j] =  S[i][j] - aox[i][j]*ucorr[i][j] - an[i][j]*ucorr[i-1][j]
                        - as[i][j]*ucorr[i+1][j] - ae[i][j]*ucorr[i][j+1] - aw[i][j]*ucorr[i][j-1];
       }
   }

   // Left 2nd vertical cells

   int j=1;

   for (int i=1;i<M-1;i++)
   {
     resid[i][j] =  S[i][j] - aox[i][j]*ucorr[i][j] - an[i][j]*ucorr[i-1][j]
                    - as[i][j]*ucorr[i+1][j] - ae[i][j]*ucorr[i][j+1];
   }

  // Right cell vertical

    j=N-1;

   for (int i=1;i<M-1;i++)
   {
     resid[i][j] =  S[i][j] - aox[i][j]*ucorr[i][j] - an[i][j]*ucorr[i-1][j]
                    - as[i][j]*ucorr[i+1][j] - aw[i][j]*ucorr[i][j-1];
   }

  // Top Edge

    int i=0;

   for (int j=1;j<N-1;j++)
   {
     resid[i][j] =  S[i][j] - aox[i][j]*ucorr[i][j] - as[i][j]*ucorr[i+1][j]
                    - ae[i][j]*ucorr[i][j+1] - aw[i][j]*ucorr[i][j-1];
   }

   // Bottom Edge

    i=M-1;

   for (int j=1;j<N-1;j++)
   {
     resid[i][j] =  S[i][j] - aox[i][j]*ucorr[i][j] - an[i][j]*ucorr[i-1][j]
                    - ae[i][j]*ucorr[i][j+1] - aw[i][j]*ucorr[i][j-1];
   }


   // Top Left Corner

    i=0;

    j =1;

    resid[i][j] =  S[i][j] - aox[i][j]*ucorr[i][j] - as[i][j]*ucorr[i+1][j]
                    - ae[i][j]*ucorr[i][j+1];


   // Top Right Corner

    i=0;

    j =N-1;

    resid[i][j] =  S[i][j] - aox[i][j]*ucorr[i][j] - as[i][j]*ucorr[i+1][j]
                    - aw[i][j]*ucorr[i][j-1];

   // Bottom Left Corner

    i=M-1;

    j =1;

    resid[i][j] =  S[i][j] - aox[i][j]*ucorr[i][j] - an[i][j]*ucorr[i-1][j]
                    - ae[i][j]*ucorr[i][j+1];

   // Bottom Right Corner

    i=M-1;

    j =N-1;

    resid[i][j] =  S[i][j] - aox[i][j]*ucorr[i][j] - an[i][j]*ucorr[i-1][j]
                    - aw[i][j]*ucorr[i][j-1];

   // Compute Residual

   for (int i=0;i<M;i++)
   {
       for (int j=1;j<N;j++)
       {
          temp = resid[i][j];
          residroot = residroot + pow(temp,2);
       }
   }

   residroot = sqrt(residroot);

   return(residroot);

}


