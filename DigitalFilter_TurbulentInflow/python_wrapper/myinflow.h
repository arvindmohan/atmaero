/*DIGITAL FILTER INFLOW GENERATION - C++ Implementation based on Klein et al. */
/*------- Arvind Mohan ---- May 2013 */
/********************************************************************/
// 	                 MAIN FUNCTION
/*******************************************************************/
#ifndef INFLOW_H_INCLUDED
#define INFLOW_H_INCLUDED

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <chrono>
#include <random>
#include <cassert>
#include "arrayops.h"
#include "printing.h"
#include "inflowgen.h"

// Rows = My, Columns - Mz
  struct variables
   {
   std::vector<double>  u_val;
   std::vector<double>  v_val;
   std::vector<double>  w_val;
   std::vector<double>  p_val;
   std::vector<double>  rho_val;
   std::vector<double>  u_fluct;
   std::vector<double>  v_fluct;
   std::vector<double>  w_fluct;
   };

variables inflow(int My,int Mz,std::vector<double>& umean,std::vector<double>& vmean,
                  std::vector<double>& wmean,std::vector<double>& pmean,std::vector<double>& rhomean,
                  std::vector<double>& Re11,std::vector<double>& Re21,
                 std::vector<double>& Re22,std::vector<double>& Re33)
{
   arrayops <double> g;
   inflowgen f;

   double lx,ly,lz,tau,dt;
   //double dy,dz;
   int ny,nz;
   int Ny,Nz;
   lx = 1e-03;
   ly = 1e-03;
   lz = 1e-03;
   tau = lx/5.0;
   dt = 1e-04;
   ny = 2;
   nz = 2;
   Ny = 2*ny; Nz = 2*nz;
   int ysize = (My + 2*Ny + 1);
   int zsize = (Mz + 2*Nz + 1);

   // Convert all to 2d Array
   std::vector< std::vector<double> > R11 = convert2d(Re11,My,Mz);
   std::vector< std::vector<double> > R21 = convert2d(Re21,My,Mz);
   std::vector< std::vector<double> > R22 = convert2d(Re22,My,Mz);
   std::vector< std::vector<double> > R33 = convert2d(Re33,My,Mz);
   std::vector< std::vector<double> > um = convert2d(umean,My,Mz);
   std::vector< std::vector<double> > vm = convert2d(vmean,My,Mz);
   std::vector< std::vector<double> > wm = convert2d(wmean,My,Mz);
   std::vector< std::vector<double> > pm = convert2d(pmean,My,Mz);
   std::vector< std::vector<double> > rhom = convert2d(rhomean,My,Mz);

   // Allocating memory for Random Matrices
   std::vector< std::vector<double> > Rx = g.array2dgen(ysize,zsize,0.0);
   std::vector< std::vector<double> > Ry = g.array2dgen(ysize,zsize,0.0);
   std::vector< std::vector<double> > Rz = g.array2dgen(ysize,zsize,0.0);

   // Computing random matrices
   Rx = f.random_matrix(My,Mz,Ny,Nz);
   Ry = f.random_matrix(My,Mz,Ny,Nz);
   Rz = f.random_matrix(My,Mz,Ny,Nz);

   // Allocating memory for Filter coefficients
   std::vector< std::vector<double> > B = g.array2dgen(ysize,zsize,0.0);

   // Calculating Filter Coefficients
   B = f.filter_coeff(My,Mz,ly,lz,Ny,Nz);

   // Allocating memory for velocity fluctuations
   std::vector< std::vector<double> > u_fluc = g.array2dgen(My,Mz,0.0);
   std::vector< std::vector<double> > v_fluc = g.array2dgen(My,Mz,0.0);
   std::vector< std::vector<double> > w_fluc = g.array2dgen(My,Mz,0.0);

   // Calculating Velocity fluctuations
   u_fluc = f.vel_fluc(My,Mz,Rx,B,Ny,Nz);
   v_fluc = f.vel_fluc(My,Mz,Ry,B,Ny,Nz);
   w_fluc = f.vel_fluc(My,Mz,Rz,B,Ny,Nz);

   // Allocating memory for Temporally Correlated Velocity Fluctuations
   std::vector< std::vector<double> > pu = g.array2dgen(My,Mz,0.0);
   std::vector< std::vector<double> > pv = g.array2dgen(My,Mz,0.0);
   std::vector< std::vector<double> > pw = g.array2dgen(My,Mz,0.0);
   std::vector< std::vector<double> > pf = g.array2dgen(My,Mz,0.0);
   std::vector< std::vector<double> > rhof = g.array2dgen(My,Mz,0.0);

   // Calculating Temporally Correlated Velocity Fluctuations
     pu = f.corr_vel(u_fluc,tau,dt,u_fluc);
     pv = f.corr_vel(v_fluc,tau,dt,v_fluc);
     pw = f.corr_vel(w_fluc,tau,dt,w_fluc);

   // Allocating memory final inflow velocity

     variables flow;
     std::vector< std::vector<double> > u = g.array2dgen(My,Mz,0.0);
     std::vector< std::vector<double> > v = g.array2dgen(My,Mz,0.0);
     std::vector< std::vector<double> > w = g.array2dgen(My,Mz,0.0);
     std::vector< std::vector<double> > p = g.array2dgen(My,Mz,0.0);
     std::vector< std::vector<double> > rho = g.array2dgen(My,Mz,0.0);

   // Lund Transformation
      lundfluc values;
      values = lund_transform(pu,pv,pw,R11,R21,R22,R33,My,Mz);

   // Calculate pressure and density fluctuations using
   // SRA (Strong Reynolds Analogy)
   std::vector< std::vector<double> > Tm = g.array2dgen(My,Mz,0.0);
   std::vector< std::vector<double> > Tf = g.array2dgen(My,Mz,0.0);

      for (int i=0;i<My;i++)
      {
          for (int j=0;j<Mz;j++)
          {
              Tm[i][j] =  pm[i][j]/8.3144*rhom[i][j];
          }
      }


      for (int i=0;i<My;i++)
      {
          for (int j=0;j<Mz;j++)
          {
              Tf[i][j] = (um[i][j]*um[i][j])/(1.4*8.3144)*(values.uf[i][j]/um[i][j])*(1.4 - 1)*Tm[i][j];
          }
      }


      for (int i=0;i<My;i++)
      {
          for (int j=0;j<Mz;j++)
          {
              rhof[i][j] = -(Tf[i][j]/Tm[i][j])*rhom[i][j];
          }
      }



      for (int i=0;i<My;i++)
      {
          for (int j=0;j<Mz;j++)
          {
              pf[i][j] = rhof[i][j]*8.3144*Tf[i][j];
          }
      }
     // print_array2d(values.uf,My,Mz);
   // Computing final inflow velocity

     u =  sum(um,values.uf);
     v =  sum(vm,values.vf);
     w =  sum(wm,values.wf);
     p =  sum(pm,pf);
     rho =  sum(rhom,rhof);

     // Final 1d conversion to return data

     flow.u_val = convert1d(u,My,Mz);
     flow.v_val = convert1d(v,My,Mz);
     flow.w_val = convert1d(w,My,Mz);
     flow.p_val = convert1d(p,My,Mz);
     flow.rho_val = convert1d(rho,My,Mz);
     flow.u_fluct = convert1d(u_fluc,My,Mz);
     flow.v_fluct = convert1d(v_fluc,My,Mz);
     flow.w_fluct = convert1d(w_fluc,My,Mz);
     return (flow);

}


#endif // INFLOW_H_INCLUDED
