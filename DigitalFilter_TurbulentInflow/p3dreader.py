"""
Python reader for Plot3d Fortran Unformatted Files
by Arvind Mohan
Code unformatierten Fortran-Dateien mit Python lesen
von Arvind Mohan
See README for details.
"""
import numpy as np
import fortranfile as fofi

"""  Function definitions """

def extract_variable(data,size,position):
   """ Extracts variables from plot3d files"""
   name = []
   for i in xrange(size):
    name.append(data[position*size +i])
   return name
   
def get_variables():
  """ Stores data in variables"""
  varname = []
  for i in range(num_var):
    varname.append('var' + str(i+1))
    varname[i] = extract_variable(data,vecsize,i) 
  return varname  
  
def output_asciifile(name):
  """ outputs unformatted data to ASCII file """
  fhandle = file(name,'w')
  np.savetxt(fhandle,(header),fmt='%d',newline=" ")
  fhandle.write('\n')
  np.savetxt(fhandle,(data),newline=" ")
  fhandle.close()
  
def  read_file(filename):
  f = fofi.FortranFile(filename,endian='>',header_prec='i')
  global header
  global data
  global num_var
  global vecsize
  global xsize
  global ysize
  global zsize
  header = f.readInts()
  data = f.readReals('f')
  xsize = header[0]
  ysize = header[1]
  zsize = header[2]

  num_var = header[3]
  vecsize = xsize*ysize*zsize
  print 'the x,y and z sizes are',xsize,ysize,zsize
  print 'size of each variable',vecsize
  print 'no. of variables in the file',num_var
  
def  read_plot3d(filename,i):
  print 'Reading file ... '
  read_file(filename)
  print  'Extracting individual variables ...'
  vars = get_variables()
  if  i == 1:
      output_name = raw_input('Enter a file name for ASCII output: ')
      output_asciifile(output_name)
  return (vars,xsize,ysize,zsize,vecsize)
  

  




