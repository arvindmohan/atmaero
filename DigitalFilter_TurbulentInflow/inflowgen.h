/*DIGITAL FILTER INFLOW GENERATION - C++ Implementation based on Klein et al. */
/*------- Arvind Mohan ---- May 2013 */
/********************************************************************/
//       CLASS FILE CONTAINING FUNCTIONS
//         TO PERFORM ALL Digital Filter OPERATIONS
/*******************************************************************/
#ifndef INFLOWGEN_H_INCLUDED
#define INFLOWGEN_H_INCLUDED
/*
#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <chrono>
#include <random>
#include <cassert>
#include <time.h>
#include <fstream>
#include "arrayops.h"
#include "printing.h"
*/

class inflowgen
{
private:
    double ly,lz,dy,dz,tau,dt;
    int Ny,Nz,My,Mz;
    int ny,nz,n;
    double filter;
    std::vector< std::vector<double> >  newfluc;
    std::vector< std::vector<double> >  oldfluc;
    std::vector< std::vector<double> >  corr_fluc;
    arrayops <double> f;

public:

    std::vector<double>  mersennetwister(const int&,const int&,const int&,const int&);
    std::vector<double>  box_muller(const int&,const int&,const int&,const int&);
    std::vector< std::vector<double> >  random_matrix(const int&,const int&,const int&,const int&);
    const double filter_function(const int&,const double&);
    std::vector< std::vector<double> >  filter_coeff(const double&,const double&,const double&,
                                                         const double&,const int&,const int&);
    std::vector< std::vector<double> >  vel_fluc(const int&,const int&,const std::vector< std::vector<double> >&,
                                                     const std::vector< std::vector<double> >&,const int&,const int&);
    std::vector< std::vector<double> >  corr_vel(const std::vector< std::vector<double> >&,const double&,
                                                    const double&,const std::vector< std::vector<double> >&,const int& My,const int& Mz);
 //   lundfluc  lund_fluc(std::vector< std::vector<double> >&,std::vector< std::vector<double> >&,
  //                                                std::vector< std::vector<double> >&,std::vector< std::vector<double> >&,
  //                                                std::vector< std::vector<double> >&,std::vector< std::vector<double> >&,
  //                                                std::vector< std::vector<double> >&,const int&,const int&);
} ;

// Mersenne Twister Generator

std::vector<double> inflowgen::mersennetwister(const int& My,const int& Mz,const int& Ny,const int& Nz)
{
    int ysize = (My + 2*Ny + 1);
    int zsize = (Mz + 2*Nz + 1);
    int matsize = ysize*zsize;
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937_64 generator (seed);
    std::uniform_real_distribution<double> distribution(0,1);
    std::vector<double> randarray = f.array1dgen(matsize,0);
    for (int i=0;i<matsize;++i)
    {
       randarray[i] = distribution(generator);
    }
    return(randarray);
}

// Box Muller Generator

std::vector<double> inflowgen::box_muller(const int& My,const int& Mz,const int& Ny,const int& Nz)
{
  const double pi = 3.14159265358;
  const double twopi = 2.0*pi;
  inflowgen g;
  std::vector<double> a = g.mersennetwister(My,Mz,Ny,Nz);
  std::vector<double> b = g.mersennetwister(My,Mz,Ny,Nz);
  std::vector<double> c = mul(sqrt(mul(double(-2),(ln(a)))),cos(mul(twopi,b)));
  std::vector<double> d = mul(sqrt(mul(double(-2),(ln(a)))),sin(mul(twopi,b)));
  std::vector<double> e = sum(c,d);
  return(e);
}

// Final Random Matrix generator
std::vector< std::vector<double> >  inflowgen::random_matrix(const int& My,const int& Mz,const int& Ny,
                                                                                                    const int& Nz)
{
   inflowgen g;
   int ysize = (My + 2*Ny + 1);
   int zsize = (Mz + 2*Nz + 1);
//   int matsize = ysize*zsize;
   std::vector< std::vector<double> > r = f.array2dgen(ysize,zsize,0.0);
   std::vector<double> e = g.box_muller(My,Mz,Ny,Nz);
   int k =0;
   for (int i=0;i<ysize;++i)
    {
         for (int j=0;j<zsize;++j)
          {
              r[i][j] = e[k];
              k++;
          }
    }
    return(r);
}

// Filter Function

//   std::ofstream filt;
//   filt.open("filtercount.dat");

const double inflowgen::filter_function(const int& k,const double& n)
{
   const double pi = 3.14159265358;
   filter = exp(-pi*k/n);
//   std::cout  <<  filter << "\t";
/*   if (filter == 1)
     {
      std::cout << "k value is" << k << "\t";
     } */
   return(filter);
}


// Filter Coefficient Calculator

std::vector< std::vector<double> >  inflowgen::filter_coeff(const double& My,const double& Mz,const double& ly,
                                                                const double& lz,const int& Ny,const int& Nz)
{
    inflowgen g;
    int ysize = (My + 2*Ny + 1);
    int zsize = (Mz + 2*Nz + 1);
    std::vector<double> by(ysize,0);
    std::vector<double> bz(zsize,0);
    std::vector< std::vector<double> > B = f.array2dgen(ysize,zsize,0.0);
    // Computing individual filter coefficients
    // By
    double s = 0;
    for (int k=0;k<ysize;k++)
    {
        s = s + g.filter_function(k,ly);
    }
    s = sqrt(s);
    for (int k=0;k<ysize;k++)
    {
        by[k] = g.filter_function(k,ly)/s;
    }
    // Bz
    s = 0;
    for (int k=0;k<zsize;k++)
    {
        s = s + g.filter_function(k,lz);
    }
    s = sqrt(s);
    for (int k=0;k<zsize;k++)
    {
        bz[k] = g.filter_function(k,lz)/s;
    }

    // Computing 2D coefficient
    for (int i=0;i<ysize;i++)
    {
        for (int j=0;j<zsize;j++)
        {
            B[i][j] = by[i]*bz[j];
        }
    }

    return(B);
}

// Calculate Velocity fluctuations

std::vector< std::vector<double> >  inflowgen::vel_fluc(const int& My,const int& Mz,const std::vector< std::vector<double> >& R,
                                                const std::vector< std::vector<double> >& B,const int& Ny,const int& Nz)
{
    std::vector< std::vector<double> > fluc = f.array2dgen(My,Mz,0.0);
    int ysize = 2*Ny + 1;
    int zsize = 2*Nz + 1;

    for (int i=0;i<My;i++)
    {
        for (int j=0;j<Mz;j++)
        {
           for (int ip=0;ip<ysize;ip++)
           {
               for (int jp=0;jp<zsize;jp++)
               {
                   fluc[i][j] = fluc[i][j] +  B[ip][jp]*R[i+ip][j+jp];
                   if ( j == 34 && k == 64 && ip == 2 && jp == 2)
                   { 
                      std::cout << " At these points " << ip << std::setw(3) << jp << std::setw(3) << j << std::setw(3) << k << std::setw(3) << "\n";
                      std::cout << " fluc is" << fluc[i][j]; << "\n";
                   }
               }
           }
        }
    }

    return(fluc);

}

// Compute Time-correlated velocity

std::vector< std::vector<double> >  inflowgen::corr_vel(const std::vector< std::vector<double> >& newfluc,const double& tau,
                                                const double& dt,const std::vector< std::vector<double> >& oldfluc,const int& My,const int& Mz)
{
      const double pi = 3.14159265358;
      double value1 = exp(-pi*dt/(2.0*tau));
      std::vector< std::vector<double> > term1 = mul(value1,oldfluc,My,Mz);
      double value2 = sqrt(1 - exp(-pi*dt/tau));
      std::vector< std::vector<double> > term2 = mul(value2,newfluc,My,Mz);
      std::vector< std::vector<double> > corr_v =  sum(term1,term2,My,Mz);
      return(corr_v);
}

    struct lundfluc
     {
         std::vector< std::vector<double> > uf;
         std::vector< std::vector<double> > vf;
         std::vector< std::vector<double> > wf;
     };


lundfluc lund_transform(std::vector< std::vector<double> >& pu,std::vector< std::vector<double> >& pv,
                                                  std::vector< std::vector<double> >& pw,std::vector< std::vector<double> >& R11,
                                                  std::vector< std::vector<double> >& R21,std::vector< std::vector<double> >& R22,
                                                  std::vector< std::vector<double> >& R33,const int& My,const int& Mz)
{
     arrayops <double> f;
     lundfluc newvalue;
      double a11;
      double a21;
      double a22;
      double a33;
     newvalue.uf = f.array2dgen(My,Mz,0.0);
     newvalue.vf = f.array2dgen(My,Mz,0.0);
     newvalue.wf = f.array2dgen(My,Mz,0.0);
     
     std::ofstream out11;
     out11.open("a11.dat");
     std::ofstream out21;
     out21.open("a21.dat");
      
     // Calculate u-vel fluctuations
     for (int i=0;i<My;i++)
     {
         for (int j=0;j<Mz;j++)
         {
            a11 = sqrt(std::max(R11[i][j],1.0e-10));
            a21 = (std::max(R21[i][j],1.0e-10)/a11);
            a22 = sqrt(std::max(R22[i][j],1.0e-10) - a21*a21);
            a33 = sqrt(std::max(R33[i][j],1.0e-10));
            out11 << a11 << "\t";
            out21 << a21 << "\t";           
            newvalue.uf[i][j] = a11*pu[i][j];
            newvalue.vf[i][j] = a21*pu[i][j] + a22*pv[i][j];
            newvalue.wf[i][j] = a33*pw[i][j];
         }
     }
     out11.close();
     out21.close();
     return(newvalue);

}






#endif // INFLOWGEN_H_INCLUDED




