"""
Python interface to Digital Filter inflow generator for LES.

To use:  Execute the script as "python callinflow.py" after reading data from your mean and Re Stress files.
The script writes a random inlet velocity at every timestep (or select no. of frames)  to a file "timestep(number).dat", 
with (number) denoting the suffix for the time step number.

Arvind Mohan - May 2013
"""
import numpy as np
import inflow    # importing C++ inflow library
from p3dreader import *
import os

def extract_from_struct(size,name):
  """ Extracts data from the struct returned
        by the  inflow function into a list"""
  data=[]
  for i in xrange(size):
    data.append(name[i])
  #print 'struct data is',data[0:2] 
  data = list(sum(data,()))
  return data
  
def convertto_stdvector(size,py_vec,std_vec):
   """ Converts Python List to std::vector<double> """
   for i in range(size):
      temp = py_vec[i]
      std_vec[i] = float(temp)  
   return std_vec
        
def convertto_pylist(size,std_vec):
   """ Converts Python List to std::vector<double> """
   py_vec=[]
   for i in range(size):
      temp = std_vec[i]
      py_vec.append(float(temp))  
   return py_vec                  


def write_tofile(py_list):
  """ Write to file any variable """
  fname = str(py_list) + '.' + 'dat'
  with open(fname,'w') as f:
    f.write(py_list)
  
  
# Get mean variables from unformatted file, with the size,length etc.

var_mean,xsize_mean,ysize_mean,zsize_mean,vecsize_mean = read_plot3d('meanplane.fcn',0)
  
# Get variables from unformatted file , with the size,length etc.

var_rey,xsize_rey,ysize_rey,zsize_rey,vecsize_rey = read_plot3d('extract.bin',0)

 #####################################################################################   

# Read Mean data from file and save as array
vecsize_mean = int(vecsize_mean)
umean = inflow.DoubleVector(vecsize_mean)
vmean = inflow.DoubleVector(vecsize_mean)
wmean = inflow.DoubleVector(vecsize_mean)
pmean = inflow.DoubleVector(vecsize_mean)
rhomean = inflow.DoubleVector(vecsize_mean)
Re11 = inflow.DoubleVector(vecsize_mean)
Re21 = inflow.DoubleVector(vecsize_mean)
Re22 = inflow.DoubleVector(vecsize_mean)
Re33 = inflow.DoubleVector(vecsize_mean)

umeanC = var_mean[0]
vmeanC = var_mean[1]
wmeanC = var_mean[2]
pmeanC = var_mean[3]
rhomeanC = var_mean[4]
Re11C = var_rey[0]
Re21C = var_rey[1]
Re22C = var_rey[3]
Re33C = var_rey[5]
My = int(ysize_mean)
Mz = int(zsize_mean)

umean = convertto_stdvector(vecsize_mean,umeanC,umean)
vmean = convertto_stdvector(vecsize_mean,vmeanC,vmean)
wmean = convertto_stdvector(vecsize_mean,wmeanC,wmean)
pmean = convertto_stdvector(vecsize_mean,pmeanC,pmean)
rhomean = convertto_stdvector(vecsize_mean,rhomeanC,rhomean)
Re11 = convertto_stdvector(vecsize_mean,Re11C,Re11)
Re21 = convertto_stdvector(vecsize_mean,Re21C,Re21)
Re22 = convertto_stdvector(vecsize_mean,Re22C,Re22)
Re33 = convertto_stdvector(vecsize_mean,Re33C,Re33)

u_oldfluc = inflow.DoubleVector()
v_oldfluc = inflow.DoubleVector()
w_oldfluc = inflow.DoubleVector()

u_oldfluc = [0.0]*vecsize_mean
v_oldfluc = [0.0]*vecsize_mean
w_oldfluc = [0.0]*vecsize_mean

# Calling  "inflow" function in loop
os.mkdir('timestep_data')
os.chdir('timestep_data')
print 'Running Digital Filter ...'
nframes = 25
print 'Writing timestep data to folder ...'
for i in xrange(0,1001):
  z = inflow.inflow(My,Mz,umean,vmean,wmean,pmean,rhomean,Re11,Re21,Re22,Re33,u_oldfluc,v_oldfluc,w_oldfluc)
  u_oldfluc =  extract_from_struct(My,z.u_fluct)
  v_oldfluc =  extract_from_struct(My,z.v_fluct)
  w_oldfluc =  extract_from_struct(My,z.w_fluct)
  u =  extract_from_struct(My,z.u_val)
  v =  extract_from_struct(My,z.v_val)
  w =  extract_from_struct(My,z.w_val)
  p =  extract_from_struct(My,z.p_val)
  rho = extract_from_struct(My,z.rho_val)
  if i%nframes == 0:
     p3d_header = [str(xsize_mean),str(ysize_mean) ,str(zsize_mean),5] 
     filename = 'timestep' + str(i) + '.dat'
     print 'Writing',filename,'for time',i,'...'
     with open(filename,'w') as f:
        np.savetxt(f,(p3d_header),fmt="%s",newline=" ")
        f.write("\n")
        np.savetxt(f,(u,v,w,p,rho),fmt="%f")
print 'Alright.Time to make that video, buddy .' 
