!  mersenne twister subroutine
! MT19937, Mersenne Twister Random Number Generator ([0,1) Real Number)
! in Fortran90
!
! Usage: 
!   1) When you use mt19937, add the sentence "use mt19937" 
!      above the implicit sentence.
!   2) To set an initial seed, call sgrnd(seed). (The "seed" is an integer.)
!      If you do not call this, the seed is 4357.
!   3) Use the function grnd().
!      (Do not declare "real(8) :: grnd".)
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! A C-program for MT19937: Real number version
!   genrand() generates one pseudorandom real number (double)
! which is uniformly distributed on [0,1]-interval, for each
! call. sgenrand(seed) set initial values to the working area
! of 624 words. Before genrand(), sgenrand(seed) must be
! called once. (seed is any 32-bit integer except for 0).
! Integer generator is obtained by modifying two lines.
!   Coded by Takuji Nishimura, considering the suggestions by
! Topher Cooper and Marc Rieffel in July-Aug. 1997.
!
! This library is free software; you can redistribute it and/or
! modify it under the terms of the GNU Library General Public
! License as published by the Free Software Foundation; either
! version 2 of the License, or (at your option) any later
! version.
! This library is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
! See the GNU Library General Public License for more details.
! You should have received a copy of the GNU Library General
! Public License along with this library; if not, write to the
! Free Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
! 02111-1307  USA
!
! Copyright (C) 1997 Makoto Matsumoto and Takuji Nishimura.
! When you use this, send an email to: matumoto@math.keio.ac.jp
! with an appropriate reference to your work.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Fortran translation by Hiroshi Takano.  Jan. 13, 1999.
!
!   genrand()      -> double precision function grnd()
!   sgenrand(seed) -> subroutine sgrnd(seed)
!                     integer seed
!
! This program uses the following non-standard intrinsics.
!   ishft(i,n): If n>0, shifts bits in i by n positions to left.
!               If n<0, shifts bits in i by n positions to right.
!   iand (i,j): Performs logical AND on corresponding bits of i and j.
!   ior  (i,j): Performs inclusive OR on corresponding bits of i and j.
!   ieor (i,j): Performs exclusive OR on corresponding bits of i and j.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Fortran90 translation by Yusuke Konishi. May. 13, 2013.
!
! The interval is changed from [0, 1] to [0, 1).
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mt19937
  implicit none
  integer,private :: N, N1, M, MATA, UMASK, LMASK, TMASKB, TMASKC
  parameter( &
             & N = 624, &
             & N1 = 625, &
             & M = 397, &
             & MATA = -1727483681, &
             & UMASK = -2147483648, &
             & LMASK = 2147483647, &
             & TMASKB = -1658038656, &
             & TMASKC = -272236544 &
             & )
  integer,private :: mti = N1, mt(0:N-1), mag01(0:1) = (/0, MATA/)

  contains

  subroutine sgrnd(seed)
    integer,intent(in) :: seed
!
!      setting initial seeds to mt[N] using
!      the generator Line 25 of Table 1 in
!      [KNUTH 1981, The Art of Computer Programming
!         Vol. 2 (2nd Ed.), pp102]
!
    mt(0) = iand(seed, -1)
    do mti = 1, N - 1
      mt(mti) = iand(69069 * mt(mti - 1), -1)
    end do
  end subroutine sgrnd

  real(8) function grnd()
    integer :: y, kk

    if(mti >= N) then
!                   generate N words at one time
      if(mti == N + 1) then
!                        if sgrnd() has not been called,
        call sgrnd(4357)
!                          a default initial seed is used
      endif

      do kk = 0, N - M - 1
        y = ior(iand(mt(kk), UMASK), iand(mt(kk + 1), LMASK))
        mt(kk) = ieor(ieor(mt(kk + M), ishft(y, -1)), mag01(iand(y, 1)))
      end do

      do kk = N - M, N - 2
        y = ior(iand(mt(kk), UMASK), iand(mt(kk + 1), LMASK))
        mt(kk) = ieor(ieor(mt(kk + (M - N)), ishft(y, -1)), mag01(iand(y, 1)))
      end do

      y = ior(iand(mt(N - 1), UMASK), iand(mt(0), LMASK))
      mt(N - 1) = ieor(ieor(mt(M - 1), ishft(y, -1)), mag01(iand(y, 1)))
      mti = 0
    endif

    y = mt(mti)
    mti = mti + 1
    y = ieor(y, ishft(y, -11))
    y = ieor(y, iand(ishft(y, 7), TMASKB))
    y = ieor(y, iand(ishft(y, 15), TMASKC))
    y = ieor(y, ishft(y, -18))

    if(y < 0) then
      grnd = (dble(y) + 2.0d0 ** 32) / (2.0d0 ** 32)
    else
      grnd = dble(y) / (2.0d0 ** 32)
    endif
  end function grnd

end module mt19937

program digitalfilter

  use mt19937

  !   generate an upstream profile for use in digital filter routine

  real, allocatable, dimension(:,:,:) :: x3d,y3d,z3d
  real, allocatable, dimension(:,:,:) :: u,v,w,p,rho
  real, allocatable, dimension(:,:,:,:) :: productsmean,reynoldsstresses

  real, allocatable, dimension(:,:,:) :: umean3d,vmean3d,wmean3d,pmean3d,rhomean3d
  real, allocatable, dimension(:,:) :: umean,vmean,wmean,pmean,rhomean
  real, allocatable, dimension(:,:) :: uinst,vinst,winst,pinst,rhoinst
  real, allocatable, dimension(:,:) :: ufluc,vfluc,wfluc,pfluc,rhofluc
  real, allocatable, dimension(:,:) :: uf,vf,wf

  real, allocatable, dimension(:,:) :: uoldfluc,voldfluc,woldfluc,poldfluc,rhooldfluc,&
       & corr_v

  real, allocatable, dimension(:,:) :: pu,pv,pw,pf,rhof

  real, allocatable, dimension(:,:) :: R11,R21,R22,R33
  real, allocatable, dimension(:) :: R11oneD,R21oneD,R22oneD,R33oneD

  real, allocatable, dimension(:,:) :: rx,ry,rz,rxyzdmy
  real, allocatable, dimension(:,:) :: Tm,Tf

  real, allocatable, dimension(:,:) :: B
  integer :: time_info(8)
  integer :: seed

  real, allocatable, dimension(:) :: randarray1,randarray2,aa,bb,cc,dd,ee
  
  character (len=80) :: fname

  open(unit=10,file='mnf.grd',form='unformatted',status='OLD')
  read(10) il,jl,kl
  allocate (x3d(il,jl,kl))
  allocate (y3d(il,jl,kl))
  allocate (z3d(il,jl,kl))
  read(10) (((x3d(i,j,k),i=1,il),j=1,jl),k=1,kl),&
       &  y3d,z3d
  close(10)

  open(unit=11,file='mnf1.fcn',form='unformatted',status='OLD')
  read(11) il,jl,kl,nfcns
  allocate (umean3d(il,jl,kl))
  allocate (vmean3d(il,jl,kl))
  allocate (wmean3d(il,jl,kl))
  allocate (pmean3d(il,jl,kl))
  allocate (rhomean3d(il,jl,kl))
  read(11) umean3d,vmean3d,wmean3d,pmean3d,rhomean3d
  close(11)

  open(11,file='mnf1a.fcn',status='OLD',form='UNFORMATTED')
  read(11) il,jl,kl,nfcns
  print*,'nfcns is ',nfcns,' but only defining first 6 terms'
  nfcns=6
  print*,' reading mnf1a.fcn il,jl,kl,nfcns=', il,jl,kl,nfcns
  allocate(productsmean(il,jl,kl,6))
  read(11) productsmean
  close(11)

  print*,'finished reading productmean'

  allocate(reynoldsstresses(il,jl,kl,nfcns))  

  print*,'assuming that 1=uu(6),2=uv(7),3=uw(8), 4=vv(9),&
       &  5=vw(10),6=ww(11),7=up(12),8=vp(13),9=pp(23)'

  reynoldsstresses(:,:,:,1)=productsmean(:,:,:,1)-umean3d(:,:,:)*umean3d(:,:,:)
  reynoldsstresses(:,:,:,2)=productsmean(:,:,:,2)-umean3d(:,:,:)*vmean3d(:,:,:)
  reynoldsstresses(:,:,:,3)=productsmean(:,:,:,3)-umean3d(:,:,:)*wmean3d(:,:,:)
  reynoldsstresses(:,:,:,4)=productsmean(:,:,:,4)-vmean3d(:,:,:)*vmean3d(:,:,:)
  reynoldsstresses(:,:,:,5)=productsmean(:,:,:,5)-vmean3d(:,:,:)*wmean3d(:,:,:)
  reynoldsstresses(:,:,:,6)=productsmean(:,:,:,6)-wmean3d(:,:,:)*wmean3d(:,:,:)
  print*,'finished computing reynoldsstresses',ifcn

  !  pick plane
  iplane=80
  print*,'assuming plane is ',iplane

  allocate (umean(jl,kl))
  allocate (vmean(jl,kl))
  allocate (wmean(jl,kl))
  allocate (pmean(jl,kl))
  allocate (rhomean(jl,kl))

  allocate (uinst(jl,kl))
  allocate (vinst(jl,kl))
  allocate (winst(jl,kl))
  allocate (pinst(jl,kl))
  allocate (rhoinst(jl,kl))

  allocate (ufluc(jl,kl))
  allocate (vfluc(jl,kl))
  allocate (wfluc(jl,kl))
  allocate (pfluc(jl,kl))
  allocate (rhofluc(jl,kl))

  allocate (uf(jl,kl))
  allocate (vf(jl,kl))
  allocate (wf(jl,kl))

  allocate (uoldfluc(jl,kl))
  allocate (voldfluc(jl,kl))
  allocate (woldfluc(jl,kl))
  allocate (corr_v(jl,kl))

  allocate (R11(jl,kl))
  allocate (R22(jl,kl))
  allocate (R33(jl,kl))
  allocate (R21(jl,kl))

  allocate (R11oneD(jl*kl))
  allocate (R22oneD(jl*kl))
  allocate (R33oneD(jl*kl))
  allocate (R21oneD(jl*kl))

  umean(:,:)=umean3d(iplane,:,:)
  vmean(:,:)=vmean3d(iplane,:,:)
  wmean(:,:)=wmean3d(iplane,:,:)
  pmean(:,:)=pmean3d(iplane,:,:)
  rhomean(:,:)=rhomean3d(iplane,:,:)

  R11(:,:)=reynoldsstresses(iplane,:,:,1)
  R22(:,:)=reynoldsstresses(iplane,:,:,4)
  R33(:,:)=reynoldsstresses(iplane,:,:,6)
  R21(:,:)=reynoldsstresses(iplane,:,:,2)


  nsteps=500
  print*,'assuming nsteps is ',nsteps

  modmovie=25
  imovieflag=0

  do istep=1,nsteps
     
     call inflow
     if (mod(istep,modmovie) .eq. 0) then
        !  write the movie
        if (imovieflag .eq. 0) then
           imovieflag=1
           !  write the grid file for the first time around

           open(15,file='movie.grd',form='UNFORMATTED')
           
           idmymax=3
           write(15) idmymax,jl,kl
           
           write(15) (((real(idmy-1),idmy=1,idmymax),j=1,jl),k=1,kl),   &
                &(((y3d(iplane,j,k),idmy=1,idmymax),j=1,jl),k=1,kl),&
                &(((z3d(iplane,j,k),idmy=1,idmymax),j=1,jl),k=1,kl)
           close(15)

        endif
        
        fname='moviefcn'
        istrlength=len_trim(fname)
        
        write(fname(istrlength+1:istrlength+4), '(I4.4)') istep
        open(16,file=trim(fname),form='unformatted')
        print*,'opened ',trim(fname),' for writing purposes'

        nfcns=5
        write(16) idmymax,jl,kl,nfcns
        write(16) (((uinst(j,k),i=1,idmymax),j=1,jl),k=1,kl),&
             &(((vinst(j,k),i=1,idmymax),j=1,jl),k=1,kl),&
             &(((winst(j,k),i=1,idmymax),j=1,jl),k=1,kl),&
             &(((pinst(j,k),i=1,idmymax),j=1,jl),k=1,kl),&
             &(((rhoinst(j,k),i=1,idmymax),j=1,jl),k=1,kl)
        close(16)

     endif
     
  enddo
  stop

contains
  subroutine inflow

    !  integral length scales - defaulted to this but can be changed
    rlx=1.0e-3
    rly=1.0e-3
    rlz=1.0e-3

    !   lagrangian time scale = integral length scale / u
    !  QUESTION: Should the 5 be actually something less than 1?
    tau=rlx/5.0
    !
    dt=1e-04;
    print*,'assuming dt=',dt
    
    !  filter support
    ny=2;
    nz=2;
    !  both-sided filter suport
    Ny2=2*ny
    Nz2=2*nz
    
    My=jl
    Mz=kl
    iysize=(My+2*Ny2+1);
    izsize=(Mz+2*Nz2+1);

    !  rx, ry and rz are random matrices

    if (not(allocated(rx))) then
       allocate (rx(iysize,izsize))
       allocate (ry(iysize,izsize))
       allocate (rz(iysize,izsize))
    endif

    call random_matrix(rx)
    call random_matrix(ry)
    call random_matrix(rz)

    if (not(allocated(B))) then
       allocate (B(iysize,izsize))
    endif

    B=0.0
    call filter_coeff()

    call vel_fluc(ufluc,rx)
    call vel_fluc(vfluc,ry)
    call vel_fluc(wfluc,rz)

    if (not(allocated(pu))) then
       allocate (pu(jl,kl))
       allocate (pv(jl,kl))
       allocate (pw(jl,kl))
       allocate (pf(jl,kl))
       allocate (rhof(jl,kl))
    endif

    call corr_vel(pu,ufluc,uoldfluc)
    call corr_vel(pv,vfluc,voldfluc)
    call corr_vel(pw,wfluc,woldfluc)

!    allocate (u(jl,kl))
!    allocate (v(jl,kl))
!    allocate (w(jl,kl))
    

    if (not(allocated(uf))) then
       allocate (uf(jl,kl))
       allocate (vf(jl,kl))
       allocate (wf(jl,kl))
    endif

    uf=0.0
    vf=0.0
    wf=0.0

    call lund_transform()

    !  SRA

    if (not(allocated(Tm))) then
       allocate (Tm(jl,kl))
       allocate (Tf(jl,kl))
    endif
 


    xm1=2.3
    gam=1.4
    print*,'WARNING: MACH NUMBR SET TO ',XM1,' GAM =',GAM

    do i=1,My
       do j=1,Mz
          Tm(i,j)=pmean(i,j)/(gam*xm1)
       enddo
    enddo

    do i=1,My
       do j=1,Mz
          Tf(i,j)=-(gam-1.0)*xm1*xm1*ufluc(i,j)
       enddo
    enddo

    do i=1,My
       do j=1,Mz
          rhof(i,j)=-(Tf(i,j)/Tm(i,j))*rhomean(i,j)
       enddo
    enddo

    uinst=umean+uf
    vinst=vmean+vf
    winst=wmean+wf
    pinst=pmean+pf
    rhoinst=rhomean+rhof

    return
  end subroutine inflow

  subroutine random_matrix(rxyzdmy)  
    real, allocatable, dimension(:,:) :: rxyzdmy

    if (not(allocated(rxyzdmy))) then
       allocate (rxyzdmy(iysize,izsize))
    endif

    rx=0.0
    ry=0.0
    rz=0.0

    matsize=iysize*izsize

    if (not(allocated(randarray1))) then
       allocate (randarray1(iysize*izsize))
       allocate (randarray2(iysize*izsize))
       allocate (aa(iysize*izsize))
       allocate (bb(iysize*izsize))
       allocate (cc(iysize*izsize))
       allocate (dd(iysize*izsize))
       allocate (ee(iysize*izsize))
    endif

    call box_muller()

    ipointer=1
    do k=1,izsize
       do j=1,iysize
          rxyzdmy(j,k)=ee(ipointer)
          ipointer=ipointer+1
       enddo
    enddo

    return
  end subroutine random_matrix

  subroutine box_muller()

    randarray1=0.0  ! array statement

    do i=1,matsize
       call date_and_time(VALUES=time_info)
       !  seed has been declared as an integer
       seed = 1000 * time_info(7) + time_info(8)         ! a somewhat random integer
       call sgrnd(seed)
       randarray1(i)=grnd()
    enddo

    aa=randarray1
    randarray2=0.0  ! array statement

    do i=1,matsize
       call date_and_time(VALUES=time_info)
       !  seed has been declared as an integer
       seed = 1000 * time_info(7) + time_info(8)         ! a somewhat random integer
       call sgrnd(seed)
       randarray2(i)=grnd()
    enddo
    bb=randarray2

    pi=3.1415926
    cc=sqrt(-2.0*log(aa))*cos(2.0*pi*bb)
    dd=sqrt(-2.0*log(aa))*sin(2.0*pi*bb)
    ee=cc+dd

    return
  end subroutine box_muller

  subroutine filter_coeff()

    real, allocatable, dimension(:) :: by,bz

    if (not(allocated(by))) then
       allocate (by(iysize))
       allocate (bz(izsize))
    endif

    s=0 ! this is a scalar
    do k=1,iysize
       s=s+filter_function(k,rly)
    enddo
    s=sqrt(s)

    do k=1,iysize
       by(k)=filter_function(k,rly)/s
    enddo

    s=0 ! this is a scalar
    do k=1,izsize
       s=s+filter_function(k,rlz)
    enddo
    s=sqrt(s)

    do k=1,izsize
       by(k)=filter_function(k,rlz)/s
    enddo

    do i=1,iysize
       do j=1,izsize
          B(i,j)=by(i)*bz(k)
       enddo
    enddo

    return
  end subroutine filter_coeff

  real function filter_function(kdmy,rn)

    pi=3.1415926
    filter_function=exp(-pi*kdmy/rn)

    return
  end function filter_function

  subroutine vel_fluc(flucdmy,rdmy)

    real, allocatable, dimension(:,:) :: flucdmy,rdmy

    integer :: ifily,ifilz
    ifily=ny2+1
    ifilz=nz2+1

    do i=1,my
       do j=1,mz
          do ip=1,ifily
             do jp=1,ifilz
                flucdmy(i,j)=flucdmy(i,j)+B(ip,jp)*rdmy(i+ip,j+jp)
             enddo
          enddo
       enddo
    enddo
    
    return
  end subroutine vel_fluc

  subroutine corr_vel(corr_vdmy,uflucdmy,uoldflucdmy)
    real, allocatable, dimension(:,:) :: corr_vdmy,uflucdmy,uoldflucdmy

    pi=3.1415926
    value1 = exp(-pi*dt/(2.0*tau))

    uoldflucdmy=uoldflucdmy*value1

    value2 = sqrt(1.0-exp(-pi*dt/(2.0*tau)))

    uflucdmy=uflucdmy*value2

    corr_vdmy=uflucdmy+uoldflucdmy

    return
  end subroutine corr_vel

  subroutine lund_transform()

    !  calculate u-vel functions
    do i=1,My
       do j=1,Mz

          a11=sqrt(max(r11(i,j),1.0E-10))
          a21=sqrt(max(r21(i,j),1.0E-10)/a11)
          a22=sqrt(max(r22(i,j),1.0E-10)-a21*a21)
          a33=sqrt(max(r33(i,j),1.0E-10))
          
          uf(i,j)=a11*pu(i,j)
          vf(i,j)=a21*pu(i,j)+a22*pv(i,j)
          wf(i,j)=a33*pw(i,j)
       enddo
    enddo

    return
  end subroutine lund_transform

end program digitalfilter

subroutine convertfrom2to1D(array2d,array1d,jl,kl)

  print*,'convertfrom2to1D NOT FIXED YET'
  stop
!  ipointer=1
!  do j=1,jl
!     do k=1,kl
!        array1d(ipointer)=array2d(j,k)
!        ipointer=ipointer+1
!     enddo
!  enddo

  return
end subroutine convertfrom2to1D


! !*****************
! !  Generate an initial seed from system clock
! !******************
! !
! ! File: ~/numerical/random.f90
! !
! ! Program to test f90 intrinsics to generate random numbers,
! ! (somewhat) randomly seeded off the system clock.
! !
! ! C.G. (12-98)
! !
! PROGRAM random
! 
!   IMPLICIT NONE
!   INTEGER :: i, msec, n, time_info(8)
!   REAL    :: num(10)
! !
! ! Get all 8 integer fields off the date/time function:
! !   time_info(7) = seconds of the minute (range 0 to 60)
! !   time_info(8) = milliseconds of the second (range 0 to 999)
! !
!   CALL DATE_AND_TIME( VALUES = time_info )
! 
!   msec = 1000 * time_info(7) + time_info(8)         ! a somewhat random integer
! 
!   CALL RANDOM_SEED( SIZE = n )   ! get the number of integers used for the seed
! 
!   CALL RANDOM_SEED( PUT = (/ ( i * msec, i = 1, n ) /) )   ! give a proper seed
! 
!   CALL RANDOM_NUMBER( num )   ! generate a sequence of 10 pseudo-random numbers
! 
!   PRINT '(10F5.2 )', num
! 
! END PROGRAM random
! !
! ! Sample output from successive runs:
! !
! !   0.34 0.79 0.70 0.80 0.50 0.80 0.50 0.21 0.90 0.67
! !   0.82 0.22 0.25 0.03 0.34 0.34 0.26 0.07 0.29 1.00
! !   0.45 0.83 0.71 0.77 0.17 0.43 0.32 0.90 0.27 0.24
! !
! ! Note: If you don't re-seed (and just directly call 'RANDOM_NUMBER()',
! !       then you get the same sequence every time (which may or may not
! !       be a problem ... depending on what you're doing)
! !
! 
